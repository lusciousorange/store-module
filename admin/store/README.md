# README #

This module is for integrating a store into Tungsten. It includes basic functionality for shopping carts, taxes and delivery. 

# Configuration #

* Install the module, which will create the necessary database tables
* Any item that is to be sold in the store must implement the `TMi_ShoppingCartable` interface. If an item has sub-parts or 
variants then the class for those variants are the ones that must implement the interface. 
* The website's theme must instantiate and attach the `TMv_ShoppingCartTracker`. That view sets up the shopping cart 
(`TMm_ShoppingCart`) as well as the messaging that goes with it. 

# Asyncronous Updates #
 * `shopping_cart_quantity_target` must be applied to any element that should receive the latest in terms of how many
items are in the cart, each time it's changed. If ajax updates are created, the content of those items will be updated
with the latest quantity from the cart. 

# Cart Buttons #
Cart buttons are links that can be clicked on which affect the shopping cart. These buttons must point to 
`/admin/store/do/add-to-cart/<content-code>/quantity/`. The content code can be found by calling `contentCode()` on any
model that implements `TMi_ShoppingCartable`.

* `shopping_cart_add_button` must be added to any cart button that will trigger asyncronous changes to a shopping cart. 
 The click will be detected and processed asyncronously. 
 
 # Extending Store Views #
 Any store view that needs to be expanded should be done using Tungsten's standare method for dealing with extended views 
 via the TC_config.php Class Overrides. 