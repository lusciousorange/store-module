<?php

/**
 * Class TMc_StoreController
 */
class TMc_StoreController extends TSc_ModuleController
{
	/**
	 * TMc_StoreController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		$this->enableNavigationSubsections();
	}
	
	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets(); // Loads TMm_Purchases
		
		// Delete a purchase goes to the list
		$delete = $this->URLTargetWithName('delete');
		$delete->setNextURLTarget('list');
		
		$target = TSm_ModuleURLTarget::init('cancel');
		$target->setModelName('TMm_Purchase');
		$target->setModelActionMethod('cancel');
		$target->setTitle('Cancel Purchase');
		$target->setNextURLTarget('list');
		$target->setValidationClassAndMethod('TMm_Purchase', 'userCanCancel');
		$this->addModuleURLTarget($target);
		
		
		// Undo purchase creation/editing
		$this->removeURLTargetWithName('edit');
		$this->removeURLTargetWithName('create');
		
		$target = TSm_ModuleURLTarget::init('view');
		$target->setViewName('TMv_PurchaseSummary');
		$target->setModelName('TMm_Purchase');
		$target->setTitleUsingModelMethod('title()');
		$target->setTitle('View Purchase');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('list');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('edit');
		$target->setViewName('TMv_PurchaseForm');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('view');
		$target->setValidationClassAndMethod('TMm_Purchase', 'userCanEdit');
		$target->setTitle('Edit Settings');
		$this->addModuleURLTarget($target);
		
		// Handle Taxes
		$this->generateDefaultURLTargetsForModelClass('TMm_Tax', 'tax', false);
		
		$this->purchaseListURLTargets();
		
		
		// Handle Shopping Carts
		$this->shoppingCartURLTargets();
		
		$this->purchaseURLTargets();
		
		$this->defineSubmenuGroupingWithURLTargets('list', 'purchase-item-list');
		
		$this->defineSubmenuGroupingWithURLTargets('view', 'edit');
		
		
	}
	
	public function purchaseListURLTargets()
	{
		// Purchase Item List
		$target = TSm_ModuleURLTarget::init('purchase-item-list');
		$target->setTitle('Purchase Items');
		$target->setViewName('TMv_PurchaseItemList');
		
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init('download-purchase-list');
		$target->setParentURLTargetWithName('list');
		$target->setTitle('Download Excel');
		$target->setModelName('TMv_PurchaseList');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setModelActionMethod('generatePurchaseListExcel');
		$target->setNextURLTarget(NULL);
		$target->setAsRightButton('fa-file-excel');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init('download-purchase-item-list');
		$target->setParentURLTargetWithName('purchase-item-list');
		$target->setTitle('Download Excel');
		$target->setModelName('TMv_PurchaseItemList');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		
		$target->setModelActionMethod('generatePurchaseItemListExcel');
		$target->setNextURLTarget(NULL);
		$target->setAsRightButton('fa-file-excel');
		$this->addModuleURLTarget($target);
		
	}
	
	
	/**
	 * Defines the URL targets for shopping carts
	 */
	public function shoppingCartURLTargets()
	{
		// Cart List
		$target = TSm_ModuleURLTarget::init('shopping-cart-list');
		$target->setTitle('Shopping Carts');
		$target->setViewName('TMv_ShoppingCartList');
		$target->setAsSubsectionParent();
		$this->addModuleURLTarget($target);
		
		// View Cart
		$target = TSm_ModuleURLTarget::init('cart-view');
		$target->setAsSkipsAuthentication();
		$target->setTitle('Shopping Cart');
		$target->setViewName('TMv_ShoppingCart');
		$target->setModelName('TMm_ShoppingCart');
		$target->setTitleUsingModelMethod('title');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setParentURLTargetWithName('shopping-cart-list');
		$target->setValidationClassAndMethod('TMm_ShoppingCart', 'userCanEdit');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('cart-view-json');
		$target->setAsSkipsAuthentication();
		$target->setTitle('Shopping Cart');
		$target->setViewName('TMv_ShoppingCart');
		$target->setModelName('TMm_ShoppingCart');
		$target->setTitleUsingModelMethod('title');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('shopping-cart-list');
		$target->setValidationClassAndMethod('TMm_ShoppingCart', 'userCanEdit');
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);
		
		
		
		$target = TSm_ModuleURLTarget::init('add-to-cart');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelActionMethod('addToCartWithContentCode()');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setPassValuesIntoActionMethodType('get', 'content_code', 'quantity','1','2','3','4','5');
		$target->setNextURLTarget('redirect-to-cart');
		$this->addModuleURLTarget($target);
		
		// URL Target that allows us to specify a cart ID, which means we need to validate
		$target = TSm_ModuleURLTarget::init('add-to-cart-with-id');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelActionMethod('addToCartWithContentCode()');
		$target->setModelInstanceRequired();
		$target->setPassValuesIntoActionMethodType('get', 'content_code', 'quantity','1','2','3','4','5');
		$target->setNextURLTarget('redirect-to-cart');
		$target->setValidationClassAndMethod('TMm_ShoppingCart','userCanEdit');
		$this->addModuleURLTarget($target);
		
		
		
		// uses a cart method to detect the cart page if set, and redirect to it
		$target = TSm_ModuleURLTarget::init('redirect-to-cart');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelActionMethod('redirectToCartPage()');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setNextURLTarget(null);
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('update-cart-quantity');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCartItem');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired();
		$target->setModelActionMethod('update()');
		$target->setPassValuesIntoActionMethodType('get', 'quantity');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('add-to-cart-with-barcode');
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setModelActionMethod('addItemWithBarcode');
		$target->setPassValuesIntoActionMethodType('url', 2);
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init('delete-cart');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setModelActionMethod('delete()');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('remove-from-cart');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMm_ShoppingCartItem');
		$target->setModelActionMethod('delete()');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('delivery-options');
		$target->setAsSkipsAuthentication();
		$target->setModelName('TMv_ShoppingCartDeliveryForm');
		$target->setModelActionMethod('deliveryOptionRadioButtons');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setReturnAsJSON();
		$target->setNextURLTarget(NULL);
		$this->addModuleURLTarget($target);
		
		
		//////////////////////////////////////////////////////
		//
		// ADMIN CART
		//
		//////////////////////////////////////////////////////
		
		$target = TSm_ModuleURLTarget::init('add-to-admin-cart');
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setModelActionMethod('addToAdminCartWithContentCode()');
		$target->setPassValuesIntoActionMethodType('get', 'content_code', 'quantity');
		$target->setNextURLTarget('view-admin-cart');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('view-admin-cart');
		$target->setModelName('TMm_ShoppingCart');
		$target->setModelInstanceRequired();
		$target->setModelInstanceIDRequired(false);
		$target->setModelActionMethod('redirectToAdminCart()');
		//$target->setPassValuesIntoActionMethodType('get','content_code', 'quantity');
		$target->setNextURLTarget(null);
		$this->addModuleURLTarget($target);
		
		
	}
	
	/**
	 * Defines the URL targets for shopping carts
	 */
	public function purchaseURLTargets()
	{
		$target = TSm_ModuleURLTarget::init('preview-purchase-email');
		//$target->setAsSkipsAuthentication();
		$target->setViewName('TMv_PurchaseEmail');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('send-purchase-email');
		$target->setModelName('TMm_Purchase');
		$target->setModelActionMethod('resendCustomerEmail');
		$target->setModelInstanceRequired();
		$target->setTitle('Re-Send Email');
		$target->setAsRightButton('fa-envelope');
		$target->setNextURLTarget('referrer');
		$target->setParentURLTargetWithName('view');
		$this->addModuleURLTarget($target);
		
//		$target = TSm_ModuleURLTarget::init('reactivate-purchase-cart');
//		$target->setAsSkipsAuthentication();
//		$target->setModelName('TMm_Purchase');
//		$target->setModelActionMethod('reactivateShoppingCart');
//		$target->setModelInstanceRequired();
//		$target->setNextURLTarget(null); // redirect happens in method
//		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('refund');
		$target->setAsSkipsAuthentication();
		$target->setViewName('TMv_PurchaseRefundForm');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('view');
		$target->setTitle('Refund');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('add-to-purchase');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('view');
		$target->setTitle('Add Product');
		$target->setModelActionMethod('addItem');
		//$target->setPassValuesIntoActionMethodType('get','add_product_id');
		$target->setNextURLTarget('referrer');
		$target->setPassValuesIntoActionMethodType('get', 'content_code', 'quantity');
		
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('add-to-purchase-with-barcode');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$target->setModelActionMethod('addItemWithBarcode');
		$target->setPassValuesIntoActionMethodType('url', 2);
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init('remove-from-purchase');
		$target->setModelName('TMm_PurchaseItem');
		$target->setModelActionMethod('delete()');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('update-purchase-quantity');
		$target->setModelName('TMm_PurchaseItem');
		$target->setModelActionMethod('update()');
		$target->setPassValuesIntoActionMethodType('get', 'quantity');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('update-purchase-price');
		$target->setModelName('TMm_PurchaseItem');
		$target->setModelActionMethod('updatePrice()');
		$target->setPassValuesIntoActionMethodType('get', 'price','show_discount');
		$target->setNextURLTarget('referrer');
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('user-last-purchase-address');
		$target->setModelName('TMm_User');
		$target->setModelActionMethod('lastPurchaseAddressDetails()');
		$target->setReturnAsJSON();
		$target->setNextURLTarget(null);
		$this->addModuleURLTarget($target);
		
		
		$target = TSm_ModuleURLTarget::init('purchase-cart-view');
		$target->setViewName('TMv_PurchaseCart');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$this->addModuleURLTarget($target);
		
		$target = TSm_ModuleURLTarget::init('purchase-cart-view-json');
		$target->setViewName('TMv_PurchaseCart');
		$target->setModelName('TMm_Purchase');
		$target->setModelInstanceRequired();
		$target->setReturnAsJSON();
		$this->addModuleURLTarget($target);
		
		
	}
	
	/**
	 * Defines the URL targets that will be shown in the Users Module. This must be an array of defined URL targets
	 * in the same way they are defined in any controller. The list will be added to the `edit` url target and
	 * require that a model name be provided of a TMm_User
	 * @return TSm_ModuleURLTarget[]
	 */
	public static function defineURLTargetsForUsersModule()
	{
		$url_targets = array();
		
		$target = TSm_ModuleURLTarget::init('purchase-list');
		$target->setViewName('TMv_PurchaseList');
		$target->setModelName('TMm_User');
		$target->setModelInstanceRequired();
		$target->setTitleUsingModelMethod('title');
		$target->setTitle('Purchases');
		$target->setParentURLTargetWithName('edit');
		$url_targets[] = $target;
		
		
		return $url_targets;
	}
	
}
?>