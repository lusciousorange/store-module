<?php
interface TMi_DeliveryOption
{
	/**
	 * Returns the title for this item
	 * @return string
	 */
	public function title();
	
	
	/**
	 * Returns the title for this item
	 * @return string
	 */
	public function description();
	
	
	/**
	 * Returns the price for delivery
	 * @return float
	 */
	public function price();
	
	
}