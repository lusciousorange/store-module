<?php
/**
 * Interface TMi_DeliveryProvider
 *
 * An interface that can be applied to a class which can return a series of delivery options when provided with a
 * shopping cart object
 */
interface TMi_DeliveryProvider
{
	/**
	 * Returns a collection of delivery options for the provided shopping cart. THe implementation of this function
	 * must process the shopping cart to determine which products are an option.
	 * @param TMm_ShoppingCart $cart
	 * @return array The array must be formatted with a `title`, `description` and `price` as an associative array
	 * with the index being a unique ID for that option
	 */
	public function deliveryOptionsForCart($cart);



}