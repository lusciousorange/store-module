<?php
/**
 * Interface TMi_ShoppingCartable
 *
 * An interface that can be added to any model which adds the ability for that model to be added to a shopping cart.
 *
 * If you're using a model that doesn't have an associated ID, you must set the static value of
 * $fails_initialization_with_id_0 for that class to false. This will allow the item to be instantiated as a
 * dummy for cart purposes with an ID.
 */
interface TMi_ShoppingCartable
{
	/**
	 * Returns the number of items in stock for this item. A boolean value of false indicates that stock is not tracked
	 * @return int|bool
	 */
	public function inStock();

	/**
	 * Indicates if stock warnings should be shown if the cart automatically reduces the quantity in the cart due to stock
	 * limitations.
	 * @return bool
	 */
	public function showStockAdjustmentWarnings();
	
	/**
	 * The message to show if we want to indicate that this item is out of stock
	 * @return string
	 */
	public function outOfStockMessage();
	
	
	
	/**
	 * Returns the price of one of these items
	 * @return float
	 */
    public function price();
	
	/**
	 * Returns the barcode for this item
	 * @return float
	 */
	public function barcode();
	
	/**
	 * Returns the discount to be applied per item. Often 0.
	 * @return float
	 */
	public function discountPerItem();

	/**
	 * Returns the weight of one of these items, in KG
	 * @return mixed
	 */
	public function weight();

	/**
	 * Returns if this cartable item uses the provided tax
	 * @param TMm_Tax $tax
	 * @return bool
	 */
    public function usesTax($tax);


	/**
	 * Returns the value that is taxable for this item. This is useful is a particular cartable item must specify how
	 * much is actually taxable, rather than the full value.
	 * @param TMm_Tax $tax
	 * @param float $discount The potential discount being applied to this item
	 * @return float
	 */
	public function taxableAmountForTax($tax, $discount = 0);



	/**
	 * Returns the description to be shown in the cart. This method may differ for different items, so the cart item
	 * is provided in case properties from it are needed for an accurate description.
	 * @param TMm_ShoppingCartItem $cart_item
	 * @return TCv_View|string
	 */
	public function cartDescription($cart_item);

	/**
	 * Returns the title to be shown in the cart. In most cases, returning the title() works well, however it can be
	 * customized if necessary.
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $cart_item
	 * @return string
	 */
	public function cartTitle($cart_item);

	/**
	 * Returns the file that is for the photo file. Returns false if no photo is found
	 * @return TCm_File|bool|TCv_View
	 */
	public function cartPhotoFile();

	/**
	 * Returns the URL where this item can be viewed. Returns false if it can't be viewed in a separate window.
	 * @return TCv_View|string|bool
	 */
	public function viewURL();

	/**
	 * Returns the maximum addable to a cart
	 * @return int
	 */
	public function maxAddable();
	
	/**
	 * Validates this item to identify if there are any errors with the cart. The cart will track these errors.
	 * @param TMm_ShoppingCartItem $cart_item
	 * @return string[]|array() An array of error messages related to this cart item. An empty array indicates no errors. The response can also
	 * be an array of values with "text", "button_text" and "button_url" which provide slightly more functionality to take
	 * action on the issues.
	 */
	public function validateCartItem($cart_item);
	
	/**
	 * Returns an array of values indexed with integers from 1 to 26. When this item is added to a cart, this method
	 * is called and any returned values are saved with the cart item.
	 * @return array
	 */
	public function addToCartVariableValues();
	
	/**
	 * A hook method which can be used to trigger additional cart actions as needed. Default implementation should do
	 * nothing.
	 * @param TMm_ShoppingCart $cart
	 */
	public function additionalCartActions($cart);
	
	//////////////////////////////////////////////////////
	//
	// Processing Methods
	//
	// These methods are related to the processing of the cart
	// item after it is successfully purchased
	//
	//////////////////////////////////////////////////////

	/**
	 * A hook method that allows the item to be updated if necessary with the purchase item related to it.
	 *
	 * @param TMm_PurchaseItem $purchase_item The purchase item associated with the item
	 *
	 */
	public function processAfterPurchase($purchase_item);
	
	/**
	 * Returns the name of the model class that is created when this item is purchased. This returns one of three
	 * likely scenarios.
	 *
	 * 1. Purchasing this item doesn't create any additional models in the system, so return false.
	 * 2. This class is the thing that is created, so return it's own model name.
	 * 3. This class generates another type of model, so return that model name.
	 *
	 * @return string|bool
	 */
	public static function purchaseCreatedModelClassName();
	
	/**
	 * Indicates if the item allows updates to the purchase price on unpaid purchases
	 * @return bool
	 */
	public function allowsUnpaidPurchasePriceUpdates();
	
	//////////////////////////////////////////////////////
	//
	// TCu_Item and TCm_Model methods
	//
	// These methods are built into the TCu_Item and
	// TCm_Model classes. They are required for a cartable to
	// work effectively and are listed here for completeness
	// and code validation
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the title for this item
	 * @return string
	 */
	public function title();

	/**
	 * Returns the id for the item
	 * @return mixed
	 */
	public function id();


	/**
	 * Returns teh content code for this item. Any class that extends TCu_Item will have this built in.
	 * @return mixed
	 */
	public function contentCode();

	/**
	 * A method to delete this cartable item. Any class that extends TCu_Item will have this built in.
	 */
	public function delete();
	
	
	/**
	 * A code for this item. Most accounting systems require this as part of setup for reporting.
	 * @return string
	 */
	public function accountingCode();
	

}