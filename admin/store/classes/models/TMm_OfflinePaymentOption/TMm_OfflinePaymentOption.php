<?php
class TMm_OfflinePaymentOption extends TCm_Model
{
	use TMt_AlternatePaymentOption;
	
	public function __construct()
	{
		parent::__construct('TMt_AlternatePaymentOption');
		
		$this->configureAPIValues();
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_AlternatePaymentOption
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * The title for this payment option
	 * @return string
	 */
	public static function paymentOptionTitle()
	{
		return 'Pay Offline';
	}
	
	/**
	 * Returns if this payment option is permitted on unpaid purchases. Some options might not be allowable, so it
	 * must be defined for each payment option.
	 * @return bool
	 */
	public function usableWithUnPaidPurchases()
	{
		return false;
	}
	
	/**
	 * Returns the associative array of api fields for this payment processor
	 * @return array
	 */
	public static function apiFields()
	{
		return array(
			'payment_option_explanation' => array(
				'title' => 'Payment Option Explanation',
				'help_text' => '',
				'default' => 'Cheque or In-Person Payment'
			
			)
		);
		
	}
	
	/**
	 * processes the payment option with a card and form processor. That's everything needed to perform whatever
	 * transactions are necessary.
	 * @param TMm_ShoppingCart|TMm_Purchase $shopping_cart
	 * @param TCc_FormProcessor $form_processor
	 * @return array An associative array that has a `transaction_id`, 'message' and `override_values` for processing
	 * the cart
	 * into a purchase.
	 */
	public function processOptionPayment($shopping_cart, $form_processor)
	{
		$override_properties = array();
		$override_properties['type'] = 'unpaid';
		$override_properties['num_payments'] = '0'; // indicator for not paid


		return array(
			'transaction_id' => '',// no real transaction ID. Nothing happened
			'message' => '',
			'override_values' => $override_properties
		);
	}
	
}