<?php
class TMm_Purchase extends TCm_Model
{
	use TMt_PageModelViewable;
	
	protected $purchase_id;
	protected $user_id, $first_name, $last_name, $email, $phone, $cart_id, $type, $buyer_id;
	protected $billing_street, $billing_street_2, $billing_city, $billing_province, $billing_country, $billing_postal,
		$shipping_street, $shipping_street_2, $shipping_city, $shipping_province, $shipping_country,
		$shipping_postal, $is_active;
	
	protected $transaction_id, $transaction_id_2, $num_payments, $split_payment_amount_1, $split_payment_amount_2;
	
	protected $access_code;
	
	protected $total, $subtotal, $delivery_cost, $delivery_method, $instructions;
	protected $sandbox_mode;
	protected $subtotal_calc, $total_calc;
	protected $payment_date, $payment_method, $payment_method_2;
	
	protected $items = false;
	protected $applicable_taxes = false;
	protected $tax_totals = array();
	
	// Purchase Editing
	protected $revision_number, $original_purchase_id;
	protected $refunds;
	
	public static $table_id_column = 'purchase_id';
	public static $table_name = 'purchases';
	public static $model_title = 'Purchase';
	public static $primary_table_sort = 'date_added DESC';
	
	public static $transaction_types = array(
		'online' => 'Credit Card',
		'check' => 'Check',
		'cash' => 'Cash',
		'debit' => 'Debit/Credit Machine',
		'comp' => 'Complimentary',
		'free' => 'Free',
		'unpaid' => 'Unpaid',
		'gift_certificate' => 'Gift Certificate',
		'eft' => 'Electronic Funds Transfer (EFT)',
	);
	
	// These originally stem from bambora codes, but work for lots of types
	public static $credit_card_types = array(
		'DB' => array('title' => 'Debit', 'icon'=>'fab fa-cc'),
		'MC' => array('title' => 'MasterCard', 'icon'=>'fab fa-cc-mastercard'),
		'VI' => array('title' => 'Visa', 'icon'=>'fab fa-cc-visa'),
		'AM' => array('title' => 'American Express', 'icon'=>'fab fa-cc-amex'),
		'NN' => array('title' => 'Discover', 'icon'=>'fab fa-cc-discover'),
		'DI' => array('title' => 'Diners Club', 'icon'=>'fab fa-cc-diners-club'),
	);
	
	public function __construct($purchase_id)
	{
		parent::__construct($purchase_id);
		if(!$this->exists) { return null; };
		
		
	}
	
	public function runAfterInit()
	{
		$this->recalculateUnPaidTotals();
	}
	
	/**
	 * Returns the user for this purchase
	 * @return bool|TMm_User|TMt_Store_User
	 */
	public function user()
	{
		return TMm_User::init( $this->user_id);
	}
	
	/**
	 * Returns the user for this purchase
	 * @return bool|TMm_User|TMt_Store_User
	 */
	public function buyer()
	{
		return TMm_User::init( $this->buyer_id);
	}
	
	
	
	/**
	 * Returns the name of the buyer
	 * @return string
	 */
	public function buyerName()
	{
		return $this->firstName().' '.$this->lastName();
	}
	
	
	/**
	 * @return string
	 */
	public function firstName()
	{
		if($this->user())
		{
			return $this->user()->firstName();
		}
		else
		{
			return $this->first_name;
		}
	}
	
	/**
	 * @return string
	 */
	public function lastName()
	{
		if($this->user())
		{
			return $this->user()->lastName();
		}
		else
		{
			return $this->last_name;
		}
	}
	
	/**
	 * @return string
	 */
	public function phone()
	{
		if($this->phone != '')
		{
			return $this->phone;
		}
		else
		{
			return $this->user()->mobilePhone();
		}
	}
	
	public function email()
	{
		return $this->email;
	}
	
	public function type()
	{
		return $this->type;
	}
	
	public function typeSecondPayment()
	{
		return $this->type_2;
	}
	
	/**
	 *
	 * @return string
	 * @deprecated Use transactionTypeTitle() instead
	 */
	public function typeTitle()
	{
		return $this->transactionTypeTitle();
	}
	
	/**
	 * Returns the title of the transaction type
	 * @return string
	 */
	public function transactionTypeTitle()
	{
		if($this->isUnpaid())
		{
			return '––';
		}
		else
		{
			return static::transactionTypes()[$this->type];
		}
	}
	
	/**
	 * Returns the title of the transaction type
	 * @return string
	 */
	public function transactionTypeSecondTitle()
	{
		if($this->isUnpaid())
		{
			return '––';
		}
		else
		{
			return static::transactionTypes()[$this->typeSecondPayment()];
		}
	}
	
	/**
	 * Returns the array of transaction types
	 * @return string[]
	 */
	public static function transactionTypes()
	{
		return static::$transaction_types;
	}
	
	public function paymentMethod()
	{
		return $this->payment_method;
	}
	
	public function splitPaymentMethod()
	{
		return $this->payment_method_2;
	}
	
	/**
	 * Returns if the purchase is unpaid, which means it was processed and marked as to be paid later.
	 * @return bool
	 */
	public function isUnpaid()
	{
		return $this->num_payments == 0;
	}
	
	/**
	 * Returns if the purchase is paid
	 * @return bool
	 */
	public function isPaid()
	{
		return $this->num_payments > 0;
	}
	
	/**
	 * Updates this purchase to mark it as paid with a transaction id
	 * @param int|string $transaction_id
	 * @param string $type (Optional) Default "online". Indicates the type of transaction
	 */
	public function markAsPaidWithTransaction($transaction_id, $type = 'online', $payment_method = null)
	{
		if($payment_method != null)
		{
			$values['payment_method'] = $payment_method;
		}
		$values['transaction_id'] = $transaction_id;
		$values['type'] = $type;
		$values['num_payments'] = '1';
		$values['payment_date'] = date('Y-m-d H:i:s'); // Track the payment date
		
		
		$this->updateWithValues($values);
	}
	
	/**
	 * Updates this purchase to mark it as paid with a transaction id
	 * @param int|string $transaction_id_1
	 * @param string $type_1 (Optional) Default "online". Indicates the type of transaction
	 * @param float $amount_1 (Optional) Default null. The amount to charge, if null, then full value is charged.
	 * @param string $payment_method_1
	 * @param int|string $transaction_id_2
	 * @param string $type_2 Indicates the type of transaction
	 * @param float $amount_2
	 * @param string $payment_method_2
	 */
	public function markAsPaidWithTwoTransaction($transaction_id_1,
	                                             $type_1 ,
	                                             $amount_1,
	                                             $payment_method_1,
	                                             $transaction_id_2,
	                                             $type_2 ,
	                                             $amount_2,
												 $payment_method_2)
	{
		$values['num_payments'] = '2';
		$values['payment_date'] = date('Y-m-d H:i:s'); // Track the payment date
		
		$values['transaction_id'] = $transaction_id_1;
		$values['type'] = $type_1;
		$values['split_payment_amount_1'] = $amount_1;
		$values['payment_method'] = $payment_method_1;
		
		$values['transaction_id_2'] = $transaction_id_2;
		$values['type_2'] = $type_2;
		$values['split_payment_amount_2'] = $amount_2;
		$values['payment_method_2'] = $payment_method_2;
		
		$this->updateWithValues($values);
	}
	
	public function isOnline()
	{
		return $this->type() == 'online';
	}
	
	public function isComp()
	{
		return $this->type() == 'comp';
	}
	
	public function isCheck()
	{
		return $this->type() == 'check';
	}
	
	public function isCash()
	{
		return $this->type() == 'cash';
	}
	
	public function isDebitCredit()
	{
		return $this->type() == 'debit';
	}
	
	public function isTestTransaction()
	{
		return $this->sandbox_mode > 0;
	}
	
	/**
	 * Sends the emails for both staff and recipient
	 */
	public function sendEmail()
	{
		// Send the staff email first
		$email = TMv_PurchaseEmailStaff::init($this);
		$email->send();
	
		$email = TMv_PurchaseEmail::init($this);
		$email->send();
		
	}
	
	/**
	 * Adds the sendEmail() function to the cron list to be triggered when a purchase is made
	 * @return void
	 */
	public function addEmailsToCron() : void
	{
		// Create a cron job, call the async
		TSm_CronTask::createWithValues(
			[
				'model_class_name' => 'TMm_Purchase',
				'model_id' => $this->id(),
				'method_name' => 'sendEmail'
			]);
		
		// Run the async cron to speed up potential emails
		// If it doesn't run, the regular cron still catches it
		$manager = TSm_CronManager::init();
		$manager->runForClassAsynchronously('TMm_Purchase');
	}
	
	/**
	 * Resends the customer email
	 */
	public function resendCustomerEmail()
	{
		$email = TMv_PurchaseEmail::init($this);
		$email->send();
		
		TC_message('Purchase #'.$this->id().' email resent');
	}
	
	/**
	 * The method called when the purchase is paid and we need to send an email
	 */
	public function sendPaidEmail()
	{
		$this->resendCustomerEmail();
	}
	
	/**
	 * Returns the title of the purchase
	 */
	public function title()
	{
		return 'Order '.$this->id();
		
	}
	
	
	/**
	 * Returns the purchase number which includes the ID
	 */
	public function purchaseNumber()
	{
		// 65 = A, counting starts at 1
		if($this->revision_number > 1)
		{
			$purchase_version = (chr(64 + $this->revision_number));
			return $this->original_purchase_id . '-' . $purchase_version;
		}
		
		return $this->id();
		
		
	}
	
	public function processRevisedPurchases()
	{
		if($this->id() != $this->original_purchase_id && $this->original_purchase_id > 0)
		{
			$query = "UPDATE purchases SET is_replaced = 1 WHERE original_purchase_id = :opi AND revision_number < :my_revision";
			$this->DB_Prep_Exec($query, array(
				'opi' => $this->original_purchase_id,
				'my_revision'=>$this->revision_number));
		}
	}
	
	/**
	 * Returns the transaction ID for the payment system.
	 * @return mixed
	 */
	public function transactionID()
	{
		return $this->transaction_id;
	}
	
	/**
	 * Returns the potential second transaction ID which happens if purchased from two different transactions on the
	 * payment systems.
	 * @return mixed
	 */
	public function transactionIDSecondPayment()
	{
		return $this->transaction_id_2;
	}
	
	
	/**
	 * Returns the last four  number used for the purchase
	 * @return string|bool
	 */
	public function cardLastFourDigits()
	{
		return false;
	}
	
	/**
	 * Returns the last four  number used for the purchase
	 * @return string|bool
	 */
	public function cardIconCode()
	{
		
		return 'fa-credit-card';
	}
	
	/**
	 * Returns the payment date, with optional formatting
	 * @param bool|string $format
	 * @return string
	 */
	public function paymentDate($format = false)
	{
		$date = $this->date_added;
		if($this->paymentDateIsDifferent())
		{
			$date = $this->payment_date;
		}
		
		if($format)
		{
			return date($format, strtotime($date));
		}
		else
		{
			return $date;
		}
		
	}
	
	public function paymentDateIsDifferent()
	{
		return $this->payment_date != null;
	}
	
	/**
	 * @return bool
	 */
	public function hasSecondPayment()
	{
		return $this->num_payments == 2;
	}
	
	//////////////////////////////////////////////////////
	//
	// CARTS AND GROUPING
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the ID for the related shopping cart
	 * @return int
	 */
	public function shoppingCartID()
	{
		return $this->cart_id;
		
	}
	
	
	/**
	 * Returns the the related shopping cart
	 * @return TMm_ShoppingCart|bool
	 */
	public function shoppingCart()
	{
		if($this->cart_id > 0)
		{
			return TMm_ShoppingCart::init($this->cart_id);
		}
		
		return false;
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// COSTS
	//
	// Functions related to costs
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the total for the shopping cart as it was originally defined
	 * @return float
	 */
	public function originalTotal()
	{
		return $this->total;
		
	}
	
	
	/**
	 * Returns the total for the shopping cart
	 * @return float
	 */
	public function total()
	{
		// Note, this calculation does NOT use significant digits. The math should always match the individual values
		// being presented to the user.
		if($this->total_calc == null)
		{
			$this->total_calc = $this->subtotal(false);
			
			// Loop through all taxes
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$this->total_calc += $this->totalForTax($tax, false);
			}
			
			$this->total_calc += $this->deliveryPrice(false);
		}
		
		return $this->formatCurrency($this->total_calc);
		
	}
	
	/**
	 *
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function splitPayment1Total($use_sig_digits = false)
	{
		return $this->formatCurrency($this->split_payment_amount_1, $use_sig_digits);
	}
	
	/**
	 *
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function splitPayment2Total($use_sig_digits = false)
	{
		return $this->formatCurrency($this->split_payment_amount_2, $use_sig_digits);
	}
	
	/**
	 * Returns the subtotal for the shopping cart as it was originally defined
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function originalSubtotal($use_sig_digits = false)
	{
		return $this->formatCurrency($this->subtotal, $use_sig_digits);
	}
	
	/**
	 * Resets the calculated values to ensure the next call re-acquires the values from scratch. This
	 */
	public function resetCalculations()
	{
		$this->total_calc = null;
		$this->subtotal_calc = null;
		$this->tax_totals = array();
	}
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function subtotal($use_sig_digits = false)
	{
		if($this->subtotal_calc == null)
		{
			$this->subtotal_calc = 0;
			foreach($this->purchaseItems() as $purchase_item)
			{
				// Subtotals don't use sig digits, they need to match the values shown
				$this->subtotal_calc += $purchase_item->subtotal(false);
			}
		}
		
		return $this->formatCurrency($this->subtotal_calc, $use_sig_digits);
	}
	
	/**
	 * Returns the dollar value for the original tax
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function originalTotalForTax($tax, $use_sig_digits = false)
	{
		$var_name = 'tax_'.$tax->id().'_paid';
		return $this->formatCurrency( $this->$var_name, $use_sig_digits);
	}
	
	/**
	 * Returns the dollar value for the original delivery total
	 * @return float
	 */
	public function originalTotalForDelivery()
	{
		return $this->formatCurrency($this->delivery_cost);
	}
	
	/**
	 * Returns the dollar value for the tax provided
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function totalForTax($tax, $use_sig_digits = false)
	{
		if(!isset($this->tax_totals[$tax->id()]))
		{
			// This method must calculate everything based on saved, values,
			// no current values since they might all change.
			
			$tax_amount = 0;
			foreach($this->purchaseItems() as $purchase_item)
			{
				//if($purchase_item->percentageForTax($tax) > 0)
				//{
				$tax_amount += $purchase_item->totalForTax($tax, true );
				//}
			}
			
			if($this->hasDeliverySet() && $tax->appliesToShipping())
			{
				$tax_amount += $this->deliveryPriceForTax($tax, true);
			}
			
			$this->tax_totals[$tax->id()] = $tax_amount;
		}
		
		// Return the tax amount
		return $this->formatCurrency($this->tax_totals[$tax->id()],$use_sig_digits);
	}
	
	/**
	 * Returns the total for all the taxes
	 * @return float|int
	 */
	public function totalForAllTaxes()
	{
		$total = 0;
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$total += $this->totalForTax($tax);
		}
		
		return $total;
	}
	
	
	/**
	 * A method that removes the cached tax values so that they calculate correctly
	 */
	public function clearCachedCostValues()
	{
		$this->tax_totals = null;
		$this->total_calc = null;
	}
	
	/**
	 * Returns the percent for the tax provided
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function percentageForTax($tax)
	{
		$var_name = 'tax_'.$tax->id().'_percent';
		return $this->$var_name;
	}
	
	/**
	 * Returns the total discounts applied to all items
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function totalDiscountForItems($use_sig_digits = false)
	{
		$subtotal = 0;
		foreach($this->purchaseItems() as $cart_item)
		{
			$subtotal += $cart_item->quantity() * $cart_item->discountPerItem(true);
		}
		return $this->formatCurrency($subtotal, $use_sig_digits);
	}
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function subtotalNoDiscounts($use_sig_digits = false)
	{
		
		$subtotal = 0;
		foreach($this->purchaseItems() as $purchase_item)
		{
			$subtotal += $purchase_item->quantity() * ($purchase_item->pricePerItem(true) +
					$purchase_item->discountPerItem(true));
			
		}
		return $this->formatCurrency($subtotal, $use_sig_digits);
	}
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function originalSubtotalNoDiscounts($use_sig_digits = false)
	{
		
		$subtotal = 0;
		foreach($this->purchaseItems() as $purchase_item)
		{
			if(!$purchase_item->isRefund())
			{
				$subtotal += $purchase_item->quantity() * ($purchase_item->pricePerItem(true) +
						$purchase_item->discountPerItem(true));
			}
			
		}
		return $this->formatCurrency($subtotal, $use_sig_digits);
	}
	
	/**
	 * Recalculates the subtotals and saves them to match the values in the system. This only has an effect on unpaid
	 * purchases, since paid items should not be editable.
	 *
	 * If a purchase is unpaid, it will update the values to ensure it matches anything else associated with the
	 * purchase.
	 */
	public function recalculateUnPaidTotals()
	{
		// Only bother if the purchase is unpaid and it also has items
		// The second one ensures that this code doesn't run when a purchase is created, as that would wipe out
		// necessary values on paid items.
		if($this->isUnpaid() && $this->numPurchaseItems() > 0)
		{
			/////////////////////////////////////
			
			// Step 1: Update percentages for purchase, used by items
			//$this->addConsoleDebug('---- STEP 1 ----');
			$values = [];
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$varname = 'tax_' . $tax->id() . '_percent';
				$percent = $this->percentageForTax($tax);
				if($this->$varname != $percent)
				{
					$this->$varname = $percent; // save to the object, so that calculations use that value
					$values[$varname] = $percent;
				}
			}
			if(count($values) > 0)
			{
				$this->updateWithValues($values);
			}
			
			/////////////////////////////////////
			
			// Step 2: Recalculate individual items
			//$this->addConsoleDebug('---- STEP 2 ----');
			foreach($this->purchaseItems() as $purchase_item)
			{
				$purchase_item->recalculateUnPaidTotals();
			}
			
			/////////////////////////////////////
			
			// Step 3: Recalculate remaining purchase values using new item values
			//$this->addConsoleDebug('---- STEP 3 ----');
			$values = [];
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$total_for_tax = $this->totalForTax($tax, true);
				$varname = 'tax_' . $tax->id() . '_paid';
				if($this->$varname != $total_for_tax)
				{
					$values[$varname] = $total_for_tax;
				}
				
				// Delivery taxes
				$varname = 'tax_' . $tax->id(). '_delivery_paid';
				$delivery_for_tax = $this->deliveryPriceForTax($tax, true );
				if($this->$varname != $delivery_for_tax)
				{
					$values[$varname] = $delivery_for_tax;
				}
				
			}
			if(count($values) > 0)
			{
				$this->updateWithValues($values);
			}
			
			/////////////////////////////////////
			
			// Step 4: Subtotals and totals
			//$this->addConsoleDebug('---- STEP 4 ----');
			$values = [];
			// Check for subtotal
			if($this->subtotal !== $this->subtotal(true))
			{
				$values['subtotal'] = $this->subtotal(true);
			}
			
			// Undo internal caching
			$this->total_calc = null;
			$this->applicable_taxes = false;
			if(round($this->total,2) != $this->total())
			{
				$values['total'] = $this->total();
			}
			
			
			if(count($values) > 0)
			{
				$this->updateWithValues($values);
			}
			
			
			
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// REFUNDS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * @return TMm_PurchaseRefund[]
	 */
	public function refunds()
	{
		if(is_null($this->refunds))
		{
			// Calls the DB to find all refunds, even those not tied to an item
			$this->refunds = array();
			$query = "SELECT * FROM purchase_refunds WHERE purchase_id = :purchase_id";
			$result = $this->DB_Prep_Exec($query, array('purchase_id' => $this->id()));
			while($row = $result->fetch())
			{
				$refund = TMm_PurchaseRefund::init($row);
				$this->refunds[$refund->id()] = $refund;
			}
		}
		
		return $this->refunds;
	}
	
	/**
	 * @return bool
	 */
	public function hasRefunds()
	{
		return count($this->refunds()) > 0;
	}
	
	/**
	 * Returns how many of a given purchase item have been refunded
	 * @param TMm_PurchaseItem $purchase_item
	 * @return int
	 */
	public function numRefundedPurchaseItems($purchase_item)
	{
		$num = 0;
		foreach($this->purchaseItems() as $this_purchase_item)
		{
			if($this_purchase_item->refundPurchaseItemID() == $purchase_item->id())
			{
				$num += abs($this_purchase_item->quantity()); // positive count of the number refunded
			}
		}
		return $num;
	}
	
	/**
	 * Returns the refund processor for this purchase
	 * @return TMt_PaymentProcessor
	 */
	public function paymentProcessor()
	{
		if($this->refundProcessorClass())
		{
			/** @var TMt_PaymentProcessor $processor */
			$processor = ($this->refundProcessorClass())::init();
			
			// Ensure the sandbox modes match so that the refunds go to the correct spot
			$processor->setSandboxMode($this->sandbox_mode);
			return $processor;
		}
		
		TC_triggerError('Attempting to access a payment processor that is not defined for purchase ID '.$this->id());
		
	}
	
	/**
	 * The name of the payment processing class for this purchase. This method must be extended and return a valid
	 * string name for a class that uses the `TMt_PaymentProcessor` trait. A value of false indicates that this
	 * purchase was not run through a payment processor and therefore cannot be refunded.
	 * @return string|bool
	 */
	public function refundProcessorClass()
	{
		return TC_getModuleConfig('store','payment_processor');
	}
	
	/**
	 * Returns the refund processor for this purchase
	 * @return TMt_PaymentProcessor
	 */
	public function refundProcessor()
	{
		return $this->paymentProcessor();
	}
	
	
	
	/**
	 * Returns if it's possible to refund the payment. This is calculated based on various factors.
	 * @param bool $trigger_errors Indicates if errors should be triggered when testing
	 * @return bool
	 */
	public function paymentRefundPossible($trigger_errors = false)
	{
		// No transaction ID
		if($this->transactionID() == '')
		{
			$this->addConsoleDebug('Testing transaction ID');
			if($trigger_errors)
			{
				TC_message('Refund Skipped : No Transaction ID');
			}
			return false;
		}
		
		// Not an online purchase
		if($this->type() != 'online')
		{
			if($trigger_errors)
			{
				TC_message('Refund Skipped : Not Online');
			}
			return false;
		}
		
		// No refund processing class
		if(!$this->refundProcessorClass())
		{
			if($trigger_errors)
			{
				TC_message('Refund Skipped : No Refund Processor Class');
			}
			return false;
		}
		
		$first_three =substr($this->transactionID(),0,3);
		
		// Skipped Transactions
		if($first_three == 'SK_')
		{
			if($trigger_errors)
			{
				TC_message('Refund Skipped : Transaction ID is Skipped (SK_)');
			}
			return false;
		}
		
		// Zero Dollar Transactions
		if($first_three == 'ZR_')
		{
			if($trigger_errors)
			{
				TC_message('Refund Skipped : transaction ID is Zeroed (ZR_)');
			}
			return false;
		}
		
		
		
		return true;
	}

	//////////////////////////////////////////////////////
	//
	// DELIVERY
	//
	// Functions related to delivery
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the cart is set for pickup
	 * @return bool
	 */
	public function isPickup()
	{
		return $this->delivery_method == 'pickup';
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return string
	 */
	public function deliveryTitle()
	{
		if($this->delivery_method == 'pickup')
		{
			return 'Pick-Up';
		}
		
		$parts = @explode('--', $this->delivery_method);
		$class_name = $parts['0'];
		if(class_exists($class_name))
		{
			$delivery = TC_initClassWithContentCode($this->delivery_method);
			if($delivery)
			{
				return $delivery->title();
			}
		}
		
		return 'Delivery Not Set';
		
		
	}
	
	/**
	 * Returns the delivery method
	 * @return string
	 */
	public function deliveryMethod()
	{
		return $this->delivery_method;
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function deliveryPrice($use_sig_digits = false)
	{
		$total = $this->delivery_cost;
		foreach($this->refunds() as $refund)
		{
			$total -= $refund->deliveryPrice();
		}
		
		return $this->formatCurrency($total, $use_sig_digits);

	}
	
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return bool
	 */
	public function deliveryIsUsed()
	{
		return TC_getModuleConfig('store','shipping_enabled');
	}
	
	
	/**
	 * Returns the price for a tax for the delivery
	 *
	 * @param TMm_Tax $tax
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function deliveryPriceForTax($tax, $use_sig_digits = false )
	{
		$price = 0;
		if($this->hasDeliverySet())
		{
			if($this->delivery_method != 'pickup')
			{
				if($tax->appliesToShipping())
				{
					$price += $this->percentageForTax($tax)/100 * $this->deliveryPrice(true);
				}
			}
		}
		
		return $this->formatCurrency($price, $use_sig_digits);
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return bool
	 */
	public function hasDeliverySet()
	{
		return $this->delivery_method != '';
	}
	
	/**
	 * Returns the instructions for the cart, if any
	 * @return string
	 */
	public function instructions()
	{
		return $this->instructions;
	}
	
	/**
	 * Returns if taxes are ready to be viewed.
	 * @return bool
	 */
	public function taxesAreReady()
	{
		return true;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ADDRESSES
	//
	// Functions related to addresses
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this item uses addresses
	 * @return bool
	 */
	public function usesAddresses()
	{
		return $this->deliveryIsUsed();
	}
	
	/**
	 * Return if shipping addresses should be used wiht this purchase
	 * @return bool
	 */
	public function useShippingAddress()
	{
		return true;
	}
	
	/**
	 * Returns if this item shows billing addreses as part of the interface. This
	 * return bool
	 */
	public function showBillingAddress()
	{
		return $this->hasBillingAddress();
		
		
	}
	
	
	/**
	 * Returns if this cart has all the necessary shipping fields filled out
	 * @return bool
	 */
	public function hasShippingAddress()
	{
		return $this->shipping_street != '' && $this->shipping_city != '' && $this->shipping_province != '' &&
			$this->shipping_country != '';
	}
	
	/**
	 * Returns if this cart has all the necessary billing fields filled out
	 * @return bool
	 */
	public function hasBillingAddress()
	{
		return $this->billing_street != '' && $this->billing_city != '' && $this->billing_province != '' &&
			$this->billing_country != '';
	}
	
	/**
	 * @param bool $show_country
	 * @return string
	 */
	public function shippingAddressFormatted($show_country = true)
	{
		$address = $this->shipping_street;
		if($this->shipping_street_2 != '')
		{
			$address .= '<br />'.$this->shipping_street_2;
		}
		$address .= '<br />'.$this->shipping_city;
		$address .= ', '.$this->shipping_province;
		if($show_country)
		{
			$address .= '<br />' . $this->shipping_country;
		}
		$address .= '<br />'.$this->shipping_postal;
		
		
		return $address;
	}
	
	/**
	 * @param bool $show_country
	 * @return string
	 */
	public function billingAddressFormatted($show_country = true)
	{
		$address = $this->billing_street;
		if($this->billing_street_2 != '')
		{
			$address .= '<br />'.$this->billing_street_2;
		}
		$address .= '<br />'.$this->billing_city;
		$address .= ', '.$this->billing_province;
		if($show_country)
		{
			$address .= '<br />'.$this->billing_country;
		}
		$address .= '<br />'.$this->billing_postal;
		
		
		return $address;
	}
	
	/**
	 * @return string[]
	 */
	public function billingAddressValues()
	{
		return array(
			'address_1' => $this->billing_street,
			'address_2' => $this->billing_street_2,
			'city' => $this->billing_city,
			'province' => $this->billing_province,
			'postal_code' => $this->billing_postal,
			'country' => $this->billing_country,
		);
	}
	
	
	/**
	 * @return string[]
	 */
	public function shippingAddressValues()
	{
		return array(
			'address_1' => $this->shipping_street,
			'address_2' => $this->shipping_street_2,
			'city' => $this->shipping_city,
			'province' => $this->shipping_province,
			'postal_code' => $this->shipping_postal,
			'country' => $this->shipping_country,
		);
	}
	
	
	
	/**
	 * @param string $address_1
	 * @param string $address_2
	 * @param string $city
	 * @param string $province
	 * @param string $postal_code
	 * @param string $country
	 */
	public function setBillingAddress($address_1, $address_2, $city, $province, $postal_code, $country)
	{
		$values = array(
			'billing_street' => $address_1,
			'billing_street_2' => $address_2,
			'billing_city' => $city,
			'billing_province' => $province,
			'billing_postal' => $postal_code,
			'billing_country' => $country,
		);
		$this->updateWithValues($values);
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PURCHASE ITEMS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Convenience function that returns all the items in the purchase, which references cartItems()
	 * @return TMm_PurchaseItem[]
	 */
	public function purchaseItems()
	{
		return $this->cartItems();
	}
	
	
	/**
	 * Returns all the items in the purchase. Defined this way to allow extending of Purchases and Carts together
	 * with a common programming structure.
	 * @return TMm_PurchaseItem[]
	 * @see TMm_Purchase::purchaseItems()
	 */
	public function cartItems()
	{
		if($this->items === false)
		{
			$this->items = array();
			// Uses date-added to ensure the order respects possible insertions that use parent dates
			$query = "SELECT * FROM purchase_items WHERE purchase_id = :purchase_id ORDER BY refund_id, date_added ASC";
			$result = $this->DB_Prep_Exec($query, array('purchase_id' => $this->id()) );
			while($row = $result->fetch())
			{
				$purchase_item = TMm_PurchaseItem::init($row);
				$this->addNewPurchaseItem($purchase_item);
				
			}
		}
		return $this->items;
	}
	
	/**
	 * @param TMm_PurchaseItem $purchase_item
	 */
	public function addNewPurchaseItem($purchase_item)
	{
		$this->items[$purchase_item->id()] = $purchase_item;
		$this->resetCalculations();
		
	}
	
	/**
	 * Returns the number of different products in this cart. If there are 3 of a product, it will only return 1. For a count of all products, see numCartItems.
	 * @return int
	 */
	public function numPurchaseItems()
	{
		$count = 0;
		foreach($this->purchaseItems() as $cart_item)
		{
			$count += $cart_item->quantity();
		}
		
		return $count;
	}
	
	/**
	 * Returns if the cart is empty
	 * @return bool
	 */
	public function isEmpty()
	{
		return sizeof($this->cartItems()) == 0;
	}
	
	public function hasErrors()
	{
		return false;
	}
	
	/**
	 * @return bool
	 */
	public function hasErrorsOrWarnings()
	{
		return false;
		
	}
	
	public function isComplete()
	{
		return true;
	}
	
	/**
	 * Returns all the cart items that matches the cartable item provided. It's possible to have more than one.
	 * @param TMi_ShoppingCartable $cartable
	 * @return TMm_PurchaseItem[]
	 */
	public function purchaseItemsWithCartable($cartable)
	{
		$items = array();
		foreach($this->purchaseItems() as $cart_item)
		{
			if($cartable->contentCode() == $cart_item->item()->contentCode())
			{
				$items[] = $cart_item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Adds an item to the purchase using a barcode
	 * @param string $barcode
	 * @see TMm_ShoppingCartList::findCartableItemWithBarcode()
	 */
	public function addItemWithBarcode($barcode)
	{
		$list = TMm_ShoppingCartList::init();
		$item = $list->findCartableItemWithBarcode($barcode);
		if($item)
		{
			$this->addItem($item->contentCode());
		}
		else
		{
			TC_message('Item with barcode not found', false);
		}
	}
	
	/**
	 * Adds an item to this purchase. This only works for unpaid purchases.
	 * @param string $content_code
	 * @param int $quantity (Optional) Default 1.
	 * @return bool|int|TMm_PurchaseItem
	 */
	public function addItem($content_code, $quantity = 1)
	{
		if($this->isPaid())
		{
			TC_triggerError('Paid purchases cannot have their items altered');
		}
		
		/** @var TMi_ShoppingCartable $item */
		$item = TC_initClassWithContentCode($content_code);
		
		if(!$item)
		{
			TC_triggerError('Item with code '.$content_code.' could not be found');
		}
		
		$this->addConsoleDebugObject('Adding Item '. $item->title(), $item);
		
		// Check for existing item, update the quantity and get out
		foreach($this->purchaseItemsWithCartable($item) as $purchase_item)
		{
			// Need to find a match with not only the content code but also all 26 variables
			$existing_found = true;
			// We're adding a truly unique item, create it and return
			if($purchase_item->pricePerItem() == $item->price());
			{
				$purchase_item->update($purchase_item->quantity() + $quantity);
				return $purchase_item;
			}
			
		}
		
		$price_paid =  $item->price() - $item->discountPerItem();
		
		$values = array();
		$values['purchase_id'] = $this->id();
		$values['quantity'] = $quantity;
		$values['price_per_item'] =$price_paid;
		$values['discount_per_item'] = $item->discountPerItem();
		$values['subtotal'] = $price_paid * $quantity;
		$values['item_id'] = $item->id();
		$values['item_class'] = get_class($item);
		$values['cart_title'] = $item->cartTitle(false);
		$values['cart_description'] = $item->cartDescription(false);
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			if($item->usesTax($tax))
			{
				$tax_percent = $this->percentageForTax($tax);
				$tax_paid =  $this->formatCurrency($price_paid * $tax_percent / 100, true); // just let it have all the precision
				$values['tax_'.$tax->id().'_percent'] = $tax_percent;
				$values['tax_'.$tax->id().'_paid_per_item'] = $tax_paid;
				$values['tax_'.$tax->id().'_paid'] = $tax_paid * $quantity;
			}
			else // ensure taxes are correct
			{
				$values['tax_'.$tax->id().'_percent'] = 0;
			}
		}
		
		// Init all the items
		$this->purchaseItems();
		$purchase_item = TMm_PurchaseItem::createWithValues($values);
		$this->items[$purchase_item->id()] = $purchase_item;
		
		return $purchase_item;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * returns the access code for viewing this purchase online without a password
	 * @return string
	 */
	public function accessCode()
	{
		if($this->access_code == '')
		{
			$code = new TCu_Text(TCu_Text::generatePassword()); // generate the password
			$this->updateDatabaseValue('access_code',$code->hash());
		}
		
		return $this->access_code;
	}
	
	public function validateAccessCodeFromURL()
	{
		// Check for access code via URL
		if(isset($_GET['id_2']))
		{
			
			if($this->access_code != '' && $this->access_code == $_GET['id_2'])
			{
				return true;
			}
			
		}
		
		return false;
		
	}
	
	
	/**
	 * Checks if the provided user has permission to view this purchase.
	 * It will permit it for any admin or if the user made this purchase.
	 * @param bool|TMm_User $user (Optional) Default false which pulls current user
	 * @return bool
	 */
	public function userCanView ($user = false)
	{
		if(!$user)
		{
			$user = TC_currentUser();
		}
		
		if(!$user)
		{
			return false;
		}
		
		if($user->hasTungstenAccess())
		{
			return true;
		}
		
		return $user->id() == $this->user()->id();
		
	}
	
	/**
	 * @param bool|TMm_User $user
	 * @return bool
	 */
	public function userCanViewPublic($user = false)
	{
		
		if($this->userIsBuyer($user))
		{
			return true;
		}
		
		if($this->validateAccessCodeFromURL())
		{
			return true;
		}
		return false;
		
	}
	
	/**
	 * Checks if the provided user is the buyer for this purchase
	 * @param bool|TMm_User $user (Optional) Default false which pulls current user
	 * @return bool
	 */
	public function userIsBuyer($user = false)
	{
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		return $user->id() == $this->user()->id();
		
	}
	
	/**
	 * Checks if the provided user has permission to view this purchase.
	 * It will permit it for any admin or if the user made this purchase.
	 * @param bool|TMm_User $user (Optional) Default false which pulls current user
	 * @return bool
	 */
	public function userCanEdit($user = false)
	{
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		if($this->isCancelled())
		{
			return false;
		}
		
		if($user->hasTungstenAccess())
		{
			return true;
		}
		
		return false;
		
	}
	
	
	public function userCanDelete($user = false)
	{
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		// Detect if transactions are enabled
		if(TC_getModuleConfig('store','transactions_disabled'))
		{
			return false;
		}
		
		if($user->isAdmin())
		{
			// If it's not paid, we can delete it
			return !$this->isPaid();
		}
		
		return false;
	}
	
	/**
	 * Returns if the user can cancel this purchase
	 * @param bool|TMm_User $user
	 * @return
	 */
	public function userCanCancel($user = false)
	{
		return $this->userCanDelete() && $this->is_active;
	}
	
	public function cancel()
	{
		$this->updateDatabaseValue('is_active',0);
	}
	
	/**
	 * Returns if this purchase is cancelled
	 * @return bool
	 */
	public function isCancelled()
	{
		return $this->is_active == 0;
	}
	
	/**
	 * Returns if this purchase is cancelled
	 * @return bool
	 */
	public function isNotCancelled()
	{
		return $this->is_active == 1;
	}
	
	//////////////////////////////////////////////////////
	//
	// REACTIVATING CARTS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The values that should
	 * @param array $override_values
	 * @return array|bool
	 */
	public function duplicateForCartValues($override_values = array())
	{
		$values = $this->duplicationValues($override_values);
		
		
		// Find the list of fields to remove any that shouldn't exist
		$query = "SELECT * FROM shopping_carts LIMIT 1";
		$result = $this->DB_Prep_Exec($query);
		$db_fields = $result->fetch();
		
		foreach($values as $index => $saved_value)
		{
			if(!isset($db_fields[$index]))
			{
				unset($values[$index]);
			}
		}
		
		unset($values['cart_id']); // unset the cart id
		$values['user_id'] = $this->buyer_id; // ensure we're assigning the cart to the buyer
		
		// Deal with revision Number
		if($cart = $this->shoppingCart())
		{
			$values['revision_number'] = $cart->revisionNumber()+1;
			
		}

		return $values;
	}
	
	/**
	 * Converts this purchase, back into a shopping cart to be edited by a user
	 * @param bool $delete_current_cart Flag if the current cart for the user should be deleted
	 */
	public function reactivateShoppingCart($delete_current_cart = true)
	{
		if($delete_current_cart)
		{
			// DELETE THE CURRENT CART, we can't have 2 active carts
			$current_cart = TMm_ShoppingCart::init();
			$current_cart->delete(false);
		}
		
		
		// FIND THE RELEVANT STAGE, which is the one before the final
		if($this->shoppingCart() && $this->shoppingCart()->checkoutStages())
		{
			$stage_names = array_keys( $this->shoppingCart()->checkoutStages() );
			$last_stage = array_pop($stage_names); // skip the last one
			$last_stage = array_pop($stage_names); // grab the second last one
		}
		else // Checkout stages not used
		{
			$last_stage = 0;
		}
		
		// Set the override values
		$override_values = array(
			'checkout_stage' => $last_stage,
			'is_complete' => 0,
			'purchase_id' => 0
		);
		
		// Reactivate based on the purchase, not the shopping cart.
		// The cart might not exist OR it might have been edited by an admin
		$values = $this->duplicateForCartValues($override_values);
		$new_cart = TMm_ShoppingCart::createWithValues($values);
		
		// Create the cart items
		if($new_cart)
		{
			foreach($this->purchaseItems() as $purchase_item)
			{
				$item_values = $purchase_item->duplicateForCartValues();
				$item_values['cart_id'] = $new_cart->id();
				
				$cart_item = TMm_ShoppingCartItem::createWithValues($item_values);
			}
		}
		
		$new_cart->redirectToCartPage();
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PageModelViewable
	//
	// Methods related to the model being viewable on a
	// page on the public website.
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * A method to indicate if items are linkable in the pages module. This should be disabled for models that aren't
	 * publicly linkable in the pages module.
	 * @return bool
	 */
	public static function isPageLinkable()
	{
		return false;
	}
	
	
	/**
	 * Returns if this class uses the advanced SEO for meta values such as titles and keywords. If the feature switch
	 * for `use_seo_advanced` is enabled this is checked to see the URL target should be presented.
	 * @return bool
	 */
	public static function modelUsesAdvancedSEO()
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		return parent::schema() + [
				
				'cart_id' => [
					'comment'       => 'The ID of the cart that generated this purchase',
					'type'          => 'TMm_ShoppingCart',
					'nullable'      => true,
					
					// If the cart is deleted, only permit if not used
					'foreign_key'   => [
						'model_name'    => 'TMm_ShoppingCart',
						'delete'        => 'RESTRICT'
					],
				],
				
				'user_id' => [
					'comment'       => 'The id of the user who created the purchase, not necessarily the buyer',
					'type'          => 'int(10) unsigned',
					'nullable'      => true,
			
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'SET NULL'
					],
				
				],
				
		
				'is_active' => [
					'comment'       => 'Indicates if the purchase is the current version',
					'type'          => 'tinyint(1) DEFAULT 1',
					'nullable'      => false,
				],
				
				
				'access_code' => [
					'comment'       => 'The access code for the purchase',
					'type'          => 'text',
					'nullable'      => true,
				],
				'instructions' => [
					'comment'       => 'The instructions provided with the cart ',
					'type'          => 'text',
					'nullable'      => true,
				],
				'sandbox_mode' => [
					'comment'       => 'Indicate if sandbox mode was used ',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
				
				////////////////////////////////////////////////
				//
				// BASIC FINANCES
				//
				////////////////////////////////////////////////
				'total' => [
					'comment'       => 'The total cost for the purchase ',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => false,
				],
				'subtotal' => [
					'comment'       => 'The subtotal cost for the purchase ',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => false,
				],
				'currency' => [
					'comment'       => 'The currency used for the purchase ',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				
				
				////////////////////////////////////////////////
				//
				// PAYMENTS
				//
				////////////////////////////////////////////////
				
				'payment_date' => [
					'comment'       => 'The datetime of the purchase',
					'type'          => 'datetime',
					'nullable'      => true,
				],
				'num_payments' => [
					'comment'       => 'Indicates if the number of payments used to create this purchase',
					'type'          => 'int(10)',
					'nullable'      => false,
				],
				'type' => [
					'comment'       => 'The type of payment for the first payment',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'type_2' => [
					'comment'       => 'The type of payment for the second payment',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'transaction_id' => [
					'comment'       => 'The transaction ID from they payment processor',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'payment_method' => [
					'comment'       => 'The method of payment for the first payment',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'payment_method_2' => [
					'comment'       => 'The method of payment for the second payment',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'split_payment_amount_1' => [
					'comment'       => 'The amount for the first payment',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => true,
				],
				'split_payment_amount_2' => [
					'comment'       => 'The amount for the second payment',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => true,
				],
				
				////////////////////////////////////////////////
				//
				// EDITING
				//
				////////////////////////////////////////////////
				'original_purchase_id' => [
					'comment'       => 'The id of the original purchase, if this purchase was changed',
					'type'          => 'TMm_Purchase',
					'nullable'      => false,
					'validations'   => [
						'required'      => false, // Not required since it's possible for the container to be zero
					]
				],
				'revision_number' => [
					'comment'       => 'The revision number if multiples are used',
					'type'          => 'tinyint(3) unsigned DEFAULT 1',
					'nullable'      => false,
				],
				'is_replaced' => [
					'comment'       => 'Indicates if the purchase has been replaced',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
					'index'         => 'is_replaced',
				],
				
			
				
				////////////////////////////////////////////////
				//
				// BUYER
				//
				////////////////////////////////////////////////
				'buyer_id' => [
					'comment'       => 'The ID of the buyer',
					'type'          => 'TMm_User',
					'nullable'      => true,
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'SET NULL'
					],
				],
				'first_name' => [
					'comment'       => 'The first name of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'last_name' => [
					'comment'       => 'The last name of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'email' => [
					'comment'       => 'The email of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'phone' => [
					'comment'       => 'The phone number of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
				////////////////////////////////////////////////
				//
				// ADDRESSES
				//
				////////////////////////////////////////////////
				'billing_street' => [
					'comment'       => 'The billing street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_street_2' => [
					'comment'       => 'The second line of the billing street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_city' => [
					'comment'       => 'The billing city of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_province' => [
					'comment'       => 'The billing province of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_country' => [
					'comment'       => 'The billing country of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_postal' => [
					'comment'       => 'The billing postal code of the buyer',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'shipping_street' => [
					'comment'       => 'The shipping street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_street_2' => [
					'comment'       => 'The second line of the shipping street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_city' => [
					'comment'       => 'The shipping city of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_province' => [
					'comment'       => 'The shipping province of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_country' => [
					'comment'       => 'The shipping country of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_postal' => [
					'comment'       => 'The shipping postal code of the buyer',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				
				////////////////////////////////////////////////
				//
				// DELIVERY
				//
				////////////////////////////////////////////////
				
				'delivery_method' => [
					'comment'       => 'The delivery method used',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'delivery_cost' => [
					'comment'       => 'The delivery cost ',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => true,
				],
				
				
				
		
		
//		'table_keys' => [
//					//'is_complete' => ['index' => "is_complete"],
//					//'user_id' => ['index' => "user_id"],
//				]
			
		];
	}
	
	
}
?>