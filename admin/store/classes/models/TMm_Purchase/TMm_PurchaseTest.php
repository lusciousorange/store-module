<?php
class TMm_PurchaseTest extends TMm_StoreModelTestCase
{
	protected $purchase;
	protected $shopping_cart;
	
	protected function setUp () : void
	{
		parent::setUp();
		$this->shopping_cart = $this->createFilledShoppingCart();
	}
	
	public function testConvertCartToPurchase_Empty()
	{
		// Get an empty Shopping Cart
		$shopping_cart = $this->shoppingCart();
		
		$purchase = $shopping_cart->convertToPurchase('1234');
		$this->assertIsObject($purchase);
		$this->assertEquals('1234', $purchase->transactionID());
		$this->assertEquals(0, $purchase->subtotal());
		$this->assertEquals(0, $purchase->total());
		
	}
	
	/**
	 * Tests the process of converting the cart to a purchase
	 */
	public function testConvertCartToPurchase()
	{
		$shopping_cart = $this->shopping_cart;
		$tax_5 = static::tax_5_percent();
		
		$purchase = $shopping_cart->convertToPurchase('2345');
		
		$this->assertEquals(3, $purchase->numPurchaseItems());
		
		// 3.329 + 10.668
		$this->assertEquals(13.33, $purchase->subtotal());
		$this->assertEquals(1.77, $purchase->deliveryPrice());
		
		$this->assertEquals(0.089, $purchase->deliveryPriceForTax($tax_5, true));
		$this->assertEquals(0.09, $purchase->deliveryPriceForTax($tax_5));
		
		// 0.159 + 0.508 + 0.089
		$this->assertEquals(0.756, $purchase->totalForTax($tax_5, true));
		$this->assertEquals(0.76, $purchase->totalForTax($tax_5));;
		
		// subtotal + delivery + tax
		// 13.33 + 1.77 + 0.76
		$this->assertEquals(15.86, $purchase->total());
		
	}

	public function testUnPaidPurchaseAdjustments()
	{
		$shopping_cart = $this->shopping_cart;
		
		$purchase = $shopping_cart->convertToPurchase(null);
		$this->assertTrue($purchase->isComplete());
		$this->assertFalse($purchase->isPaid(),'Purchase incorrectly marked as paid with a false transaction_id');
		$this->assertTrue($purchase->isUnpaid());
		
		
		
		
	}
	
	/**
	 * A test to confirm that subtotals with 1/2 cents work properly when purchased
	 */
	public function testHalfCentTwoQuantity()
	{
		$shopping_cart = $this->shoppingCart();
		
		
		$values = array(
			'title' => 'Calculator',
			'price' => 5.75,
			'in_stock' => 15,
			'discount' => 2.875
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$cart_item = $shopping_cart->addToCart($product, 2);
		
		// 5.76 = 2.88*2
		
		// Math should be the normal price * quantity, since that's what's presented to the users
		// This should still be true for significant digits
		
		$this->assertEquals(5.76, $cart_item->subtotal());
		$this->assertEquals(5.750, $cart_item->subtotal(true));
		$this->assertEquals(5.76, $shopping_cart->subtotal());
		$this->assertEquals(5.76, $shopping_cart->subtotal(true));
		
		$purchase = $shopping_cart->convertToPurchase('half_8765432352');
		$this->assertEquals(5.76, $purchase->subtotal());
		$this->assertEquals(5.76, $purchase->subtotal(true));
		
		$this->assertEquals(5.76, $purchase->totalDiscountForItems());
		
	}
	
}