<?php

/**
 * Class TMm_PurchaseItem
 */
class TMm_PurchaseItem extends TCm_Model
{
	protected float $subtotal, $price_per_item, $discount_per_item, $amount_paid, $tax_1_paid, $tax_2_paid, $tax_1_paid_per_item, $tax_2_paid_per_item;

	protected ?string $cart_title, $cart_description, $item_class, $timestamp;


	protected ?int $purchase_item_id, $product_id, $purchase_id, $quantity, $cart_item_id, $item_id, $refund_id, $refund_purchase_item_id,
		$num_refunded, $tax_1_percent, $tax_2_percent, $uses_tax_1, $uses_tax_2;

	protected ?string $var_1, $var_2, $var_3, $var_4, $var_5, $var_6, $var_7, $var_8, $var_9, $var_10, $var_11, $var_12,
		$var_13, $var_14, $var_15, $var_16, $var_17, $var_18, $var_19, $var_20, $var_21, $var_22, $var_23, $var_24, $var_25, $var_26;

	
	protected $created_items = false;

	public static $table_id_column = 'purchase_item_id';
	public static $table_name = 'purchase_items';
	public static $model_title = 'Purchase Item';
	public static $primary_table_sort = 'date_added DESC';


	/**
	 * TMm_PurchaseItem constructor.
	 * @param array|int $purchase_item_id
	 */
	public function __construct($purchase_item_id)
	{
		parent::__construct($purchase_item_id);
		if (!$this->exists) {
			return false;
		};

	}

	/**
	 * Returns the quantity for this cart item
	 * @return int
	 */
	public function quantity()
	{
		return $this->quantity;
	}
	
	/**
	 * Returns the shopping cart item that was for this item
	 * @return TMm_ShoppingCartItem|bool
	 */
	public function cartItem()
	{
		return TMm_ShoppingCartItem::init($this->cart_item_id);
	}


	/**
	 * Returns the purchase for this item
	 * @return TMm_Purchase|bool
	 */
	public function purchase()
	{
		return TMm_Purchase::init($this->purchase_id);
	}

	/**
	 * Returns the price per item
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function pricePerItem($use_sig_digits = false)
	{
		return $this->formatCurrency($this->price_per_item,$use_sig_digits);
	}

	/**
	 * Returns the item in the cart
	 * @return TMi_ShoppingCartable|TCu_Item
	 */
	public function item()
	{
		return ($this->item_class)::init($this->item_id);
	}
	
	/**
	 * The name of the class that is a TMi_ShoppingCartable class name
	 * @return string
	 */
	public function itemClass()
	{
		return $this->item_class;
	}
	
	/**
	 * Returns the percentage between 0 and 100
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function percentageForTax($tax)
	{
		// Only bother if we have an amount we paid
		//if($this->totalForTax($tax) > 0)
		//{
			return $this->purchase()->percentageForTax($tax);
		//}
		//return 0;
		
	}
	

	
	/**
	 * Returns the dollar value for the tax provided
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function totalForTax($tax,$use_sig_digits = false)
	{
		$var_name = 'tax_'.$tax->id().'_paid';
		return $this->formatCurrency($this->$var_name, $use_sig_digits);
	}
	
	
	/**
	 * Returns the subtotal in the cart
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function subtotal($use_sig_digits = false)
	{
		return $this->formatCurrency($this->subtotal, $use_sig_digits);
		
	}
	
	/**
	 * Returns the total for the shopping cart
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function total($use_sig_digits = false)
	{
		$total = $this->subtotal(true);
		
		$tax_list = TMm_TaxList::init();
		foreach ($tax_list->taxes() as $tax)
		{
			$total += $this->totalForTax($tax, true);
			
		}
		return $this->formatCurrency($total, $use_sig_digits);
		
	}
	
	/**
	 *
	 * @return bool
	 */
	public function isUnpaid()
	{
		return $this->purchase()->isUnpaid();
	}
	
	
	/**
	 * Returns the title to be shown in the cart. In most cases, returning the title() works well, however it can be
	 * customized if necessary.
	 * @return string
	 */
	public function cartTitle()
	{
		$cart_title = '';
		
		// Try and use the saved cart title if it exists
		if($this->cart_title != '')
		{
			return $cart_title . $this->cart_title;
		}
		
		if($this->item())
		{
			return $cart_title . $this->item()->cartTitle($this);
		}
		else
		{
			if(class_exists($this->item_class))
			{
				return $cart_title . 'Unknown '.($this->item_class)::$model_title;
			}
			
		}
		
		return $cart_title . 'Unknown';
	}
	
	/**
	 * Function to deal with legacy to show old cart titles when necessary
	 */
	public function hasSavedCartTitle()
	{
		return $this->cart_title != '';
	}
	
	
	/**
	 * Returns the cart description for this item
	 * @return string|TCv_View
	 */
	public function cartDescription()
	{
		// Description overridden for refunds
		if($this->isRefund())
		{
			return '';
		}
		
		// Try and use the saved cart description if it exists
		if($this->cart_description != '')
		{
			return $this->cart_description;
		}
		
		if($this->item())
		{
			return $this->item()->cartDescription($this);
		}
		else
		{
			if(class_exists($this->item_class))
			{
				return '';
			}
			
		}
		return '';
	}
	
	//////////////////////////////////////////////////////
	//
	// CREATED ITEMS
	//
	// These are the items that are created as a result of
	// this purchase item. The values are always stored in
	// the field `purchase_item_ids` in the corresponding table
	//
	//////////////////////////////////////////////////////
	
	/**
	 * @return TCm_Model[]
	 */
	public function createdItems()
	{
		if($this->created_items === false)
		{
			$this->created_items = array();
			
			// Find the table for the corresponding class name
			
			/** @var string|TMi_ShoppingCartable $item_class_name */
			$item_class_name = $this->item_class; // The name of the class that was purchased
			if(class_exists($item_class_name))
			{
				$created_class_name = $item_class_name::purchaseCreatedModelClassName();
				if($created_class_name != false && class_exists($created_class_name))
				{
					// We look in the created class name table to find the purchase IDs
					$table_name = $created_class_name::$table_name;
					
					// Possibly comma-separated values, so
					$query = "SELECT * FROM $table_name WHERE FIND_IN_SET(" . $this->id() . ",purchase_item_ids)";
					$result = $this->DB_Prep_Exec($query);
					while($row = $result->fetch())
					{
						$item = $created_class_name::init($row);
						if($item)
						{
							$this->created_items[] = $item;
						}
						
					}
				}
			}
			
		}
		
		return $this->created_items;
	}
	
	/**
	 * A method that can be overloaded by parent classes to which can be useful for saving on loading time.
	 *
	 * @return array An array of TCu_Items
	 */
	public function modelsToLoadWhenActive()
	{
		return array($this->purchase());
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// EDITING
	//
	// Editing is only permitted until it is paid. After that
	// it can't be edited and refunds are required.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Updates the quantity of the item in this purchase item
	 * @param int $quantity
	 */
	public function update($quantity)
	{
		if($this->purchase()->isPaid())
		{
			TC_triggerError('Purchase item prices cannot be edited on paid purchases');
		}
		
		if($quantity < 1)
		{
			$this->delete();
			// Stock adjustments handled by the delete method
		}
		else
		{
			// STOCK ADJUSTMENTS
			// Positive number means we're *Adding* stock, so the actual quantity went down
			// Negative number means we're *removing* stock, so the actual quantity went up
			// eg: we had 5, the new number is 7, the adjustment is -2 since there are two less in stock
			$stock_adjustment = $this->quantity - $quantity;
			if(method_exists($this->item(), 'updateInStock'))
			{
				$this->item()->updateInStock($stock_adjustment);
			}
			
			// Update Values
			$values = array();
			$values['quantity'] = $quantity;
			$values['subtotal'] = $this->pricePerItem()*$quantity;
			
			// tax values must be updated because it's a purchase with set values
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$varname = 'tax_'.$tax->id().'_paid_per_item';
				$values['tax_'.$tax->id().'_paid'] = $this->formatCurrency($this->$varname * $quantity,true);
			}
			$this->updateWithValues($values);
		}
		
	}
	
	/**
	 * extends delete method to deal with potential stock changes
	 * @param string $action_verb
	 */
	public function delete($action_verb = 'deleted')
	{
		// STOCK ADJUSTMENTS
		if(method_exists($this->item(), 'updateInStock'))
		{
			// Add in the amount that was there before
			$this->item()->updateInStock($this->quantity());
		}
		parent::delete($action_verb);
	}
	
	/**
	 * @param float $price
	 * @param bool $show_discount Indicates if the discount compared to the full price should be shown.
	 * @param bool $validate_paid_status Indicates if the function should check the paid status
	 */
	public function updatePrice($price, $show_discount, $validate_paid_status = true)
	{
		if($validate_paid_status && $this->purchase()->isPaid())
		{
			TC_triggerError('Purchase item prices cannot be edited on paid purchases');
		}
		
		$values = array();
		$values['price_per_item'] = $price;
		$values['subtotal'] = $price * $this->quantity;
		
		if($show_discount)
		{
			$discount = $this->item()->price() - $price;
			
			// Avoid negative discounts
			if($discount >= 0)
			{
				$values['discount_per_item'] = $discount;
			}
			
		}
		else
		{
			$values['discount_per_item'] = 0;
		}
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$values = array_merge($values, $this->taxUpdateValues($tax, $price));
			
		}
		
		
		
		$this->updateWithValues($values);
		
		
	}
	
	/**
	 * Returns the array of values that are the values to use in an update for a particular tax.
	 * @param TMm_Tax $tax The tax to use
	 * @param float|null $price (Optional) Default value null. The price to be calculated. If null, we use the
	 * taxable amount
	 * @return array
	 */
	protected function taxUpdateValues($tax, $price = null)
	{
		// Price not provided, we use the actual price per item
		if($price === null)
		{
			$price = $this->pricePerItem();
		}
		
		$tax_per_item = 0;
		
		// If we use this tax, calculate based on the current precentage
		if($this->item()->usesTax($tax))
		{
			$percent = $this->percentageForTax($tax);
			$tax_per_item = $percent / 100 * $price;
			
		}
		
		$values = [];
		$values['tax_'.$tax->id().'_paid_per_item'] = $this->formatCurrency($tax_per_item,true);
		$values['tax_'.$tax->id().'_paid'] = $this->formatCurrency($tax_per_item * $this->quantity, true);
	
		return $values;
	}
		
		
		
		//////////////////////////////////////////////////////
	//
	// REFUNDS
	//
	// Methods related to refund items
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this item is a refund item
	 * @return bool
	 */
	public function isRefund()
	{
		return $this->refund_id > 0;
	}
	
	/**
	 * Returns the purchase refund if it exits
	 * @return TMm_PurchaseRefund|bool
	 */
	public function refund()
	{
		if($this->isRefund())
		{
			return TMm_PurchaseRefund::init($this->refund_id);
		}
		return false;
	}
	
	/**
	 * Returns if this item is refundable at all
	 * @return bool
	 */
	public function isRefundable()
	{
		if($this->isRefund())
		{
			return false;
		}
		
		return $this->numRefundable() > 0;
	}
	
	/**
	 * Returns how many items are refundable from this item.
	 */
	public function numRefundable()
	{
		return $this->quantity() - $this->numRefunded();
	}
	
	/**
	 * Returns the number of these items that have been refunded
	 * @return int
	 */
	public function numRefunded()
	{
		if(!isset($this->num_refunded))
		{
			$this->num_refunded = $this->purchase()->numRefundedPurchaseItems($this);
		}
		
		return $this->num_refunded;
		
	}
	
	/**
	 * Returns if this purchase item has refunds on it
	 * @return bool
	 */
	public function hasRefunds()
	{
		return $this->numRefunded() > 0;
	}
	
	
	/**
	 * @return int
	 */
	public function refundPurchaseItemID()
	{
		return $this->refund_purchase_item_id;
	}
	
	/**
	 * Refunds this item
	 * @param TMm_PurchaseRefund $refund The
	 * @param bool|int $quantity The quantity to refund. No value or a value of false will refund all possible items
	 */
	public function refundQuantity($refund, $quantity = false)
	{
		if($quantity === false)
		{
			$quantity = $this->quantity() - $this->numRefunded();
		}
		
		// Start with a duplicate of this item
		$refund_values = $this->duplicationValues();
		
		// The `per-item` values just need to be negated
		// Calculated subtotals much account for the new quantity
		
		$refund_values['quantity'] = $quantity*-1; // negative quantity
		$refund_values['price_per_item'] *= -1; // negative quantity
		$refund_values['discount_per_item'] *= -1; // negative quantity
		$refund_values['subtotal'] = $quantity * $refund_values['price_per_item'];
		unset($refund_values['date_added']); // we want the current date
		
		// Handle Taxes, ensure we don't use current values since they might change
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			if($refund_values['tax_'.$tax->id().'_paid_per_item'] > 0)
			{
				$refund_values['tax_' . $tax->id() . '_paid_per_item'] *= -1; // negative quantity
				$refund_values['tax_' . $tax->id() . '_paid'] = $this->formatCurrency($refund_values['tax_' . $tax->id() . '_paid_per_item'] * $quantity,true);
			}
		}
		
		// Clear away data that shouldn't exist since it references the parent
		$refund_values['cart_title'] = '';
		$refund_values['cart_description'] = '';
		
		// Set the actual refund specific values
		$refund_values['refund_id'] = $refund->id();
		$refund_values['refund_purchase_item_id'] = $this->id();
		
		// Create the refund purchase item
		$refund_purchase_item = TMm_PurchaseItem::createWithValues($refund_values);
		
		// Stock adjustments happen within createWithValues(). The positive number corrects the inventory already
		
		TC_message(''.$quantity.' '.$this->cartTitle());
		
		
		
	}
	
	/**
	 * Refunds this item using a partial amount
	 * @param TMm_PurchaseRefund $refund The
	 * @param float $amount  The amount to be refunded
	 */
	public function refundPartial($refund, $amount)
	{
		if($amount > $this->pricePerItem())
		{
			TC_message('Unable to refund more than the cost of the item', false);
			return;
		}
		
		// Start with a duplicate of this item
		$refund_values = $this->duplicationValues();
		$refund_values['quantity'] = -1;
		$refund_values['discount_per_item'] = 0;
		$refund_values['price_per_item'] = $amount * -1; // negative quantity
		$refund_values['subtotal'] = $amount * -1;
		unset($refund_values['date_added']); // we want the current date
		
		// Handle Taxes, ensure we don't use current values since they might change
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			
			// Find the percentage for this transaction, we don't just want the tax percentage
			if($refund_values['tax_'.$tax->id().'_paid_per_item'] > 0)
			{
				$tax_percent = $this->purchase()->percentageForTax($tax);
				
				$tax_amount = $this->formatCurrency($amount * $tax_percent / 100,true);
				$refund_values['tax_'.$tax->id().'_paid_per_item'] = $tax_amount * -1;
				$refund_values['tax_'.$tax->id().'_paid'] = $tax_amount * -1;
			}
		}
		
		// Clear away data that shouldn't exist since it references the parent
		$refund_values['cart_title'] = '';
		$refund_values['cart_description'] = '';
		
		// Set the actual refund specific values
		$refund_values['refund_id'] = $refund->id();
		$refund_values['refund_purchase_item_id'] = $this->id();
		
		// Create the refund purchase item
		$refund_purchase_item = TMm_PurchaseItem::createWithValues($refund_values);
		
		TC_message('$'.$amount.' '.$this->cartTitle());
		
		
		
		
	}
	
	public static function createWithValues($values, $bind_param_values = array(), $return_new_model = true)
	{
		// Ensure the purchase is updated immediately so that future calls have the correct information
		if($new_purchase_item = parent::createWithValues($values, $bind_param_values, $return_new_model))
		{
			
			// HANDLE IN STOCK
			$quantity = $values['quantity'];
			if(method_exists($new_purchase_item->item(), 'updateInStock'))
			{
				// Reduce the quantity by however much was just added to the purchase
				$new_purchase_item->item()->updateInStock(-1 * $new_purchase_item->quantity());
			}
			
			if($purchase = $new_purchase_item->purchase())
			{
				$purchase->addNewPurchaseItem($new_purchase_item);
			}
		}
		
		return $new_purchase_item;
		
	}
	
	/**
	 * The values that should
	 * @param array $override_values
	 * @return array|bool
	 */
	public function duplicateForCartValues($override_values = array())
	{
		if($this->cart_item_id && $cart_item = TMm_ShoppingCartItem::init($this->cart_item_id ))
		{
			$values = $cart_item->duplicationValues($override_values);
		}
		else // item created in Tungsten admin
		{
			$values = $this->duplicationValues($override_values);
		}
		
		// values we always want to pull from the purchase
		$values['price_per_item'] = $this->price_per_item;
		$values['discount_per_item'] = $this->discount_per_item;
		$values['quantity'] = $this->quantity;
		
		// Find the list of fields to remove any that shouldn't exist
		$query = "SELECT * FROM shopping_cart_items LIMIT 1";
		$result = $this->DB_Prep_Exec($query);
		$db_fields = $result->fetch();
		
		foreach($values as $index => $saved_value)
		{
			if(!isset($db_fields[$index]))
			{
				unset($values[$index]);
			}
		}

		return $values;
	}
	
	//////////////////////////////////////////////////////
	//
	// DISCOUNT PER ITEM
	//
	// Methods related to the discount price per item that this is being sold for
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the discount per item
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function discountPerItem($use_sig_digits = false)
	{
		return $this->formatCurrency($this->discount_per_item, $use_sig_digits);
	}

	/**
	 * Returns the discount per subtotal
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function discountForSubtotal($use_sig_digits = false)
	{
		return $this->formatCurrency($this->discount_per_item * $this->quantity(), $use_sig_digits);
	}


	/**
	 * Returns if the item has a discount applied
	 * @return bool
	 */
	public function hasDiscount()
	{
		return  $this->discount_per_item > 0;
	}

	//////////////////////////////////////////////////////
	//
	// VARIABLES
	//
	// Cart items can have variables saved which provides
	// an open area to track cart-specific properties
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns a variable with the provided number
	 * @param int $number
	 * @return string
	 */
	public function variable($number)
	{
		$variable_name = 'var_'.$number;
		return $this->$variable_name;
	}


	//////////////////////////////////////////////////////
	//
	// ERRORS
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns a list of errors for this cart item
	 * @return bool|string[]
	 */
	public function errors()
	{
		return array();
	}

	/**
	 * Returns if there are errors for this cart item
	 * @return bool
	 */
	public function hasErrors()
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// RECALCULATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Recalculates the price for these items based on the latest information. Only runs if the purchase is unpaid
	 */
	public function recalculateUnPaidTotals()
	{
		$purchase = $this->purchase();
		// Only bother if the purchase is unpaid and it also has items
		// The second one ensures that this code doesn't run when a purchase is created, as that would wipe out
		// necessary values on paid items.
		if($purchase->isUnpaid())
		{
			$values = [];
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$values = array_merge($values, $this->taxUpdateValues($tax, $this->pricePerItem()));
				
			}
			
			$update_required = false;
			foreach($values as $column => $new_value)
			{
				if($this->$column != $new_value)
				{
					$update_required = true;
				}
			}
			
			if($update_required)
			{
				$this->updateWithValues($values);
			}
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		
		$schema = parent::schema() + [
				
				'purchase_id' => [
					'comment'       => 'The ID of the purchase',
					'type'          => 'TMm_Purchase',
					'nullable'      => false,
					'foreign_key'   => [
						'model_name'    => 'TMm_Purchase',
						'delete'        => 'CASCADE'
					],
				],
				'cart_item_id' => [
					'comment'       => 'The ID of the shopping cart item',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
				],
				'item_class' => [
					'comment'       => 'The model class name of the item in the cart',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
				'item_id' => [
					'comment'       => 'The id of the item, which might be a string value ',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				
				],
				'quantity' => [
					'comment'       => 'The quantity for this particular item in the cart',
					'type'          => 'int(10)', // may be negative for refunds
					'nullable'      => false,
				],
				'price_per_item' => [
					'comment'       => 'The cost for one of these items in the cart',
					'type'          => 'float(11,3) ', // may be negative for refunds
					'nullable'      => false,
				],
				'discount_per_item' => [
					'comment'       => 'The amount discounted for one of these items in the cart',
					'type'          => 'float(11,3)', // may be negative for refunds
					'nullable'      => false,
				],
				'subtotal' => [
					'comment'       => 'The subtotal for the purchase item',
					'type'          => 'float(11,3) ', // may be negative for refunds
					'nullable'      => false,
				],
				
				'timestamp' => [
					'comment'       => 'A timestamp for the item',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
	
				'cart_title' => [
					'comment'       => 'The title of the cart item',
					'type'          => 'text',
					'nullable'      => false,
				],
				'cart_description' => [
					'comment'       => 'The description of the cart item',
					'type'          => 'text',
					'nullable'      => false,
				],
				
				// REFUNDS
				'refund_purchase_item_id' => [
					'comment'       => 'The ID of the purchase item ID for the refund',
					'type'          => 'TMm_PurchaseItem',
					'nullable'      => true,
//					'validations'   => [
//						'required'      => false,
//					]
				
				],
				'refund_id' => [
					'comment'       => 'The ID of the refund',
					'type'          => 'TMm_Refund',
					'nullable'      => true,
//					'validations'   => [
//						'required'      => false,
//					]
					'foreign_key'   => [
						'model_name'    => 'TMm_PurchaseRefund',
						'delete'        => 'CASCADE'
					],
				],
				
			
			
			
			];
		
		
		// Add the variable columns
		for($i = 1; $i <= 26; $i++)
		{
			$schema['var_'.$i] = [
				'title'         => 'Variable '.$i,
				'comment'       => '',
				'type'          => 'varchar(128)',
				'nullable'      => false,
			];
			
		}
		
		
		$schema['table_keys']  = [
					'refund_id' => ['index' => "refund_id"],
				];
		
		
		return $schema;
	}
}
?>