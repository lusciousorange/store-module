<?php
class TMm_PurchaseItemListExcel extends TCm_Excel
{
	protected $purchase_items;
	protected $filter_title;
	
	protected $border_top_style = array(
		'borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
	);
	
	protected $big_heading_style = array(
		'font' => array(
			'bold' => true,
			'size' => 14,
		),
	);
	
	protected $heading_style = array(
		'font' => array(
			'bold' => true,
			'size' => 12,
		),
	);
	
	protected $unpaid_style = array(
		'font'  => array(
			'color' => array('rgb' => '990000'),
		));
	
	protected $refund_style = array(
		'font'  => array(
			'color' => array('rgb' => '5A00C8'),
		));
	
	/**
	 * @param TMm_PurchaseItem[] $purchase_items
	 * @param string|bool $filter_title The title of the filtering for this list
	 */
	public function __construct($purchase_items = array(), $filter_title = false)
	{
		parent::__construct('purchase_list');
		
		$this->purchase_items = $purchase_items;
		$this->filter_title = $filter_title;
		
		$this->sheet->setTitle('Purchase Item List ');
		$this->setFilename('Purchase Item List');
		
		$this->createTitleRow();
		
		// Create Header Row
		$this->createHeaderRow();
		
		// Process Purchase Items
		$this->addPurchaseItems();
		
		
		
	}
	
	/**
	 * Adds a row
	 * @throws PHPExcel_Exception
	 */
	public function createTitleRow()
	{
		if($this->filter_title)
		{
			$range = 'A'.$this->current_row.':D'.$this->current_row;
			$this->sheet->mergeCells($range);
			
			
			
			$this->setCellValue('A'.$this->current_row, $this->filter_title);
			$this->setCellStyle($range, $this->big_heading_style);
			
		}
		
		$this->nextRow();
		$this->nextRow();
	}
	
	/**
	 * Columns in the sheet. A dollar sign in front indicates it's a currency value. A plus sign + at the end,
	 * indicates it's a summation column
	 * @return array
	 */
	public function columns() : array
	{
		$columns = [];
		
		if(TC_getModuleConfig('store','use_accounting_codes'))
		{
			$columns['accounting_code'] = 'Code';
		}
		$columns['type'] = 'Type';
		$columns['title'] = 'Title';
		$columns['product_id'] = 'Product ID';
		
		
		$columns['buyer'] = 'Buyer';
		$columns['date'] = 'Date';
		
		$columns['quantity'] = 'Quantity';
		$columns['$unit_cost'] = 'Init Cost';
		
		$columns['$subtotal+'] = 'Subtotal';
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$columns['$tax_'.$tax->id().'+'] = $tax->title();
		}
		
		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			$columns['$shipping+'] = 'Shipping';
		}
		
		$columns['$total+'] = 'Total';
		
		// PAYMENT PROCESSORS
		$columns['payment_type'] = 'Payment Type';
		$columns['transaction_id'] = 'Transaction ID';
		
		$columns['payment_type_2'] = 'Payment Type 2';
		$columns['transaction_id_2'] = 'Transaction ID 2';
		
		
		$columns['purchase_id'] = 'Purchase ID';
		$columns['purchase_item_id'] = 'Purchase Item ID';
		$columns['refund_id'] = 'Refund ID';
		
		return $columns;
		
	}
	
	/**
	 * Returns the column values for a given purchase. The order of the items in the array doesn't matter, just that
	 * the indices match
	 * @param TMm_PurchaseItem $purchase_item
	 * @return array
	 */
	public function columnValuesForPurchaseItem($purchase_item): array
	{
		$purchase = $purchase_item->purchase();
		
		
		$columns = [];
		
		$columns['type'] = $purchase_item->itemClass()::modelTitleSingular();
		
		if($purchase_item->item())
		{
			$columns['title'] = $purchase_item->item()->title();
			$columns['product_id'] = $purchase_item->item()->id();
			$columns['accounting_code'] = $purchase_item->item()->accountingCode();
		}
		else // Item not found
		{
			$class_name = $purchase_item->itemClass();
			$class_title = 'Item';
			if(class_exists($class_name))
			{
				$class_title = $class_name::$model_title;
				
			}
			$columns['title'] = $class_title.' Not Found';
			$columns['product_id'] = '';
			$columns['accounting_code'] = '';
		}
		
		// Buyer
		if($buyer = $purchase->buyer())
		{
			$columns['buyer'] = $buyer->fullName().' ('.$buyer->id().')';
		}
		else
		{
			$columns['buyer'] = 'User Not Found';
		}
		
		$columns['date'] = $purchase_item->dateAdded();
		$columns['quantity'] = $purchase_item->quantity();
		
		$columns['unit_cost'] = $purchase_item->pricePerItem();
		$columns['subtotal'] = $purchase_item->subtotal();
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$columns['tax_'.$tax->id()] = $purchase_item->totalForTax($tax);
		}
		
		
		$columns['total'] = $purchase_item->total();
		
		// PAYMENT PROCESSORS
		$columns['payment_type'] = $purchase->transactionTypeTitle();
		$columns['transaction_id'] = $purchase->transactionID();
		
		$columns['payment_type_2'] = $purchase->transactionTypeSecondTitle();
		$columns['transaction_id_2'] = $purchase->transactionIDSecondPayment();
		
		$columns['purchase_id'] = $purchase->id();
		$columns['purchase_item_id'] = $purchase_item->id();
		
		if($refund = $purchase_item->refund())
		{
			$columns['refund_id'] = $refund->id();
		}
		
		
		return $columns;
	}
	
	public function createHeaderRow()
	{
	
		$this->setCellStyle($this->current_row, $this->heading_style);
		
		$letter = 'A';
		
		foreach($this->columns() as $code => $title)
		{
			$this->setCellValue($letter++.$this->current_row, $title);
		}
		
		
		$this->nextRow();
		
		
	}
	
	/**
	 * Adds purchases based on the most recent filter values. This involves getting the filter values from
	 */
	public function addPurchaseItems()
	{
		$columns = $this->columns();
		$currency_columns = [];
		$summation_columns = [];
		foreach($this->purchase_items as $purchase_item)
		{
			
			if($purchase_item->purchase()->isUnpaid())
			{
				$this->setCellStyle($this->current_row, $this->unpaid_style);
			}
			
			if($purchase_item->isRefund())
			{
				$this->setCellStyle($this->current_row, $this->refund_style);
			}
			
			$letter = 'A';
			
			
			$column_values = $this->columnValuesForPurchaseItem($purchase_item);
			foreach($columns as $code => $column_title)
			{
				// Deal with currency columns
				if(substr($code,0,1) == '$')
				{
					$currency_columns[$letter] = $letter;
				}
				
				// Deal with summation columns
				if(substr($code,-1) == '+')
				{
					$summation_columns[$letter] = $letter;
				}
				
				// Remove the indicators which aren't needed for the column values
				$cleaned_code = str_ireplace(['$','+'],'', $code);
				
				$this->setCellValue($letter++.$this->current_row, $column_values[$cleaned_code]);
			}

			$this->nextRow();
			
		}
		
		
		
		// Currency Styling
		foreach($currency_columns as $currency_letter)
		{
			$range = $currency_letter.'2:'.$currency_letter.$this->current_row;
			$this->setCellStyle($range, PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		}
		
		// Sum Values
		$this->nextRow();
		$start_row = 4;
		
		// Set the border for the relevant columns
		$this->setCellStyle('A'.$this->current_row.':'.$this->last_column_used.$this->current_row, $this->border_top_style);
		
		// Set the sum values for the currency columns
		foreach($summation_columns as $letter)
		{
			$this->setCellValue($letter . $this->current_row,
								'=SUM(' . $letter . $start_row . ':' . $letter . '' . ($this->current_row - 1) . ')',
								PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		}
		
		
	}
	
	
	
}