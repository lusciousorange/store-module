<?php

/**
 * Class TMm_PurchaseList
 */
class TMm_PurchaseList extends TCm_ModelList
{
	/**
	 * TMm_PurchaseList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_Purchase',false);
		
	}
	
	/**
	 * Indicates if this item has ever been purchased
	 * @param TMi_ShoppingCartable|TCm_Model $cartable_item
	 * @return bool
	 */
	public function itemHasBeenPurchased($cartable_item)
	{
		$query = "SELECT * FROM purchase_items WHERE item_class=:class_name AND item_id = :item_id LIMIT 1";
		$result = $this->DB_Prep_Exec($query,
									  array('class_name' => get_class($cartable_item),
										  'item_id' => $cartable_item->id()));
		return $result->rowCount() > 0;
	}
	
	/**
	 * @param $filter_values
	 * @return TMm_Purchase[]|PDOStatement
	 */
	public function processFilterList($filter_values)
	{
		// Left Join in case a user was deleted. Unlikely but possible via privacy requests
		$query = "SELECT p.*  FROM purchases p LEFT JOIN users u ON(p.buyer_id = u.user_id)";
		$db_values = array();
		$where_clauses = array();
		$where_clauses[] = "is_replaced = 0";
		
		if(isset($filter_values['search']) && $filter_values['search'] != '')
		{
			$where_clauses[] = "(p.purchase_id = :search_id OR  u.first_name LIKE :search OR u.last_name LIKE :search OR transaction_id LIKE :transaction_search)";
			$db_values[':search'] = '%'.$filter_values['search'].'%';
			$db_values[':transaction_search'] = $filter_values['search'].'%'; // match start of string
			$db_values[':search_id'] = $filter_values['search']; // match start of string
			
		}
		
		if(isset($filter_values['type']) && $filter_values['type'] != '')
		{
			if($filter_values['type'] == 'unpaid')
			{
				$where_clauses[] = "p.num_payments = 0";
				
				
			}
			else
			{
				$where_clauses[] = "p.type = :type";
				$db_values[':type'] = $filter_values['type'];
			}
			
		}
		
		if(isset($filter_values['purchase_year']) && $filter_values['purchase_year'] != '')
		{
			$where_clauses[] = "(YEAR(p.date_added) = :year)";
			$db_values[':year'] = $filter_values['purchase_year'];
			
		}
		
		if(isset($filter_values['purchase_month']) && $filter_values['purchase_month'] != '')
		{
			$where_clauses[] = "(MONTH(p.date_added) = :month)";
			$db_values[':month'] = $filter_values['purchase_month'];
			
		}
		
		// Purchase item filter
		if(isset($filter_values['purchase_item_code']) && $filter_values['purchase_item_code'] != ''  && $filter_values['purchase_item_code'] != 'all')
		{
			// Need to add in the PI join
			$query .= ' INNER JOIN purchase_items pi ON(p.purchase_id = pi.purchase_id)';
			
			$values = explode('--',$filter_values['purchase_item_code']);
			$where_clauses[] = "pi.item_class = :purchase_item_class";
			$model_name =  $values[0]::baseClass();
			$db_values[':purchase_item_class'] =$model_name;
			if(!is_null($values[1]))
			{
				$db_values[':purchase_item_id'] = $values[1];
				$where_clauses[] =  'pi.item_id = :purchase_item_id';
			}
		}
		
		if(isset($filter_values['buyer_id']) && $filter_values['buyer_id'] != '')
		{
			$where_clauses[] = "p.buyer_id = :buyer_id";
			$db_values[':buyer_id'] = $filter_values['buyer_id'];
			
		}
		
		// ----- COMBINE INTO RESULTS -----
		
		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
			
		}
		
		return [
			'db_query' => $query,
			'db_params' => $db_values,
			'primary_table_alias' => 'p',
		];
		
	}
	
	/**
	 * @param $filter_values
	 * @param $ignore_refunded_items Indicates if we should ignore refunded items. That's true for the interface, but false for reporting
	 * @return TMm_PurchaseItem[]
	 *
	 */
	public function processFilterPurchaseItems($filter_values, $ignore_refunded_items = true)
	{
		$cartable_classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		
		//	$cartable_classes = ['TMm_ProductVariant' => 'TMm_FFT_ProductVariant'];
		// MULTIPLE QUERIES, each different for each cartable class, union them together
		$queries = [];
		
		$db_values = array();
		$where_clauses = array();
		if($ignore_refunded_items)
		{
			$where_clauses[] = 'quantity > 0'; // avoid refunded purchase items
		}
		
		if(isset($filter_values['purchase_year']) && $filter_values['purchase_year'] != '')
		{
			$where_clauses[] = "(YEAR(pi.date_added) = :year)";
			$db_values[':year'] = $filter_values['purchase_year'];
			
		}
		
		if(isset($filter_values['purchase_month']) && $filter_values['purchase_month'] != '')
		{
			$where_clauses[] = "(MONTH(pi.date_added) = :month)";
			$db_values[':month'] = $filter_values['purchase_month'];
			
		}
		
		if(isset($filter_values['buyer_id']) && $filter_values['buyer_id'] != '')
		{
			$where_clauses[] = "p.buyer_id = :buyer_id";
			$db_values[':buyer_id'] = $filter_values['buyer_id'];
			
		}
		
		// Purchase item filter
		if(isset($filter_values['purchase_item_code']) && $filter_values['purchase_item_code'] != ''  && $filter_values['purchase_item_code'] != 'all')
		{
			$values = explode('--',$filter_values['purchase_item_code']);
			$model_name =  $values[0]::classNameForInit();
			$where_clauses[] = "item_class = :purchase_item_class";
			$db_values[':purchase_item_class'] = $model_name;
			if(!is_null($values[1]))
			{
				$db_values[':purchase_item_id'] = $values[1];
				$where_clauses[] =  'pi.item_id = :purchase_item_id';
			}
			
			// Only loop through the one we care about
			// Changes the array to have this set
			$cartable_classes = [$model_name];
			
		}


//		// CARTABLE CLASS passed in, possibly a content code
//		if(isset($filter_values['item_class']) && $filter_values['item_class'] != '' && $filter_values['item_class'] != 'all')
//		{
//			$class_name = $filter_values['item_class'];
//			$model_id = null;
//			if(str_contains($class_name,'--'))
//			{
//				list($class_name, $model_id) = explode('--', $class_name);
//				$where_clauses[] = "(pi.item_id = :model_id)";
//				$db_values[':model_id'] = $model_id;
//
//			}
//
//
//			// Only loop through the one we care about
//			// Changes the array to have this set
//			$cartable_classes = [$class_name];
//
//
//		}
		
		if(isset($filter_values['search']) && $filter_values['search'] != '')
		{
			// SEARCH CAN BE MESSY BECAUSE OF ITEM TYPES
			$search_items = [];
			
			if(TC_getModuleConfig('store', 'use_accounting_codes'))
			{
				$search_items[] = "( accounting_code = :accounting_code )";
				$db_values[':accounting_code'] = $filter_values['search'];
			}
			
			// Doesn't work, not every one has a title column
//			$search_items[] = "( t.title LIKE :title_search )";
//			$db_values[':title_search'] = '%'.$filter_values['search'].'%';
			
			
			if(count($search_items) > 0)
			{
				$where_clauses[] = implode(' AND ', $search_items);
			}
		}
		
		
		// Loop through each cartable class and find the associated purchase items
		$class_num = 1;
		foreach($cartable_classes as $cartable_class)
		{
			$query = "SELECT pi.* FROM `purchase_items` pi
				INNER JOIN `".TMm_Purchase::tableName()."` p USING(purchase_id)
				INNER JOIN `".$cartable_class::tableName()."` t
				ON(pi.item_id = t.".$cartable_class::$table_id_column." AND pi.item_class=:class_name_".$class_num.")"
			;
			$db_values['class_name_'.$class_num] = $cartable_class;
			
			if(sizeof($where_clauses) > 0)
			{
				$query .= " WHERE ".implode(' AND ', $where_clauses);
				
			}
			
			$queries[$cartable_class] = $query;



//			$result = $this->DB_Prep_Exec($query,$all_db_values);
//
//			while($row = $result->fetch())
//			{
//				$purchase_item =  TMm_PurchaseItem::init($row);
//
//				$search = trim($filter_values['search']);
//				// No search, easy
//				if($search == '')
//				{
//					$items[] = $purchase_item;
//					continue;
//				}
//
//
//
//				// NOTE Handle search using PHP since it requires function calls for title, description and
//				// accountingCode all need to use classes and not try
//				if(TC_getModuleConfig('store','use_accounting_codes'))
//				{
//					$accounting_code = $purchase_item->item()->accountingCode();
//					if(stripos($accounting_code, $search) !== false)
//					{
//						$items[] = $purchase_item;
//						continue;
//					}
//				}
//
//				$title = $purchase_item->cartTitle();
//				if(stripos($title, $search) !== false)
//				{
//					$items[] = $purchase_item;
//					continue;
//				}
//
//				$description =  strip_tags($purchase_item->cartDescription());
//				if(stripos($description, $search) !== false)
//				{
//					$items[] = $purchase_item;
//					continue;
//				}
//
//
//
//			}
			
			// Go to the next class
			$class_num++;
		}
		
		return [
			'db_query' => $queries,
			'db_params' => $db_values
		];
		
		//return $items;
	}
	
	public function dailySales($where_statements = array(), $query_params = array(), $join = false)
	{
		$query = "SELECT pi.date_added, SUM(pi.subtotal) as daily_subtotal FROM purchase_items pi";
		if($join)
		{
			$query .= " ".$join." ";
		}
		if(is_string($where_statements))
		{
			$query .= " WHERE ".$where_statements;
		}
		elseif(is_array($where_statements) && count($where_statements) > 0)
		{
			$query .= " WHERE ".implode('  AND ', $where_statements);
		}
		
		$query .= " GROUP BY DAYOFYEAR(pi.date_added) ORDER BY pi.date_added ASC";
		$result = $this->DB_Prep_Exec($query, $query_params);
		
		$daily_sales = array();
		$date = false;
		
		
		while($row = $result->fetch())
		{
			$row_date = new DateTime($row['date_added']);
			// First one, save it
			if($date === false)
			{
				$date = clone $row_date;
				$date->modify("-2 DAY");
			}
			
			// Loop until we find the next day that matches
			while($date->format('Y-m-d') < $row_date->format('Y-m-d'))
			{
				$daily_sales[$date->format('Y-m-d')] = 0;
				$date->modify("+1 DAY");
			}
			
			// Same day as $row
			$daily_sales[$row_date->format('Y-m-d')] = $row['daily_subtotal'];
			$date->modify("+1 DAY");
			
			
		}
		
		
		return $daily_sales;
	}
	
	public function cumulativeSales($where_statements = array(), $query_params = array(), $join = false)
	{
		$daily_sales = $this->dailySales($where_statements, $query_params, $join);
		$cumulative_sales = array();
		$cumulative_total = 0;
		foreach($daily_sales as $date => $sales_amount)
		{
			$cumulative_total += $sales_amount;
			$cumulative_sales[$date] = $cumulative_total;
		}
		
		return $cumulative_sales;
	}
	
}

?>