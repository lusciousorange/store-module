<?php
class TMm_PurchaseListExcel extends TCm_Excel
{
	protected $purchases;
	protected $filter_title;
	
	protected $border_top_style = array(
		'borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
	);
	
	protected $big_heading_style = array(
		'font' => array(
			'bold' => true,
			'size' => 14,
		),
	);
	
	protected $heading_style = array(
		'font' => array(
			'bold' => true,
			'size' => 12,
		),
	);
	
	protected $unpaid_style = array(
		'font'  => array(
			'color' => array('rgb' => '990000'),
		));
	
	protected $refund_style = array(
		'font'  => array(
			'color' => array('rgb' => '5A00C8'),
		));
	
	/**
	 * @param TMm_Purchase[] $purchases
	 * @param string|bool $filter_title The title of the filtering for this list
	 */
	public function __construct($purchases = array(), $filter_title = false)
	{
		parent::__construct('purchase_list');
		
		$this->purchases = $purchases;
		$this->filter_title = $filter_title;
		
		$this->sheet->setTitle('Purchase List ');
		$this->setFilename('Purchase List');
		
		$this->createTitleRow();
		
		// Create Header Row
		$this->createHeaderRow();
		
		// Process Purchases
		$this->addPurchases();
		
		
		
	}
	
	/**
	 * Adds a row
	 * @throws PHPExcel_Exception
	 */
	public function createTitleRow()
	{
		if($this->filter_title)
		{
			$range = 'A'.$this->current_row.':D'.$this->current_row;
			$this->sheet->mergeCells($range);
			
			
			
			$this->setCellValue('A'.$this->current_row, $this->filter_title);
			$this->setCellStyle($range, $this->big_heading_style);
			
		}
		
		$this->nextRow();
		$this->nextRow();
	}
	
	public function columns() : array
	{
		$columns = [];
		
		$columns['id'] = 'ID';
		$columns['buyer'] = 'Buyer';
		$columns['date'] = 'Date';
		$columns['items'] = 'Items';
		$columns['$subtotal+'] = 'Subtotal';
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$columns['$tax_'.$tax->id().'+'] = $tax->title();
		}
		
		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			$columns['$shipping+'] = 'Shipping';
		}
		
		$columns['$total+'] = 'Total';
		
		// PAYMENT PROCESSORS
		$columns['payment_type'] = 'Payment Type';
		$columns['transaction_id'] = 'Transaction ID';
		
		$columns['payment_type_2'] = 'Payment Type 2';
		$columns['transaction_id_2'] = 'Transaction ID 2';
		
		return $columns;
		
	}
	
	public function createHeaderRow()
	{
	
		$this->setCellStyle($this->current_row, $this->heading_style);
		
		$letter = 'A';
		
		foreach($this->columns() as $code => $title)
		{
			$this->setCellValue($letter++.$this->current_row, $title);
			
		}
		
		$this->nextRow();
		
		
	}
	
	/**
	 * Returns the column values for a given purchase. The order of the items in the array doesn't matter, just that
	 * the indices match
	 * @param TMm_Purchase $purchase
	 * @return array
	 */
	public function columnValuesForPurchase($purchase): array
	{
		$columns = [];
		$columns['id'] =$purchase->id();
		
		// Buyer
		if($buyer = $purchase->buyer())
		{
			$columns['buyer'] = $buyer->fullName().' ('.$buyer->id().')';
		}
		else
		{
			$columns['buyer'] = 'User Not Found';
		}
		
		$columns['date'] = $purchase->dateAdded();
		$columns['items'] = $purchase->numPurchaseItems();
		$columns['subtotal'] = $purchase->subtotal();
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$columns['tax_'.$tax->id()] = $purchase->totalForTax($tax);
		}
		
		$columns['shipping'] = $purchase->deliveryPrice(); // set it either way, won't get shown
		
		$columns['total'] = $purchase->total();
		
		// PAYMENT PROCESSORS
		$columns['payment_type'] = $purchase->transactionTypeTitle();
		$columns['transaction_id'] = $purchase->transactionID();
		
		$columns['payment_type_2'] = $purchase->transactionTypeSecondTitle();
		$columns['transaction_id_2'] = $purchase->transactionIDSecondPayment();
		
		
		return $columns;
	}
	
	/**
	 * Adds purchases based on the most recent filter values. This involves getting the filter values from
	 */
	public function addPurchases()
	{
		$columns = $this->columns();
		$currency_columns = [];
		$summation_columns = [];
		foreach($this->purchases as $purchase)
		{
			$letter = 'A';
			
			if($purchase->isUnpaid())
			{
				$this->setCellStyle($this->current_row, $this->unpaid_style);
			}
			
			if($purchase->hasRefunds())
			{
				$this->setCellStyle($this->current_row, $this->refund_style);
			}
			
			$column_values = $this->columnValuesForPurchase($purchase);
			foreach($columns as $code => $column_title)
			{
				// Deal with currency columns
				if(substr($code,0,1) == '$')
				{
					$currency_columns[$letter] = $letter;
				}
				
				// Deal with summation columns
				if(substr($code,-1) == '+')
				{
					$summation_columns[$letter] = $letter;
				}
				
				// Remove the indicators which aren't needed for the column values
				$cleaned_code = str_ireplace(['$','+'],'', $code);
				
				
				$this->setCellValue($letter++.$this->current_row, $column_values[$cleaned_code]);
			}
			
			$this->nextRow();
			
		}
		
		
		
		// Currency Styling
		foreach($currency_columns as $currency_letter)
		{
			$range = $currency_letter.'2:'.$currency_letter.$this->current_row;
			$this->setCellStyle($range, PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		}
		
		// Sum Values
		$this->nextRow();
		$start_row = 4;
		
		// Set the border for the relevant columns
		$this->setCellStyle('A'.$this->current_row.':'.$this->last_column_used.$this->current_row, $this->border_top_style);
		
		// Set the sum values for the currency columns
		foreach($summation_columns as $letter)
		{
			$this->setCellValue($letter . $this->current_row,
			                    '=SUM(' . $letter . $start_row . ':' . $letter . '' . ($this->current_row - 1) . ')',
			                    PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		}
		
		
	}
	
	
	
}