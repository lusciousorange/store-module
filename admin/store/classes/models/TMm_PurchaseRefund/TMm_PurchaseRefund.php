<?php
class TMm_PurchaseRefund extends TCm_Model
{
	protected $refund_id, $purchase_id, $notes, $refund_transaction_id, $attempt_transaction, $user_id;
	protected $delivery_cost, $type, $payment_method, $transaction_id;
	
	protected $purchase_items = false;
	protected $subtotal_calc, $total_calc;
	
	public static $table_id_column = 'refund_id';
	public static $table_name = 'purchase_refunds';
	public static $model_title = 'Refund';
	public static $primary_table_sort = 'date_added DESC';

	
	public function __construct($refund_id)
	{
		parent::__construct($refund_id);
		if(!$this->exists) { return null; };
		
		$this->addDeleteTable('purchase_items', 'refund_id');
		
		
	}
	
	/**
	 * @return bool|TMm_User
	 */
	public function user()
	{
		return TMm_User::init($this->user_id);
	}
	
	//////////////////////////////////////////////////////
	//
	// NOTES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this refund has notes
	 * @return bool
	 */
	public function hasNotes()
	{
		return $this->notes != '';
	}
	
	/**
	 * @return string
	 */
	public function notes()
	{
		return $this->notes;
	}
	
	//////////////////////////////////////////////////////
	//
	// PURCHASE ITEMS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * @return bool|TMm_Purchase
	 */
	public function purchase()
	{
		return TMm_Purchase::init($this->purchase_id);
	}
	
	
	/**
	 * Obtains all the items in the refund
	 * @return TMm_PurchaseItem[]
	 */
	public function purchaseItems()
	{
		if($this->purchase_items === false)
		{
			$this->purchase_items = array();
			$query = "SELECT * FROM purchase_items WHERE refund_id = :refund_id ORDER BY purchase_item_id ASC";
			$result = $this->DB_Prep_Exec($query, array('refund_id' => $this->id()) );
			while($row = $result->fetch())
			{
				$purchase_item = TMm_PurchaseItem::init($row);
				$this->purchase_items[$row['purchase_item_id']] = $purchase_item;
				
			}
		}
		return $this->purchase_items;
	}
	
	/**
	 * @return string|float
	 */
	public function deliveryPrice()
	{
		return $this->formatCurrency($this->delivery_cost);
	}
	
	//////////////////////////////////////////////////////
	//
	// CALCULATIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the total for the shopping cart
	 * @return float
	 */
	public function total()
	{
		if($this->total_calc == null)
		{
			$this->total_calc = $this->subtotal();
			
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$this->total_calc += $this->totalForTax($tax);
			}
			
			$this->total_calc += $this->deliveryPrice();
		}
		return $this->formatCurrency(abs($this->total_calc));
		
	}
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @return float
	 */
	public function subtotal()
	{
		if($this->subtotal_calc == null)
		{
			$this->subtotal_calc = 0;
			foreach($this->purchaseItems() as $purchase_item)
			{
				$this->subtotal_calc += $purchase_item->subtotal();
			}
			
			
		}
		return $this->formatCurrency(abs($this->subtotal_calc));
	}
	
	/**
	 * Returns the dollar value for the tax provided. Note the values are always positive within the refund class.
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function totalForTax($tax)
	{
		$tax_total = 0;
		foreach($this->purchaseItems() as $purchase_item)
		{
			$tax_total += $purchase_item->totalForTax($tax);
		}
		
		if($this->deliveryPrice() > 0)
		{
			if($tax->appliesToShipping())
			{
				$tax_total += $this->deliveryPrice() *$tax->percent() / 100;
			}
		}
		return $this->formatCurrency(abs($tax_total));
	}
	
	//////////////////////////////////////////////////////
	//
	// PAYMENT TRANSACTION
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Attempts to perform the refund transaction. This action will be attempted once. This only has an effect for
	 * `online` transactions since the other transactions must be handled manually.
	 * @return array An array of two values with a success boolean and a message
	 */
	public function performRefundTransaction()
	{
		// Perform the transaction with the payment processor
		if($this->attempt_transaction)
		{
			if($this->purchase()->paymentRefundPossible(true))
			{
				$payment_processor = $this->purchase()->refundProcessor();
				$payment_processor->processRefund($this);
				
				return array(
					'success' => $payment_processor->statusIsSuccess(),
					'message' => $payment_processor->message());
				
				
				
			}
			else
			{
				return array(
					'success' => false,
					'message' => 'Refund could not be processed');
				
			}
			
			
		}
		
		// Scenario where we have a refund and it's been created, but no transaction happened.
		return array(
			'success' => true,
			'message' => 'Refund created, financial transaction ('.$this->purchase()->transactionTypeTitle().') must be handled separately');
		
		
	}
	
	/**
	 * Updates the refund transaction ID
	 * @param string $transaction_id
	 */
	public function setRefundTransactionID($transaction_id)
	{
		$this->updateDatabaseValue('refund_transaction_id', $transaction_id);
	}
	
	public function paymentType()
	{
		return $this->type;
	}
	
	public function paymentCardType()
	{
		return $this->payment_method;
	}
	
	public function transactionID()
	{
		if($this->paymentType() == 'online')
		{
			return $this->refund_transaction_id;
		}
		return $this->transaction_id;
	}
	
	
	public function transactionTypeTitle()
	{
		if($this->paymentType() != '')
		{
			return TMm_Purchase::$transaction_types[$this->paymentType()];
		}
		
		return '';
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	public function userCanDelete($user = false)
	{
		// permit admins since they run it in the admin with refunds
		return TC_currentUser() && TC_currentUser()->isAdmin();
	}
	
	public function userCanEdit($user = false)
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'purchase_id' => [
					'comment'       => 'The ID of the purchase',
					'type'          => 'TMm_Purchase',
					'nullable'      => false,
					
					'foreign_key'   => [
						'model_name'    => 'TMm_Purchase',
						'delete'        => 'CASCADE'
					],
				],
				
				'user_id' => [
					'comment'       => 'The id of the user who created the refund',
					'type'          => 'int(10) unsigned',
					'nullable'      => true,
					
					'foreign_key'   => [
						'model_name'    => 'TMm_User',
						'delete'        => 'SET NULL'
					],
				
				],
				'attempt_transaction' => [
					'comment'       => 'Indicates if the transaction was attempted',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
				'transaction_id' => [
					'comment'       => 'The original transaction ID from the payment processor',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'refund_transaction_id' => [
					'comment'       => 'The transaction ID for the refund action, from the payment processor',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'type' => [
					'comment'       => 'The type of payment for the first payment',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'delivery_cost' => [
					'comment'       => 'The delivery cost ',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => true,
				],
				'payment_method' => [
					'comment'       => 'The method of payment for the first payment',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				'notes' => [
					'comment'       => 'The notes for the refund ',
					'type'          => 'text',
					'nullable'      => false,
				],
				
			];
	}


}