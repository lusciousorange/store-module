<?php

class TMm_ShoppingCart extends TCm_Model
{
	protected $user_id, $is_locked, $is_testing, $is_complete, $session_id, $use_same_address, $is_pickup, $purchase_id;
	protected $billing_street, $billing_street_2, $billing_city, $billing_province, $billing_country, $billing_postal,
		$shipping_street, $shipping_street_2, $shipping_city, $shipping_province, $shipping_country, $shipping_postal;
	protected $delivery_method, $delivery_cost, $instructions;
	protected $first_name, $last_name, $email, $phone;
	protected $payment_method;
	protected $has_db_entry = false;
	
	// Purchase Editing
	protected $revision_number, $original_purchase_id;
	
	protected $cart_item_errors_processed = false;
	protected $cart_id = false;
	protected $user = false;
	protected $total = false;
	
	protected $errors = array();
	protected $warnings = array();
	
	// Cart Items
	protected $items = false;
	protected $items_by_code = false;
	protected $cart_item_by_id = false;
	
	protected $tax_totals = array();
	
	// Cart Services
	protected $cart_services = array();
	
	// Cart Customization
	protected $cart_query_where_statements = array();
	
	public static $table_id_column = 'cart_id';
	public static $table_name = 'shopping_carts';
	public static $model_title = 'Shopping Cart';
	public static $primary_table_sort = 'date_added DESC';
	
	protected $checkout_stage  = false;
	
	// By default, carts don't use checkout stages
	protected $checkout_stages = false;
//	protected $checkout_stages = array(
//		'account' => array('title' => 'Account', 'view_url' => '/checkout/account/'),
//		'address' => array('title' => 'Address', 'view_url' => '/checkout/address/'),
//		'delivery' => array('title' => 'Delivery', 'view_url' => '/checkout/delivery/'),
//		'payment' => array('title' => 'Payment', 'view_url' => '/checkout/payment/'),
//		'confirm' => array('title' => 'Confirm', 'view_url' => '/checkout/confirm/'),
//	);
	
	/**
	 * TMm_ShoppingCart constructor.
	 * @param bool|int $cart_id
	 * @param bool|int $override_user_id
	 */
	public function __construct($cart_id = false, $override_user_id = false)
	{
		$user_id = 0;
		if($cart_id === false)
		{
			
			if($override_user_id !== false)
			{
				$user_id = $override_user_id;
			}
			elseif($this->user = TC_currentUser())
			{
				$user_id = $this->user->id();
			}
			
			// Otherwise the $user_id is 0
		}
		
		
		// Figure out cart row
		$cart_row = $this->getRealCartRow($cart_id, $user_id);
		parent::__construct($cart_row);
		//if(!$this->exists) { return false; };
		
		// Only bother if we actually have a DB entry
		if($this->has_db_entry)
		{
			if(!$this->user && $this->user_id > 0)
			{
				$this->user = TMm_User::init($this->user_id);
				
			}
			
			
			if($this->user)
			{
				$this->addSessionCartsToUserCart();
			}
			
			
			// GET ALL THE SHOPPING CART ITEMS
			$this->cartItems();
			
			if($this->checkoutStage() == '' && $this->checkout_stages != '')
			{
				reset($this->checkout_stages);
				$first_stage = key($this->checkout_stages);
				$this->updateDatabaseValue('checkout_stage', $first_stage);
			}
			
			// Check for a cart that has a user but is still set to "account"
			// They can edit that, but let them jump forward
			// Fixes issue with a login happening, then showing wrong ong
			if($this->checkoutStage() == 'account' && $this->user())
			{
				$this->setCheckoutStageToNextAfter('account');
			}
			
			if($this->user() && !$this->isComplete())
			{
				$this->validateUserCartSettings();
				
				// Update blank addresses for with user values
				$this->updateBlankAddressesWithUserInfo();
				
			}
			
			$this->addConsoleMessage('Cart ID : '. $this->id());
		}
		
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// USERS
	//
	// Methods related to users for the cart
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the user for this cart.
	 * @return bool|TMm_User|TMt_Store_User
	 */
	public function user()
	{
		return $this->user;
	}
	
	/**
	 * Sets the user for this cart
	 * @param TMm_User $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}
	
	/**
	 * Validates the user against the cart values. Updates the cart to match the user
	 */
	protected function validateUserCartSettings()
	{
		$user = $this->user();
		if($user)
		{
			$values = array();
			if($this->firstName() != $user->firstName())
			{
				$values['first_name'] = $user->firstName();
			}
			
			if($this->lastName() != $user->lastName())
			{
				$values['last_name'] = $user->lastName();
			}
			
			if($this->phone() != $user->cellPhone())
			{
				$values['phone'] = $user->cellPhone();
			}
			
			if($this->email() != $user->email())
			{
				$values['email'] = $user->email();
			}
			
			if(sizeof($values) > 0)
			{
				$this->updateWithValues($values);
			}
			
		}
		
		
	}
	
	/**
	 * Overrides the ability to edit a shopping cart to limit it to the actual user associated with it.
	 * This only impacts trying to load the cart in a form
	 *
	 * @param TMm_User|TMt_Store_User|bool $user (Optional) Default false which means the current user will be tested
	 * against.
	 * @return bool
	 * @uses TC_currentUser()
	 * @uses TMm_User::hasTungstenAccess()
	 */
	public function userCanEdit($user = false)
	{
		if(!$user)
		{
			$user = TC_currentUser();
		}
		
		if($this->is_complete)
		{
			return false;
		}
		
		// Admin viewing in backend
		if(TC_isTungstenView() && $user && $user->userCanEditAllCarts())
		{
			return true;
		}
		
		// Cart has a user and we have a user to compare to
		if($this->user() && $user)
		{
			return $this->user()->id() == $user->id();
		}
		else
		{
			// We're worried about session IDs
			return $this->sessionID() == session_id();
		}
	}
	
	public function userCanView($user = false)
	{
		// Same as can edit
		return $this->userCanEdit($user);
	}
	
	/**
	 * Overrides the ability to delete a shopping cart to limit it to the actual user associated with it.
	 * This only impacts trying to load the cart in a form
	 *
	 * @param TMm_User|bool $user (Optional) Default false which means the current user will be tested against.
	 * @return bool
	 * @uses TC_currentUser()
	 * @uses TMm_User::hasTungstenAccess()
	 */
	public function userCanDelete($user = false)
	{
		if($this->is_complete)
		{
			return false;
		}
		return $this->userCanEdit();
		
		
	}
	
	public function title()
	{
		return 'Cart #'.$this->id();
	}
	
	/**
	 * Returns false since we don't want cart titles appear in the title head
	 * @return bool
	 */
	public function isUsableAsPageTitle()
	{
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// LOCKING / COMPLETION
	//
	// Functions related to locking the cart or
	// complete carts that arent' editable
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Verifies if the shopping cart is still editable. This function should be called before any code that would alter quantities in a shopping cart
	 *
	 * @return bool
	 */
	public function verifyEditable()
	{
		return !$this->is_complete; // returns opposite of is_complete.
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// CART SETUP
	//
	// Functions related to the initialization of the cart
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Internal function used to determine the values for the cart given a range of parameters that are possible. This
	 * function will account for a user ID that is provided in the constructor. The priority will go to an actual cart
	 * ID provided. If no ID can be found, it will try and use the user ID provided. If no cart can be found for the user
	 * or for any other id, a new cart item will be generated in the database and used. This method must be called as the
	 * first action in the constructor in order to have any effect.
	 *
	 * @param bool|int $cart_id
	 * @param bool|int $user_id
	 * @return array|bool|int
	 */
	protected function getRealCartRow($cart_id = false, $user_id  = false)
	{
		// Any successful case will set "init_array" which is a property of TCm_Model. It will allow for proper construction of this cart without excesive queries.
		
		// CART ID IS AN ARRAY AUTO RETURN
		if(is_array($cart_id))
		{
			// values provided, manually generating, probably an admin cart
			$this->has_db_entry = true;
			return $cart_id;
		}
		
		// OVERRIDE CART ID PROVIDED - if it's found then it should be used and skip all other options
		if($cart_id > 0)
		{
			$cart_query = "SELECT * FROM shopping_carts WHERE cart_id = :cart_id ";
			
			if(count($this->cart_query_where_statements) > 0)
			{
				$cart_query .= ' AND '.implode(' AND ',$this->cart_query_where_statements);
			}
			$cart_query .= " LIMIT 1";
			$cart_result = $this->DB_Prep_Exec($cart_query, array('cart_id' => $cart_id));
			if($row = $cart_result->fetch()) // cart was found
			{
				$this->has_db_entry = true;
				return $row;
			}
		}
		
		// USER IS SET - The User ID provided is greater than 0 so try to find a cart with that ID
		// a user exists but a cart hasn't been found yet
		// This also requires "show authentication" to be turned on in the pages module
		if($user_row = $this->getUserCartRow($user_id))
		{
			$this->has_db_entry = true;
			return $user_row;
		}
		
		// NO CART, NO USER, TRY SESSION - At this point there is no user and no override cart id so there is the possibility of a cart with the existing session id
		if($session_row = $this->getSessionCartRow($user_id))
		{
			$this->has_db_entry = true;
			return $session_row;
		}
		
		// ----- NEW CART -----
		// The last step is that there is no cart found for this session ID or the user. IE: make a new cart
		if($this->checkout_stages === false)
		{
			$first_stage = '';
		}
		else
		{
			reset($this->checkout_stages);
			$first_stage = key($this->checkout_stages);
			
		}
		
		// use a non-DB empty cart so it returns an array of values without creating anything in the actual DB
		$empty_cart_values = array(
			'user_id' => $user_id,
			'session_id' => session_id(),
			'checkout_stage' => $first_stage,
			'cart_id' => -1
		
		);
		
		return $empty_cart_values;
		
	}
	
	/**
	 * Returns the user row if it exists for this site. This requires the `Pages` settings for uses authentication to
	 * be enabled.
	 * @param int $user_id
	 * @return bool|array
	 */
	protected function getUserCartRow($user_id)
	{
		if($this->userAccountsEnabled() && $user_id)
		{
			$cart_query = "SELECT * FROM shopping_carts WHERE user_id = :user_id AND is_complete = 0 ";
			if(count($this->cart_query_where_statements) > 0)
			{
				$cart_query .= ' AND '.implode(' AND ',$this->cart_query_where_statements);
			}
			$cart_query .= " ORDER BY date_added DESC LIMIT 1";
			$cart_result = $this->DB_Prep_Exec($cart_query, array('user_id' => $user_id));
			if($row = $cart_result->fetch()) // cart was found
			{
				return $row;
			}
		}
		return false;
	}
	
	/**
	 * Returns the session cart row if it exists for this site. It will match the session_id. If a user is logged in,
	 * then it will convert that cart to belong to that user.
	 * @param int $user_id
	 * @return bool|array
	 */
	protected function getSessionCartRow($user_id)
	{
		$cart_query = "SELECT * FROM shopping_carts WHERE session_id = :session_id AND user_id = 0 AND is_complete = 0 ";
		if(count($this->cart_query_where_statements) > 0)
		{
			$cart_query .= ' AND '.implode(' AND ',$this->cart_query_where_statements);
		}
		$cart_query .= " ORDER BY date_added DESC LIMIT 1";
		$cart_result = $this->DB_Prep_Exec($cart_query, array('session_id' => session_id()));
		if($row = $cart_result->fetch()) // cart was found
		{
			// A cart is found using the session id with no user associated with it. If a user is logged in then this cart should be assigned to that user
			if($this->userAccountsEnabled() && $user_id > 0)
			{
				$user_query = "UPDATE shopping_carts SET user_id = :user_id, session_id = '' WHERE cart_id = :cart_id LIMIT 1";
				$this->DB_Prep_Exec($user_query, array('user_id' => $user_id, 'cart_id' => $row['cart_id']));
				$row['user_id'] = $user_id;
				$row['session_id'] = '';
			}
			return $row;
		}
		
		return false;
	}
	
	/**
	 * Performs the action of actually creating the cart in the database. This is only necessary when they need to
	 * interact with it in a real way.
	 */
	public function verifyCartInDatabase()
	{
		if(!$this->has_db_entry)
		{
			$values = array(
				'user_id' => $this->user_id, // set via the init no matter what
				'session_id' => $this->session_id,
				'checkout_stage' => $this->checkout_stage,
				'shipping_city' => TC_getModuleConfig('store', 'default_city'),
				'shipping_province' => TC_getModuleConfig('store', 'default_province'),
				'shipping_country' => TC_getModuleConfig('store', 'default_country'),
				'billing_city' => TC_getModuleConfig('store', 'default_city'),
				'billing_province' => TC_getModuleConfig('store', 'default_province'),
				'billing_country' => TC_getModuleConfig('store', 'default_country'),
			);
			
			$query = "INSERT INTO shopping_carts SET
                               user_id = :user_id,
                               session_id = :session_id,
                               checkout_stage = :checkout_stage,
                               shipping_city = :shipping_city,
                               shipping_province = :shipping_province,
                               shipping_country = :shipping_country,
                               billing_city = :billing_city,
                               billing_province = :billing_province,
                               billing_country = :billing_country,
                               date_added = now()";
			$this->DB_Prep_Exec($query, $values );
			
			$this->cart_id = $this->DB()->insertedID();
			$this->id = $this->cart_id;
			$this->has_db_entry = true;
		}
	}
	
	
	/**
	 * Finds any carts that are tied to the same session but not tied to the user. If they exist, they will be added to
	 * this cart. This is only valid if there is currently a user logged in. Otherwise, the session cart IS this cart.
	 */
	public function addSessionCartsToUserCart()
	{
		if($this->user())
		{
			$values = array();
			$values['session_id'] = session_id();
			$values['cart_id'] = $this->id();
			
			$query = "SELECT * FROM shopping_carts WHERE session_id = :session_id AND user_id = 0 AND is_complete = 0 AND cart_id != :cart_id LIMIT 1";
			$result = $this->DB_Prep_Exec($query, $values);
			while($row = $result->fetch())
			{
				$cart = TMm_ShoppingCart::init($row);
				$this->addCartToThisCart($cart);
				
			}
		}
	}
	
	
	/**
	 * Function to merge two carts together by taking one cart and adding it to this cart. Once the merger is complete, the old cart will be deleted.
	 * @param TMm_ShoppingCart $cart
	 */
	public function addCartToThisCart($cart)
	{
		foreach($cart->cartItems() as $cart_item)
		{
			if($this_cart_item = $this->cartItemWithContentCode($cart_item->itemContentCode()))
			{
				$this_cart_item->addQuantity($cart_item->quantity());
			}
			else
			{
				$cart_item->moveToCart($this);
			}
		}
		
		// delete the old cart
		$cart->delete(false); // silent
		
		// recalculate all of the cart items
		$this->reaquireCartItems();
	}
	
	/**
	 * Returns if user accounts are enabled on this site. This is done via the Pages Settings for
	 * `show_authentication`.
	 * @return bool
	 */
	public function userAccountsEnabled()
	{
		return TC_getModuleConfig('pages','show_authentication');
		
	}
	
	//////////////////////////////////////////////////////
	//
	// SESSIONS
	//
	// Functions related to sessions
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the session ID
	 * @return string
	 */
	public function sessionID()
	{
		return $this->session_id;
	}
	
	/**
	 * Updates the session id for this cart
	 * @param string $session_id
	 */
	public function updateSessionID($session_id)
	{
		$this->updateDatabaseValue('session_id', $session_id);
	}
	
	
	//////////////////////////////////////////////////////
	//
	// CART ITEMS
	//
	// Functions related to the cart items
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Verifies i the provided item is indeed cartable
	 * @param TMi_ShoppingCartable $cartable
	 */
	public function verifyCartableItem($cartable)
	{
		$interfaces = class_implements($cartable);
		
		if(!isset($interfaces['TMi_ShoppingCartable']))
		{
			if($cartable instanceof TCm_Model)
			{
				TC_triggerError('Object "<em>'.get_class($cartable).'</em>"provided does not implement TMi_ShoppingCartable');
			}
			else
			{
				TC_triggerError('TMi_ShoppingCartable item was of type "<em>'
								.gettype($cartable).'</em>", must be an TCm_Model');
			}
		}
		
	}
	
	/**
	 * Re-acquires all the cart items from the database.
	 */
	public function reaquireCartItems()
	{
		$this->addConsoleMessage('Re-Acquiring Cart Items from Database');
		TMm_ShoppingCartItem::clearAllModelsFromMemory();
		$this->items = false;
		$this->items_by_code = false;
		$this->cartItems();
		
	}
	
	/**
	 * Obtains all the items in the Cart
	 * @return TMm_ShoppingCartItem[]
	 */
	public function cartItems()
	{
		if($this->items === false)
		{
			$this->items = array();
			$this->items_by_code = array();
			
			// Only bother if we have an entry, otherwise we know it's not an issue
			if($this->has_db_entry)
			{
				$cart_class_name = TC_actualClassNameUsed('TMm_ShoppingCartItem');
				
				$query = "SELECT * FROM shopping_cart_items WHERE cart_id = :cart_id ORDER BY "
					. $cart_class_name::$primary_table_sort;
				
				$result = $this->DB_Prep_Exec($query, array('cart_id' => $this->id()));
				while($row = $result->fetch())
				{
					// send in just the cart id so that
					$cart_item = TMm_ShoppingCartItem::init($row);
					if($cart_item->item())
					{
						$this->items[$row['cart_item_id']] = $cart_item;
						$this->items_by_code[$cart_item->itemContentCode()] = $cart_item;
						
					}
					else
					{
						// Called during init, cannot do normal delete call
						$query = "DELETE FROM shopping_cart_items WHERE cart_item_id = :id LIMIT 1";
						$this->DB_Prep_Exec($query, array('id' => $cart_item->id()));
					}
				}
			}
		}
		return $this->items;
	}
	
	/**
	 * Returns if the cart is empty
	 * @return bool
	 */
	public function isEmpty()
	{
		return sizeof($this->cartItems()) == 0;
	}
	
	
	/**
	 * Returns a cart item that matches the cartable item provided
	 * @param TMi_ShoppingCartable $cartable
	 * @return bool|TMm_ShoppingCartItem
	 */
	public function cartItemWithCartable($cartable)
	{
		$this->verifyCartableItem($cartable);
		foreach($this->cartItems() as $cart_item)
		{
			if($cartable->contentCode() == $cart_item->itemContentCode())
			{
				return $cart_item;
			}
		}
		return false;
	}
	
	
	/**
	 * Returns all the cart items that matches the cartable item provided. It's possible to have more than one.
	 * @param TMi_ShoppingCartable $cartable
	 * @return TMm_ShoppingCartItem[]
	 */
	public function cartItemsWithCartable($cartable)
	{
		$items = array();
		$this->verifyCartableItem($cartable);
		foreach($this->cartItems() as $cart_item)
		{
			if($cartable->contentCode() == $cart_item->itemContentCode())
			{
				$items[] = $cart_item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Returns the cart item with the provided code
	 * @param string $content_code
	 * @return bool|TMm_ShoppingCartItem
	 */
	public function cartItemWithContentCode($content_code)
	{
		$this->cartItems();
		
		if(isset($this->items_by_code[$content_code]))
		{
			return $this->items_by_code[$content_code];
		}
		return false;
	}
	
	
	/**
	 * Deletes all items from the shopping_cart_items
	 */
	public function emptyCart()
	{
		$this->verifyCartInDatabase();
		if($this->verifyEditable())
		{
			$query = "DELETE FROM shopping_cart_items WHERE cart_id = :cart_id";
			$this->DB_Prep_Exec($query, array('cart_id' =>$this->id()));
			
			$this->items = array();
			$this->items_by_code = array();
		}
	}
	
	/**
	 * Adds a cartable item to the cart with a certain quantity
	 * @param TMi_ShoppingCartable $cartable
	 * @param int $quantity
	 * @param array $variables (Optional) Default false. An array of values with integer indices correlating the
	 * the variables that should be associated with this cart item. Note this method overrides any values set in
	 * `addToCartVariableValues()`
	 * @param bool $force_create_new Indicates if this should create a new cart item even if existing ones are in
	 * place with the same class and ID. This can happen if two items need to exist with different costs or variables.
	 * @return bool|TMm_ShoppingCartItem
	 * @see TMi_ShoppingCartable::addToCartVariableValues()
	 */
	public function addToCart($cartable, $quantity, $variables = array(), $force_create_new = false)
	{
		$this->verifyCartInDatabase();
		$this->verifyCartableItem($cartable);
		
		if($this->verifyEditable())
		{
			// Determine cart variables
			// used to determine if a cart item already exists in the system
			$values = array(
				'item_id' => $cartable->id(),
				'item_class' => $cartable->baseClassName(),
				'quantity' => $quantity,
				'price_per_item' => $cartable->price() ,
				'discount_per_item' => $cartable->discountPerItem() ,
				'cart_id' => $this->id()
			);
			
			// Grab values from the cart values
			foreach($cartable->addToCartVariableValues() as $index => $variable_value)
			{
				$values['var_'.$index] = $variable_value;
			}
			
			// grab any override cart values
			foreach($variables as $index => $variable_value)
			{
				$values['var_'.$index] = $variable_value;
			}
			
			// Check for existing items if they are around
			if(!$force_create_new)
			{
				foreach($this->cartItemsWithCartable($cartable) as $cart_item)
				{
					// Need to find a match with not only the content code but also all 26 variables
					$existing_found = true;
					
					
					// Ensure values are also unique
					//$difference_found = false;
					for($i = 1; $i <= 26; $i++)
					{
						
						if(!isset($values['var_' . $i]))
						{
							$values['var_' . $i] = '';
						}
						
						if($cart_item->variable($i) != $values['var_' . $i])
						{
							$existing_found = false;
						}
						
					}
					
					// We're adding a truly unique item, create it and return
					if($existing_found)
					{
						$cart_item->update($cart_item->quantity() + $quantity);
						$this->validateAndCleanCart(); // clean directly after adding, to address any issues
						$cartable->additionalCartActions($this); // make sure to run the actions too
						return $cart_item;
					}
					
				}
				
			}
			
			// At this point, we're unable to find an exact match OR we set `force_create_new`
			
			// create new
			if($cartable->inStock() === false || $cartable->inStock() > 0)
			{
				// Taxes
				$tax_list = TMm_TaxList::init();
				foreach($tax_list->models() as $tax)
				{
					if($cartable->usesTax($tax))
					{
						$values['tax_'.$tax->id().'_percent'] = $this->percentageForTax($tax);
					}
				}
				
				$cart_item = TMm_ShoppingCartItem::createWithValues($values);
				if($cart_item)
				{
					$this->items[$cart_item->id()] = $cart_item;
					$this->items_by_code[$cart_item->itemContentCode()] = $cart_item;
					
					$this->validateAndCleanCart(); // clean directly after adding, to address any issues
					$cartable->additionalCartActions($this);
					return $cart_item;
				}
				else
					{
						TC_message('Item could not be added to cart. Please try again.',false);
						return null;
					}
					
				}
			else
			{
				$this->addConsoleWarning($cartable->title().' is out of stock but attempt was made to add to cart');
			}
			
			
			
		}
		
		
		
		// failsafe return
		return false;
	}
	
	/**
	 * Adds an item to the cart using a content code
	 * @param string $content_code
	 * @param int $quantity (Optional) Default 1
	 * @param array $variables Additional Parameters, which could be any number of them
	 * @return bool|TMm_ShoppingCartItem
	 */
	public function addToCartWithContentCode($content_code, $quantity = 1, $variables = array() )
	{
		$parts = explode('--', $content_code);
		$class_name = $parts[0];
		
		if(!isset($parts[1]) || $parts[1] == '')
		{
			$id = 0;
		}
		else
		{
			$id = $parts[1];
		}
		
		if(class_exists($class_name))
		{
			/** @var TMi_ShoppingCartable $cartable */
			$cartable = ($class_name)::init($id);
			return $this->addToCart($cartable, $quantity, $variables);
		}
		else
		{
			TC_message('Item could not be added to cart. Please try again.',false);
			return null;
		}
	}
	
	/**
	 * Adds an item to the purchase using a barcode
	 * @param string $barcode
	 * @param array $variables Additional Parameters, which could be any number of them
	 * @see TMm_ShoppingCartList::findCartableItemWithBarcode()
	 */
	public function addItemWithBarcode($barcode, $variables = array())
	{
		$list = TMm_ShoppingCartList::init();
		$item = $list->findCartableItemWithBarcode($barcode);
		if($item)
		{
			$this->addToCartWithContentCode($item->contentCode(), $variables);
		}
		else
		{
			TC_message('Item with barcode not found', false);
		}
	}
	
	/**
	 * Updates the cart with the values provided
	 * @param TMi_ShoppingCartable $cartable
	 * @param int $quantity
	 * @return bool|TMm_ShoppingCartItem
	 */
	public function updateCart($cartable, $quantity)
	{
		$this->verifyCartableItem($cartable);
		if($this->verifyEditable())
		{
			if($cart_item = $this->cartItemWithContentCode($cartable->contentCode()))
			{
				$cart_item->update($quantity);
			}
			else
			{
				$cart_item = $this->addToCart($cartable, $quantity);
			}
			$this->validateAndCleanCart();
			return $cart_item;
		}
		
		// failsafe
		return false;
	}
	
	/**
	 * Returns the number of different products in this cart. If there are 3 of a product, it will only return 1. For a count of all products, see numCartItems.
	 * @return int
	 */
	public function numCartProducts()
	{
		return sizeof($this->cartItems());
	}
	
	/**
	 * Returns the number of different products in this cart. If there are 3 of a product, it will only return 1. For a count of all products, see numCartItems.
	 * @return int
	 */
	public function numCartItems()
	{
		$count = 0;
		foreach($this->cartItems() as $cart_item)
		{
			$count += $cart_item->quantity();
		}
		
		return $count;
	}
	
	/**
	 * Function to get the number of items in the cart regardless of the quantity of each
	 * @return int
	 */
	public function numItems()
	{
		return $this->numCartProducts();
	}
	
	/**
	 * Goes through the cart and clears away any invalid items in the cart
	 */
	public function validateAndCleanCart()
	{
		$this->total = false; // allow it to recalculate
		$this->tax_totals = array();
		$this->processCartItemErrors();
		
		foreach($this->cartItems() as $id =>  $cart_item)
		{
			$product = $cart_item->item();
			
			// CHECK STOCK QUANTITIES
			if($product->inStock() >= 0 && $product->inStock() != false) // A value of false indicates don't track stock
			{
				if ($cart_item->quantity() > $product->inStock())
				{
					if ($product->inStock() == 0)
					{
						TC_message('<strong>' . $product->title() . '</strong> is no longer available.');
						$this->removeFromCart($cart_item);
					}
					else
					{
						if($product->showStockAdjustmentWarnings())
						{
							TC_message('<strong>' . $product->title() . '</strong> is low in stock.<br />We can only provide you with ' . $product->inStock() . ' at this time.');
						}
						$cart_item->update($product->inStock());
					}
				}
				
				if($product->maxAddable() !== false)
				{
					if($cart_item->quantity() > $product->maxAddable())
					{
						TC_message('<strong>' . $product->title() . '</strong> has a maximum of ' .
								   $product->maxAddable() . ' in your cart.');
						$cart_item->update($product->maxAddable());
						
					}
				}
			}
			
			// Correct pricing on things like discounts, but never try if a promotion is applied
			if(!$cart_item->promotion())
			{
				// Correct Price Per Item
				$real_price = $product->price() - $product->discountPerItem();
				if($cart_item->pricePerItem() != $real_price)
				{
					$cart_item->updatePricePerItem($real_price);
				}
				
				// Correct Discount Value
				$real_discount = $product->discountPerItem();
				if($cart_item->discountPerItem() != $real_discount)
				{
					$cart_item->updateDiscountPerItem($real_discount);
				}
			}
			
		}
		
	}
	
	/**
	 * Removes an item from the cart. This can be a cart item or a cartable item that is to be removed
	 * @param TMi_ShoppingCartable|TMm_ShoppingCartItem $item
	 * @param string|bool $action_verb (Optional) Default 'deleted'. The verb used for removing. False for no message.
	 * @param bool $remove_shipping_settings (Optional) Default true.
	 */
	public function removeFromCart($item, $action_verb = 'deleted', $remove_shipping_settings = true)
	{
		if($this->verifyEditable())
		{
			
			if($item instanceof TMm_ShoppingCartItem)
			{
				$cart_item = $item;
				$cart_item->delete($action_verb);
				if($remove_shipping_settings)
				{
					$this->removeShippingSettings();
				}
				
			}
			else
			{
				$this->verifyCartableItem($item);
				$cart_item = $this->cartItemWithCartable($item);
				if($cart_item)
				{
					$cart_item->delete($action_verb);
					if($remove_shipping_settings)
					{
						$this->removeShippingSettings();
					}
					
				}
				
			}
		}
	}
	
	/**
	 * Clears the cart item from history
	 * @param TMm_ShoppingCartItem $cart_item
	 */
	public function clearCartItemFromHistory($cart_item)
	{
		unset($this->items_by_code[$cart_item->item()->contentCode()]);
		unset($this->items[$cart_item->id()]);
		
	}
	
	/**
	 * Function that redirects to the cart page, which is detected based on the store setting for what page is the
	 * actual cart page. If it's not found, then it goes to the referrer.
	 */
	public function redirectToCartPage()
	{
		header("Location:".$this->cartPageURL());
		exit();
		
	}
	
	/**
	 * Returns the URL to view the cart
	 * @return string
	 */
	public function cartPageURL()
	{
		$menu_item = TMm_PagesMenuItem::init(TC_getModuleConfig('store','cart_menu_id'));
		if($menu_item)
		{
			$path = $menu_item->pathToFolder();
		}
		else
		{
			$path = $_SERVER['HTTP_REFERER'];
		}
		
		return $path;
	}
	
	//////////////////////////////////////////////////////
	//
	// CHECKOUT STAGES
	//
	// Checkout stages can be adjusted by the developer,
	// so these methods are used to grab the values regardleess.
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns if this cart has a checkout stage with a given name
	 * @param string $name The name of the folder for the checkout stage
	 * @return bool
	 */
	public function hasCheckoutStageWithName($name)
	{
		return isset($this->checkout_stages[$name]);
	}
	
	/**
	 * The current code for the checkout stage
	 * @return string
	 */
	public function checkoutStage()
	{
		return $this->checkout_stage;
		
	}
	
	public function validateCheckoutStageVisible($checkout_stage)
	{
		// checkout stage not set
		if(!isset($this->checkout_stages[$checkout_stage]))
		{
			return false;
		}
		
		$keys = array_keys($this->checkout_stages);
		$current_stage_num = array_search($this->checkoutStage(), $keys);
		$validation_stage_num =  array_search($checkout_stage, $keys);
		
		return $validation_stage_num <= $current_stage_num;
	}
	
	/**
	 * Returns the view URL for the current stage
	 * @param bool|string $stage_code (Optional) Default false, which means the current stage.
	 * @return string
	 */
	public function checkoutStageViewURL($stage_code = false)
	{
		if ($stage_code === false)
		{
			$stage_code = $this->checkoutStage();
		}
		
		return $this->checkout_stages[$stage_code]['view_url'];
		
	}
	
	/**
	 * Sets the checkout stage for this cart
	 * @param string $stage
	 */
	public function setCheckoutStage($stage)
	{
		if($this->verifyEditable())
		{
			$this->updateDatabaseValue('checkout_stage', $stage);
		}
	}
	
	/**
	 * Sets the checkout stage to the next one after the code provided
	 * @param string $stage_code
	 */
	public function setCheckoutStageToNextAfter($stage_code)
	{
		$this->setCheckoutStage($this->nextCheckoutStageCode($stage_code));
	}
	
	/**
	 * Returns the list of checkout stages
	 * @return array
	 */
	public function checkoutStages()
	{
		return $this->checkout_stages;
	}
	
	/**
	 * Returns the last checkout stage
	 * @return string
	 */
	public function lastCheckoutStage()
	{
		$last_stage = '';
		foreach($this->checkoutStages() as $stage => $values)
		{
			$last_stage = $stage;
		}
		return $last_stage;
	}
	
	/**
	 * Returns the stage code for the next checkout stage.
	 * @param bool|string $stage_code (Optional) Default false, which means the current stage.
	 * @return bool|int|string
	 */
	public function nextCheckoutStageCode($stage_code = false)
	{
		if ($stage_code === false)
		{
			$stage_code = $this->checkoutStage();
		}
		
		$found = false;
		if($this->checkoutStages())
		{
			foreach($this->checkoutStages() as $loop_stage_code => $values)
			{
				// If found, then that means it was the previous loop. Return this one.
				if($found)
				{
					return $loop_stage_code;
				}
				if($loop_stage_code == $stage_code)
				{
					$found = true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns the url for the next checkout stage.
	 * @param bool|string $stage_code (Optional) Default false, which means the current stage.
	 * @return bool|int|string
	 */
	public function nextCheckoutStageURL($stage_code = false)
	{
		if($stage_code === false)
		{
			$stage_code = $this->checkoutStage();
		}
		$next_stage_code = $this->nextCheckoutStageCode($stage_code);
		return $this->checkoutStageViewURL($next_stage_code);
		
	}
	
	public function checkoutStageIsAccessible($stage_code)
	{
		foreach($this->checkoutStages() as $code => $values)
		{
			// We hit the provide stage before hitting the current checkout stage
			// This means the checkout stage is ahead, so we're just fine
			if($code == $stage_code)
			{
				return true;
			}
			
			
			// We hit the chekcout stage and we haven't hit the current stage yet
			if($code == $this->checkoutStage())
			{
				return false;
			}
			
		}
		
		return false;
	}
	
	/**
	 * Returns if the cart is completed
	 * @return bool
	 */
	public function isComplete()
	{
		return $this->is_complete;
		
	}
	
	/**
	 * Marks the cart as complete or overrides the current value with the one provided
	 * @param int $complete (Optional) Default 1.
	 */
	public function markCompleted($complete = 1)
	{
		if($this->verifyEditable())
		{
			$this->updateDatabaseValue('is_complete', $complete);
		}
	}
	
	//////////////////////////////////////////////////////
	//
	// CONTACT INFORMATION
	//
	//////////////////////////////////////////////////////
	
	public function firstName()
	{
		return $this->first_name;
	}
	
	public function lastName()
	{
		return $this->last_name;
	}
	
	public function email()
	{
		return $this->email;
	}
	
	public function phone()
	{
		return $this->phone;
	}
	
	//////////////////////////////////////////////////////
	//
	// ADDRESSES
	//
	// Functions related to addresses
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if addresses are used in the system
	 * @return bool
	 */
	public function usesAddresses()
	{
		return $this->deliveryIsUsed();
	}
	
	/**
	 * Returns if a shipping address is required. This is true if shipping is enabled AND the delivery method is not
	 * set to `pickup` which is a special case for most stores.
	 * @return bool
	 */
	public function requiresShippingAddress()
	{
		return TC_getModuleConfig('store','shipping_enabled')
			&& $this->deliveryMethod() !== 'pickup';
	}
	
	/**
	 * Returns if this cart has all the necessary shipping fields filled out
	 * @return bool
	 */
	public function hasShippingAddress()
	{
		return $this->shipping_street != ''
			&& $this->shipping_city != ''
			&& $this->shipping_province != ''
			&& $this->shipping_country != ''
			&& $this->shipping_postal != ''
			;
	}
	
	/**
	 * Returns if there are issues with the shipping address. If it's required, it will validate it
	 * @return bool
	 */
	public function hasValidShippingAddress()
	{
		if($this->requiresShippingAddress())
		{
			return $this->hasShippingAddress();
		}
		
		return true;
	}
	
	/**
	 * Returns if this cart has all the necessary billing fields filled out
	 * @return bool
	 */
	public function hasBillingAddress()
	{
		return $this->billing_street != ''
			&& $this->billing_city != ''
			&& $this->billing_province != ''
			&& $this->billing_country != ''
			&& $this->billing_postal != ''
			;
	}
	
	/**
	 * @return string
	 */
	public function shippingCity()
	{
		return $this->shipping_city;
	}
	
	/**
	 * @return string
	 */
	public function shippingProvince()
	{
		return $this->shipping_province;
	}
	
	/**
	 * @return string
	 */
	public function shippingCountry()
	{
		return $this->shipping_country;
	}
	
	public function shippingValuesAreDifferent($city, $province, $country)
	{
		return $this->shippingCity() != $city || $this->shippingProvince() != $province || $this->shippingCountry() != $country;
	}
	
	public function shippingAddressFormatted($show_country = true)
	{
		$address = $this->shipping_street;
		if($this->shipping_street_2 != '')
		{
			$address .= '<br />'.$this->shipping_street_2;
		}
		$address .= '<br />'.$this->shipping_city;
		$address .= ', '.$this->shipping_province;
		if($show_country)
		{
			$address .= '<br />' . $this->shipping_country;
		}
		$address .= '<br />'.$this->shipping_postal;
		
		
		return $address;
	}
	
	public function billingAddressFormatted($show_country = true)
	{
		$address = $this->billing_street;
		if($this->billing_street_2 != '')
		{
			$address .= '<br />'.$this->billing_street_2;
		}
		$address .= '<br />'.$this->billing_city;
		$address .= ', '.$this->billing_province;
		if($show_country)
		{
			$address .= '<br />' . $this->billing_country;
			
		}
		$address .= '<br />'.$this->billing_postal;
		
		
		return $address;
	}
	
	/**
	 * Returns the shipping address as a string separated
	 * @param string $separator (Optional) Default ", "
	 * @return string
	 */
	public function shippingAddressString($separator = ", ")
	{
		$address = $this->shipping_street;
		if($this->shipping_street_2 != '')
		{
			$address .= $separator.$this->shipping_street_2;
		}
		$address .= $separator.$this->shipping_city;
		$address .= $separator.$this->shipping_province;
		$address .= $separator.$this->shipping_country;
		$address .= $separator.$this->shipping_postal;
		
		
		return $address;
	}
	
	/**
	 * Internal method to update the cart with values from the user. This only works on fields that are not set and
	 * only if the user exists and has valid data.
	 */
	public function updateBlankAddressesWithUserInfo()
	{
		// If the cart does not have a billing address
		// We set it with the user
		if($this->user())
		{
			$values = array();
			
			if($this->email() == '')
			{
				$values['email'] = $this->user()->email();
			}
			
			if($this->phone() == '' && $this->user()->cellPhone() != '')
			{
				$values['phone'] = $this->user()->cellPhone();
			}
			
			// No Billing Address
			if(!$this->hasBillingAddress() && $this->user()->hasAddress())
			{
				$values['billing_street'] = $this->user()->address1();
				$values['billing_street_2'] = $this->user()->address2();
				$values['billing_city'] = $this->user()->city();
				$values['billing_province'] = $this->user()->province();
				$values['billing_country'] = $this->user()->country();
				$values['billing_postal'] = $this->user()->postalCode();
			}
			
			// No Shipping Address
			if(!$this->hasShippingAddress() && $this->user()->hasAddress())
			{
				$values['shipping_street'] = $this->user()->address1();
				$values['shipping_street_2'] = $this->user()->address2();
				$values['shipping_city'] = $this->user()->city();
				$values['shipping_province'] = $this->user()->province();
				$values['shipping_country'] = $this->user()->country();
				$values['shipping_postal'] = $this->user()->postalCode();
			}
			
			if(count($values) > 0)
			{
				$this->updateWithValues($values);
			}
		}
	}
	
	/**
	 * Function to find the appropriate country code for the provided province
	 * @param string $province
	 * @return string
	 */
	public static function countryCodeForProvince($province)
	{
		// Find the associated country
		if($province != '')
		{
			foreach(static::countryCodeRegions() as $country_name => $country_values)
			{
				foreach($country_values['provinces'] as $province_values)
				{
					if($province_values['code'] == $province)
					{
						return $country_values['code'];
					}
				}
			}
			
		}
		
		return '';
	}
	
	/**
	 * Returns an associative array of the regions for country codes
	 * @return array
	 */
	public static function countryCodeRegions()
	{
		$string = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/admin/store/classes/models/TMm_ShoppingCart/regions.json");
		return json_decode($string, true);
	}
	
	//////////////////////////////////////////////////////
	//
	// DELIVERY
	//
	// Functions related to delivery
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the cart is set for pickup
	 * @return bool
	 */
	public function isPickup()
	{
		return $this->delivery_method == 'pickup';
	}
	
	/**
	 * Sets the delivery method to pickup
	 */
	public function setIsPickup()
	{
		$this->updateWithValues(['delivery_method' => 'pickup','delivery_cost' => 0]);
	}
	
	/**
	 * @param TCm_Model|TMi_DeliveryOption $delivery_model
	 */
	public function setDeliveryWithModel($delivery_model)
	{
		$this->updateWithValues(['delivery_method' => $delivery_model->contentCode()
			                        ,'delivery_cost' => $delivery_model->price()]);
		
	}
	
	/**
	 * Returns if the cart is set for delivery. This will be true if the method is not blank and not set for pickup
	 * @return bool
	 */
	public function isDelivery()
	{
		return $this->delivery_method != 'pickup' && $this->delivery_method != '';
	}
	
	/**
	 * Removes the shipping settings that are currently applied to this cart
	 */
	public function removeShippingSettings()
	{
		if($this->deliveryMethod() != '')
		{
			$this->updateDatabaseValue('delivery_method','');
		}
		
	}
	
	/**
	 * Returns the weight of this cart in kg
	 * @return int
	 */
	public function weight()
	{
		$weight = 0;
		foreach($this->cartItems() as $cart_id => $cart_item)
		{
			$product = $cart_item->item(); // get the product information
			$weight += $cart_item->quantity()*$product->weight(); // update the running total
		}
		
		return $weight;
		
	}
	
	/**
	 * Returns the array of delivery option values as an associative array.
	 * @return array
	 */
	public function deliveryOptionValues()
	{
		$values = array();
		
		// Handle Pickup
		if(TC_getModuleConfig('store','pickup_permitted'))
		{
			$values['pickup'] = array(
				'title' => 'Pickup',
				'description' =>TC_getModuleConfig('store','pickup_description'),
				'price' => 0
			);
		}
		
		// Loop through all delivery providers
		// Grab the list of delivery providers and if they use the interface, add their options to the list
		$delivery_provider_classes = explode(',', TC_getModuleConfig('store','delivery_provider_classes'));
		foreach($delivery_provider_classes as $delivery_provider_class_name)
		{
			/** @var TMi_DeliveryProvider $provider */
			$provider = ($delivery_provider_class_name)::init();
			$interfaces = class_implements($provider);
			if (isset($interfaces['TMi_DeliveryProvider']))
			{
				$values = array_merge($values, $provider->deliveryOptionsForCart($this));
			}
		}
		
		return $values;
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return bool
	 */
	public function hasDeliverySet()
	{
		return $this->delivery_method != '';
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return bool
	 */
	public function deliveryIsUsed()
	{
		return TC_getModuleConfig('store','shipping_enabled');
	}
	
	/**
	 * Returns if this item shows billing addreses as part of the interface. This
	 * return bool
	 */
	public function showBillingAddress()
	{
		return $this->hasBillingAddress();
		
		
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @return string
	 */
	public function deliveryTitle()
	{
		if($this->delivery_method == 'pickup')
		{
			return 'Pick-Up';
		}
		
		if($this->delivery_method == 'delivery')
		{
			return 'Delivery Method Not Set';
		}
		$delivery = TC_initClassWithContentCode($this->delivery_method);
		return $delivery->title();
		
		
	}
	
	/**
	 * Returns the delivery method
	 * @return string
	 */
	public function deliveryMethod()
	{
		return $this->delivery_method;
	}
	
	/**
	 * Returns if the cart has the delivery value set
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function deliveryPrice($use_sig_digits = false)
	{
		return $this->formatCurrency($this->delivery_cost, $use_sig_digits);
		
	}
	
	/**
	 * Returns the price for a tax for the delivery
	 *
	 * @param TMm_Tax $tax
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function deliveryPriceForTax($tax,$use_sig_digits = false)
	{
		$price = 0;
		if($this->hasDeliverySet())
		{
			if($this->delivery_method != 'pickup')
			{
				if($tax->appliesToShipping())
				{
					$price += $this->percentageForTax($tax)/100 * $this->deliveryPrice(true);
				}
			}
		}
		
		return $this->formatCurrency($price, $use_sig_digits);
	}
	
	
	/**
	 * Returns the instructions for the cart, if any
	 * @return string
	 */
	public function instructions()
	{
		return $this->instructions;
	}
	
	//////////////////////////////////////////////////////
	//
	// ERRORS
	//
	// Functions related to errors that must be manually
	// corrected by the person with the cart.
	//
	//////////////////////////////////////////////////////
	
	protected function processCartItemErrors()
	{
		if($this->cart_item_errors_processed === false)
		{
			foreach($this->cartItems() as $cart_item)
			{
				$errors = $cart_item->errors();
				if(is_array($errors) && count($errors) > 0)
				{
					$this->errors = array_merge($this->errors, $cart_item->errors());
					
				}
				
			}
			
			$this->cart_item_errors_processed = true;
		}
	}
	
	/**
	 * Adds an error to this cart with a message
	 * @param string|array $message
	 */
	public function addError($message)
	{
		if(is_array($message))
		{
			if($message['error_type'] == 'warning')
			{
				$this->warnings[] = $message;
			}
			else
			{
				$this->errors[] = $message;
			}
		}
		else
		{
			$this->errors[] = $message;
		}
		
	}
	
	/**
	 * Returns the list of errors
	 * @return array
	 */
	public function errors()
	{
		$this->processCartItemErrors();
		return $this->errors;
	}
	
	/**
	 * Returns if this cart currently has errors
	 * @return bool
	 */
	public function hasErrors()
	{
		return count($this->errors()) > 0;
		
	}
	
	/**
	 * Returns the list of warnings
	 * @return array
	 */
	public function warnings()
	{
		$this->processCartItemErrors();
		return $this->warnings;
	}
	
	/**
	 * Returns if this cart currently has errors
	 * @return bool
	 */
	public function hasWarnings()
	{
		return count($this->warnings()) > 0;
		
	}
	
	/**
	 * Returns the errors and warnings for this cart
	 * @return array
	 */
	public function errorsAndWarnings()
	{
		return array_merge($this->errors(), $this->warnings());
	}
	
	/**
	 * @return bool
	 */
	public function hasErrorsOrWarnings()
	{
		return count($this->errorsAndWarnings()) > 0;
		
	}
	
	
	/**
	 * An all encompassing function which returns if the cart is ready for payment. This checks the various states of
	 * the checkout process as well as if there are any errors
	 * @return bool
	 */
	public function isReadyForPayment()
	{
		// If there are any errors, we get out
		if($this->hasErrors())
		{
			return false;
		}
		
		// If we're using shipping on the site
		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			// If there is no shipping address or no delivery method chosen
			if(!$this->hasValidShippingAddress() || !$this->hasDeliverySet())
			{
				return false;
			}
			
		}
		
		
		return true;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ACCOUNTING
	//
	// Functions related to accounting
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float|string
	 */
	public function subtotal($use_sig_digits = false)
	{
		$subtotal = 0;
		foreach($this->cartItems() as $cart_item)
		{
			$subtotal += $cart_item->quantity() * $cart_item->pricePerItem();
		}
		
		return $this->formatCurrency($subtotal, $use_sig_digits);
		
	}
	
	/**
	 * Returns the total discounts applied to all items
	 * @return float
	 * @TODO TEST
	 */
	public function totalDiscountForItems()
	{
		$subtotal = 0;
		foreach($this->cartItems() as $cart_item)
		{
			$subtotal += $cart_item->quantity() * $cart_item->discountPerItem();
		}
		
		return number_format($subtotal, 2, '.', '');
	}
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @return float
	 * @TODO TEST
	 */
	public function subtotalNoDiscounts()
	{
		
		$subtotal = 0;
		foreach($this->cartItems() as $cart_item)
		{
			$subtotal += $cart_item->quantity() * ($cart_item->pricePerItem() + $cart_item->discountPerItem());
		}
		
		return number_format($subtotal, 2, '.', '');
	}
	
	
	/**
	 * Returns the subtotal for this cart for all the products before taxes and services.
	 * @return float
	 */
	public function originalSubtotalNoDiscounts()
	{
		return $this->subtotalNoDiscounts();
	}
	
	
	/**
	 * Returns the total for the shopping cart
	 * @return float
	 */
	public function total()
	{
		// Note, this calculation does NOT use significant digits. The math should always match the individual values
		// being presented to the user.
		if($this->total === false)
		{
			$this->total = 0;
			
			// PRODUCTS
			$this->total += $this->subtotal(false);
			
			// DELIVERY
			$this->total += $this->deliveryPrice(false);
			
			// TAXES
			foreach($this->taxValues() as $tax_id => $values)
			{
				if($values['amount'] >= 0)
				{
					$this->total += $this->formatCurrency($values['amount']);
				}
			}
			
		}
		
		return $this->formatCurrency($this->total);
		
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TAXES
	//
	// Functions related to taxes
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array with TMm_Tax objects and the amount of money due for that tax.
	 * @return array
	 */
	public function taxValues()
	{
		$values = array();
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->models() as $tax)
		{
			$values[$tax->id()] = array('tax' => $tax, 'amount' => $this->totalForTax($tax, true));
			
		}
		
		return $values;
	}
	
	/**
	 * Returns the dollar value for the tax provided. This is calculated "per item".
	 * @param TMm_Tax $tax
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function totalForTax($tax, $use_sig_digits = false)
	{
		if(!isset($this->tax_totals[$tax->id()]))
		{
			$tax_amount = 0;
			foreach($this->cartItems() as $cart_item)
			{
				$tax_amount += $cart_item->valueForTax($tax, true); // use sig_digits for internal calcs
			}
			
			if($this->hasDeliverySet() && $tax->appliesToShipping())
			{
				$tax_amount += $this->deliveryPriceForTax($tax,true);
			}
			
			$this->tax_totals[$tax->id()] = $tax_amount;
			
		}
		
		$value = $this->tax_totals[$tax->id()];
		return $this->formatCurrency($value, $use_sig_digits);
		
	}
	
	/**
	 * Returns the percentage value for the tax provided
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function percentageForTax($tax)
	{
		return $tax->percent();
	}
	
	/**
	 * Returns if taxes are ready to be viewed.
	 * @return bool
	 */
	public function taxesAreReady()
	{
		// Directly tied to if shipping values are set
		return $this->hasValidShippingAddress();
	}
	
	//////////////////////////////////////////////////////
	//
	// CONVERSION
	//
	// Functions related to converting carts to transactions
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Converts this Shopping cart into a Purchase in the system.
	 *
	 * This must be called before the cart is marked as complete
	 * @param ?string $transaction_id The id for this transaction
	 * @param null|array $override_properties An array of override properties for the purchase
	 * @return bool|TMm_Purchase If the purchase already exists, it will return the purchase associated with the cart
	 *
	 */
	public function convertToPurchase(?string $transaction_id = null, $override_properties = null)
	{
		
		//----- CHECK FOR INVALID CARTS -----
		if($this->isComplete())
		{
			if($this->purchaseID() > 0)
			{
				return TMm_Purchase::init($this->purchaseID());
			}
			else
			{
				$this->addConsoleError('Cart is already complete, no purchase exists');
				return null;
			}
		}
		
		// WE're using stages and we have issues with the order
		if($this->checkout_stages)
		{
			if($this->checkoutStage() != $this->lastCheckoutStage())
			{
				$this->addConsoleError('Cart incomplete, invalid checkout stage ('.$this->checkoutStage().'), expected ('.$this->lastCheckoutStage().')');
				return null;
			}
		}
		
		$values = $this->purchaseConversionValues();
		
		if(is_array($override_properties))
		{
			foreach($override_properties as $override_index => $override_value)
			{
				$values[$override_index] = $override_value;
			}
		}
		
		// Deal with comp values
		// Override any value that would be a value, and make it zero
		if($values['type'] == 'comp')
		{
			$zero_fields = array('total','subtotal','delivery_cost');
			
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$zero_fields[] ='tax_'.$tax->id().'_paid';
			}
			
			foreach($zero_fields as $field)
			{
				$values[$field] = 0;
			}
			
		}
		
		//$values['transaction_id'] = $transaction_id;
		$purchase = TMm_Purchase::createWithValues($values);
		
		if(!$purchase)
		{
			TC_message('Purchase could not be created', false);
			return null;
			
		}
		
		// FREE transactions should get processed as paid
		if($this->total() == 0)
		{
			$purchase->markAsPaidWithTransaction('','free');
		}
		
		
		// If a transaction was passed in, we mark it as paid
		// Old systems might pass in an empty string
		elseif(!is_null( $transaction_id) && $transaction_id != '')
		{
			if(isset($values['type']))
			{
				$purchase->markAsPaidWithTransaction($transaction_id, $values['type']);
			}
			else
			{
				$purchase->markAsPaidWithTransaction($transaction_id);
			}
		}
		
		else
		{
			$this->addConsoleMessage('SKIPPED : Mark as paid');
			TSm_ErrorLogItem::createWithValues(['message' => 'Transaction ID '. $transaction_id.' '.gettype($transaction_id),
				                                   'class_name' => 'TMm_Purchase::'.$purchase->id()]);
		}
		
		
		
		
		$this->updateWithPurchase($purchase);
		if(!$purchase)
		{
			TC_message('There was an error placing your order. {Purchase Creation Failure}');
			return null;
		}
		
		// Deal with replaced purchases. Must happen after updateWithPurchase, which sets the original_purchase_id
		$purchase->processRevisedPurchases();
		
		
		//----- MARK AS COMPLETE -----
		// Do this before processing cart items as they could potentially create unforeseen consequences
		$this->markCompleted();
		
		//----- PURCHASE ITEMS -----
		foreach($this->cartItems() as $cart_item)
		{
			$cart_item->convertToPurchaseItem($purchase);
		}
		
		//----- RE-INSTANTIATE -----
		$purchase->clearCachedCostValues();
		$purchase = TMm_Purchase::init($this->purchaseID());
		
		//----- TRACK PURCHASE IN GA4 -----
		$values = [
			'currency' => 'CAD',
			'transaction_id' => $purchase->id(),
			'value' => $purchase->total(),
			'shipping' => $purchase->deliveryPrice(),
			'tax' => $purchase->totalForAllTaxes(),
			'items' => []
		
		];
		
		foreach($purchase->purchaseItems() as $purchase_item)
		{
			$values['items'][] = TSv_GoogleAnalytics::itemValuesForModel($purchase_item);
		}
		
		TSv_GoogleAnalytics::trackEvent('purchase', $values);
		
		if(class_exists('TSv_FacebookPixel'))
		{
			$fb_values = [
				'currency' => 'CAD',
				'value' => $purchase->total(),
			];
			
			// Capital P is correct
			TSv_FacebookPixel::trackEvent('Purchase', $fb_values);
		}
		
		if(class_exists('TSv_TikTokPixel'))
		{
			$fb_values = [
				'currency' => 'CAD',
				'value' => $purchase->total(),
			];
			
			TSv_TikTokPixel::trackEvent('CompletePayment', $fb_values);
		}
		
		
		return $purchase;
	}
	
	/**
	 * Converts this Shopping cart into a Purchase in the system.
	 *
	 * This must be called before the cart is marked as complete
	 * @param bool|array $override_properties An array of override properties for the purchase
	 * @return bool|TMm_Purchase If the purchase already exists, it will return the purchase associated with the cart
	 *
	 */
	public function convertToPurchasePendingOfflinePayment($override_properties = false)
	{
		if(!is_array($override_properties))
		{
			$override_properties = array();
		}
		
		$override_properties['type'] = 'check';
		$override_properties['num_payments'] = '0'; // indicator for not paid
		
		$purchase = $this->convertToPurchase(null, $override_properties);
		
		if($purchase)
		{
			$purchase->addEmailsToCron();
		}
		return $purchase;
	}
	
	
	/**
	 * Duplicates this cart and returns the newly created cart
	 * @param array|bool $override_values An array of values that are overridden in the duplication process
	 * @return static|bool
	 */
	public function duplicate($override_values = false)
	{
		$new_cart = parent::duplicate($override_values);
		if($new_cart instanceof TMm_ShoppingCart)
		{
			// Loop through cart items and create new ones for the new cart
			foreach($this->cartItems() as $cart_item)
			{
				$new_cart_item = $cart_item->duplicate(array('cart_id' => $new_cart->id()));
			}
			
			// Make sure we grab the latest for the new cart
			$new_cart->reaquireCartItems();
			
		}
		
		return $new_cart;
	}
	
	public function revisionNumber()
	{
		return $this->revision_number;
	}
	
	/**
	 * Updates the value of this cart with the provided purchase
	 * @param TMm_Purchase $purchase
	 */
	protected function updateWithPurchase($purchase)
	{
		if($purchase)
		{
			$this->updateDatabaseValue('purchase_id', $purchase->id());
			
			$values = array(
				'revision_number' => $this->revision_number +1
			);
			
			// First time making a purchase
			if($this->original_purchase_id < 1)
			{
				$values['original_purchase_id'] = $purchase->id();
			}
			$this->updateWithValues($values);
			$purchase->updateDatabaseValue('original_purchase_id', $this->original_purchase_id);
			
		}
	}
	
	/**
	 * Returns the original purchase
	 * @return bool|TMm_FFT_Purchase
	 */
	public function originalPurchase()
	{
		if($this->original_purchase_id > 0)
		{
			/** @var TMm_Purchase $purchase */
			return TMm_Purchase::init($this->original_purchase_id);
			
		}
		
		return false;
	}
	
	/**
	 * Returns the original purchase date
	 * @return bool|string
	 */
	public function originalPurchaseDate()
	{
		if($purchase = $this->originalPurchase())
		{
			return $purchase->dateAdded();
		}
		return false;
	}
	
	/**
	 * Returns if this cart is actually editing a previous order
	 * @return bool
	 */
	public function isEditingPreviousOrder()
	{
		return $this->original_purchase_id > 0;
	}
	
	
	
	/**
	 * Returns an associative array of values that will be used to convert the cart into a purchase
	 * @return array
	 */
	public function purchaseConversionValues()
	{
		$values = array();
		$values['type'] = 'online';
		$values['cart_id'] = $this->id();
		
		$values['buyer_id'] = ($this->user() ? $this->user()->id() : '0');
		$values['num_payments'] = 0; // Payments happen after, shouldn't automatically be set if there isn't one
		$values['delivery_method'] = $this->delivery_method;
		$values['instructions'] = $this->instructions;
		$values['payment_method'] = $this->payment_method;
		$values['revision_number'] = $this->revision_number ;
		
		// COSTS
		$values['total'] = $this->total(true);
		
		// deal with a null delivery cost
		if($this->delivery_cost == null)
		{
			$values['delivery_cost'] = 0;
		}
		else
		{
			$values['delivery_cost'] = $this->delivery_cost;
		}
		
		$values['subtotal'] = $this->subtotal(true);
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$values['tax_'.$tax->id().'_percent'] = $this->percentageForTax($tax);
			$values['tax_'.$tax->id().'_paid'] = $this->totalForTax($tax, true);
		}
		
		$values['user_id'] = $this->user_id;
		
		// values in cart are auto-synced to match the user
		$values['first_name'] = $this->first_name;
		$values['last_name'] = $this->last_name;
		$values['email'] = $this->email;
		$values['phone'] = $this->phone;
		
		
		// BILLING ADDRESS
		$values['billing_street'] = $this->billing_street;
		$values['billing_street_2'] = $this->billing_street_2;
		$values['billing_city'] = $this->billing_city;
		$values['billing_province'] = $this->billing_province;
		$values['billing_country'] = $this->billing_country;
		$values['billing_postal'] = $this->billing_postal;
		
		// SHIPPING ADDRESS
		$values['shipping_street'] = $this->shipping_street;
		$values['shipping_street_2'] = $this->shipping_street_2;
		$values['shipping_city'] = $this->shipping_city;
		$values['shipping_province'] = $this->shipping_province;
		$values['shipping_country'] = $this->shipping_country;
		$values['shipping_postal'] = $this->shipping_postal;
		
		return $values;
	}
	
	/* 	FUNCTION	: 	setPurchaseID
		DESCRIPTION	:	Sets the purchaseID for this cart
		PARAMS		:	purchase_id [int]
		RESULT		:
	 */
	public function setPurchaseID($purchase_id)
	{
		$query = "UPDATE shopping_carts SET purchase_id = '".mysql_real_escape_string($purchase_id)."' WHERE cart_id = ".$this->cart_id;
		$this->mysqlQuery($query);
		$this->purchase_id = $purchase_id;
	}
	
	/* 	FUNCTION	: 	purchaseID
		DESCRIPTION	:	Returns the purchase ID for this cart
		PARAMS		:
		RESULT		:
	 */
	public function purchaseID()
	{
		return $this->purchase_id;
	}
	
	/* 	FUNCTION	: 	eraseCreditCardInfo
		DESCRIPTION	:	Erases the existing Session variables that are stored regarding credit card info. Must be called at the last step of a purchase
		PARAMS		:
		RESULT		:
	 */
	public function eraseCreditCardInfo()
	{
		unset($_SESSION['cc_details']);
	}
	
	/**
	 * Static method that deletes all the old carts from the system. The number of days which are delete defaults to
	 * 7 unless there is a different value set in $PFconfig['store']['num_days_to_save_cart']
	 * @param int $num_days_to_save Default 30
	 */
	public static function deleteOldCarts($num_days_to_save = 30)
	{
		
		$model = new TCm_Model(false);
		$query = "DELETE FROM shopping_carts
			WHERE is_complete = 0 AND purchase_id = 0 AND date_added < DATE_SUB(now(), INTERVAL ".$num_days_to_save." day) ";
		$model->DB_Prep_Exec($query);
		
	}
	
	//////////////////////////////////////////////////////
	//
	// ADMIN CARTS
	//
	// Admin carts are a special case that allow administrators
	// in the system to fill a cart and process it using alternative
	// means such as a cheque, complimentary. There is only ever
	// on admin cart active at a time.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Finds if there is an existing admin shopping cart and generates it if it doesn't exist
	 * @return TMm_ShoppingCart
	 */
	public static function adminCart()
	{
		// Reject any non-admins attempting to load the admin cart
		$user = TC_currentUser();
		if(!$user || !$user->isAdmin())
		{
			TC_triggerError('Unable to access admin cart');
		}
		
		
		$query = "SELECT * FROM shopping_carts WHERE is_complete = 0 AND session_id = 'admin' LIMIT 1";
		$model = new TCm_Model(false);
		$result = $model->DB_Prep_Exec($query);
		// We find an admin cart and return it
		if($cart_row = $result->fetch())
		{
			return TMm_ShoppingCart::init($cart_row);
		}
		
		// No Admin Cart exists
		$values = array();
		$values['session_id'] = 'admin';
		return TMm_ShoppingCart::createWithValues($values);
	}
	
	/**
	 * Adds an item to the admin cart
	 * @param $content_code
	 * @param int $quantity
	 */
	public static function addToAdminCartWithContentCode($content_code, $quantity = 1)
	{
		$cart = static::adminCart();
		$cart->emptyCart();
		$cart->addToCartWithContentCode($content_code, $quantity);
	}
	
	public static function redirectToAdminCart()
	{
		$cart = static::adminCart();
		header("Location: /admin/store/do/cart-view/".$cart->id());
		exit();
	}
	
	/**
	 * Returns if this cart is an admin cart
	 * @return bool
	 */
	public function isAdminCart()
	{
		return $this->session_id == 'admin';
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'session_id' => [
					'comment'       => 'The ID of the session for the cart',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'checkout_stage' => [
					'comment'       => '',
					'type'          => 'varchar(128)',
					'nullable'      => false,
				],
				'is_complete' => [
					'comment'       => 'Indicates if the cart is completed and purchased',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
				
				////////////////////////////////////////////////
				//
				// BUYER
				//
				////////////////////////////////////////////////
				
				// Not a foreign key since they often don't exist, historical values have 0 in there
				'user_id' => [
					'comment'       => 'The id of the user ',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
					
				],
				'first_name' => [
					'comment'       => 'The first name of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'last_name' => [
					'comment'       => 'The last name of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'email' => [
					'comment'       => 'The email of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'phone' => [
					'comment'       => 'The phone number of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
				////////////////////////////////////////////////
				//
				// ADDRESSES
				//
				////////////////////////////////////////////////
				
				'billing_street' => [
					'comment'       => 'The billing street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_street_2' => [
					'comment'       => 'The second line of the billing street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_city' => [
					'comment'       => 'The billing city of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_province' => [
					'comment'       => 'The billing province of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_country' => [
					'comment'       => 'The billing country of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'billing_postal' => [
					'comment'       => 'The billing postal code of the buyer',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'shipping_street' => [
					'comment'       => 'The shipping street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_street_2' => [
					'comment'       => 'The second line of the shipping street of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_city' => [
					'comment'       => 'The shipping city of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_province' => [
					'comment'       => 'The shipping province of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_country' => [
					'comment'       => 'The shipping country of the buyer',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'shipping_postal' => [
					'comment'       => 'The shipping postal code of the buyer',
					'type'          => 'varchar(64)',
					'nullable'      => false,
				],
				'same_billing_shipping' => [
					'title'         => 'Same billing and shipping address',
					'comment'       => 'Indicates if the cart uses the same address for billing and shipping',
					'type'          => 'tinyint(1) unsigned DEFAULT 1',
					'nullable'      => false,
				],
			
				// DELIVERY
				
				'delivery_method' => [
					'comment'       => 'The delivery method used',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				'delivery_cost' => [
					'comment'       => 'The delivery cost ',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => true,
				],
				'purchase_id' => [
					'comment'       => 'The related purchase if it was set ',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
				],
				'instructions' => [
					'comment'       => 'The instructions provided with the cart ',
					'type'          => 'text',
					'nullable'      => true,
				],
				
				'payment_method' => [
					'comment'       => 'The payment method used',
					'type'          => 'varchar(255)',
					'nullable'      => true,
				],
				
				// EDITING
				'original_purchase_id' => [
					'comment'       => 'The orginal purchase ID, used if changed',
					'type'          => 'int(10) unsigned',
					'nullable'      => false,
				],
				
				'revision_number' => [
					'comment'       => 'The revision number if multiples are used',
					'type'          => 'tinyint(3) unsigned DEFAULT 1',
					'nullable'      => false,
				],
				'is_replaced' => [
					'comment'       => 'Indicates if the cart has been replaced',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
				
				'table_keys' => [
					'is_complete' => ['index' => "is_complete"],
					'user_id' => ['index' => "user_id"],
				]
			
			];
	}
	
}

?>