<?php
class TMm_ShoppingCartTest extends TMm_StoreModelTestCase
{
	
	protected $shopping_cart;
	
	protected function setUp(): void
	{
		parent::setUp();
		$this->shopping_cart = $this->shoppingCart();
	}
	
	protected function tearDown(): void
	{
		$this->clearTaxes();
	}
	
	public function testEmptyCart()
	{
		$this->assertEquals(0, $this->shopping_cart->total());
	}
	
	public function testAddToCartBasics()
	{
		// Create a test product
		$values = array(
			'title' => 'Sandles',
			'price' => 2.26,
			'in_stock' => 15,
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		// Add a quantity of 1
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		$this->assertEquals(2.26, $this->shopping_cart->total());
		$this->assertEquals(1, $this->shopping_cart->numCartItems()); // three items in cart
		
		// Add a second item with a quantity of 2, total quantity is now 3
		$cart_item = $this->shopping_cart->addToCart($product, 2);
		$this->assertEquals(6.78, $this->shopping_cart->subtotal());
		$this->assertEquals(6.78, $this->shopping_cart->total());
		$this->assertEquals(3, $this->shopping_cart->numCartItems()); // three items in cart
		
		// Remove an item from the cart
		$cart_item = $this->shopping_cart->addToCart($product, -1);
		$this->assertEquals(4.52, $this->shopping_cart->subtotal());
		$this->assertEquals(4.52, $this->shopping_cart->total());
		$this->assertEquals(2, $this->shopping_cart->numCartItems());
		
		// Test passing variables
		// should create a new line item
		$variable_cart_item = $this->shopping_cart->addToCart($product, 1, array(1 => 'some value'));
		$this->assertNotEquals($cart_item->id(), $variable_cart_item->id()); // assert it added a new row
		$this->assertEquals('some value', $variable_cart_item->variable(1));
		$this->assertEquals(3, $this->shopping_cart->numCartItems()); // 3 total items in the cart
		$this->assertEquals(2, $this->shopping_cart->numCartProducts()); // 2 different product rows
		$this->assertEquals(6.78, $this->shopping_cart->total());
		
		// Test force create new
		$forced_cart_item = $this->shopping_cart->addToCart($product, 1, array(), true);
		$this->assertEquals(4, $this->shopping_cart->numCartItems()); // 4 items in the cart
		$this->assertEquals(3, $this->shopping_cart->numCartProducts()); // 3 cart_item rows
		$this->assertEquals(9.04, $this->shopping_cart->total());
		
	}
	
	/**
	 * Test the calculations related to discounts for carts and cart items
	 */
	public function testDiscounts()
	{
		// Create a test product
		$values = array(
			'title' => 'Bottle',
			'price' => 5.00,
			'in_stock' => 15,
			'discount' => 1.40
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		// Add 2 items
		$cart_item = $this->shopping_cart->addToCart($product, 2);
		
		// (5.00 - 1.40) * 2
		$this->assertEquals(7.20, $this->shopping_cart->total());
		$this->assertEquals(3.60, $cart_item->pricePerItem());
		$this->assertEquals(1.40, $cart_item->discountPerItem());
		
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		
		// (5.00 - 1.40) * 3
		$this->assertEquals(10.8, $this->shopping_cart->total());
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TAXES
	//
	//////////////////////////////////////////////////////
	
	public function testBasicTaxes()
	{
		$tax_5 = static::tax_5_percent();
		$this->assertEquals($tax_5->percent(), $this->shopping_cart->percentageForTax($tax_5));
		
		// Create a test product
		$values = array(
			'title' => 'Nail File',
			'price' => 2.50,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 1
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$this->shopping_cart->addToCart($product, 3);
		
		// 2.50 * 3
		$this->assertEquals(7.50, $this->shopping_cart->subtotal(true));
		$this->assertEquals(7.50, $this->shopping_cart->subtotal());
		
		// 2.50 * 3 * 0.05
		$this->assertEquals(0.375, $this->shopping_cart->totalForTax($tax_5, true));
		$this->assertEquals(0.38, $this->shopping_cart->totalForTax($tax_5));
		
		// 7.50 + 0.38
		$this->assertEquals(7.88, $this->shopping_cart->total());
		
		
		// ----- ADD ANOTHER PRODUCT -----
		
		$values = array(
			'title' => 'Eraser',
			'price' => 1.64,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 0
		);
		
		$eraser = TMm_StoreUnitTestProduct::createWithValues($values);
		$this->shopping_cart->addToCart($eraser, 2);
		
		// 7.50 + (1.64 * 2)
		$this->assertEquals(10.78, $this->shopping_cart->subtotal(true));
		$this->assertEquals(10.78, $this->shopping_cart->subtotal());
		
		// Same as above
		$this->assertEquals(0.375, $this->shopping_cart->totalForTax($tax_5, true));
		$this->assertEquals(0.38, $this->shopping_cart->totalForTax($tax_5));
		
		// 10.78 + 0.38
		$this->assertEquals(11.16, $this->shopping_cart->total());
		
		
		
	}
	
	/**
	 * Tests calculations on taxes on items that have discounts
	 */
	public function testTaxesWithDiscounts()
	{
		// Create a tax
		$tax = static::tax_5_percent();
		
		// Create a test product
		$values = array(
			'title' => 'Cup',
			'price' => 8.00,
			'discount' => 0.17,
			'in_stock' => 15,
			'uses_tax_'.$tax->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$this->assertEquals($product->taxableAmountForTax($tax, 0.17 ),(8-0.17));
		
		
		
		$cart_item = $this->shopping_cart->addToCart($product, 3);
		
		// (8 - 0.17) * 3
		$this->assertEquals(23.49, $this->shopping_cart->subtotal());
		
		// (8 - 0.17) * 3 * 0.05
		$this->assertEquals(1.175, $this->shopping_cart->totalForTax($tax, true));
		$this->assertEquals(1.18, $this->shopping_cart->totalForTax($tax));
		
		// 23.49 + 1.18
		$this->assertEquals(24.67, $this->shopping_cart->total());
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// DELIVERY COSTS
	//
	//////////////////////////////////////////////////////
	
	public function testDeliveryCostsPickup()
	{
		$this->shopping_cart->setIsPickup();
		$this->assertTrue($this->shopping_cart->hasDeliverySet());
		$this->assertEquals('pickup', $this->shopping_cart->deliveryMethod());
		$this->assertEquals(0, $this->shopping_cart->deliveryPrice());
		
	}
	
	public function testDeliveryCostsWithTax()
	{
		// Create a tax
		$tax = static::tax_5_percent();
		
		$values= array(
			'delivery_option_id' => '634',
			'title' => 'Unit Test Delivery',
			'price' => 1.77,
		);
		$delivery_option = new TMm_StoreUnitTestDeliveryOption($values);
		
		// Change delivery
		$this->shopping_cart->setDeliveryWithModel($delivery_option);
		$this->assertEquals('TMm_StoreUnitTestDeliveryOption--634',
		                    $this->shopping_cart->deliveryMethod());
		$this->assertEquals(1.77, $this->shopping_cart->deliveryPrice());
		
		// Note tax is set to apply to shipping, so that's where it finds out
		
		// Subtotal doesn't include shipping
		$this->assertEquals(0, $this->shopping_cart->subtotal());
		
		// 1.77 * 0.05
		$this->assertEquals(0.089, $this->shopping_cart->totalForTax($tax,true));
		$this->assertEquals(0.09, $this->shopping_cart->totalForTax($tax));
		
		// 1.77 + 0.09
		$this->assertEquals(1.86, $this->shopping_cart->total());
		
		
		
	}
	
	public function testDeliveryCostsWithoutTax()
	{
		// Create a non-delivery tax
		$tax_values = array(
			'title' => 'Tax No Delivery',
			'percent' => '3',
			'country' => 'CA',
			'applies_to_shipping' => 0,
			'number' => ''
		
		);
		
		$tax = $this->createTaxWithValues($tax_values);
		
		$values= array(
			'delivery_option_id' => '346',
			'title' => 'Unit Test Delivery',
			'price' => 1.48,
		);
		$delivery_option = new TMm_StoreUnitTestDeliveryOption($values);
		
		// Change delivery
		$this->shopping_cart->setDeliveryWithModel($delivery_option);
		$this->assertEquals('TMm_StoreUnitTestDeliveryOption--346',
		                    $this->shopping_cart->deliveryMethod());
		$this->assertEquals(1.48, $this->shopping_cart->deliveryPrice());
		
		
		// Subtotal doesn't include shipping
		$this->assertEquals(0, $this->shopping_cart->subtotal());
		
		// No taxes should exist
		$this->assertEquals(0, $this->shopping_cart->totalForTax($tax));
		$this->assertEquals(1.48, $this->shopping_cart->total());
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// CONVERSION VALUES
	//
	//////////////////////////////////////////////////////
	
	public function testPurchaseConversionValues()
	{
		$tax_5 = static::tax_5_percent();
		
		// Create Product 1
		// Subtotal: 8.00
		$values = array(
			'title' => 'Grapes',
			'price' => 8.00,
			'in_stock' => 15,
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		
		
		// Create Product 2
		// Subtotal: 10.56 = (4.73 - 1.21) * 3
		// Tax 1 :   0.528
		// Total :   11.088
		$values = array(
			'title' => 'Funnel',
			'price' => 4.73,
			'in_stock' => 15,
			'discount' => 1.21,
			'uses_tax_'.$tax_5->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$cart_item = $this->shopping_cart->addToCart($product, 3);
		
		// Create Product 3
		// Subtotal: 7.30
		// Tax 1 :  0.365
		// Total :  7.665
		$values = array(
			'title' => 'Oranges',
			'price' => 3.65,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$cart_item = $this->shopping_cart->addToCart($product, 2);
		
		// Add Delivery
		// Subtotal: 2.64
		// Tax 1 :  0.132 // calculated based on the tax property
		// Total :  2.772
		$values= array(
			'delivery_option_id' => '123',
			'title' => 'Unit Test Delivery',
			'price' => 2.64,
		);
		$delivery_option = new TMm_StoreUnitTestDeliveryOption($values);
		
		// Change delivery
		$this->shopping_cart->setDeliveryWithModel($delivery_option);
		
		
		// Generate the values, passing in a null object for the ID
		$values = $this->shopping_cart->purchaseConversionValues();
		
		// 8.00 + 10.56 + 7.30 : adding up the three products
		$this->assertEquals(25.86, $values['subtotal']);
		
		// 2.64 : just the cost of delivery
		$this->assertEquals(2.64, $values['delivery_cost']);
		
		// 0 + 0.528 + 0.365 + 0.132 : first one doesn't use taxes
		$this->assertEquals(1.025, $values['tax_'.$tax_5->id().'_paid']);
		
		
		// 25.86 + 2.64 + 1.03
		$this->assertEquals(29.53, $values['total']);
		
		
	}
	
	
}