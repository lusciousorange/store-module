<?php
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php"); 
	
	$cart = getTCInstanceForClass('TMm_CEEA_ShoppingCart', $_GET['id']);
			
	if($cart->isComplete())
	{
		$cart->processIsTesting();
		
		foreach($cart->cartItems() as $cart_item)
		{
			$item = getTCInstanceForClass($cart_item->itemClass(), $cart_item->itemID());
			$item->processCartItem($cart_item);
		}
		//$cart->markCompleted();
		
		//$email = new TMv_PurchaseEmail($cart);
		//$email->send();
	}
	
	/**********************************************/
	/*										   	  */
	/*               PRINT WEBSITE                */
	/*										   	  */
	/**********************************************/
	print $tungsten->html();
	
?>