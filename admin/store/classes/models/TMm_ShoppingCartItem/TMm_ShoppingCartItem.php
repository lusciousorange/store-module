<?php
/**
 * Class TMm_ShoppingCartItem
 */
class TMm_ShoppingCartItem extends TCm_Model
{

	protected $cart_id = false;

	protected ?int $cart_item_id, $tax_1_percent, $tax_2_percent, $item_id, $quantity, $is_processed;
	protected ?string $item_class, $price_per_item, $discount_per_item;

	protected ?string $var_1, $var_2, $var_3, $var_4, $var_5, $var_6, $var_7, $var_8, $var_9, $var_10, $var_11, $var_12,
		$var_13, $var_14, $var_15, $var_16, $var_17, $var_18, $var_19, $var_20, $var_21, $var_22, $var_23, $var_24, $var_25, $var_26;


	/** @var TMi_ShoppingCartable $cartable_item */
	protected $cartable_item= false;
	
	protected $errors = false;
	
	protected $discount = false;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'cart_item_id';
	public static $table_name = 'shopping_cart_items';
	public static $model_title = 'Shopping Cart Item';
	public static $primary_table_sort = 'cart_item_id ASC';
	
	/**
	 * TMm_ShoppingCartItem constructor.
	 * @param array|int $cart_item_id
	 */
	public function __construct($cart_item_id)
	{
		parent::__construct($cart_item_id);
		if(!$this->exists) { return null; };
		
		$this->cartable_item = ($this->item_class)::init($this->item_id);
	}
	
	//////////////////////////////////////////////////////
	//
	// QUANTITY
	//
	// Methods related to the quantity of the cart item.
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Updates the quantity of the item in this cart
	 * @param int $quantity
	 */
	public function update($quantity)
	{
		if($quantity < 1)
		{
			TSv_GoogleAnalytics::trackEvent('remove_from_cart',[
				'currency' => 'CAD',
				'value' => $this->subtotal(),
				'items' => [TSv_GoogleAnalytics::itemValuesForModel($this)]
			
			]);
			$this->delete();
		}
		else
		{
			$this->updateDatabaseValue('quantity', $quantity);
			
			TSv_GoogleAnalytics::trackEvent('add_to_cart',[
				'currency' => 'CAD',
				'value' => $this->subtotal(),
				'items' => [TSv_GoogleAnalytics::itemValuesForModel($this)]
			
			]);
			
			
		}
	}
	
	/**
	 * Adds X amount of this item to the cart. Negative numbers are also permitted.
	 * @param int $quantity
	 */
	public function addQuantity($quantity)
	{
		$this->update($this->quantity + $quantity);
	}
	
	/**
	 * Returns the quantity for this cart item
	 * @return int
	 */
	public function quantity()
	{
		return $this->quantity;
	}
	
	/**
	 * Extend delete method to not give a message
	 * @param bool|string $action_verb
	 */
	public function delete($action_verb = false)
	{
		$this->quantity = 0;
		$this->cart()->clearCartItemFromHistory($this);
		parent::delete(false);
	}
	
	//////////////////////////////////////////////////////
	//
	// TAXES
	//
	// Methods related to taxes.
	//
	// *** Requires the taxes module in order to function properly
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the percentage for the provided tax
	 * @param TMm_Tax $tax
	 * @return float|int
	 */
	public function percentForTax($tax)
	{
		$tax_name = 'tax_'.$tax->id().'_percent';
		return $this->$tax_name;
	}
	
	/**
	 * Returns the value for the provided tax
	 * @param TMm_Tax $tax
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function valueForTax($tax, $use_sig_digits = false)
	{
		
		// Use the values in the cart and not the ones for the products themselves
		$value = $this->taxableAmount($tax) * $this->percentForTax($tax)/100;
		return $this->formatCurrency($value, $use_sig_digits);
		
		
	}
	
	/**
	 * Returns the total amount of tax for one of these items
	 *
	 * This method does not take quantity into account. If there are two taxes at 4% and 6%, it will return the value
	 * of what 10% would be for one item.
	 *
	 * @return string|float
	 */
	public function totalTaxPerItem()
	{
		$total_tax = 0;
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->models() as $tax)
		{
			$total_tax  += (float)$this->taxPerItemForTax($tax);
		}
		
		
		return round( $total_tax , 3);
	}
	
	/**
	 * Returns the taxable amount per item for a given tax
	 * @param TMm_Tax $tax
	 * @param bool $use_sig_digits
	 * @return string
	 */
	public function taxPerItemForTax($tax, $use_sig_digits = false)
	{
		// Use the values in the cart and not the ones for the products themselves
		$value = $this->pricePerItem() * $this->percentForTax($tax)/100;
		return $this->formatCurrency($value, $use_sig_digits);
	}
	
	/**
	 * Returns the amount that is taxable for this cart item
	 * @param TMm_Tax $tax
	 * @return float
	 */
	public function taxableAmount($tax)
	{
		if($this->item()->usesTax($tax))
		{
			return $this->subtotal();
		}
		return 0;
		
	}
	
	//////////////////////////////////////////////////////
	//
	// PRICE PER ITEM
	//
	// Methods related to the regular price per item that this is being sold for
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the price per item
	 * @return string|float
	 */
	public function pricePerItem()
	{
		return $this->formatCurrency($this->price_per_item);
	}
	
	/**
	 * Temporarily sets the price per item which updates the class value but not the database
	 * @param float $price
	 */
	public function temporarilySetPricePerItem($price)
	{
		$this->price_per_item = $price;
	}
	
	/**
	 * Updates the price per item
	 * @param float $new_price
	 */
	public function updatePricePerItem($new_price)
	{
		$this->updateDatabaseValue('price_per_item', $new_price);
	}
	
	/**
	 * Sets the price per item
	 * @param float $price
	 * @deprecated use updatePricePerItem
	 * @see TMm_ShoppingCartItem::updatePricePerItem()
	 */
	public function setPricePerItem($price)
	{
		$this->updatePricePerItem($price);
	}
	
	/**
	 * Resets this item to the original prices
	 */
	public function resetToItemPrices()
	{
		$values = array(
			'price_per_item' => $this->item()->price(),
			'discount_per_item' => $this->item()->discountPerItem()
		);
		$this->updateWithValues($values);
	}
	
	//////////////////////////////////////////////////////
	//
	// DISCOUNT PER ITEM
	//
	// Methods related to the discount price per item that this is being sold for
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the discount per item
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function discountPerItem($use_sig_digits = false)
	{
		return $this->formatCurrency($this->discount_per_item, $use_sig_digits);
		
	}
	
	/**
	 * Returns the discount per subtotal
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function discountForSubtotal($use_sig_digits = false)
	{
		$value = $this->discount_per_item * $this->quantity();
		return $this->formatCurrency($value, $use_sig_digits);
		
	}
	
	
	
	
	/**
	 * Returns if the item has a discount applied
	 * @return bool
	 */
	public function hasDiscount()
	{
		return  $this->discount_per_item > 0;
	}
	
	/**
	 * Returns the promotion for this cart item, if it exists
	 * @return bool
	 */
	public function promotion()
	{
		return false;
	}
	
	/**
	 * Updates the discount per item for this item
	 * @param float $discount
	 */
	public function updateDiscountPerItem($discount)
	{
		$this->updateDatabaseValue('discount_per_item', $discount);
	}
	
	/**
	 * Temporarily sets the discount per item which changes the value in the class without altering the database.
	 * @param float $discount
	 */
	public function temporarilySetDiscountPerItem($discount)
	{
		$this->discount_per_item = $discount;
	}
	
	//////////////////////////////////////////////////////
	//
	// PROCESSING
	//
	// Methods related to processing the items
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Updates this item to be marked as processed
	 */
	public function updateAsProcessed()
	{
		$this->updateDatabaseValue('is_processed', 1);
	}
	
	/**
	 * Returns if this cart item si processed
	 * @return bool
	 */
	public function isProcessed()
	{
		return $this->is_processed;
	}
	
	/**
	 * A method that can be overloaded by parent classes to which can be useful for saving on loading time.
	 *
	 * @return array An array of TCu_Items
	 */
	public function modelsToLoadWhenActive()
	{
		return array($this->cart());
	}
	
	
	//////////////////////////////////////////////////////
	//
	// TOTALLING
	//
	// Methods related to adding up the subtotals and
	// totals for this cart item.
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the subtotal for this item which calculates using the price per item. The discount per item is only
	 * provided as note and not as a calculation item.
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return string|float
	 */
	public function subtotal($use_sig_digits = false)
	{
		$price_per = $this->formatCurrency($this->price_per_item,$use_sig_digits);
		
		return $this->formatCurrency($price_per*$this->quantity, $use_sig_digits);
	}
	
	/**
	 * Returns the total including any taxes
	 * @param bool $use_sig_digits (Optional) Default false. Indicates if three significant digits should be returned.
	 * Otherwise the value is formatted to XX.YY
	 * @return float
	 */
	public function total($use_sig_digits = false)
	{
		$subtotal = $this->subtotal(true); // always use for initial calc
		$total = $subtotal;
		
		$tax_list = TMm_TaxList::init();
		
		foreach($tax_list->taxes() as $tax)
		{
			$tax_name = 'tax_'.$tax->id().'_percent';
			
			if($this->$tax_name > 0)
			{
				$total += $this->$tax_name/100 *$subtotal;
			}
		}
		
		return $this->formatCurrency($total, $use_sig_digits);
		
	}
	
	//////////////////////////////////////////////////////
	//
	// CART ITEM MODEL
	//
	// Each cart item has a model that it is connected to
	// There's a "thing" being sold which is a model
	// in Tungsten that uses the TMi_ShoppingCartable interface
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns the item in the cart
	 * @return TMi_ShoppingCartable|TCu_Item
	 */
	public function item()
	{
		return $this->cartable_item;
	}
	
	/**
	 * The name of the class that is a TMi_ShoppingCartable class name
	 * @return string
	 */
	public function itemClass()
	{
		return $this->item_class;
	}
	
	/**
	 * The ID of the item
	 * @return int
	 */
	public function itemID()
	{
		return $this->item_id;
	}
	
	/**
	 * The item's content code
	 * @return string
	 */
	public function itemContentCode()
	{
		return $this->item()->contentCode();
	}
	
	
	//////////////////////////////////////////////////////
	//
	// VARIABLES
	//
	// Cart items can have variables saved which provides
	// an open area to track cart-specific properties
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Sets one of the many variable options for this cart item. These are programmer specific
	 * @param int $number
	 * @param string $value
	 */
	
	public function setVariable($number, $value)
	{
		$variable_name = 'var_'.$number;
		
		$this->updateDatabaseValue($variable_name, $value);
	}
	
	/**
	 * Returns a variable with the provided number
	 * @param int $number
	 * @return string
	 */
	public function variable($number)
	{
		$variable_name = 'var_'.$number;
		return $this->$variable_name;
	}
	
	//////////////////////////////////////////////////////
	//
	// PURCHASE ITEM CONVERSION
	//
	// Methods to convert a cart into a purchase
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Converts this shopping cart item into a purchase item. This will also update the quantity of each product
	 * @param TMm_Purchase $purchase
	 */
	public function convertToPurchaseItem($purchase)
	{
		$values = $this->purchaseConversionValues($purchase);
		
		// VARIABLES
		for($var_num = 1; $var_num <= 26; $var_num++)
		{
			$field_name = 'var_'.$var_num;
			$values[$field_name] = @$this->$field_name;
		}
		
		if($purchase->isComp())
		{
			$zero_fields = array('price_per_item','discount_per_item','amount_paid','subtotal');
			
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				$zero_fields[] ='tax_'.$tax->id().'_paid';
				$zero_fields[] ='tax_'.$tax->id().'_paid_per_item';
				
			}
			
			foreach($zero_fields as $field)
			{
				$values[$field] = 0;
			}
			
		}
		
		
		/** @var TMm_PurchaseItem $purchase_item */
		$purchase_item = TMm_PurchaseItem::createWithValues($values);
		// In-Stock dealt with in createWithValues()
		
		$this->item()->processAfterPurchase($purchase_item);
	}
	
	/**
	 * Returns an associative array of values that will be used to convert the cart item into a purchase item
	 * @param TMm_Purchase $purchase
	 * @return array
	 */
	public function purchaseConversionValues($purchase)
	{
		$values = array();
		$values['purchase_id'] = $purchase->id();
		$values['quantity'] = $this->quantity();
		$values['cart_item_id']= $this->id();
		$values['product_id']= $this->item()->id();
		$values['price_per_item']= $this->pricePerItem();
		$values['discount_per_item']= $this->discountPerItem();
		$values['subtotal']= $this->subtotal();
		$values['item_class'] = $this->itemClass();
		$values['item_id'] = $this->itemID();
		
		// Saved to ensure long-term consistency
		$values['cart_title'] = $this->cartTitle();
		$values['cart_description'] = $this->cartDescription();
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			if($this->item()->usesTax($tax))
			{
				$values['tax_'.$tax->id().'_percent'] = $this->cart()->percentageForTax($tax);
			}
			else // tax not used, so zero percent
			{
				$values['tax_'.$tax->id().'_percent'] = 0;
			}
			
			$values['tax_'.$tax->id().'_paid'] = $this->valueForTax($tax, true);
			
			$tax_per_item = $this->pricePerItem() * $this->percentForTax($tax) / 100;
			$values['tax_'.$tax->id().'_paid_per_item'] = $this->formatCurrency($tax_per_item, true);
		}
		
		return $values;
	}
	
	public static function createWithValues(array $values, $bind_param_values = array(), $return_new_model = true)
	{
		$cart_item = parent::createWithValues($values, $bind_param_values, $return_new_model);
		
		TSv_GoogleAnalytics::trackEvent('add_to_cart',[
			'currency' => 'CAD',
			'value' => $cart_item->subtotal(),
			'items' => [TSv_GoogleAnalytics::itemValuesForModel($cart_item)]
		
		]);
		
		return $cart_item;
		
	}
	
	
	//////////////////////////////////////////////////////
	//
	// CARTS AND USERS
	//
	// Methods related to users for the cart
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the cart for this item
	 * @return TMm_ShoppingCart
	 */
	public function cart()
	{
		// Avoid double-load when trying to find the cart for this item
		// Most cases, already loaded, so return that
		$current_cart = TMm_ShoppingCart::init();
		if($current_cart->id() == $this->cart_id)
		{
			return $current_cart;
		}
		
		return TMm_ShoppingCart::init($this->cart_id);
	}
	
	/**
	 * Moves this cart item to a new shopping cart
	 * @param TMm_ShoppingCart $cart
	 */
	public function moveToCart($cart)
	{
		$this->updateDatabaseValue('cart_id', $cart->id());
	}
	
	
	/**
	 * @return bool|TMm_User
	 * @deprecated use user() instead
	 * @see TMm_ShoppingCartItem::user()
	 */
	public function member()
	{
		return $this->user();
	}
	
	/**
	 * Returns the user for this cart item
	 * @return bool|TMm_User
	 */
	public function user()
	{
		return $this->cart()->user();
	}
	
	/**
	 * Returns the title to be shown in the cart. In most cases, returning the title() works well, however it can be
	 * customized if necessary.
	 * @return string
	 */
	public function cartTitle()
	{
		return $this->item()->cartTitle($this);
	}
	
	
	/**
	 * Returns the cart description for this item
	 * @return string|TCv_View
	 */
	public function cartDescription()
	{
		return $this->item()->cartDescription($this);
	}
	
	//////////////////////////////////////////////////////
	//
	// ACCESS PERMISSIONS
	//
	//////////////////////////////////////////////////////
	
	public function userCanDelete($user = false)
	{
		return $this->cart()->userCanDelete();
	}
	
	public function userCanEdit($user = false)
	{
		return $this->cart()->userCanEdit();
	}
	
	public function userCanView($user = false)
	{
		return $this->cart()->userCanEdit();
	}
	
	/**********************************************/
	/*										   	  */
	/*             PAYPAL INTERFACE               */
	/*										   	  */
	/**********************************************/
	// ! ----- PAYPAL INTERFACE -----
	
	/* 	FUNCTION	: 	amountPaid
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[string]
	*/
	public function amountPaid()
	{
		return 0;
	}
	
	
	/* 	FUNCTION	: 	productTitle
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[string]
	*/
	public function productTitle()
	{
		return $this->cartable_item->cartTitle($this);
	}
	
	/* 	FUNCTION	: 	paypalDescription
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[string]
	*/
	public function paypalDescription()
	{
		return $this->cartable_item->cartDescription($this);
	}
	
	
	
	/* 	FUNCTION	: 	quantityOwing
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[float]
	*/
	public function quantityOwing()
	{
		return $this->quantity;
	}
	
	
	/* 	FUNCTION	: 	isExpectingCheque
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[string]
	*/
	public function isExpectingCheque()
	{
		return false;
	}
	
	/* 	FUNCTION	: 	paypalAmountPaid
		DESCRIPTION	:
		PARAMS		:
		RESULT		:	[string]
	*/
	public function paypalAmountPaid()
	{
		return 0;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ERRORS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns a list of errors for this cart item
	 * @return bool|string[]
	 */
	public function errors()
	{
		if($this->errors === false)
		{
			$this->errors = $this->item()->validateCartItem($this);
		}
		
		return $this->errors;
	}
	
	/**
	 * Returns if there are errors for this cart item
	 * @return bool
	 */
	public function hasErrors()
	{
		return count($this->errors()) > 0;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{

		$schema = parent::schema() + [
				
				'cart_id' => [
					'comment'       => 'The ID of the session for the cart',
					'type'          => 'TMm_ShoppingCart',
					'nullable'      => false,
					'foreign_key'   => [
						'model_name'    => 'TMm_ShoppingCart',
						'delete'        => 'CASCADE'
					],
				],
				'item_class' => [
					'comment'       => 'The model class name of the item in the cart',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				],
				
				'item_id' => [
					'comment'       => 'The id of the item, which might be a string value ',
					'type'          => 'varchar(255)',
					'nullable'      => false,
				
				],
				'quantity' => [
					'comment'       => 'The quantity for this particular item in the cart. ',
					'type'          => 'int(10)',
					'nullable'      => false,
				],
				'price_per_item' => [
					'comment'       => 'The cost for one of these items in the cart',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => false,
				],
				'discount_per_item' => [
					'comment'       => 'The amount discounted for one of these items in the cart',
					'type'          => 'float(11,3) unsigned',
					'nullable'      => false,
				],
				
				'is_processed' => [
					'comment'       => 'Indicates if the item has been processed. Use case specific.',
					'type'          => 'tinyint(1) unsigned',
					'nullable'      => false,
				],
			
			
			];


		// Add the variable columns
		for($i = 1; $i <= 26; $i++)
		{
			$schema['var_'.$i] = [
				'title'         => 'Variable '.$i,
				'comment'       => '',
				'type'          => 'varchar(128)',
				'nullable'      => false,
			];
	
		}
		
		
		return $schema;
	}
	
}
?>