<?php
class TMm_ShoppingCartItemTest extends TMm_StoreModelTestCase
{
	
	protected $shopping_cart;
	
	protected function setUp(): void
	{
		parent::setUp();
		$this->shopping_cart = $this->shoppingCart();
	}
	
	public function testAdjustQuantity()
	{
		// Create a test product
		$values = array(
			'title' => 'Sandwich',
			'price' => 4,
			'in_stock' => 15,
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		$this->assertEquals(1, $cart_item->quantity());
		
		$cart_item->addQuantity(5);
		$this->assertEquals(6, $cart_item->quantity());
		
		$cart_item->addQuantity(-2);
		$this->assertEquals(4, $cart_item->quantity());
		
		$cart_item->update(7);
		$this->assertEquals(7, $cart_item->quantity());
		
		$cart_item->update(0);
		$this->assertEquals(0, $cart_item->quantity());
		
		// Confirm the cart can't see it
		$this->assertFalse($this->shopping_cart->cartItemWithCartable($product));
		
		
	}
	
	public function testBasicItemMath()
	{
		// Create a test product
		$values = array(
			'title' => 'Bowl',
			'price' => 3.71,
			'in_stock' => 15,
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		
		// Add a quantity of 1
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		$this->assertTrue($cart_item instanceof TMm_ShoppingCartItem);
		$this->assertEquals(3.71, $cart_item->subtotal());
		$this->assertEquals(3.71, $cart_item->total(true));
		$this->assertEquals(3.71, $cart_item->total());
		
		// Add 2 more
		$cart_item->addQuantity(2);
		$this->assertEquals(11.13, $cart_item->subtotal());
		$this->assertEquals(11.13, $cart_item->total(true));
		$this->assertEquals(11.13, $cart_item->total());
		
	}
	
	public function testTaxedItemMath()
	{
		$tax_5 = static::tax_5_percent();
		
		// Create a test product
		$values = array(
			'title' => 'Pen',
			'price' => 3.71,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$this->assertTrue($product->usesTax($tax_5));
		
		
		// Add a quantity of 1
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		$this->assertTrue($cart_item instanceof TMm_ShoppingCartItem);
		$this->assertEquals(5, $cart_item->percentForTax($tax_5));
		
		// 3.71 *0.05
		$this->assertEquals(0.186, $cart_item->valueForTax($tax_5, true));
		$this->assertEquals(0.19, $cart_item->valueForTax($tax_5));
		
		// 3.71 * 1.05
		$this->assertEquals(3.896, $cart_item->total(true));
		$this->assertEquals(3.90, $cart_item->total());
		
		// ----- ADD TWO MORE -----
		
		$cart_item->addQuantity(2);
		
		// 3.71 * 3 * 0.05
		$this->assertEquals(0.557, $cart_item->valueForTax($tax_5, true));
		$this->assertEquals(0.56, $cart_item->valueForTax($tax_5));
		
		// 3.71 * 3
		$this->assertEquals(11.13, $cart_item->subtotal());
		
		// 11.13 + 0.557
		$this->assertEquals(11.687, $cart_item->total(true));
		$this->assertEquals(11.69, $cart_item->total());
		
	}
	
	public function testDiscountedTaxedItemMath()
	{
		$tax_5 = static::tax_5_percent();
		
		// Create a test product
		$values = array(
			'title' => 'Pencil',
			'price' => 3.71,
			'in_stock' => 15,
			'discount' => 0.15,
			'uses_tax_'.$tax_5->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$this->assertTrue($product->usesTax($tax_5));
		
		
		// Add a quantity of 1
		$cart_item = $this->shopping_cart->addToCart($product, 1);
		$this->assertTrue($cart_item instanceof TMm_ShoppingCartItem);
		$this->assertEquals(5, $cart_item->percentForTax($tax_5));
		$this->assertEquals(0.15, $cart_item->discountPerItem());
		$this->assertEquals(0.15, $cart_item->discountForSubtotal());
		$this->assertTrue($cart_item->hasDiscount());
		
		// (3.71 - 0.15) * 0.05
		$this->assertEquals(0.178, $cart_item->valueForTax($tax_5, true));
		$this->assertEquals(0.18, $cart_item->valueForTax($tax_5));
		
		// (3.71 - 0.15) + 0.178
		$this->assertEquals(3.738, $cart_item->total(true));
		$this->assertEquals(3.74, $cart_item->total());
		
		// ----- ADD TWO MORE -----
		
		$cart_item->addQuantity(2);
		
		$this->assertEquals(0.15, $cart_item->discountPerItem());
		$this->assertEquals(0.45, $cart_item->discountForSubtotal());
		
		// (3.71 - 0.15) *0.05 * 3
		$this->assertEquals(0.534, $cart_item->valueForTax($tax_5, true));
		$this->assertEquals(0.53, $cart_item->valueForTax($tax_5));
		
		// (3.71 - 0.15) * 3
		$this->assertEquals(10.68, $cart_item->subtotal());
		
		// ( (3.71 - 0.15) * 3 ) + 0.534
		$this->assertEquals(11.214, $cart_item->total(true));
		$this->assertEquals(11.21, $cart_item->total());
	
	}
	
	public function testPurchaseConversionValues()
	{
		$tax_5 = static::tax_5_percent();
		
		// Create a test product
		$values = array(
			'title' => 'Stapler',
			'price' => 4.73,
			'in_stock' => 15,
			'discount' => 1.21,
			'uses_tax_'.$tax_5->id() => 1
		);
		
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$cart_item = $this->shopping_cart->addToCart($product, 3);
		
		// Generate the values, passing in a null object for the ID
		$values = $cart_item->purchaseConversionValues(new TCm_Model(false));
		$this->assertEquals(3, $values['quantity']);
		$this->assertEquals($product->id(), $values['product_id']);
		$this->assertEquals(3.52, $values['price_per_item']);
		$this->assertEquals(1.21, $values['discount_per_item']);
		$this->assertEquals(10.56, $values['subtotal']);
		$this->assertEquals('TMm_StoreUnitTestProduct', $values['item_class']);
		$this->assertEquals($tax_5->percent(), $values['tax_'.$tax_5->id().'_percent']);
		$this->assertEquals(0.176, $values['tax_'.$tax_5->id().'_paid_per_item']);
		
	}
	
	/**
	 * Tests scenario where partial cents are being rounded in the total tax paid, but not in the per item
	 */
	public function testPartialCentConversionValueMultiplier()
	{
		$tax_5 = static::tax_5_percent();
		
		$values = array(
			'title' => 'Rounding Bowl',
			'price' => 6.48,
			'in_stock' => 15,
			'discount' => 1.40,
			'uses_tax_'.$tax_5->id() => 1
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		
		$cart_item = $this->shopping_cart->addToCart($product, 2);
		
		// Generate the values, passing in a null object for the ID
		$values = $cart_item->purchaseConversionValues(new TCm_Model(false));
		
		// 5.08 * 2
		$this->assertEquals(10.16, $cart_item->subtotal());
		$this->assertEquals(10.16, $values['subtotal']);
		
		// 5.08 * 0.05
		$this->assertEquals(0.254, $values['tax_'.$tax_5->id().'_paid_per_item']);
		
		// 5.08 * 0.05 * 2
		//$this->assertEquals(0.508, $cart_item->totalForTax($tax_5, true));
		$this->assertEquals(0.508, $values['tax_'.$tax_5->id().'_paid']);
		
		// 32.56 + 2.74 + 1.365
		//$this->assertEquals(36.665, $values['total']);
		
		
	}
	
}