<?php

/**
 * Class TMm_ShoppingCartList
 */
class TMm_ShoppingCartList extends TCm_ModelList
{
	/**
	 * TMm_ShoppingCartList constructor.
	 */
	public function __construct()
	{
		parent::__construct('TMm_ShoppingCart', false);
		
	}
	
	/**
	 * @param $filter_values
	 * @return TMm_Purchase[]
	 */
	public function processFilterList($filter_values)
	{
		$query = "SELECT c.* FROM shopping_carts c LEFT JOIN users u ON(c.user_id = u.user_id) ";
		$db_values = array();
		$where_clauses = array();
		$where_clauses[] = 'c.is_complete = 0';
		$where_clauses[] = 'c.user_id > 0';
		$where_clauses[] = 'c.session_id != "admin"';


//
		if($filter_values['search'] != '')
		{
			$where_clauses[] = "(c.cart_id = :search_full OR u.first_name LIKE :search OR u.email LIKE :search OR u.last_name LIKE :search OR CONCAT(u.first_name,' ',u.last_name) LIKE :search  )";
			$db_values[':search'] = '%'.$filter_values['search'].'%';
			$db_values[':search_full'] = $filter_values['search'];
		//	$db_values[':transaction_search'] = $filter_values['search'].'%'; // match start of string

		}

		// ----- COMBINE INTO RESULTS -----
		
		if(sizeof($where_clauses) > 0)
		{
			$query .= " WHERE ".implode(' AND ', $where_clauses);
			
		}
		
		
		$query .= "  ORDER BY c.".TMm_ShoppingCart::$primary_table_sort;
		
		$result = $this->DB_Prep_Exec($query, $db_values);
		return $result;

		
	}
	
	/**
	 * @param string $barcode
	 * @return TCm_Model|TMi_ShoppingCartable|bool
	 */
	public function findCartableItemWithBarcode($barcode)
	{
		$cartable_classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		foreach($cartable_classes as $class_name)
		{
			$query = "SELECT * FROM ".$class_name::$table_name." WHERE barcode = :barcode LIMIT 1";
			$result = $this->DB_Prep_Exec($query, array('barcode' => $barcode));
			if($row = $result->fetch())
			{
				$item = $class_name::init($row);
				return $item;
			}
			
		}
		
		return false;
	}
	
	/**
	 * This class leverages the existing searchable methods, which won't always exist
	 *
	 * @param string $search_string
	 * @param int $limit The limit on the number of results
	 * @return TCm_Model[]|TMi_ShoppingCartable[]
	 *
	 * @see TMt_Searchable
	 */
	public function findCartableItemsWithSearchString($search_string, $limit = 0)
	{
		$items = array();
		$cartable_classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		foreach($cartable_classes as $class_name)
		{
			$where_clauses[] = "`title` LIKE :search";
			
			// This method leverages the methods from `TMt_Searchable` which can be used or just filled in manually.
			if(method_exists($class_name,'searchQuerySelectPortion'))
			{
				$query = $class_name::searchQuerySelectPortion();
			}
			else
			{
				$query = "SELECT * FROM ".$class_name::$table_name ;
			}
			
			
			if(method_exists($class_name,'searchQueryWherePortion'))
			{
				$where_values = $class_name::searchQueryWherePortion();
			}
			else
			{
				$where_values['where'] = array();
				$where_values['parameters'] = array();
			}
			
			if(method_exists($class_name,'searchFields'))
			{
				$search_fields = $class_name::searchFields();
			}
			else
			{
				$search_fields = array('title');
			}
			
			
			
			$count = 1;
			$search_snippets = [];
			foreach($search_fields as $search_field_name)
			{
				$this_search_string = '%'.$search_string.'%';
				if(strpos($search_field_name,'%') !== false)
				{
					// We detect a %, so we swap out with the search string
					// This allows people to customize how the search works
					$temp_search_field_name = str_replace('%','',$search_field_name);
					$this_search_string = str_replace($temp_search_field_name,$search_string, $search_field_name);
					$search_field_name = $temp_search_field_name;
				}
				
				$search_snippets[] = ' '.$search_field_name.' LIKE :search_'.$count;
				$where_values['parameters']['search_'.$count] = $this_search_string;
				$count++;
			}
			$where_values['where'][] = '('.implode(' OR ', $search_snippets).') ';
			
			
			$query .= " WHERE ";
			if(sizeof($where_values['where']) > 0)
			{
				$query .= implode(' AND ', $where_values['where']);
				
			}
			
			
			if($limit > 0)
			{
				$query .= ' LIMIT '.$limit;
			}
			
			
			$result = $this->DB_Prep_Exec($query, $where_values['parameters']);
			while($row = $result->fetch())
			{
				$items[] = $class_name::init($row);
			}
			
		}
		
		return $items;
	}
}
