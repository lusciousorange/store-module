<?php

/**
 * Class TMm_SkipPayment
 *
 * A payment processor that can be used, which skips the entire processing of the purchase altogether
 */
class TMm_SkipPayment extends TCm_Model
{
	use TMt_PaymentProcessor;
	
	/**
	 */
	public function __construct()
	{
		parent::__construct('skip_payment');
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PaymentProcessor
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Extend the shopping cart functionality to deal with adding the items from the cart
	 * @param TMm_ShoppingCart $shopping_cart
	 */
	protected function hook_configureShoppingCart($shopping_cart)
	{

	}
	
	/**
	 * Enables the sandbox mode for this payment processor.
	 */
	protected function hook_configureSandboxMode()
	{
	}
	
	/**
	 * Hook method to configure the live mode. This method is called whenever someone sets the mode to live and this should
	 * reset to the original settings and undo any sandbox settings that might have been set
	 *
	 * @see TMt_PaymentProcessor::setUseSandbox()
	 */
	protected function hook_configureLiveTransactionMode()
	{
	
	}
	
	/**
	 * Returns the sandbox mode for this payment payment processor
	 * @return int
	 */
	public function defaultSandboxMode()
	{
		return 0;
	}
	
	/**
	 * A url that points to a page where sandbox credit card information can be found.
	 * @return string
	 */
	public function sandboxTestCardURL()
	{
		return '';
	}
	
	/**
	 * Hook method to configure the billing details. this varies with each processor, so it must be set
	 */
	protected function hook_configureBillingAddress()
	{

	}
	
	
	/**
	 * This is the method that performs the actual transaction to the payment processor.
	 * @return bool|string Returns a transaction ID or a boolean false if it was unsuccessful
	 */
	protected function performTransaction(): ?string
	{
		// Return the transaction ID which is just prefixed
		return 'SK_' . str_pad($this->shopping_cart->id(),8,'0', STR_PAD_LEFT);
		
	}
	
	
	
	/**
	 * This is the method that performs the actual transaction to the payment processor.
	 * @param TMm_PurchaseRefund $refund
	 * @return array An array of two values with a success boolean and a message
	 */
	protected function performRefund($refund)
	{
		return array('success' => true, 'message' => '');
	}
	
	
	/**
	 * Returns the human readable name for the processor
	 * @return string
	 */
	public static function processorName()
	{
		return 'Skip Payment';
	}
	
	/**
	 * Returns the name of the payment form for this processor
	 * @return string
	 */
	public static function paymentFormClassName()
	{
		return 'TMv_SkipPaymentForm';
	}
	
	/**
	 * Returns the string shown on the button for selecting it as a payment option
	 * @return string
	 */
	public function paymentOptionButtonTitle()
	{
		return 'Skip Payment';
	}
	
}