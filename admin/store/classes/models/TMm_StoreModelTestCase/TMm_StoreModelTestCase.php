<?php
class TMm_StoreModelTestCase extends TC_ModelTestCase
{
	protected static $tax_5_percent;
	
	protected function setUp () : void
	{
		$class_names = array(
			'TMm_Purchase', 'TMm_PurchaseItem', 'TMm_PurchaseList', 'TMm_PurchaseRefund', 'TMm_ShoppingCart', 'TMm_ShoppingCartItem',
			'TMm_ShoppingCartList', 'TMm_SkipPayment', 'TMm_StoreModuleSettings', 'TMm_Tax', 'TMm_TaxList'
		);
		$this->addResetModelClassNames($class_names);
		
		parent::setUp();
	}
	
	
	/**
	 * Creates and returns
	 * @return TMm_User
	 */
	public function generateUser()
	{
		$random_id = rand(100,10000);
		$values = array(
			'first_name' => 'First'.$random_id,
			'last_name' => 'Last',
			'email' => 'random_'.$random_id.'@domain.com',
		
		
		);
		
		return TMm_User::createWithValues($values);
	}
	
	/**
	 * Creates and returns the tax at 5% which is consistently loaded as a tax 1
	 * @return TMm_Tax
	 */
	public function tax_5_percent()
	{
		// detect if we've created this tax yet
		if(@!isset($GLOBALS['TC_classes']['classes']['TMm_Tax'][1]))
		{
			$values = array(
				'tax_id' => 1,
				'title' => 'Tax 5',
				'percent' => '5',
				'country' => 'CA',
				'applies_to_shipping' => 1,
				'number' => '5percenttax'
			
			);
			$tax_1 = $this->createTaxWithValues($values);
			
		}
		else
		{
			$tax_1 = TMm_Tax::init(1);
		}
		
		
		return $tax_1;
	}
	
	protected function clearTaxes()
	{
		$query = "DELETE FROM taxes"; // Doesn't truncate so future tests can still fill in ID #1
		$this->DB_Prep_Exec($query);
		unset($GLOBALS['TC_classes']['classes']['TMm_Tax']);
	}
	
	/**
	 * Handles the creation of the tax along with some caching
	 * @param array $values
	 * @return TMm_Tax
	 */
	protected function createTaxWithValues($values)
	{
		$tax = TMm_Tax::createWithValues($values);
		$tax->validateTaxTableColumns();
		TC_clearMemoryForClass('TMm_TaxList');
		
		return $tax;
	}

//
//	/**
//	 * Creates and returns
//	 * @return TMm_Tax
//	 */
//	public function pst()
//	{
//		if(is_null(static::$pst))
//		{
//			$values = array(
//				'title' => 'PST',
//				'percent' => '7',
//				'country' => 'CA',
//				'province' => 'MB',
//				'applies_to_shipping' => 0,
//
//			);
//			static::$pst = TMm_Tax::createWithValues($values);
//
//		}
//
//		return static::$pst;
//	}
	
	public function shoppingCart($override_values = array())
	{
		$user = $this->generateUser();
		
		$values = array(
			'title' => 'test cart',
			'session_id' => 'unit_test'.rand(100,10000),
			'user_id' => $user->id(),
			'first_name' => 'Unit',
			'last_name' => 'Test',
			'email' => 'unit_test@domain.com',
			'phone' => '(204) 987-1234',
			'billing_street' => '123 Unit Rd',
			'billing_street_2' => 'Apt 55',
			'billing_city' => "Winnipeg",
			'billing_province' => 'MB',
			'billing_country' => 'CA',
			'billing_postal' => 'U0N 1T5',
			'shipping_street' => '123 Unit Rd',
			'shipping_street_2' => 'Apt 55',
			'shipping_city' => "Winnipeg",
			'shipping_province' => 'MB',
			'shipping_country' => 'CA',
			'shipping_postal' => 'U0N 1T5',
			'delivery_date' => '2027-02-12 17:30:00',
		
		);
		
		foreach($override_values as $index => $value)
		{
			$values[$index] = $value;
		}
		
		return TMm_ShoppingCart::createWithValues($values);
		
	}
	
	/**
	 * Creates a shopping cart that can be used to create a purchase. Different purchases might require alterations
	 * to the cart before being processed.
	 * @return TMm_ShoppingCart
	 */
	protected function createFilledShoppingCart()
	{
		// Get an empty shopping cart
		$shopping_cart = $this->shoppingCart();
		
		$tax_5 = static::tax_5_percent();
		
		// ADD PRODUCT
		// Subtotal: $3.17
		// Tax 1 : 0.159
		// Total :  $3.329
		$values = array(
			'title' => 'Spoons',
			'price' => 3.17,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 1
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$shopping_cart->addToCart($product, 1);
		
		// ADD PRODUCT
		// Subtotal: $10.16
		// (6.48-1.40) * 2
		// Tax 1 :   $0.508
		// Total :   $10.668
		$values = array(
			'title' => 'Knives',
			'price' => 6.48,
			'in_stock' => 15,
			'uses_tax_'.$tax_5->id() => 1,
			'discount' => 1.40
		);
		$product = TMm_StoreUnitTestProduct::createWithValues($values);
		$shopping_cart->addToCart($product, 2);
		
		// DELIVERY
		// Subtotal: $1.77
		// Tax 1 :   $0.089
		// Total :   $1.859
		
		$values= array(
			'delivery_option_id' => '4965',
			'title' => 'Unit Test Delivery',
			'price' => 1.77,
			'uses_tax_'.$tax_5->id() => 1
		
		);
		$delivery_option = new TMm_StoreUnitTestDeliveryOption($values);
		
		// Change delivery
		$shopping_cart->setDeliveryWithModel($delivery_option);
		
		
		return $shopping_cart;
	}
	
}