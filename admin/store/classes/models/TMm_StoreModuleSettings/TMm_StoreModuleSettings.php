<?php
class TMm_StoreModuleSettings extends TSm_ModuleSettings
{
	/**
	 * TSm_Module constructor.
	 * @param TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
	}
	
	/**
	 * @return bool
	 */
	public function cartIsValid()
	{
		$cart = TMm_ShoppingCart::init();
		return !$cart->hasErrors();
	}
	
	/**
	 * @return bool
	 */
	public function cartIsEmpty()
	{
		$cart = TMm_ShoppingCart::init();
		return $cart->isEmpty();
	}
	
	
	
	
}
?>