<?php
class TMm_Tax extends TCm_Model
{
	protected string $title, $country, $province, $applies_to_shipping, $number;
	protected int $tax_id;
	protected float $percent;
	
	protected $view_path = '/admin/taxes/edit/';
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'tax_id';
	public static $table_name = 'taxes';
	public static $model_title = 'Tax';
	public static $model_title_plural = 'Taxes';
	
	/**
	 * TMm_Product constructor.
	 * @param array|int $tax_id
	 */
	public function __construct($tax_id)
	{
		parent::__construct($tax_id);
		if(!$this->exists) { return null; }
	}
	
	
	/**
	 * An internal method that validates if this tax is properly setup in all the other tables in which taxes are
	 * referenced by ID.
	 * @return TSu_InstallProcessor
	 */
	public function validateTaxTableColumns()
	{
		$installer = new TSu_InstallProcessor('installer');
		$installer->setLogQueriesInConsole(true);
		$installer->addTableRows(array('tax_'.$this->id().'_percent float(5,2) unsigned NOT NULL'), 'shopping_cart_items');
		
		$installer->addTableRows(
			array(
				'tax_'.$this->id().'_percent float(5,2) NOT NULL',
				'tax_'.$this->id().'_paid float(11,3) NOT NULL',
				'tax_'.$this->id().'_paid_per_item float(11,3) NOT NULL'
			), 'purchase_items');
		
		$installer->addTableRows(
			array(
				'tax_'.$this->id().'_percent float(5,2) unsigned NOT NULL',
				'tax_'.$this->id().'_paid float(11,3) unsigned NOT NULL'
			), 'purchases');
		
		return $installer;
	}
	
	/**
	 * @return string
	 */
	public function viewURL()
	{
		return $this->view_path.$this->id().'/'.$this->cleanForURL($this->title);
	}
	
	/**
	 * Returns the title for this item
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}
	
	/**
	 * Returns the title with an optional separator of the number
	 * @param string $separator
	 * @param bool $number_first Indicates if the number should be displayed first
	 * @return string
	 */
	public function titleWithNumber($separator = ' – ', $number_first = false)
	{
		
		$title = $this->title();
		
		if($this->number() != '')
		{
			if($number_first)
			{
				$title = $this->number().$separator.$title;
			}
			else
			{
				$title .= $separator.$this->number();
			}
			
		}
		
		return $title;
	}
	/**
	 * Returns the tax number for this tax. Optional value that represents something to be shown on all relevant
	 * documents
	 * @return string
	 */
	public function number()
	{
		return $this->number;
	}
	
	/**
	 * The percentage as a value from 0 to 100
	 * @return string
	 */
	public function percent()
	{
		return $this->percent;
	}
	
	/**
	 * The 2-digit country code for this tax
	 * @return string
	 */
	public function country()
	{
		return $this->country;
	}
	
	/**
	 * The 2-digit provice code for this tax
	 * @return string
	 */
	public function province()
	{
		return $this->province;
	}
	
	/**
	 * Indicates if it applies to shipping
	 * @return bool
	 */
	public function appliesToShipping()
	{
		return $this->applies_to_shipping;
	}

	/**
	 * @return string
	 */
	public function titleWithPercentAndLocations()
	{
		$title = $this->title().' ('.$this->percent().'%)';
		if($this->province() != '')
		{
			$title .= ', '.$this->province();
		}
		if($this->country() != '')
		{
			$title .= ', '.$this->country();
		}

		return $title;
	}
	
	
	/**
	 * Validates if the tax value is valid
	 * @param $tax_percentage The tax percentage
	 * @return bool
	 */
	public static function validateTaxPercentage($tax_percentage)
	{
		return $tax_percentage >= 0 && $tax_percentage <= 100;
		
	}
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema()
	{
		return parent::schema() + [
				
				'title' => [
					'comment'       => 'The title of the tax ',
					'type'          => 'varchar(255)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
						]
				],
				
				'number' => [
					'comment'       => 'The number for the tax, unique to the company',
					'type'          => 'varchar(64)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
					
					]
				],
				
				'percent' => [
					'comment'       => 'The percent value as a number from 0 to 100',
					'type'          => 'float(4,2)',
					'nullable'      => false,
					'validations'   => [
						'required'      => true,
						'methods'   => [
							[
								'method_name' => 'validateTaxPercentage',
								'parameter_field_names' => ['percent'],
								'error_message' => 'Tax must be between 0 and 100'
							],
						]
					]
				],
				
				'country' => [
					'comment'       => 'The two-digit code for the country, if applicable',
					'type'          => 'varchar(4)',
					'nullable'      => false,
				],
				
				'province' => [
					'comment'       => 'The two-digit code for the province or state, if applicable',
					'type'          => 'varchar(4)',
					'nullable'      => false,
				],
				
				'applies_to_shipping' => [
					'comment'       => 'Flag if it applies to shipping costs',
					'type'          => 'tinyint(1)',
					'nullable'      => false,
				],
			
			];
	}
	
}
