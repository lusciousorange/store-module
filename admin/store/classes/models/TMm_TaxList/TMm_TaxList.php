<?php

/**
 * Class TMm_TaxList
 */
class TMm_TaxList extends TCm_ModelList
{

	/**
	 * TMm_TaxList constructor.
	 * @param bool $init_model_list
	 */
	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_Tax',$init_model_list);
		
	}

	/**
	 * Returns all the taxes
	 * @return TMm_Tax[]
	 */
	public function taxes()
	{
		return $this->models();
	}

}

?>