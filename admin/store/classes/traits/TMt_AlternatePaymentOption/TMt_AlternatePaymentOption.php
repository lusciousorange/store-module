<?php
trait TMt_AlternatePaymentOption
{
	protected $payment_option_explanation = '';
	
	/**
	 * Returns if this payment option is permitted on unpaid purchases. Some options might not be allowable, so it
	 * must be defined for each payment option.
	 * @return bool
	 */
	public abstract function usableWithUnPaidPurchases();

	/**
	 * The title for this payment option
	 * @return string
	 */
	public abstract static function paymentOptionTitle();
	
	
	/**
	 * The brief explanation of this payment option
	 * @return string
	 */
	public function paymentOptionExplanation()
	{
		return $this->payment_option_explanation;
	}
	
	/**
	 * @param string $title
	 */
	public function setPaymentOptionExplanation($title)
	{
		$this->payment_option_explanation = $title;
	}
	
	/**
	 * An array of additional text fields that must be entered when using this option. The array must be an
	 * associative array with the index being the `field_id` and the value being the `title`
	 * @return string[]
	 */
	public function additionalPaymentTextFields()
	{
		return array();
	}
	
	/**
	 * processes the payment option with a card and form processor. That's everything needed to perform whatever
	 * transactions are necessary.
	 * @param TMm_ShoppingCart|TMm_Purchase $shopping_cart
	 * @param TCc_FormProcessor $form_processor
	 * @return array An associative array that has a `transaction_id` and `override_values` for processing the cart
	 * into a purchase.
	 */
	public abstract function processOptionPayment($shopping_cart, $form_processor);
	
	
	/**
	 * Returns the associative array of api fields for this payment processor. The value for each item must be
	 * another associative array that has two values for `title` and `help_text`.
	 *
	 * return array(
	 *     'field_id_1' => array(
	 *         'title' => 'Title',
	 *         'help_text' => 'Help Text',
	 *         'default' => 'Default Value' // OPTIONAL
	 *     )
	 * );
	 *
	 * @return array
	 */
	public static function apiFields()
	{
		return array(
			'payment_option_explanation' => array(
				'title' => 'Payment Option Explanation',
				'help_text' => '',
				
	     )
	  );
		
		
		
		
	}
	
	/**
	 * Auto collects the api values from the module setting and saves it to this option
	 */
	protected function configureAPIValues()
	{
		foreach($this->apiFields() as $field_id => $values)
		{
			$folder = $this->moduleForThisClass()->folder();
			$this->setAPIValue($field_id, TC_getModuleConfig($folder,$field_id));
		}
	}
	
	/**
	 * Sets an API value
	 * @param string $field_id
	 * @param string $value
	 */
	public function setAPIValue($field_id, $value)
	{
		$this->$field_id = $value;
	}
	
	/**
	 * Sets the api values with an array
	 * @param $array
	 */
	public function setAPIValues($array)
	{
		foreach($array as $field_id => $value)
		{
			$this->setAPIValue($field_id, $value);
		}
		
	}
	
}