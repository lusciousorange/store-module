<?php
trait TMt_CartNote
{
	protected $cart_or_purchase;
	/**
	 * @param TMm_FFT_Purchase|TMm_FFT_ShoppingCart $cart_or_purchase
	 */
	protected function configureForNotes($cart_or_purchase)
	{
		$this->cart_or_purchase = $cart_or_purchase;
		
		if($this->allowNoteEditing())
		{
			$this->columns = array(
				'photo' => array('title' => '', 'method' => 'photoCellForShoppingCartItem'),
				'title' => array('title' => 'Item', 'method' => 'titleCellForShoppingCartItem'),
				'edit_note' => array('title' => '', 'method' => 'editNoteCellForItem'),
				'price' => array('title' => 'Price', 'method' => 'priceCellForShoppingCartItem'),
				'quantity' => array('title' => 'Quantity', 'method' => 'quantityCellForShoppingCartItem'),
				'subtotal' => array('title' => 'Subtotal', 'method' => 'subtotalCellForShoppingCartItem'),
			);
		}
		$this->addClassCSSFile('TMt_CartNote');
		$this->addClass('TMt_CartNote');
		
	}
	
	/**
	 * INdicates if the note can be edited
	 * @return bool
	 */
	protected function allowNoteEditing()
	{
		
		// Option 1 : It's a shopping cart that is not done yet
		if($this->cart_or_purchase instanceof TMm_ShoppingCart )
		{
			// Cart isn't complete and this view isn't styled as a mini-cart, which doesn't have room
			if(!$this->cart_or_purchase->isComplete() && !$this->style_as_mini_cart )
			{
				return true;
			}
			
		}
		
		// Option 2 : It's a purchase and we're in the admin
		elseif($this->cart_or_purchase instanceof TMm_Purchase && TC_isTungstenView())
		{
			return true;
		}
		
		return false;
		
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// NOTE EDITING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The edit note form for the cart
	 * @param TMm_FFT_ShoppingCartItem|TMm_FFT_PurchaseItem $cart_item
	 * @return TCv_HTMLTableCell|bool
	 */
	protected function editNoteCellForItem($cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('edit_note_column');
		
		// Non-admin and customer notes not allowed
		if(!TC_isTungstenView() && !$cart_item->item()->allowCustomerNotes())
		{
			return $cell;
		}
		
		// Don't bother showing the link for things ths have notes.
		// Also don't show for purchase refunded items
		if($cart_item->note() != '' || ($cart_item instanceof TMm_PurchaseItem && $cart_item->isRefund() ) )
		{
			return $cell;
		}
		
		$cell->attachView($this->editNoteButton($cart_item));
	
		
		return $cell;
	}
	
	
	public function editNoteButton($cart_item)
	{
		$button = new TCv_Link();
		$button->addClass('edit_purchase_note_button');
		$button->setIconClassName('fa-pencil');
		$button->setURL('#');
		$button->addClass('action_button');
		$button->setTitle('Add Note');
		$button->addDataValue('purchase-item-id', $cart_item->id());
		$button->setAttribute('onclick',"$('#edit_note_form_".$cart_item->id()."').slideToggle(); $('#edit_note_form_"
		                               .$cart_item->id()." #note').focus(); return false;");
		return $button;
		
		
	}
	
	/**
	 * The edit note form for the cart
	 * @param TMm_FFT_ShoppingCartItem|TMm_FFT_PurchaseItem $item
	 * @return TCv_Form|bool
	 */
	public function editNoteFormForPurchaseItem($item)
	{
		if(!$this->allowNoteEditing())
		{
		}
		
		$allow_note_editing = $this->allowNoteEditing();
		
		// Non-admin and customer notes not allowed
		if(!TC_isTungstenView() && !$item->item()->allowCustomerNotes())
		{
			$allow_note_editing = false;
		}
		
		
		$form = new TCv_Form('edit_note_form_'.$item->id(), TCv_Form::$DISABLE_FORM_TRACKING);
		$form->addClass('edit_note_form');
		
		if($allow_note_editing)
		{
			$this->addClass('editable');
			if($item instanceof TMm_ShoppingCartItem)
			{
				$form->setAction('/admin/store/do/update-cart-item-note/' . $item->id());
			}
			else
			{
				$form->setAction('/admin/store/do/update-purchase-item-note/' . $item->id());
				
			}
		}
		
		$form->setMethod('get');
		
		
		$field = new TCv_FormItem_TextField('note', '');
		$field->setDefaultValue($item->note());
		
		if($item->note() == '')
		{
			$form->addClass('empty_note');
		}
		$field->addClass('cart_update_note');
		$field->disableAutoComplete();
		$field->setPlaceholderText('Add Note');
		$field->setShowTitleColumn(false);
		$form->attachView($field);
		
		$form->setButtonText('Update Note');
		
		// No Editing, hide button, remove action
		if(!$this->allowNoteEditing())
		{
			$form->hideSubmitButton();
			$form->setAction('#');
		}
		return $form;
		
	}
}