<?php

/**
 * Trait TMt_CartNoteItem
 *
 * A trait to be added to a TMm_ShoppingCartItem and/or TMm_PurchaseItem
 */
trait TMt_CartNoteItem
{
	protected $note;
	
	
	public function note()
	{
		return $this->note;
	}
	
	/**
	 * @param string $note
	 */
	public function updateNote($note)
	{
		$this->updateDatabaseValue('note', $note);
		TC_message('Note Updated');
	}
}