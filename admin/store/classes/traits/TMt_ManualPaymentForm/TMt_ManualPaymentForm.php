<?php
trait TMt_ManualPaymentForm
{
	protected $saved_cards = array();
	
	/**
	 * Returns the transaction types that are visible
	 * @return string[]
	 */
	public function visibleTransactionTypes()
	{
		$called_class = TMm_Purchase::classNameForInit();
		return $called_class::transactionTypes();
	}
	
	/**
	 * Returns the transaction types that are visible
	 * @return string[]
	 */
	public function visibleCreditCardTypes()
	{
		$called_class = TMm_Purchase::classNameForInit();
		
		return $called_class::$credit_card_types;
	}
	
	protected function configureManualPaymentFormCSSandJS()
	{
		$this->addClass('TMt_ManualPaymentForm');
		$this->addClassJQueryFile('TMt_ManualPaymentForm');
		$this->addClassJQueryInit('TMt_ManualPaymentForm');
		
	}
	
	protected function attachPaymentFormFields()
	{
		$this->configureManualPaymentFormCSSandJS();
		
		$field = new TCv_FormItem_Heading('main_heading', 'Manual Payment Form');
		$this->attachView($field);
		
		// Field to trigger looking for split payments
		$field = new TCv_FormItem_Select('is_split_payment', 'Split');
		$field->addOption('0','No – Single Payment');
		$field->addOption('1','Yes - Two Payments');
		$this->attachView($field);
		
		
		$this->attachView($this->splitPaymentHeadingField(1));
		$this->attachView($this->splitPaymentField(1));
		$this->attachView($this->splitTypeField(1));
		$this->attachView($this->splitCardField(1));
		$this->attachView($this->splitCreditCardTypeField(1));
		$this->attachView($this->splitTransactionIDField(1));
		
		// ------ SPLIT PAYMENT
		$split_group = new TCv_FormItem_Group('split_payment_group','');
		$split_group->addClass('split_payment_row');
		
		$split_group->attachView($this->splitPaymentHeadingField(2));
		$split_group->attachView($this->splitPaymentField(2));
		$split_group->attachView($this->splitTypeField(2));
		$split_group->attachView($this->splitCardField(2));
		$split_group->attachView($this->splitCreditCardTypeField(2));
		$split_group->attachView($this->splitTransactionIDField(2));
		
		$this->attachView($split_group);
	}
	
	/**
	 * @param int $num
	 * @return TCv_FormItem_HTML
	 */
	protected function splitPaymentHeadingField($num)
	{
		$field = new TCv_FormItem_HTML('first_payment_heading'.$num, '');
		$field->addText('<h3>'.$this->nth($num).' Payment</h3>');
		if($num == 1)
		{
			$field->addClass('split_payment_row');
		}
		
		return $field;
		
		
		
		
	}
	
	/**
	 * @param int $num
	 * @return TCv_FormItem_TextField
	 */
	protected function splitPaymentField($num)
	{
		
		$field = new TCv_FormItem_TextField('split_payment_amount_'.$num, $this->nth($num).' Amount $ ');
		$field->setDefaultValue($this->model()->total());
		if($num == 1)
		{
			$field->addClass('split_payment_row');
		}
		return $field;
		
	}
	
	/**
	 * @return bool
	 */
	protected function isRefundForm()
	{
		return $this instanceof TMv_PurchaseRefundForm;
	}
	
	/**
	 * @param int $num The number of the field, 1, 2, 3 etc
	 * @return TCv_FormItem_Select
	 */
	protected function splitTypeField($num)
	{
		$id = 'type_'.$num;
		if($num == 1)
		{
			$id = 'type'; // special case for default type
		}
		$field = new TCv_FormItem_Select($id, 'Transaction Type');
		$field->setHelpText('Indicate the transaction type. ');
		$field->addOption('', 'Select a transaction type');
		
		foreach($this->visibleTransactionTypes() as $code => $title)
		{
			// Don't show unpaid as an option. Does nothing.
			if($code != 'unpaid')
			{
				if($code == 'online')
				{
					if($this->isRefundForm() || count($this->saved_cards) > 0)
					{
						$field->addOption($code, $title);
					}
					else // online but no saved cards to charge to
					{
						$field->addOption($code, $title . ' – No Saved Cards');
						$field->disableOption($code);
					}
				}
				else // option is good
				{
					$field->addOption($code, $title);
				}
			}
		}
		
		if($num == 1)
		{
			$field->setIsRequired();
		}
		else
		{
			$field->setIsRequired(true);
		}
		
		
		return $field;
		
	}
	
	/**
	 * @param int $num
	 * @return bool|TCv_FormItem_Select
	 */
	protected function splitCardField($num)
	{
		// Saved Cards
		if($this->isRefundForm() || count($this->saved_cards) > 0)
		{
			$id = 'card_id_'.$num;
			if($num == 1)
			{
				$id = 'card_id'; // special case for default type
			}
			
			$field = new TCv_FormItem_Select($id, 'Credit Card');
			$field->setHelpText('Select a saved card for this person');
			//$field->addOption('', 'Select a card');
			foreach($this->saved_cards as $card)
			{
				$title = $card['number']. ' '.$card['expiry'];
				$field->addOption($card['id'], $title);
			}
			
			if(!$this->isRefundForm())
			{
				$card_message = new TCv_View();
				$card_message->addClass('card_warning_message');
				$card_message->addText('This card will be charged immediately for the amount indicated');
				$field->attachViewAfter($card_message);
			}
			return $field;
			
		}
		return false;
	}
	
	/**
	 * @param int $num
	 * @return TCv_FormItem_TextField
	 */
	protected function splitCreditCardTypeField($num)
	{
		$id = 'credit_card_type_'.$num;
		if($num == 1)
		{
			$id = 'credit_card_type'; // special case for default type
		}
		$field = new TCv_FormItem_Select($id, 'Card Type ');
		
		$field->setIsRequired(true);
		$field->addOption('','Select a Card Type');
		foreach($this->visibleCreditCardTypes() as $code => $type_values)
		{
			$field->addOption($code,$type_values['title']);
		}
		
		return $field;
		
	}
	
	
	/**
	 * @param int $num
	 * @return TCv_FormItem_TextField
	 */
	protected function splitTransactionIDField($num)
	{
		$id = 'transaction_id_'.$num;
		if($num == 1)
		{
			$id = 'transaction_id'; // special case for default type
		}
		$field = new TCv_FormItem_TextField($id, 'Note / Check # ');
		$field->setHelpText('Transaction Number, Check Number, Etc');
		//$field->setIsRequired();
		return $field;
		
	}
	
	/**
	 * Returns the string ending for the number 1st, 2nd, 3rd
	 * @param int $num
	 * @return string
	 */
	protected function nth($num)
	{
		if($num == 1)
		{
			return $num.'st';
		}
		elseif($num == 2)
		{
			return $num.'nd';
		}
		elseif($num == 3)
		{
			return $num.'rd';
		}
		
		return $num.'th';
	}
	
	
	/**
	 * A validation method that must be called from within the form validator
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function validateManualPaymentFormProcessor($form_processor)
	{
		/** @var TMm_Purchase $purchase */
		$purchase = $form_processor->model();
		
		// Online requires card id
		if($form_processor->formValue('type') == 'online')
		{
			$form_processor->validateIsBlank('card_id');
		}
		
		// VALIDATION FOR SPLIT VALUES
		if($form_processor->formValue('is_split_payment'))
		{
			$total = $purchase->total();
			$calc_total = $form_processor->formValue('split_payment_amount_1') + $form_processor->formValue('split_payment_amount_2');
			$form_processor->validateIsBlank('split_payment_amount_1');
			$form_processor->validateIsBlank('split_payment_amount_2');
			$form_processor->validateIsBlank('type_2');
			
			// Check for exact value between the two
			if($form_processor->formatCurrency($total) != $form_processor->formatCurrency($calc_total))
			{
				$form_processor->failFormItemWithID('split_payment_amount_1',
				                                    'Combined value of payments must add up to $' . $total.
				                                    '. They actually add up to $'.$calc_total);
			}
			
			
			if($form_processor->formValue('type_2') == 'online')
			{
				$form_processor->validateIsBlank('card_id_2');
				
				//BOTH ARE CREDIT CARDS
				if($form_processor->formValue('type') == 'online')
				{
					// Same CC chosen for both, that's just silly.
					if($form_processor->formValue('card_id') == $form_processor->formValue('card_id_2'))
					{
						$form_processor->failFormItemWithID('card_id', "Same credit card cannot be chosen for both payments.");
						
					}
				}
				else // Second is CC, but first isn't, which is a no go
				{
					$form_processor->failFormItemWithID('type', "Split payments with credit cards must have the first payment type be the credit card transaction.");
					
				}
			}
			
			
		}
	}
	
	/**
	 * Updates the purchase with the values from the form processor. This will potentially process payments and
	 * requires a purchase to already exist in the system.
	 * @param TMm_Purchase $purchase
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function processPaymentsForPurchase($purchase, $form_processor)
	{
		$processing_error = false;
		$payment_processor = $purchase->paymentProcessor();
		
		if($purchase->isPaid())
		{
			$form_processor->fail('The purchase was already marked as paid. No action was taken.');
			return;
		}
		
		
		// DEAL WITH SPLIT PAYMENT
		if($form_processor->formValue('is_split_payment'))
		{
			// ONLINE FOR PAYMENT 1
			if($form_processor->formValue('type') == 'online')
			{
				// We need to charge the card right here
				$transaction_id_1 = $payment_processor->chargeAmountToSavedCard(
					$form_processor->formValue('split_payment_amount_1'),
					$form_processor->formValue('card_id')
				);
				
				// Detect if we had an error
				if(!$payment_processor->statusIsSuccess())
				{
					$processing_error = true;
				}
			}
			else
			{
				$transaction_id_1 = $form_processor->formValue('transaction_id');
			}
			
			// ONLINE FOR PAYMENT 2
			if($form_processor->formValue('type_2') == 'online')
			{
				// We need to charge the card right here
				$transaction_id_2 = $payment_processor->chargeAmountToSavedCard(
					$form_processor->formValue('split_payment_amount_2'),
					$form_processor->formValue('card_id_2')
				);
				
				// Detect if we had an error
				if(!$payment_processor->statusIsSuccess())
				{
					$processing_error = true;
				}
			}
			else
			{
				$transaction_id_2 = $form_processor->formValue('transaction_id_2');
			}
			
			// Only mark as paid if both transaction IDs are valid
			if($processing_error)
			{
				$form_processor->fail(); // ensure failure. Other error messages will catch it
			}
			else
			{
				$purchase->markAsPaidWithTwoTransaction(
					$transaction_id_1,
					$form_processor->formValue('type'),
					$form_processor->formValue('split_payment_amount_1'),
					$form_processor->formValue('credit_card_type'),
					$transaction_id_2,
					$form_processor->formValue('type_2'),
					$form_processor->formValue('split_payment_amount_2'),
					$form_processor->formValue('credit_card_type_2')
				);
			}
		}
		else // SINGLE PAYMENT, FULL AMOUNT
		{
			
			// Deal with an card payment
			if($form_processor->formValue('type') == 'online')
			{
				// We need to charge the card right here
				$transaction_id = $payment_processor->chargeAmountToSavedCard(
					$purchase->total(),
					$form_processor->formValue('card_id')
				);
				
				// Detect if we had an error
				if(!$payment_processor->statusIsSuccess())
				{
					$processing_error = true;
				}
			}
			else
			{
				$transaction_id = $form_processor->formValue('transaction_id');
			}
			
			// Only mark as paid if transaction IDs are valid
			if($processing_error)
			{
				$form_processor->fail(); // ensure failure. Other error messages will catch it
			}
			else
			{
				// Use the method to process it
				$purchase->markAsPaidWithTransaction(
					$transaction_id,
					$form_processor->formValue('type'),
					$form_processor->formValue('credit_card_type'));
			}
			
		}
	}
}