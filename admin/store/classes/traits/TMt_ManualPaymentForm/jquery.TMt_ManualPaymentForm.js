(function ($)
{
	// INIT
	$.fn.TMt_ManualPaymentForm = function(options)
	{
		let $form = $('.TMt_ManualPaymentForm');

		var settings = $.extend(
			{
			}, options || {});

		return this.each(function()
		{
			// add listeners
			$form.find('select#is_split_payment').change(function(e) { splitChanged(); });
			$form.find('select#type').change(function(e) { typeChanged(1); });
			$form.find('select#type_2').change(function(e) { typeChanged(2); });
			splitChanged();
			typeChanged(1);
			typeChanged(2);

		});



		function splitChanged()
		{
			let is_split = $form.find('select#is_split_payment').val();
			if(is_split == '1')
			{
				$form.find('.split_payment_row').slideDown();
			}
			else
			{
				$form.find('.split_payment_row').slideUp();
			}

		}


		function typeChanged(num)
		{
			var suffix = '';
			if(num > 1)
			{
				suffix = '_'+num;
			}
			let type = $form.find('select#type'+suffix).val();

			// Remove all the required attributes
			$form.find('#credit_card_type'+suffix+'_row').find('select').val('').removeAttr('required');
			$form.find('#card_id'+suffix+'_row').find('select').removeAttr('required');


			if(type == 'online')
			{
				$form.find('#card_id'+suffix+'_row').slideDown();
				$form.find('#credit_card_type'+suffix+'_row').slideUp();
				$form.find('#transaction_id'+suffix+'_row').slideUp();

				$form.find('#card_id'+suffix+'_row').find('select').attr('required','required');

			}
			else if(type == 'debit')
			{
				$form.find('#card_id'+suffix+'_row').slideUp();
				$form.find('#transaction_id'+suffix+'_row').slideUp();
				$form.find('#credit_card_type'+suffix+'_row').slideDown();

				$form.find('#credit_card_type'+suffix+'_row').find('select').attr('required','required');
			}
			else if(type == '')
			{
				$form.find('#card_id'+suffix+'_row').slideUp();
				$form.find('#credit_card_type'+suffix+'_row').slideUp();
				$form.find('#transaction_id'+suffix+'_row').slideUp();

			}
			else
			{
				$form.find('#card_id'+suffix+'_row').slideUp();
				$form.find('#credit_card_type'+suffix+'_row').slideUp();
				$form.find('#transaction_id'+suffix+'_row').slideDown();
			}

		}

	};


//END THE WRAPPER
})(jQuery);
