<?php

/**
 * Trait TMt_PaymentForm
 *
 * A trait that contains commonly used fields and sections for forms related to payment.
 */
trait TMt_PaymentForm
{
	protected $require_phone_number = false;
	protected $show_billing_email_field = true;
	protected $show_billing_phone_field = true;
	
	public function hideBillingEmailField()
	{
		$this->show_billing_email_field = false;
	}
	
	public function hideBillingPhoneField()
	{
		$this->show_billing_phone_field = false;
	}

	/**
	 * Attaches the address fields to this form
	 * @param bool $required Indicates if we should require the appropriate values
	 * @param string $heading The title for the section
	 * @return TCv_FormItem_Group
	 */
	protected function billingAddressSection($required = true, $heading = 'Billing Information')
	{
		$this->addClassCSSFile('TMt_PaymentForm');
		$this->addClass('TMt_PaymentForm');
		
		$group = new TCv_FormItem_Group('billing_address_section','');
		$group->addClass('new_card_section');
		
		if($heading !== false)
		{
			$form_item = new TCv_FormItem_Heading('card_details_heading', $heading);
			$group->attachView($form_item);
		}
		
		
		/** @var TMt_Store_User|TMm_User $user */
		$user = TC_currentUser();
		
		if($this->show_billing_email_field)
		{
			$field = new TCv_FormItem_TextField('email', 'Email');
			if($required) { $field->setIsRequired(true); }
			$group->attachView($field);
		}
		if($this->show_billing_phone_field)
		{
			$field = new TCv_FormItem_TextField('phone', 'Phone');
			if($required && $this->require_phone_number) { $field->setIsRequired(true); }
			$group->attachView($field);
		}
		
		$field = new TCv_FormItem_TextField('billing_street', 'Street Address');
		if($required) { $field->setIsRequired(true); }
		$group->attachView($field);
		
		$field = new TCv_FormItem_TextField('billing_street_2', '');
		$group->attachView($field);
		
		$field = new TCv_FormItem_TextField('billing_city', 'City');
		if($required) { $field->setIsRequired(true); }
		$group->attachView($field);
		
		$group->attachView($this->provinceSelectField('billing',$required));
		
		
		$field = new TCv_FormItem_TextField('billing_postal', 'Postal Code');
		if($required) { $field->setIsRequired(true); }
		$group->attachView($field);
		
		$group->attachView($this->recentAddressField('billing'));
		
		
		return $group;
	}
	
	/**
	 * @param $disable_validation
	 * @param string $heading
	 * @return TCv_FormItem_Group
	 */
	protected function shippingAddressSection($disable_validation, $heading = 'Shipping Information')
	{
		$this->addClassCSSFile('TMt_PaymentForm');
		$this->addClass('TMt_PaymentForm');
		
		$shipping_group = new TCv_FormItem_Group('shipping_fields', 'Shipping Fields');
		
		if($heading !== false)
		{
			$form_item = new TCv_FormItem_Heading('shipping_heading', $heading);
			$shipping_group->attachView($form_item);
		}
		
		
		$field = new TCv_FormItem_TextField('shipping_street', 'Street');
		$field->setIsRequired($disable_validation);
		$shipping_group->attachView($field);
		
		$field = new TCv_FormItem_TextField('shipping_street_2', '');
		$shipping_group->attachView($field);
		
		$field = new TCv_FormItem_TextField('shipping_city', 'City');
		$field->setIsRequired($disable_validation);
		$shipping_group->attachView($field);
		
		
		$shipping_group->attachView($this->provinceSelectField('shipping'));
		
		$field = new TCv_FormItem_TextField('shipping_postal', 'Postal Code');
		$field->setIsRequired($disable_validation);
		$shipping_group->attachView($field);
		
		
		$field = new TCv_FormItem_Select('same_billing_shipping', 'Billing Address');
		$field->addOption('1','Use same address');
		$field->addOption('0','Use different address for billing');
		$shipping_group->attachView($field);
		
		$shipping_group->attachView($this->recentAddressField('shipping'));
		
		return $shipping_group;
	}
	
	/**
	 * @return TCv_FormItem_HTML|bool
	 */
	protected function recentAddressField($type = 'shipping')
	{
		// Generate the field
		$field = new TCv_FormItem_HTML($type.'_recent_addresses','Previous Addresses');
		$field->addClass('recent_address_row');
		// Track how many addresses we have
		$num_addresses = 0;
		// Add "recent addresses option"
		$model = $this->model();
		if($model instanceof TMm_ShoppingCart)
		{
			$user = $model->user();
			
			$last_address = $user->lastPurchaseAddressDetails();
			$address_values = @$last_address[$type]; // try to find
			
			
			// Ensure we have an address and that the province was a two-letter code
			if(isset($address_values) && strlen($address_values['province']) == 2)
			{
				
				$button = $this->updateAddressButtonForType($type, $address_values);
				$field->attachView($button);
				$num_addresses++;
				
			}
			
			if($user->hasAddress())
			{
				$button = $this->updateAddressButtonForType($type, $user->addressValues());
				$field->attachView($button);
				$num_addresses++;
			}
		}
		
		if($num_addresses > 0)
		{
			return $field;
		}
		
		return false;
	}
	
	/**
	 * @param string $type The type which is either 'shipping' or 'billing'
	 * @param array $values An array of address values with indices for address_1, address_2, city, province, country,
	 * postal_code
	 * @return TCv_Link
	 */
	protected function updateAddressButtonForType($type, $values)
	{
		$button = new TCv_Link();
		$button->addClass('recent_address_button');
		
		$address = $values['address_1'];
		if($values['address_2'] != '')
		{
			$address .= '<br />'.$values['address_2'];
		}
		$address .= '<br />'.$values['city'];
		$address .= ', '.$values['province'];
		$address .= ', '.$values['country'];
		$address .= '<br />'.$values['postal_code'];
		
		$button->addText($address);
		$button->setURL('#');
		$button->setAttribute('style','display:block;');
		$button->setAttribute('onclick',
		                      "$('#".$type."_street').val('".$values['address_1']."');
				                      $('#".$type."_street_2').val('".$values['address_2']."');
				                      $('#".$type."_city').val('".$values['city']."');
				                      $('#".$type."_province').val('".$values['province']."');
				                      $('#".$type."_postal').val('".$values['postal_code']."');
				                       return false;");
		
		return $button;
	}
	
	/**
	 * The province selection field, which is commonly extended to limit the options available.
	 * @param string $prefix The shipping/billing prefix
	 * @param bool $required
	 * @return TCv_FormItem_Select
	 *
	 */
	protected function provinceSelectField($prefix, $required = false)
	{
		$field = new TCv_FormItem_Select($prefix.'_province', 'Province/State');
		$field->addOption('','Select Province/State');
		$field->setDefaultValue('MB');
		if($required) { $field->setIsRequired(true); }
		
		foreach(TMm_ShoppingCart::countryCodeRegions() as $country_name => $country_values)
		{
			$field->startOptionGroup($country_values['code'], $country_name);
			
			foreach($country_values['provinces'] as $province_values)
			{
				$field->addOption($province_values['code'], $province_values['name']);
			}
		}
		
		return $field;
	}
	
	
}