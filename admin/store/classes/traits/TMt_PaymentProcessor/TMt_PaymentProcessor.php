<?php

/**
 * Trait TMt_PaymentProcessor
 *
 * A trait for payment processors which needs to be applied to any
 */
trait TMt_PaymentProcessor
{
	/** @var TMm_ShoppingCart $shopping_cart */
	protected $shopping_cart;
	protected $message = false;
	protected $total_amount = 0;
	protected $order_number;
	
	protected $processor_identifier;
	protected $payment_option;
	protected $processing_with_alternate_method = false;
	protected $token;
	protected $card_id;
	
	// SANDBOX MODES – This format stems from legacy support of previous systems
	// 1 Sandbox
	// 0 Live Transactions
	// 2 Skip Transactions
	
	protected $sandbox_mode = 0; // default to live transactions
	
	// Three Status Options
	// success : everything is good to keep going
	// abort : we stopped but there wasn't an error
	// failure : something went wrong and we want to indicate an error
	
	protected $processing_status = 'success';
	
	
	protected $billing = array(
		'first_name' => '',
		'last_name' => '',
		'email_address' => '',
		'phone_number' => '',
		'address_line1' => '',
		'address_line2' => '',
		'city' => '',
		'province' => '',
		'postal_code' => '',
		'country' => ''
	);
	
	
	/**
	 * Handles adding a shopping cart to this payment processor. The implementation of this method should handle
	 * anything related to costs for this payment processor.
	 *
	 * This should be the first action after initializing the payment processor
	 *
	 * @param TMm_ShoppingCart|TMm_Purchase $shopping_cart
	 */
	public function setShoppingCart($shopping_cart)
	{
		$this->shopping_cart = $shopping_cart;
		
		// Test if this cart is already being processed
		if(isset($_SESSION['payment_processing_cart_id']))
		{
			$this->addConsoleWarning('Abort : Double Transaction');
			$this->setStatusToAbort();
			$this->message = 'You accidentally attempted to process the cart twice';
			return;
		}
		
		// Avoid double trigger in case the cart is complete
		if($this->shopping_cart->isComplete())
		{
			$this->addConsoleWarning('Abort : Cart Is Complete');
			$this->setStatusToAbort();
			$this->message = 'Your cart was successfully processed';
			return;
		}
		
		// Don't process empty carts
		if($this->shopping_cart->numCartItems() == 0)
		{
			$this->addConsoleWarning('Abort : Empty Cart');
			$this->setStatusToAbort();
			$this->message = 'Your cart is empty. No transaction has occurred';
			return;
		}
		
		
		// Otherwise set that value to avoid duplicate calls
		$_SESSION['payment_processing_cart_id'] = $this->shopping_cart->id();
		
		// Fill in the billing info with the default buyer details
		// This requires the `TMt_Store_User` trait be applied to the user class for this site
		if($buyer = $this->shopping_cart->user())
		{
			$this->setBillingFirstName($buyer->firstName());
			$this->setBillingLastName($buyer->lastName());
			$this->setBillingEmailAddress($buyer->email());
			$this->setBillingPhoneNumber($buyer->cellPhone());
			$this->setBillingAddress1($buyer->address1());
			$this->setBillingAddress2($buyer->address2());
			$this->setBillingCity($buyer->city());
			$this->setBillingProvince($buyer->province());
			$this->setBillingPostalCode($buyer->postalCode());
			
			// Country set via the province
		}
		
		$this->total_amount = $this->shopping_cart->total();
		if($shopping_cart instanceof TMm_ShoppingCart)
		{
			$this->order_number = 'cart_' . $shopping_cart->id();
		}
		else // $shopping_cart instanceof TMm_Purchase
		{
			$this->order_number = 'purchase_' . $shopping_cart->id();
		}
		
		$this->hook_configureShoppingCart($this->shopping_cart);
	}
	
	/**
	 * Sets the order number, which is a unique identifier for this transaction
	 * @param string $order_number
	 * @return bool|void
	 */
	public function setOrderNumber($order_number)
	{
		$this->order_number = $order_number;
		
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// SANDBOX MODES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Configures the connection to the server which can potentially take into account settings related to module
	 * values coming from the settings or other items such as if the code should be setting it to skip or sandbox
	 * modes.
	 *
	 * This method MUST be called from the constructor
	 */
	protected function configureTransactionMode()
	{
		// DEAL WITH SKIP TRANSACTION MODE
		$this->setSandboxMode($this->defaultSandboxMode());
	}
	
	/**
	 * Enables the sandbox mode for this payment processor.
	 */
	public function setUseSandbox()
	{
		$this->sandbox_mode = 1;
		$this->hook_configureSandboxMode();
	}
	
	/**
	 * Returns the integer sandbox mode.
	 * @return int
	 */
	public function sandboxMode()
	{
		return $this->sandbox_mode;
	}
	
	/**
	 * Enables a mode where the transaction is skipped entirely.
	 */
	public function setUseLiveTransactions()
	{
		$this->sandbox_mode = 0;
		$this->hook_configureLiveTransactionMode();
	}
	
	/**
	 * Sets the sandbox mode for the processor
	 * @param int $mode
	 */
	public function setSandboxMode($mode)
	{
		if($mode > 0)
		{
			$this->setUseSandbox();
		}
		else
		{
			$this->setUseLiveTransactions();
		}
		
	}
	
	/**
	 * Returns if this processor is using the sandbox
	 * @return int
	 */
	public function usingSandbox()
	{
		return $this->sandbox_mode == 1;
	}
	
	/**
	 * Returns if this processor is using live transactions
	 * @return int
	 */
	public function usingLiveTransactions()
	{
		return $this->sandbox_mode == 0;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// PAYMENT OPTIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A function that accepts three parameters from the processing of a payment form, which then configures this
	 * payment processor accordingly
	 * @param string $card_option
	 * @param string $card_token
	 * @param bool $save_card
	 */
	public function setPaymentOptionForProcessing($card_option, $card_token, $save_card)
	{
		// New card, we asked for credit card fields most likely
		if($card_option == 'new')
		{
			if($card_token != '')
			{
				if($save_card)
				{
					$this->setPaymentAsNewCardWithSaving($card_token);
					return;
				}
				else
				{
					$this->setPaymentAsNewCardWithOneTimeToken($card_token);
					return;
				}
			}
			else // No token, traditional send it all to the server. Not PCI-Complaint
			{
				$this->setPaymentAsNewCardSendToServer();
				return;
			}
			
		}
		
		// Deal with a saved card ID being provided
		// Only integers means it's an ID
		elseif(ctype_digit($card_option))
		{
			$this->setPaymentAsSavedCardID($card_option);
		}
		else
		{
			$this->processing_with_alternate_method = true;
			$this->payment_option = $card_option; // saves a class name
		}
	}
	
	/**
	 * Sets the payment type to be a new card which is sent to the server. This is not ideal since it requires a
	 * higher PCI compliance but that's what's required of PayPal.
	 */
	public function setPaymentAsNewCardSendToServer()
	{
		$this->payment_option = 'new_card:send_to_server';
	}
	
	/**
	 * Sets the payment type to use a one-time token
	 * @param string $token
	 */
	public function setPaymentAsNewCardWithOneTimeToken($token)
	{
		$this->payment_option = 'new_card:one_time_token';
		$this->token = $token;
	}
	
	/**
	 * Sets the payment type to used to generate the saved card
	 * @param string $token
	 */
	public function setPaymentAsNewCardWithSaving($token)
	{
		$this->payment_option = 'new_card:save';
		$this->token = $token;
	}
	
	/**
	 * Sets the payment type to use a saved card ID. These IDs are stored in our local DB for reference and each
	 * payment processor deals with the result of this request differently.
	 * @param string $card_id
	 */
	public function setPaymentAsSavedCardID($card_id)
	{
		$this->payment_option = 'saved_card_id';
		$this->card_id = $card_id;
	}
	
	/**
	 * Returns the string shown on the button for selecting it as a payment option
	 * @return string
	 */
	public function paymentOptionButtonTitle()
	{
		return 'Pay with Card';
	}
	
	/**
	 * Returns if this payment option is a selectable option in the list. Almost all payment options would be true,
	 * however it's possible disable this.
	 * @return bool
	 */
	public function isSelectablePaymentOption()
	{
		return true;
	}
	
	//////////////////////////////////////////////////////
	//
	// STATUSES
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if the transaction was successful
	 * @return string 'success', 'abort', or 'failure'
	 */
	public function processingStatus()
	{
		return $this->processing_status;
	}
	
	/**
	 * @return bool
	 */
	public function statusIsSuccess()
	{
		return $this->processing_status == 'success';
	}
	
	/**
	 * @return bool
	 */
	public function statusIsAbort()
	{
		return $this->processing_status == 'abort';
	}
	
	
	/**
	 * @return bool
	 */
	public function statusIsFailure()
	{
		return $this->processing_status == 'failure';
	}
	
	public function setStatusToFailure()
	{
		$this->processing_status = 'failure';
		$this->addConsoleError('Processor Failed');
	}
	
	public function setStatusToAbort()
	{
		$this->processing_status = 'abort';
	}
	
	/**
	 * Returns the message for th
	 * @return bool
	 */
	public function message()
	{
		return $this->message;
	}
	
	/**
	 * Sets the message
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}
	
	//////////////////////////////////////////////////////
	//
	// BILLING INFORMATION
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Sets a billing value for this payment processor
	 * @param string $name
	 * @param string $value
	 */
	public function setBillingValue($name, $value)
	{
		$this->billing[$name] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingFirstName($value)
	{
		$this->billing['first_name'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingLastName($value)
	{
		$this->billing['last_name'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingEmailAddress($value)
	{
		$this->billing['email_address'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingPhoneNumber($value)
	{
		$this->billing['phone_number'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingAddress1($value)
	{
		$this->billing['address_line1'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingAddress2($value)
	{
		$this->billing['address_line2'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingCity($value)
	{
		$this->billing['city'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingProvince($value)
	{
		$this->billing['province'] = $value;
		$this->billing['country'] = TMm_ShoppingCart::countryCodeForProvince($value);
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingPostalCode($value)
	{
		$this->billing['postal_code'] = $value;
	}
	
	/**
	 * @param string $value
	 */
	public function setBillingCountry($value)
	{
		$this->billing['country'] = $value;
	}
	
	
	
	
	
	//////////////////////////////////////////////////////
	//
	// RUNNING TRANSACTIONS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The main method which runs the payment. This method handles the logic for each of the different scenarios that
	 * might arise. Any extension of this method should probably happen inside of the specific version or you should
	 * call parent::processPayment at the start of your methods.
	 * @param TCc_FormProcessor|null $form_processor The optional form processor which is used in some instances
	 * @return bool|TMm_Purchase
	 * @see TMt_PaymentProcessor::performTransaction()
	 * @see TMt_PaymentProcessor::convertShoppingCartToPurchase()
	 */
	public function processPayment(?TCc_FormProcessor $form_processor)
	{
		$purchase = false;
		
		// Configure the billing address
		$this->hook_configureBillingAddress();
		
		// We never want to risk running the payment, unless we're still in the successful status.
		if(!$this->statusIsSuccess())
		{
			$this->clearProcessingFlag();
			return false;
		}
		
		// Free / No-Cost Carts
		if($this->shopping_cart->subtotal() == 0)
		{
			// Generate a transaction ID that is tied to the cart
			$transaction_id = 'ZR_' . $this->shopping_cart->id();
			$purchase = $this->convertShoppingCartToPurchase($transaction_id);
			$this->clearProcessingFlag();
			return $purchase;
			
		}
		
		// Alternate Payment Methods
		// These are `TMt_AlternatePaymentOption` such as the option to pay later or a totally non-CC based option
		if($form_processor && $this->processing_with_alternate_method )
		{
			/** @var string|TMt_AlternatePaymentOption $payment_option_class_name */
			$payment_option_class_name = $this->payment_option;
			
			$payment_option = $payment_option_class_name::init();
			
			$response = $payment_option->processOptionPayment($this->shopping_cart, $form_processor);
			if($response['transaction_id'] !== false)
			{
				// We have a transaction ID, so that means we convert to a purchase
				$purchase = $this->convertShoppingCartToPurchase($response['transaction_id'], $response['override_values']);
			}
			else // something failed
			{
				$this->setStatusToFailure();
			}
			
			if(isset($response['message']) && $response['message'] !='')
			{
				TC_message($response['message']);
			}
			
			$this->clearProcessingFlag();
			return $purchase;
		}

		// Process the actual transaction
		$transaction_id = $this->performTransaction();
		$purchase = $this->purchaseForTransactionID($transaction_id);
		
		// Clear flags and return
		$this->clearProcessingFlag();
		return $purchase; // TMm_Purchase or boolean
	}
	
	
	/**
	 * Generates the purchase for given transaction ID. This might return an existing purchase if we're processing a
	 * payment for a purchase instead of cart.
	 * @param string $transaction_id
	 * @return bool|TMm_Purchase|TMm_ShoppingCart
	 */
	protected function purchaseForTransactionID($transaction_id)
	{
		if($transaction_id)
		{
			if($this->shopping_cart instanceof TMm_ShoppingCart)
			{
				return $this->convertShoppingCartToPurchase($transaction_id);
			}
			elseif($this->shopping_cart instanceof TMm_Purchase) // purchase, so mark it as paid
			{
				$purchase = $this->shopping_cart;
				$purchase->markAsPaidWithTransaction($transaction_id);
				return $purchase;
				
			}
		}
		else // No transaction means we failed
		{
			$this->setStatusToFailure();
		}
		
		return false;
	}
	
	/**
	 * Clears the internal flag to indicate we are currently processing a transaction
	 */
	protected function clearProcessingFlag()
	{
		unset($_SESSION['payment_processing_cart_id']);
	}
	
	/**
	 * Manually runs an amount against a saved card. This must be extended by the payment processor in order to work.
	 * @param float $amount
	 * @param int $card_id
	 * @return string
	 */
	public function chargeAmountToSavedCard($amount, $card_id)
	{
		TC_triggerError('Cannot attempt to charge amount to saved card');
		return 'transaction';
	}
	
	/**
	 * This method accepts a transaction ID and converts the cart to a purchase. THis is called after the payment
	 * processing occurs
	 * @param ?string $transaction_id
	 * @param array $override_properties An associative array of properties that should be used as overrides when
	 * processing the cart into a purchase.
	 * @return bool|TMm_Purchase
	 */
	protected function convertShoppingCartToPurchase(?string $transaction_id, $override_properties = array())
	{
		if($this->statusIsFailure())
		{
			$this->addConsoleError('Failed validation directly before converting Shopping Cart to Purchase');
			$this->message = 'There was an error processing the payment information';
			return false;
			
		}
		$override_properties['sandbox_mode'] = $this->sandboxMode();
		$purchase = $this->shopping_cart->convertToPurchase($transaction_id,$override_properties);
		
		
	
		if($purchase)
		{
			// Set values
			$values = [
				'message' => 'Purchase successful, ID: '.$transaction_id ,
				'class_name' => 'TMt_PaymentProcessor'
			];
			TSm_ErrorLogItem::createWithValues($values);
			// Save the address
			// Sometimes a duplicate action, however payment processor may have different billing details
			// TODO : remove this from here since processing normally handles it
			// Find scenarios where we don't ask for billing address and update the cart instead, before here
			$purchase->setBillingAddress(
				$this->billing['address_line1'],
				$this->billing['address_line2'],
				$this->billing['city'],
				$this->billing['province'],
				$this->billing['postal_code'],
				$this->billing['country']
			);
			
			$purchase->addEmailsToCron();
			return $purchase;
		}
		
		// This scenario means we encounter something completely unexpected.
		// The transaction ran but converting it failed
		// This involves a programmer and someone to review the logs
		else
		{
			// Set values
			$values = [
					'message' => 'No purchase created ' ,
					'class_name' => 'TMt_PaymentProcessor'
				];
			TSm_ErrorLogItem::createWithValues($values);
			
			$this->setStatusToFailure();
			$this->message = "Your transaction was processed however there was a technical issue with updating the system. Please contact us and refer to Cart #" . $this->shopping_cart->id();
			$this->shopping_cart->markCompleted(); // stop them from running it again
			return false;
			
		}
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// REFUNDS
	//
	// Methods related to refunding an existing purchase
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Handles the refund object. These objects are created
	 *
	 * @param TMm_PurchaseRefund $refund
	 */
	public function processRefund($refund)
	{
		$purchase = $refund->purchase();
		// Don't bother for non
		if($purchase->type() != 'online')
		{
			$this->setStatusToAbort();
			$this->message = 'Purchase was not made online. Refund must be done manually.';
		}
		else
		{
			// Check that we aren't refunding more than we should. If a refund exists, then the purchase item exists,
			// so it will affect the math. We need to add the refund total ot the purchase total to offset the fact
			// that we've already created the purchase item and it's being included in the math
			
			if($refund->total() > $purchase->total() + $refund->total())
			{
				$this->setStatusToAbort();
				$this->message = 'Attempting to refund more than the purchase amount available. Transaction Aborted.';
				return false;
			}
			
			// Process the actual transaction
			$this->performRefund($refund);
			
			// Clear the processing flag and return if we succeeded as a boolean
			$this->clearProcessingFlag();
			return $this->statusIsSuccess();
			
			
		}
		
		// Something went wrong
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// PROCESSING
	//
	// These methods are added to allow for smoother processing
	// and interaction with other systems
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns the human readable name for the processor
	 * @return string
	 */
	public static function processorName()
	{
		return get_called_class();
	}
	
	/**
	 * The identifier for instantiating the processor. By default it returns the name of the processor class which
	 * mean it will instantiate the only known one. That works for any single-payment system.
	 *
	 * Alternatively the value can be set to have a content-code for a given model. That model class requires a
	 * method called paymentProcessor() which returns the model we want.
	 * @return string
	 */
	public function processorIdentifier()
	{
		if($this->processor_identifier == null)
		{
			return get_called_class();
		}
		return $this->processor_identifier;
	}
	
	/**
	 * Sets the processor identifier
	 * @param string $identifier
	 */
	public function setIdentifier($identifier)
	{
		$this->processor_identifier = $identifier;
	}
	
	/**
	 * Returns the associative array of api fields for this payment processor. The value for each item must be
	 * another associative array that has two values for `title` and `help_text`.
	 *
	 * return array(
	 *     'field_id_1' => array(
	 *         'title' => 'Title',
	 *         'help_text' => 'Help Text'
	 *         'default' => 'Default Value' // OPTIONAL
	 *     )
	 * );
	 *
	 * @return array
	 */
	public static function apiFields()
	{
		return array();
	}
	
	//////////////////////////////////////////////////////
	//
	// SAVED CARDS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns an array of saved card values which includes an `id`, `number` and `expiry` values.
	 * @param bool|TMt_Store_User $user
	 * @return array
	 */
	public function savedCardsForUser($user = false)
	{
		return array();
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ABSTRACT METHODS
	//
	// These methods are more like an interface where they must
	// be defined and extended prior to the trait working as
	// expected
	//
	//////////////////////////////////////////////////////
	
	/**
	 * This is the method that performs the actual transaction to the payment processor. This method must be
	 * extended for every class that uses this trait.
	 *
	 * The extended functionality should also handle parsing the response and setting the appropriate flags
	 * @return null|string Returns the transaction id
	 *
	 * Additionally the billing information should be pulled and shared as well if necessary
	 */
	protected abstract function performTransaction() : ?string;
	
	/**
	 * Hook method to configure the sandbox mode. This method is called whenever someone sets the mode to sandbox and
	 * this likely means that the processor requires additional configuration to connect to the sandbox server
	 * instead of the live one.
	 *
	 * @see TMt_PaymentProcessor::setUseSandbox()
	 */
	protected abstract function hook_configureSandboxMode();
	
	/**
	 * Hook method to configure the live mode. This method is called whenever someone sets the mode to live and this should
	 * reset to the original settings and undo any sandbox settings that might have been set
	 *
	 * @see TMt_PaymentProcessor::setUseSandbox()
	 */
	protected abstract function hook_configureLiveTransactionMode();
	
	/**
	 * Returns the default sandbox mode for this payment payment processor. Usually this will return the value from
	 * the settings for the module.
	 * @return int
	 */
	public abstract function defaultSandboxMode();
	
	
	/**
	 * Hook method to configure the billing address. Each payment processor handles this information differently, so
	 * it is up to the processor to decide how to use the `billing` values that are currently saved with the processor.
	 */
	protected abstract function hook_configureBillingAddress();
	
	/**
	 * Hook method to perform additional configuration on the shopping cart
	 * @param TMm_ShoppingCart $shopping_cart
	 */
	protected abstract function hook_configureShoppingCart($shopping_cart);

	/**
	 * This is the method that performs the actual refund to the payment processor. This method must be
	 * extended for every class that uses this trait.
	 *
	 * The extended functionality should also handle parsing the response and setting the appropriate flags
	 * @param TMm_PurchaseRefund $refund
	 * Additionally the billing information should be pulled and shared as well if necessary
	 * @return array An array of two values with a success boolean and a message
	 */
	protected abstract function performRefund($refund);
	
	/**
	 * A url that points to a page where sandbox credit card information can be found. Return a blank string if there
	 * is none to reference.
	 * @return string
	 */
	public abstract function sandboxTestCardURL();
	
	/**
	 * Returns the name of the payment form for this processor
	 * @return string
	 */
	public abstract static function paymentFormClassName();
	
	
	
}