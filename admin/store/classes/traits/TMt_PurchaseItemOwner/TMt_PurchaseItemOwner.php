<?php

/**
 * Trait TMt_PurchaseItemOwner
 *
 * A trait that should be used by every model that can be a part of a purchase item. In some cases, the model that
 * was being purchased is not the same thing that will end up referencing a purchase item as part of a payment history.
 *
 * This trait can be applied to those classes to ensure consistent functionality in the system.
 *
 * Example A : A `TMm_BadgeType` is purchasable, but it's a single thing in the system can be purchased repeatedly, but
 * each time it's purchased, it creates a new `TMm_Badge`. The `TMm_BadgeType` would be `TMi_ShoppingCartable` however the
 * `TMm_Badge` would be the `TMt_PurchaseItemOwner`.
 *
 * Example B : A store has `TMm_Product` models that can be purchased. There is nothing generated other than the
 * purchase so the `TMm_Product` has the `TMi_ShoppingCartable` interface, but there is nothing with the
 * `TMt_PurchaseItemOwner` trait.
 *
 * Example C : A registration system lets you create a `TMm_Team` then pay for it. In this case the `TMm_Team` would
 * have both the `TMi_ShoppingCartable` interface (since it can be put in a cart) and it would also have the
 * `TMt_PurchaseItemOwner` trait since it needs to reference purchase items for payment history.
 */
trait TMt_PurchaseItemOwner
{
	protected $purchase_items = false;
	protected $purchase_item_ids;
	/**
	 * Returns the number of purchase items that exist for this item
	 * @return int
	 */
	public function numPurchaseItems()
	{
		return count($this->purchaseItems());
	}
	
	/**
	 * returns if this item has been purchased or not
	 * @return bool
	 */
	public function isPurchased()
	{
		return $this->numPurchaseItems() > 0;
	}
	
	/**
	 * Returns the purchase items related to this item
	 * @return TMm_PurchaseItem[]
	 */
	public function purchaseItems()
	{
		if($this->purchase_items === false)
		{
			$this->purchase_items = array();
			$items = explode(',', $this->purchase_item_ids);
			foreach($items as $purchase_item_id)
			{
				if(trim($purchase_item_id) != '')
				{
					// Avoid loading or recognizing if purchase item doesn't exist anymore
					$purchase_item = TMm_PurchaseItem::init($purchase_item_id);
					if($purchase_item)
					{
						$this->purchase_items[] = $purchase_item;
					}
					
				}
			}
		}
		return $this->purchase_items;
		
	}
	
	/**
	 * The amount paid in transactions
	 * @return float
	 * @deprecated Language is confusing.
	 * @see TMt_PurchaseItemOwner::subtotalPaid()
	 * @see TMt_PurchaseItemOwner::totalPaid()
	 *
	 */
	public function amountPaid()
	{
		return $this->subtotalPaid();
	}
	
	/**
	 * The amount paid in transactions without any taxes
	 * @return float
	 */
	public function subtotalPaid()
	{
		$total = 0;
		foreach($this->purchaseItems() as $purchase_item)
		{
			$total += $purchase_item->subtotal();
		}
		
		return $total;
		
	}
	
	/**
	 * The amount paid in transactions without any taxes
	 * @return float
	 */
	public function totalPaid()
	{
		$total = 0;
		foreach($this->purchaseItems() as $purchase_item)
		{
			$total += $purchase_item->total();
		}
		
		return $total;
	}
	
	/**
	 * Returns the amount paid as a formatted string with the dollar sign
	 * @return string
	 */
	public function amountPaidFormatted()
	{
		return '$'. number_format($this->totalPaid(),'2','.','');
	}
	
	/**
	 * Function set to be overridden which allows for scenarios when the item is still owing and can be paid. This
	 * must be defined by each of the classes using the trait.
	 * @return bool
	 */
	public function isOwing()
	{
		return $this->subtotalOwing() > 0;
	}
	
	/**
	 * Returns the amount of money owed
	 * @return float|string
	 * @deprecated Language around "amount' is ambiguous use total and subtotal methods instead
	 */
	public function amountOwing()
	{
		return $this->subtotalOwing();
	}
	
	/**
	 * Returns the amount of money owing without any taxes
	 * @return float|string
	 */
	public function subtotalOwing()
	{
		return 0;
		
	}
	
	/**
	 * Returns the amount of money owing including any taxes
	 * @return float|string
	 */
	public function totalOwing()
	{
		$amount_owing = $this->subtotalOwing();
		$total = $amount_owing;
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			if($this->usesTax($tax))
			{
				$tax_amount = $amount_owing * $tax->percent()/100;
				$total += $tax_amount;
				
			}
		}
		
		return $total;
	}
	
	/**
	 * Indicates if the item is deletable in refunds. This is different than userCanDelete()
	 * @return bool
	 */
	public function isRefundDeletable()
	{
		return true;
	}
	
}