<?php
/**
 * Trait TMt_Store_User
 *
 * A trait which adds functionality for a user that connects it to a store
 *
 *
 */
trait TMt_Store_User
{
	protected $address_1, $address_2, $city, $province, $postal_code, $country;
	
	protected $purchases = false;
	protected $purchase_query = "SELECT * FROM (SELECT * FROM purchases WHERE buyer_id = :buyer_id ORDER BY date_added DESC )
as temp_table GROUP BY LOWER(original_purchase_id) ORDER BY date_added DESC";

	/**
	 * Process the cart for the set of indicated promotions.
	 * @return TMm_Purchase[]
	 */
	public function purchases()
	{
		if($this->purchases === false)
		{
			$this->purchases = array();

			$result = $this->DB_Prep_Exec($this->purchase_query, array('buyer_id' => $this->id()));
			while($row = $result->fetch())
			{
				$this->purchases[] = TMm_Purchase::init($row);
			}
		}


		return $this->purchases;
	}
	
	/**
	 * Returns if this user can edit all carts
	 * @param bool $user
	 * @return bool
	 */
	public function userCanEditAllCarts($user = false)
	{
		if(!$user && !$user = TC_currentUser())
		{
			return false;
		}
		
		return $user->isAdmin();
	}
	
	//////////////////////////////////////////////////////
	//
	// ADDRESS FIELDS
	//
	// Methods related to the user's address. This may be defined in
	// the parent class but they are here to avoid potential call errors
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns if this user has all the necessary address fields filled out
	 * @return bool
	 */
	public function hasAddress()
	{
		return $this->address_1 != ''
			&& $this->city != ''
			&& $this->province != ''
			&& $this->country != ''
			&& $this->postal_code != ''
			;
	}
	
	/**
	 * @return string[]
	 */
	public function addressValues()
	{
		return array(
			'address_1' => $this->address_1,
			'address_2' => $this->address_2,
			'city' => $this->city,
			'province' => $this->province,
			'postal_code' => $this->postal_code,
			'country' => $this->country,
		);
	}
	
	/**
	 * @return string
	 */
	public function address1()
	{
		return $this->address_1;
	}
	
	/**
	 * @return string
	 */
	public function address2()
	{
		return $this->address_2;
	}
	
	/**
	 * returns city of user
	 * @return bool
	 */
	public function city()
	{
		return $this->city;
	}
	
	
	/**
	 * returns province of user
	 * @return bool
	 */
	public function province()
	{
		return $this->province;
	}
	
	
	/**
	 * returns postal code of user
	 * @return bool
	 */
	public function postalCode()
	{
		return $this->postal_code;
	}
	
	/**
	 * returns province of user
	 * @return bool
	 */
	public function country()
	{
		return $this->country;
	}
	
	public function lastPurchaseAddressDetails()
	{
		$values = array();
		$values['email'] = $this->email();
		$purchases = $this->purchases();
		if(count($purchases) > 0)
		{
			$last_purchase = array_values($purchases)[0];
			
			$values['phone'] = $last_purchase->phone();
			$values['billing'] = $last_purchase->billingAddressValues();
			$values['shipping'] = $last_purchase->shippingAddressValues();
			
		}
		
		return $values;
	}
	
	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////
	
	/**
	 * The schema for store users
	 * @return array[]
	 */
	public static function schema_TMt_Store_User()
	{
		return [
			'address_1' => [
				'type'=> 'varchar(255)',
				'nullable' => false
			],
			'address_2' => [
				'type'=> 'varchar(255)',
				'nullable' => false
			],
			'city' => [
				'type'=> 'varchar(255)',
				'nullable' => false
			],
			'province' => [
				'type'=> 'varchar(128)',
				'nullable' => false
			],
			'country' => [
				'type'=> 'varchar(128)',
				'nullable' => false
			],
			'postal_code' => [
				'type'=> 'varchar(10)',
				'nullable' => false
			],
		];
	}
}

?>