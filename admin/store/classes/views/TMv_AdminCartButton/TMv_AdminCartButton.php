<?php
class TMv_AdminCartButton extends TCv_Link
{
	public function render()
	{
		$this->addClassCSSFile('TMv_AdminCartButton');
		if(TC_currentUser() && TC_currentUser()->isAdmin())
		{
			$cart = TMm_ShoppingCart::adminCart();
			$this->setIconClassName('fa-shopping-cart');
			
			$inside = new TCv_View();
			$inside->addClass('inside');
			$inside->addText('Admin Cart');
			$this->attachView($inside);
			
			$count = new TCv_View();
			$count->addClass('count');
			$count->addText($cart->numCartItems());
			$this->attachView($count);
			$this->setURL('/admin/store/do/view-admin-cart');
		}
		
		
	}
}