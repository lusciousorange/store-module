<?php
class TMv_CartableSearchForm extends TCv_FormWithModel
{
	/** @var TMm_ShoppingCart|TMm_Purchase */
	protected $cart;
	
	/**
	 * TMv_CartableSearchForm constructor.
	 * @param TMm_ShoppingCart|TMm_Purchase $model
	 * @param bool $load_form_tracking
	 */
	public function __construct($model, $load_form_tracking = true)
	{
		parent::__construct($model, false);
		
		$this->setIDAttribute('TMv_CartableSearchForm');
		
		$this->setAction('#');
		
		$this->cart = $model;
		$init_values = array();
		$init_values['content_code'] = $model->contentCode();
		
		if($model instanceof  TMm_ShoppingCart)
		{
		
	//		$this->setAction('/admin/store/do/add-to-cart/'.$this->cart->id());
		}
		else
		{
		//	$this->setAction('/admin/store/do/add-to-purchase/'.$this->cart->id());
		
		
		}
		
		$this->addClassJQueryFile('TMv_CartableSearchForm','store');
		$this->addClassJQueryInit('TMv_CartableSearchForm',$init_values);
		$this->addClassCSSFile('TMv_CartableSearchForm');
		
		
		$this->setMethod('get');
		$this->setButtonText('Add Item');
		$this->setSubmitButtonID('TMv_CartableSearchForm_submit');
		$this->hideSubmitButton();
		
		
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('cartable_search','Search');
		$field->setFieldType('search');
		$field->setPlaceholderText('Find Items');
		$field->setShowTitleColumn(false);
		$field->disableAutoComplete();
		$this->attachView($field);
		
		$field = new TCv_FormItem_Hidden('cartable_cart_id','');
		$field->setValue($this->cart->id());
		$this->attachView($field);
		
		$result_container = new TCv_View('cartable_result_container');
		$this->attachView($result_container);
		
		//$item = TMm_FFT_ProductVariant::init(270);
		
		//$result_container->attachView($this->generateCartableResultLink($item));
	
		
		
	}
	
	/**
	 * @param TCm_Model|TMi_ShoppingCartable $item
	 * @return TCv_Link
	 */
	public function generateCartableResultLink($item)
	{
		if($this->cart instanceof TMm_ShoppingCart)
		{
			$url_base = '/admin/store/do/add-to-cart-with-id/';
		}
		else
		{
			$url_base = '/admin/store/do/add-to-purchase/';
		}
		
		
		$link = new TCv_Link() ;
		$link->addClass('cartable_search_result');
		$link->setURL($url_base.$this->cart->id().'?quantity=1&content_code='.$item->contentCode());
		
		$inside = new TCv_View();
		$inside->setTag('span');
		$inside->addClass('title');
		$inside->addText($item->cartTitle(false));
		$link->attachView($inside);
		
		
		
		$price = new TCv_View();
		$price->addClass('price');
		$price->setTag('span');
		
		$actual_price = $item->price() - $item->discountPerItem();
		
		$price->addText('$'.$this->formatCurrency($actual_price));
		if($item->discountPerItem() > 0)
		{
		
		}
		
		$link->attachView($price);
		
		
		return $link;
		
	}
}