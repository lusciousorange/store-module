<?php
	$skip_tungsten_authentication = true; // skip authentication to allow for loading from public pages
	require($_SERVER['DOCUMENT_ROOT']."/admin/system/headers/tungsten_header.php");
	set_time_limit(0);
	ob_start("ob_gzhandler");

	$model = TC_initClassWithContentCode($_POST['content_code']);
	if($model)
	{
		$shopping_cart_list = TMm_ShoppingCartList::init();
		
		$items = $shopping_cart_list->findCartableItemsWithSearchString($_POST['q']);
		$search_form = TMv_CartableSearchForm::init($model);
		
		foreach($items as $item)
		{
			$result_link = $search_form->generateCartableResultLink($item);
			print $result_link->html();
		}
		
		if(count($items) == 0)
		{
			print '<div class="no_results">No items found</div>';
		}
		//print $result_html;
		//
		
	}
	
	ob_end_flush();
?>