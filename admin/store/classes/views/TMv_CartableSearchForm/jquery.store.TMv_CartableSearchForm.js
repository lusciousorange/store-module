
(function ($)
{

	$.widget( "store.TMv_CartableSearchForm",
		{
			options: {
				'content_code'			: 	false,
				'active_search_ajax'	: 	false,
				'search_timeout'		: 	null,

				// Barcode Scanner Detection
				'use_barcode_scanning' : false,
				'barcode_timeout' : null,
				'last_search_batch' : '',
				'last_timestamp' : 0,
				'scanner_time_gap' : 30, // number of milliseconds allowable between keyup events


			},

			trackBarcode : function(event)
			{
				// Only bother with number keys AND
				// two very close events
				// THEN probably a barcode scan
				if(event.which == 13) // Detect enter, which ends it all
				{
					if (this.options.barcode_timeout != null)
					{
						clearTimeout(this.options.barcode_timeout );
					}

					// run it immediately
					this.updateSearchFieldWithBarcode();

				}
				if(
					(event.which >= 48 && event.which <= 57) &&
					(event.timeStamp - this.options.last_timestamp < this.options.scanner_time_gap)
				)
				{
					this.options.last_search_batch += String.fromCharCode(event.which);
					this.options.last_timestamp = event.timeStamp;

					if (this.options.barcode_timeout != null)
					{
						clearTimeout(this.options.barcode_timeout );
					}

					this.options.barcode_timeout  = setTimeout($.proxy(this.updateSearchFieldWithBarcode, this), 100);
				}
				else // We've hit the end for any number of reasons, deal with potentially typed barcode
				{
					this.options.last_search_batch = String.fromCharCode(event.which);
					// Any handling of this must happen in the timeout since we don't have anything to trigger after
					// the last key is pressed
				}
				this.options.last_timestamp = event.timeStamp;
			},

			updateSearchFieldWithBarcode : function()
			{
				// Kill the timeout value
				this.options.barcode_timeout = null;
				// If the string is 5 or more, it pretty
				if(this.options.last_search_batch.length >=3)
				{
					// Show loading
					enableCartLoadingScreen();

					$('#cartable_search').val(this.options.last_search_batch);

					var barcode = $('#cartable_search').val();
					var cart_id = $('#cartable_cart_id').val();

					var cart_view = $($('.TMv_ShoppingCart')[0]);
					var url = '/admin/store/do/';

					// Purchase
					if (cart_view.hasClass('TMv_PurchaseCart'))
					{
						url += 'add-to-purchase-with-barcode/' + cart_id + '/' + barcode;
					}
					else
					{
						url += 'add-to-cart-with-barcode/' + cart_id + '/' + barcode;
					}

					updateCartViewFromURLTarget(url);



					// Go back to the field, clear it and focus on it
					$('#cartable_search').val('').trigger('input').focus();
				}
			},

			searchFieldChanged : function()
			{
				// Cancel any ajax calls that might have happened
				if (this.options.active_search_ajax != false)
				{
					this.options.active_search_ajax.abort();
				}

				if($('#cartable_search').val().length < 3)
				{
					$('#cartable_result_container').hide();
					return;
				}

				if (this.options.barcode_timeout == null)
				{
					if (this.options.search_timeout != null)
					{
						clearTimeout(this.options.search_timeout);
					}

					this.options.search_timeout = setTimeout($.proxy(this.updateSearchResults, this), 500);


				}

			},

			updateSearchResults: function()
			{
				var search_string = $('#cartable_search').val();

				var url_data = [];
				url_data.push('content_code=' + encodeURI(this.options.content_code));
				url_data.push('q=' + encodeURI(search_string));

				this.options.active_search_ajax = jQuery.ajax(
					{
						url: '/admin/store/classes/views/TMv_CartableSearchForm/ajax_update_search_results.php',
						type: "POST",
						data: url_data.join('&'),
						success: function (html)
						{
							$('#cartable_result_container').html(html).show();


						}.bind(this)
					});

			},

			resultClicked : function(event)
			{
				event.preventDefault();

				var button = $(event.currentTarget);
			//	console.log(button.attr('href'));

				updateCartViewFromURLTarget(button.attr('href'));

				// Go back to the field, clear it and focus on it
				$('#cartable_search').val('').trigger('input').focus();



			},

			/**
			 * Creation method which initializes the widget
			 * @private
			 */
			_create: function()
			{
				$('#cartable_search').focus();
				this._on(
					{
						// "submit .filter_form"
						// 	: function(event) { event.stopPropagation(); event.preventDefault(); },

						'input #cartable_search'
							: function(event) { this.searchFieldChanged(event); },

						// 'submit form.filter_form'
						// 	: function(event) { event.preventDefault(); },

						'click .cartable_search_result'
							: function(event) { this.resultClicked(event); },


					});

				$(document).keyup(function(event) { this.trackBarcode(event); }.bind(this));

				// Hide the results when we're not in this field
				// Disabled. Breaks stuff like links and a11y
				//$('#cartable_search').blur(function(event){ $('#cartable_result_container').hide();});



			},


		});




// END THE WRAPPER
})(jQuery);
