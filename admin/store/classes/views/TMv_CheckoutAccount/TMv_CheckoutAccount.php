<?php
class TMv_CheckoutAccount extends TMv_CheckoutView
{
	use TMt_PagesContentView;

	protected $forgot_password_url = false;
	protected $forgot_password_menu_id;
	protected $edit_account_menu_id;
	protected $show_next_button = true;

	/**
	 * TMv_CheckoutAccount constructor.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addClassCSSFile('TMv_CheckoutAccount');

	}

	/**
	 * Sets the forgot password URL
	 * @param string $url
	 */
	public function setForgotPasswordURL($url)
	{
		$this->forgot_password_url = $url;
	}

	public function loggedInView()
	{
		$show_button = $this->show_next_button;
		$block = new TCv_View();
		$block->addClass('logged_in_view');

//		$view = new TCv_View();
//		$view->setTag('h2');
//		$view->addText('Logged In');
//		$block->attachView($view);

		$menu_item = TMm_PagesMenuItem::init($this->edit_account_menu_id);

		$p = new TCv_View();
		$p->setTag('h3');
		
		$link = new TCv_Link();
		$link->addClass('logout_link');
		$link->setURL('/logout/');
		$link->addText('Logout');
		$p->attachView($link);
		
		$p->addText(''.TC_currentUser()->fullName());
		$block->attachView($p);

		$p = new TCv_View();
		$p->setTag('p');
		$p->addText(TC_currentUser()->email());
		$p->addText('<br />'.TC_currentUser()->mobilePhone());
		$p->addText('<br />Joined on '.TC_currentUser()->dateAddedFormatted('F j, Y'));
		$block->attachView($p);

		// If the checkout stage is set to the account setting, move it forward since we're logged in already
		if($this->shopping_cart->checkoutStage() == $this->checkoutStage() && $show_button)
		{
			$this->shopping_cart->setCheckoutStageToNextAfter($this->checkoutStage());
		}

		if($show_button)
		{
			$next_button = new TMv_QuickButton();
			$next_button->setURL($this->nextCheckoutURL());
			$next_button->addText('Proceed With Checkout');
			$block->attachView($next_button);
		}
		
		
		return $block;
	}

	/**
	 * Returns the html for the checkout process. If user accounts are enabled it will show the login page unless they are logged in alreayd.
	 * @return string
	 */
	public function html()
	{
		if(!$this->forgot_password_url && $this->forgot_password_menu_id)
		{
			$menu_item = TMm_PagesMenuItem::init($this->forgot_password_menu_id);

			$this->forgot_password_url = $menu_item->pathToFolder();
		}

		if($this->shopping_cart->userAccountsEnabled())
		{


			// Cart has a user and they are logged in
			if($this->shopping_cart->user() && TC_currentUser())
			{
				$this->attachView($this->loggedInView());
			}
			else
			{
				$layout = new TMv_ContentLayout_2Column();

				$heading = new TCv_View();
				$heading->setTag("h2");
				$heading->addText('Sign In');
				$layout->attachViewToContentBlock($heading, 1);

				$sign_in_form= TMv_LoginForm::init();
				$sign_in_form->setSuccessURL($this->nextCheckoutURL());

				$layout->attachViewToContentBlock($sign_in_form, 1);

				if($this->forgot_password_url)
				{


					$p = new TCv_View();
					$p->setTag('p');

					$forgot_password = new TCv_Link();
					$forgot_password->setURL($this->forgot_password_url);
					$forgot_password->addText('Forgot Your Password?');
					$p->attachView($forgot_password);

					$layout->attachViewToContentBlock($p, 1);

				}

				$heading = new TCv_View();
				$heading->setTag("h2");
				$heading->addText('Create Account');
				$layout->attachViewToContentBlock($heading, 2);


				$create_form = TMv_CreateAccountForm::init();
				$create_form->setSuccessURL($this->nextCheckoutURL());
				$create_form->setUsePhoneNumber();
				$layout->attachViewToContentBlock($create_form, 2);


				$this->attachView($layout);
			}

		}
		else // No accounts, just basic information
		{
			$basic_form = TMv_ShoppingCartBasicAccountForm::init($this->shopping_cart);
			$view_url = $this->shopping_cart->checkoutStageViewURL($this->shopping_cart->nextCheckoutStageCode($this->checkout_stage));
			$basic_form->setSuccessURL($view_url);
			$this->attachView($basic_form);
		}


		return parent::html();
		
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();


		$field = new TCv_FormItem_Select('forgot_password_menu_id','Forgot Password Page');
		$field->setHelpText('Select the page that the forgot password link will point to');

		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}

			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}

		$form_items[] = $field;

		$field = new TCv_FormItem_Select('edit_account_menu_id','Edit Account Password Page');
		$field->setHelpText('Select the page that is used for editing their account');

		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}

			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}

		$form_items[] = $field;


		$field = new TCv_FormItem_Select('show_next_button','Show Next Button');
		$field->setIsRequired();
		$field->setDefaultValue(1);
		$field->addOption(0,'No');
		$field->addOption(1,'Yes');

		$form_items[] = $field;




		return $form_items;

	}



	public static function pageContent_ViewTitle(): string
	{ return 'Checkout – Account'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The checkout page for the account.';
	}


}

?>