<?php
class TMv_CheckoutAddress extends TMv_CheckoutView
{
	use TMt_PagesContentView;
	
	
	/**
	 * TMv_CheckoutDelivery constructor.
	 * @param TMv_CheckoutAddress $cart
	 */
	public function __construct($cart = null)
	{
		parent::__construct($cart);
		$this->addClassCSSFile('TMv_CheckoutAddress');

	}


			
	public function html()
	{
		/** @var TMv_ShoppingCartAddressForm $basic_form */
		$basic_form = TMv_ShoppingCartAddressForm::init($this->shopping_cart);
		
		if($this->shopping_cart->checkoutStages())
		{
			$view_url = $this->shopping_cart->checkoutStageViewURL($this->shopping_cart->nextCheckoutStageCode($this->checkout_stage));
			$basic_form->setSuccessURL($view_url);
		}
		$this->attachView($basic_form);


		return parent::html();
		
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		return parent::pageContent_EditorFormItems();
	}


	public static function pageContent_ViewTitle(): string
	{ return 'Checkout – Address'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The checkout page for the addresses.';
	}


}

?>