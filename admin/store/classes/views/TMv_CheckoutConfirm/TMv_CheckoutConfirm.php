<?php
class TMv_CheckoutConfirm extends TMv_CheckoutView
{
	use TMt_PagesContentView;
	protected $is_email = false;
	protected $show_purchase_update_section = true;
	protected $heading_tag = 'h2';

	/** @var bool|TMm_ShoppingCart|TMm_Purchase $shopping_cart */


	protected $adjustable_column_views , $success_menu_id;

	/**
	 * TMv_CheckoutConfirm constructor.
	 * @param TMm_ShoppingCart|TMm_Purchase|bool $cart_or_purchase
	 */
	public function __construct($cart_or_purchase = false)
	{
		parent::__construct($cart_or_purchase);
		$this->addClassCSSFile('TMv_CheckoutConfirm');

		if($cart_or_purchase)
		{
			$this->shopping_cart = $cart_or_purchase;
		}

		if($this->isShoppingCartView() && !$this->isLoadingInPageBuilder())
		{
			// Special case where we only want to show the confirm view if the cart is actually meant to be on confirm
			// If that's not hte case, we redirect them to the appropriate stage
			if($this->shopping_cart->checkoutStages() != false && $this->shopping_cart->checkoutStage() !== 'confirm')
			{
				header("Location: ".$this->shopping_cart->checkoutStageViewURL());
				exit();
			}
		}

		$this->adjustable_column_views = array();


	}
	
	/**
	 * Returns the purchase which is a safety function that makes it easier make calls internally
	 * @return bool|TMm_Purchase
	 */
	public function purchase()
	{
		if($this->isPurchaseView())
		{
			return $this->shopping_cart;
		}
		else
		{
			TC_triggerError('Accessing cart in purchase view mode');
		}
	}

	public function hidePurchaseUpdateSection()
	{
		$this->show_purchase_update_section = false;
	}

	/**
	 * Sets this view as being loaded in an email, which may be used to adjust the layout
	 */
	public function setAsEmailView()
	{
		$this->hidePurchaseUpdateSection();
		$this->is_email = true;

	}
	
	
	/**
	 * Returns if this item is a purchase view
	 * @return bool
	 */
	public function isPurchaseView()
	{
		return !$this->isShoppingCartView();
	}
	
	
	/**
	 * Returns if this item is a shopping cart view
	 * @return bool
	 */
	public function isShoppingCartView()
	{
		return $this->shopping_cart instanceof TMm_ShoppingCart;
	}
	
	
	
	
	/**
	 * The view that contains the shopping cart
	 * @return bool|TCv_View
	 */
	public function shoppingCartView()
	{
		$view = new TCv_View();
		$view->addClass('shopping_cart_view');

		

		// Add cart without the ability to edit
		$cart = TMv_ShoppingCart::init($this->shopping_cart);
		$cart->disableEditing();
		$view->attachView($cart);


		return $view;
	}



	/**
	 * The view that contains the contact info
	 * @return bool|TCv_View
	 */
	public function contactInfoView()
	{
		$view = new TCv_View();
		$view->addClass('contact_info_view');

		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		$heading->addText('Contact Details');
		$view->attachView($heading);

		if($user = $this->shopping_cart->buyer())
		{
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText($user->firstName().' '.$user->lastName());
			
			if($this->shopping_cart->email() != '')
			{
				$p->addText('<br /><a href="mailto:'.$this->shopping_cart->email().'">'.$this->shopping_cart->email().'</a>');
			}
			elseif($user->cellPhone() != '')
			{
				$p->addText('<br /><a href="mailto:'.$user->email().'">'.$user->email().'</a>');
			}
			
		
//			if($user->homePhone() != '')
//			{
//				$p->addText('<br />Home: '.$user->homePhone());
//			}
			
			if($this->shopping_cart->phone() != '')
			{
				$p->addText('<br />Phone: '. TCu_Text::formatPhoneNumber( $this->shopping_cart->phone() ));
			}
			elseif($user->cellPhone() != '')
			{
				$p->addText('<br />Phone: '.TCu_Text::formatPhoneNumber( $user->cellPhone() ));
			}
			$view->attachView($p);
		}
		else
		{
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText($this->shopping_cart->firstName().' '.$this->shopping_cart->lastName());
			$p->addText('<br />'.$this->shopping_cart->email());
			$p->addText('<br />'. TCu_Text::formatPhoneNumber( $this->shopping_cart->phone()) );
			$view->attachView($p);
		}

		return $view;

	}

	/**
	 * The view that contains the shipping address
	 * @param string $title
	 * @return bool|TCv_View
	 */
	public function shippingAddressView($title = 'Shipping Address')
	{
		if(!$this->shopping_cart->deliveryIsUsed())
		{
			return false;
		}

		$view = new TCv_View();
		$view->addClass('shipping_address_view');

		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		$heading->addText($title);
		$view->attachView($heading);

		$p = new TCv_View();
		$p->setTag('p');
		$p->addText($this->shopping_cart->shippingAddressFormatted());
		$view->attachView($p);


		return $view;

	}

	/**
	 * The view that contains the billing address
	 * @param string $title
	 * @return bool|TCv_View
	 */
	public function billingAddressView($title = 'Billing Address')
	{
		if($this->shopping_cart->showBillingAddress())
		{
			$view = new TCv_View();
			$view->addClass('billing_address_view');

			$heading = new TCv_View();
			$heading->setTag($this->heading_tag);
			$heading->addText($title);
			$view->attachView($heading);

			$p = new TCv_View();
			$p->setTag('p');
			$p->addText($this->shopping_cart->billingAddressFormatted());
			
			$p->addText('<br />Phone: '. TCu_Text::formatPhoneNumber($this->shopping_cart->phone()) );
			
			$view->attachView($p);


			return $view;

		}

		return false;
	}

	/**
	 * The details for delivery
	 * @param string $title
	 * @return bool|TCv_View
	 */
	public function deliveryView($title = 'Delivery')
	{
		if($this->isShoppingCartView() ||  !$this->shopping_cart->deliveryIsUsed())
		{
			return false;
		}

		$view = new TCv_View();
		$view->addClass('delivery_view');

		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		$heading->addText($title);
		$view->attachView($heading);

		$heading = new TCv_View();
		$heading->setTag('h4');
		$heading->addText($this->shopping_cart->deliveryTitle());
		$view->attachView($heading);

		$p = new TCv_View();
		$p->setTag('p');
		$p->addText('$'.$this->shopping_cart->deliveryPrice());
		$view->attachView($p);


		if($this->shopping_cart->instructions() != '')
		{
			$heading = new TCv_View();
			$heading->setTag('h4');
			$heading->addText('Instructions');
			$view->attachView($heading);

			$p = new TCv_View();
			$p->setTag('p');
			$p->addText(nl2br($this->shopping_cart->instructions()));
			$view->attachView($p);

		}
		return $view;

	}
	
	/**
	 * Shows payment view details
	 * @return bool|TCv_View
	 */
	public function paymentView()
	{
		return false;
	}
	

	
	/**
	 * The view that contains the confirmation form
	 * @return TMv_ShoppingCartConfirmForm|bool
	 */
	public function confirmFormView()
	{
		if($this->isShoppingCartView())
		{
			$view = TMv_ShoppingCartConfirmForm::init();
			$menu_item = TMm_PagesMenuItem::init( $this->success_menu_id);
			if($menu_item)
			{
				$view->setSuccessURL($menu_item->pathToFolder());

			}
			return $view;

		}
		return false;

	}

	/**
	 * Adds a view that is laid out in the adjustable columns. This ensures that views can be added or hidden and the
	 * layout will always maintain 2 or 3 columns depending on the number of views.
	 * @param bool|TCv_View $view The view to be added which will only happen if it's an instance of TCv_View
	 */
	public function addAdjustableColumnView($view)
	{
		if($view instanceof TCv_View)
		{
			$this->adjustable_column_views[] = $view;
		}

	}

	protected function attachAdjustableColumnViews()
	{
		// Both Addresses, so they always appear together, first
		if(!$this->shopping_cart->isPickup())
		{
			$this->addAdjustableColumnView($this->shippingAddressView());
		}

		$this->addAdjustableColumnView($this->billingAddressView());

		// Contact
		$this->addAdjustableColumnView($this->contactInfoView());

		// Delivery
		$this->addAdjustableColumnView($this->deliveryView());

		// Payment
		$this->addAdjustableColumnView($this->paymentView());

	}
	
	/**
	 * Returns the adjustable columns for this purchase
	 * @return TCv_View
	 */
	public function adjustableColumns()
	{
		$container = new TCv_View('cart_adjustable_boxes');
		
		$this->attachAdjustableColumnViews();
		
		$first_view_columns = 0;
		$second_view_columns = 0;
		
		$num_columns = sizeof($this->adjustable_column_views);
		if($num_columns <= 3)
		{
			$first_view_columns = $num_columns;
		}
		elseif($num_columns == 4)
		{
			$first_view_columns = 2;
			$second_view_columns = 2;
		}
		elseif($num_columns <= 7)
		{
			$first_view_columns = 3;
			$second_view_columns = $num_columns - 3;
		}

		// First Row if needed

		if($first_view_columns > 0)
		{
			$view_name = 'TMv_ContentLayout_' . $first_view_columns . 'Column';
			
			/** @var TMv_ContentLayout_4Column $layout set to avoid errors in editor */
			$layout = new $view_name;
			$layout->disableResponsive();
			$layout->addClass('details_layout');
			for($i = 1; $i <= $first_view_columns; $i++)
			{
				$view = array_shift($this->adjustable_column_views);
				$layout->attachViewToContentBlock($view, $i);
//
			}
			$container->attachView($layout);
		}
		// Second row if we need it
		if($second_view_columns > 0)
		{
			$view_name = 'TMv_ContentLayout_' . $second_view_columns . 'Column';
			$layout = new $view_name;
			$layout->disableResponsive();
			$layout->addClass('details_layout');
			for($i = 1; $i <= $second_view_columns; $i++)
			{
				$view = array_shift($this->adjustable_column_views);
				$layout->attachViewToContentBlock($view, $i);
			}
			$container->attachView($layout);
		}
		
		return $container;
	}
	
	

	public function render()
	{
		parent::render();

		$this->attachView($this->shoppingCartView());
		
		// Adjustable columns added by the cart view automatically
		// Detects when showing a purchase as part of a change related to processing purchases
		//$this->attachView($this->adjustableColumns());
		
		$this->attachView($this->confirmFormView());
	}

	

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = parent::pageContent_EditorFormItems();

		// Indicate the completion page, which will vary for different checkouts
		$redirect = new TCv_FormItem_Select('success_menu_id','Successful Order Page');
		$redirect->setHelpText('Select the menu that this a successful order will redirect to.');

		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}

			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}

		$form_items[] = $redirect;

		return $form_items;
	}


	public static function pageContent_ViewTitle(): string
	{ return 'Checkout – Confirm'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The checkout page for the confirm.';
	}


}

?>