<?php
class TMv_CheckoutPayment extends TMv_CheckoutView
{
	use TMt_PagesContentView;

	protected $redirect_to_menu = '';
	
	protected $billing_address_required = true;
	
	
	/**
	 * TMv_CheckoutPayment constructor.
	 * @param TMm_ShoppingCart $cart
	 */
	public function __construct($cart = null)
	{
		parent::__construct($cart);
	}
	
	/**
	 * Manually sets the URL that the redirect, which will have the purchase_id appended afterwards
	 * @param string $url
	 */
	public function setSuccessURL($url)
	{
		$this->redirect_to_menu = $url;
	}
	
	/**
	 * Disables the billing address requirement. This setting will remove the requirements on the form, however each
	 * individual payment processor may or may not care about those values.
	 */
	public function disableBillingAddressRequirement()
	{
		$this->billing_address_required = false;
	}
	
	
	
	/**
	 * Returns the payment form for checkout
	 * @return bool|TMv_PaymentForm
	 */
	protected function paymentForm()
	{
		$processor_class_name = TC_getModuleConfig('store','payment_processor');
		
		/** @var TMt_PaymentProcessor $processor */
		$processor = $processor_class_name::init();
		$payment_form_class_name = $processor_class_name::paymentFormClassName();
		
		if(class_exists($payment_form_class_name))
		{
			/** @var TMv_PaymentForm $form */
			$form = $payment_form_class_name::init($this->shopping_cart, $processor);
			if(!$this->billing_address_required)
			{
				$form->disableBillingAddressRequirement();
			}
			
			return $form;
			
		}
		
		return false;
	}
	
	public function render()
	{
		$payment_form = $this->paymentForm();
		
		if($this->redirect_to_menu !== false && $this->redirect_to_menu != '')
		{
			// Check for an integer string and try to instantiate the menu
			if(ctype_digit($this->redirect_to_menu) && $menu = TMm_PagesMenuItem::init($this->redirect_to_menu))
			{
				$payment_form->setSuccessURL($menu->pathToFolder());
			}
			elseif(is_string($this->redirect_to_menu) && strlen($this->redirect_to_menu) > 0)
			{
				$payment_form->setSuccessURL($this->redirect_to_menu);
			}
		}
		
		$this->attachView($payment_form);
		
		
	}
	

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////
	
	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$redirect = new TCv_FormItem_Select('redirect_to_menu','Page');
		$redirect->setHelpText('Select the menu that this will redirect to if successful');
		$redirect->addOption('','No redirect');
		
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$form_items['redirect'] = $redirect;
		
		return $form_items;
		
	}
	
	
	public static function pageContent_ViewTitle(): string
	{ return 'Checkout – Payment'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The checkout page for the payment.';
	}


}

?>