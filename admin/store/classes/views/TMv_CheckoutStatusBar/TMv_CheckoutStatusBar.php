<?php
class TMv_CheckoutStatusBar extends TCv_View
{
	use TMt_PagesContentView;

	/** @var bool|TMm_ShoppingCart $shopping_cart */
	protected $shopping_cart = false;

	/**
	 * TMv_ShoppingCart constructor.
	 *
	 */
	public function __construct()
	{
		$this->shopping_cart = TMm_ShoppingCart::init();

		parent::__construct('checkout_delivery_'.$this->shopping_cart->id());
		$this->addClassCSSFile('TMv_CheckoutStatusBar');

	}


	/**
	 * Returns the menu view for this status bar
	 * @return TCv_Menu
	 */
	public function menuView()
	{
		$menu = new TCv_Menu();
		$current_stage = $this->shopping_cart->checkoutStage();
		$stage_accessible = true;
		foreach($this->shopping_cart->checkoutStages() as $stage_id => $stage_values)
		{

			if($stage_values['view_url'] != false)
			{
				$button = new TCv_Link();
				$button->addText($stage_values['title']);
				if($stage_accessible)
				{
					$button->setURL($stage_values['view_url']);

				}
				else
				{
					$button->setTag('span');
				}
				$menu->attachView($button);
			}

			// found the last stage
			if($current_stage == $stage_id)
			{
				$stage_accessible = false;
			}


		}

		return $menu;
	}
			
	public function html()
	{
		$this->attachView($this->menuView());
		return parent::html();
		
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		return $form_items;
	}


	public static function pageContent_ViewTitle() : string { return 'Checkout – Status Bar'; }

	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	public static function pageContent_ViewDescription() : string
	{
		return 'The checkout status bar.';
	}


}

?>