<?php

/**
 * A parent class for all checkout views that are shown on the site.
 * Class TMv_CheckoutView
 */
class TMv_CheckoutView extends TCv_View
{
	/** @var bool|TMm_ShoppingCart $shopping_cart */
	protected $shopping_cart = false;
	protected $checkout_stage = false;
	protected $validate_checkout_stage = true;

	/**
	 * TMv_ShoppingCart constructor.
	 * @param TMm_ShoppingCart $shopping_cart
	 */
	public function __construct($shopping_cart = null)
	{
		if($shopping_cart)
		{
			$this->shopping_cart = $shopping_cart;
		}
		else
		{
			$this->shopping_cart = TMm_ShoppingCart::init();
			
		}
		
		parent::__construct('checkout_account_'.$this->shopping_cart->id());
		
		// Disable validation if we're not using stages
		if($this->shopping_cart instanceof TMm_ShoppingCart && $this->shopping_cart->checkoutStages() === false)
		{
			$this->disableCheckoutValidation();
		}
	}

	/**
	 * Returns the checkout stage
	 * @return bool
	 */
	public function checkoutStage()
	{
		return $this->checkout_stage;
	}

	/**
	 * Returns the next checkout URL
	 * @return string
	 */
	public function nextCheckoutURL()
	{
		return $this->shopping_cart->nextCheckoutStageURL($this->checkoutStage());
	}

	/**
	 * Sets the checkout stage for this view.
	 *
	 * This is often set via the settings for the page content view.
	 * @param string $stage_code
	 */
	public function setCheckoutStage($stage_code)
	{
		$this->checkout_stage = $stage_code;
	}

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();

		if($this->shopping_cart->checkoutStages() != false)
		{
			$field = new TCv_FormItem_Select('checkout_stage', 'Related Checkout Stage');
			$field->addOption('', 'None');
			foreach ($this->shopping_cart->checkoutStages() as $stage_id => $stage_values)
			{
				$field->addOption($stage_id, $stage_values['title']);
			}
			$form_items[] = $field;
		}
		return $form_items;
	}

	/**
	 * Disable the checkout validation which ensures that a checkout stage isn't ahead of the cart process
	 */
	public function disableCheckoutValidation()
	{
		$this->validate_checkout_stage = false;
	}

	public function html()
	{

		// validate if we even have a valid view, otherwise show an error
		if($this->validate_checkout_stage && $this->shopping_cart instanceof TMm_ShoppingCart)
		{
			if (!$this->shopping_cart->validateCheckoutStageVisible($this->checkout_stage) || !$this->checkout_stage)
			{
				return '<h3 class="checkout_ahead_warning">You are unable to view this stage of the checkout</h3>';
			}
		}

		return parent::html();

	}


}
?>