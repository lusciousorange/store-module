<?php
class TMv_FormItem_TextFieldForAPIField extends TCv_FormItem_TextField
{
	/**
	 * TMv_FormItem_TextFieldForAPIField constructor.
	 * @param $id
	 * @param array values
	 */
	public function __construct($id, $values)
	{
		if(isset($values['title']))
		{
			parent::__construct($id, $values['title']);
		}
		else
		{
			parent::__construct($id, '');
		}
		if(isset($values['help_text']))
		{
			$this->setHelpText($values['help_text']);
		}
		
		if(isset($values['default']))
		{
			$this->setDefaultValue($values['default']);
		}

	}
}