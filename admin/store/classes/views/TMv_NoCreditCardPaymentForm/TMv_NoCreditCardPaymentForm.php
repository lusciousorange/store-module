<?php
class TMv_NoCreditCardPaymentForm extends TMv_PaymentForm
{
	use TMt_PagesContentView;
	
	
	/**
	 * TMv_PaymentForm constructor.
	 * @param bool|TMm_ShoppingCart|TMm_Purchase $cart
	 */
	public function __construct($cart = null)
	{
		parent::__construct($cart);
		
		$this->setUseSavedCards(false);
		
		$this->setPaymentProcessor(TMm_NoCreditCardPayment::init());
		
	}
	
	/**
	 * Returns the Form Item group for the card section
	 * @param string|bool $heading The heading for the section
	 * @return TCv_FormItem_Group|bool
	 */
	public function creditCardSection($heading = 'Card Information')
	{
		return false;
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_PaymentProcessorForm
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * A hook method where payment fields, specific to the payment processor are set as necessary. These will vary
	 * between each processor, so most of the customization and settings happen here.
	 *
	 * This method sho
	 *
	 * @param TMt_PaymentProcessor|TMm_Stripe $payment_processor
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function hook_configurePayment($payment_processor, $form_processor)
	{
	
	}
}