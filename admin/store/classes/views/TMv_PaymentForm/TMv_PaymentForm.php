<?php
class TMv_PaymentForm extends TCv_FormWithModel
{
	
	use TMt_PaymentForm;
	
	protected bool $use_saved_cards = false;
	
	protected bool $billing_address_required = true;
	
	protected ?array $alternate_payment_options = null;
	
	/** @var TMt_PaymentProcessor */
	protected $payment_processor;
	
	protected string $checked_icon_code = 'fa-circle';
	
	protected string $sandbox_mode_explanation = "Sandbox Mode: Credit Cards will not be charged";
	//protected $skip_transaction_explanation = "Skip Transaction Mode: No Credit Card info required";
	protected string $free_cart_explanation = "Your cart has no cost. Complete your purchase by clicking the button below.";
	protected string $empty_cart_explanation = "Your Cart is Empty";
	protected bool $permit_empty_carts = false;
	
	/** @var TMm_ShoppingCart|TMm_Purchase|void */
	protected $cart;
	
	protected array $jquery_init_values = array();
	
	/**
	 * TMv_PaymentForm constructor.
	 * @param bool|TMm_ShoppingCart|TMm_Purchase $cart
	 * @param null|TMt_PaymentProcessor $payment_processor
	 */
	public function __construct($cart = null, $payment_processor = null)
	{
		// No matter what, set it to the current cart if one isn't provided
		if($cart == null)
		{
			$cart = TMm_ShoppingCart::init();
		}
		
		parent::__construct($cart);
		$this->cart = $cart;
		
		$this->setPaymentProcessor($payment_processor);
		
		// Deal with a cart being potentially a purchase
		$this->processCartIsPurchase($this->cart);
		
		$this->addClassCSSFile('TMv_PaymentForm');
		$this->addClassJQueryFile('TMv_PaymentForm');
		
		if($this->cart instanceof TMm_ShoppingCart)
		{
			$this->jquery_init_values['cart_id'] = $this->cart->id();
		}
		
		$this->setButtonText('Complete Purchase');
		
		
		$this->configureDefaultSuccessURL();
		
	}
	
	protected function configureDefaultSuccessURL()
	{
		// Set the redirect to the success page
		$success_menu_id = TC_getModuleConfig('store','success_menu_id');
		if($success_menu_id > 0)
		{
			$menu_item = TMm_PagesMenuItem::init($success_menu_id);
			if($menu_item)
			{
				
				// Sets the URL, the payment form will append the purchase ID at the end if necessary
				$this->setSuccessURL($menu_item->pageViewURLPath());
			}
		}
	}
	
	/**
	 * Handles if a purchase is potentially
	 * @param TMm_ShoppingCart|TMm_Purchase $cart
	 */
	protected function processCartIsPurchase($cart)
	{
		if($cart instanceof TMm_Purchase)
		{
			if($cart && $cart->userIsBuyer())
			{
				$field = new TCv_FormItem_Hidden('purchase_id','');
				$field->setValue($cart->id());
				$this->attachView($field);
			}
		}
	}
	
	/**
	 * Sets the value for using saved cards
	 * @param bool $use_saved_cards
	 */
	public function setUseSavedCards($use_saved_cards)
	{
		$this->use_saved_cards = $use_saved_cards;
	}
	
	/**
	 * Turns on the features related to this being a new card form, separate from purchase processing forms.
	 */
	public function setAsAddNewCardForm()
	{
		$this->addClass('new_card_form');
		
		// Turns off the "save your card option" obvs yes.
		$this->setUseSavedCards(false);
		
		$this->hideBillingEmailField();
		$this->require_phone_number = false;
		$this->jquery_init_values['add_new_card_form'] = '1';
		$this->jquery_init_values['cart_id'] = '-1';
		
		// Deal with no saved values in Form Tracker, Indicates fresh form that should always wipe
		if(!$this->formTracker() || !$this->formTracker()->hasSavedValues())
		{
			$this->jquery_init_values['force_clear'] = true;
		}
		
		
		
	}
	
	/**
	 * @param TMt_PaymentProcessor $payment_processor
	 */
	public function setPaymentProcessor($payment_processor)
	{
		$this->payment_processor = $payment_processor;
		
	}
	
	/**
	 * Disables the billing address requirement. This setting will remove the requirements on the form, however each
	 * individual payment processor may or may not care about those values.
	 */
	public function disableBillingAddressRequirement()
	{
		$this->billing_address_required = false;
	}
	
	/**
	 * Permits the interface to allow for empty carts
	 */
	public function permitEmptyCarts()
	{
		$this->permit_empty_carts = true;
	}
	
	public function configureFormElements()
	{
		// Test for missing shipping details if it's required
		if(!$this->cart->hasValidShippingAddress())
		{
			$view = new TCv_View();
			$view->addClass('warning_message');
			$view->addText('Payment cannot be completed until a shipping address is provided');
			$this->attachView($view);
			$this->hideSubmitButton();
			return;
		}
		
		parent::configureFormElements();
		
		// Track Payment Processing Settings
		
		$field = new TCv_FormItem_Hidden('payment_processor','Payment Processor');
		$field->setValue($this->payment_processor->processorIdentifier());
		$field->setAsPrivateValue();
		$field->setSaveToDatabase(false);
		$this->attachView($field);
		
		//	$this->addText("Cart ID : ".$this->cart->id()." : $".$this->cart->total());
		
		
		// Explain Sandbox Modes. This should happen no matter what, for consistency
		if($this->payment_processor->usingSandbox())
		{
			$form_item = new TCv_FormItem_HTML('is_sandbox', '');
			$form_item->setShowTitleColumn(false);
			$form_item->addText('<p><i class="fas fa-exclamation-triangle"></i> '.$this->sandbox_mode_explanation.'');
			
			$url = $this->payment_processor->sandboxTestCardURL();
			if($url != '')
			{
				$link = new TCv_Link();
				$link->addClass('sandbox_test_card_link');
				$link->setURL($url);
				$link->addText('Test Card Numbers');
				$link->openInNewWindow();
				$form_item->attachView($link);
			}
			
			$form_item->addText('</p>');
			$form_item->addClass('payment_warning');
			$this->attachView($form_item);
		}
		
		// Empty Cart
		if($this->cart->numCartItems() <= 0 && !$this->permit_empty_carts)
		{
			$field = new TCv_FormItem_HTML('empty_cart','');
			$field->addClass('payment_warning');
			$field->addText("<p>".$this->empty_cart_explanation."</p>");
			$this->attachView($field);
			$this->disableAllTitleColumns();
			$this->hideSubmitButton();
			return;
			
		}
		
		$payment_group = $this->paymentOptionSection();
		if($payment_group)
		{
			$this->attachView($payment_group);
		}
		
		// Add additional fields for the payment type
		foreach($this->alternatePaymentOptions() as $payment_option)
		{
			$option_title = $payment_option::paymentOptionTitle();
			$option_class_name = get_class($payment_option);
			if(count($payment_option->additionalPaymentTextFields()) > 0)
			{
				$group = new TCv_FormItem_Group('option_fields_'.$option_class_name, $option_title. ' Settings');
				$group->addClass('payment_option_section');
				
				$form_item = new TCv_FormItem_Heading($option_class_name.'_option_heading', $option_title. ' Settings');
				$group->attachView($form_item);
				
				
				foreach($payment_option->additionalPaymentTextFields() as $field_id => $field_title)
				{
					
					$field = new TCv_FormItem_TextField($option_class_name . '_' . $field_id,
														$field_title);
					$field->setIsRequired(true);
					$field->setSaveToDatabase(false);
					$group->attachView($field);
					
					
				}
				
				$this->attachView($group);
			}
		}
		
		// Cart requires a financial transaction
		if($this->cart->total() > 0)
		{
			$credit_card_section = $this->creditCardSection();
			$this->attachView($credit_card_section);
			
			$billing_address_section = $this->billingAddressSection($this->billing_address_required);
			$this->attachView($billing_address_section);
		}
		
		// No cost, but we have items to buy. Free carts
		elseif($this->cart->numCartItems() > 0)
		{
			$field = new TCv_FormItem_HTML('no_cost_explanation','');
			$field->addText("<p>".$this->free_cart_explanation."</p>");
			$this->attachView($field);
			$this->disableAllTitleColumns();
		}
		else // No cost, no items = Empty Cart. We can't complete the purchase
		{
			$this->hideSubmitButton();
		}
		
	}
	
	/**
	 *
	 */
	public function render()
	{
		// Configure for JQuery
		// As late as possible
		
		$this->addClassJQueryInit('TMv_PaymentForm', $this->jquery_init_values);
		
		
		parent::render();
	}
	
	/**
	 * Extend HTML function to detect the checkout status
	 * @return string|void
	 */
	public function html($show_form = true)
	{
		if($show_form && TC_getModuleConfig('store','transactions_disabled'))
		{
			$view = TMv_TransactionsDisabledWarning::init();
			$this->attachView($view);
			$show_form = false;
			
		}
		
		return parent::html($show_form);
	}
	
	
	/**
	 * Returns an array of alternate payment options for this form
	 * @return TMt_AlternatePaymentOption[]
	 */
	protected function alternatePaymentOptions()
	{
		if($this->alternate_payment_options == null)
		{
			
			
			$this->alternate_payment_options = array();
			foreach(TC_website()->classesWithTrait('TMt_AlternatePaymentOption') as $class_name)
			{
				if(TC_getModuleConfig('store', 'use_' . $class_name))
				{
					/** @var TMt_AlternatePaymentOption $option */
					$option = $class_name::init();
					if($this->cart instanceof TMm_ShoppingCart
						|| ($this->cart instanceof TMm_Purchase && $option->usableWithUnPaidPurchases())
					)
					{
						$this->alternate_payment_options[] = $class_name::init();
					}
					
					
				}
				
			}
		}
		return $this->alternate_payment_options;
	}
	
	/**
	 * Sets the icon code use when indicating which item is checked.
	 * @param string $icon_code
	 * @return void
	 */
	public function setCheckedIconCode(string $icon_code) : void
	{
		$this->checked_icon_code = $icon_code;
	}
	
	
	/**
	 * Returns the Form Item group for the card section
	 * @param string|bool $heading The heading for the section
	 * @return TCv_FormItem_Group|bool
	 */
	protected function paymentOptionSection($heading = 'Payment Options')
	{
		
		$group = new TCv_FormItem_Group('payment_option_section','');
		
		if($heading !== false)
		{
			$form_item = new TCv_FormItem_Heading('payment_options_heading', $heading);
			$group->attachView($form_item);
		}
		
		$radio = new TCv_FormItem_RadioButtons('payment_method','Card');
		$radio->setIsRequired();
		$radio->setShowTitleColumn(false);
		$radio->setCheckedIconCode($this->checked_icon_code);
		
		$num_options = 0;
		
		/** @var TMt_Store_User $user */
		$user = $this->cart->user();
		if($user)
		{
			// Part 1 : Saved Cards
			if($this->use_saved_cards && $this->cart->total() > 0)
			{
				foreach($this->payment_processor->savedCardsForUser($user) as $card)
				{
					
					$card_view = new TCv_View();
					$card_view->addClass('card_view');
					$card_view->addClass('existing_card');
					
					$subview = new TCv_View();
					$subview->addClass('card_number');
					$subview->addText($card['number']);
					$card_view->attachView($subview);
					
					$subview = new TCv_View();
					$subview->addClass('card_expiry');
					$subview->addText('Exp: ' . $card['expiry']);
					$card_view->attachView($subview);
					
					$radio->addOption($card['id'], $card_view);
					$num_options++;
				}
			}
			
			
			// Part 2 : New Card
			if($this->payment_processor->isSelectablePaymentOption() && $this->cart->total() > 0)
			{
				$card_view = new TCv_View();
				$card_view->addClass('card_view');
				
				$subview = new TCv_View();
				$subview->addClass('card_number');
				if($this->use_saved_cards)
				{
					$title = TC_getModuleConfig('store','new_card_title');
					if($title == '')
					{
						$title = 'New Card';
					}
					$subview->addText($title);
				}
				else // no saved ones, so we're just saying we're paying with one
				{
					$subview->addText($this->payment_processor->paymentOptionButtonTitle());
				}
				$card_view->attachView($subview);
				
				$subview = new TCv_View();
				$subview->addClass('card_expiry');
				$subview->addText('');
				$card_view->attachView($subview);
				
				$radio->addOption('new', $card_view);
				$num_options++;
			}
			// PART 3 : Alternate Payment Options
			
			foreach($this->alternatePaymentOptions() as $payment_option)
			{
				$card_view = new TCv_View();
				$card_view->addClass('card_view');
				
				$subview = new TCv_View();
				$subview->addClass('card_number');
				$subview->addText($payment_option::paymentOptionTitle());
				$card_view->attachView($subview);
				
				$subview = new TCv_View();
				$subview->addClass('card_expiry');
				$subview->addText($payment_option->paymentOptionExplanation());
				$card_view->attachView($subview);
				
				$radio->addOption(get_class($payment_option), $card_view);
				$num_options++;
				
			}
			
		}
		$group->attachView($radio);
		
		// After all that, we literally only have one option
		// And that one option is the primary payment_processor
		// Disabled since this breaks a bunch of functioanlty related to tokenization if there are no saved cards
//		if($num_options <= 1 && $this->payment_processor->isSelectablePaymentOption())
//		{
//			return false;
//		}
		
		
		return $group;
		
	}
	
	
	/**
	 * Returns the Form Item group for the card section
	 * @param string|bool $heading The heading for the section
	 * @return TCv_FormItem_Group
	 */
	public function creditCardSection($heading = 'Card Information')
	{
		$group = new TCv_FormItem_Group('option_fields_new','');
		$group->addClass('new_card_section');
		$group->addClass('payment_option_section');
		
		if($heading !== false)
		{
			$form_item = new TCv_FormItem_Heading('credit_card_heading', $heading);
			$group->attachView($form_item);
		}
		
		$error = new TCv_View('card_error');
		$error->addText('There was an error with adding your card, please verify your information and try again.');
		$this->attachView($error);


//		$form_item = new TCv_FormItem_RadioButtons('credit_card_type', '');
//		//$form_item->setShowTitleColumn(false);
//		$form_item->addOption('VISA','<i class="fab fa-cc-visa"></i>');
//		$form_item->addOption('MASTERCARD','<i class="fab fa-cc-mastercard"></i>');
//		$form_item->setDefaultValue('VISA');
//		$this->attachView($form_item);
		
		$form_item = new TCv_FormItem_TextField('cc_name', 'Name on Card');
		$form_item->setIsRequired(true);
		$form_item->disableAutoComplete();
		$form_item->setSaveToDatabase(false);
		$group->attachView($form_item);
		
		$form_item = new TCv_FormItem_TextField('cc_number', 'Card Number');
		$form_item->setIsRequired(true);
		$form_item->disableAutoComplete();
		$form_item->setNoSpaces(true);
		$form_item->setIsInteger();
		$form_item->addFormElementCSSClass('cc_field');
		$form_item->setSaveToDatabase(false);
		$group->attachView($form_item);
		
		$form_item = new TCv_FormItem_SelectDateTime('cc_expiry_date', 'Expiry Date');
		$form_item->setIsRequired(true);
		$form_item->setAsDateOnly();
		$form_item->includeBlankStartingValues();
		$form_item->setViewOrder('MY');
		$form_item->setYearValues(date('Y'), date('Y')+20);
		$form_item->setDigitMonthNames();
		$form_item->addFormElementCSSClass('cc_field');
		$form_item->setSaveToDatabase(false);
		$group->attachView($form_item);
		
		$form_item = new TCv_FormItem_TextField('cc_cvd', 'CVD');
		$form_item->addHelpText("Three or four digits on back of card");
		$form_item->setIsRequired(true);
		$form_item->disableAutoComplete();
		$form_item->setNoSpaces(true);
		$form_item->setIsInteger();
		$form_item->addFormElementCSSClass('cc_field');
		$form_item->setSaveToDatabase(false);
		$group->attachView($form_item);
		
		if($this->use_saved_cards)
		{
			$form_item = new TCv_FormItem_Select('cc_save_card','Save Card');
			$form_item->addOption('0','No – Do not save your card');
			$form_item->addOption('1','Yes – Save your card securely with our financial institution');
			$form_item->setSaveToDatabase(false);
			$group->attachView($form_item);
		}
		
		$field = new TCv_FormItem_Hidden('tokenization_card_name','Name On Card');
		$field->setSaveToDatabase(false);
		$group->attachView($field);
		
		$field = new TCv_FormItem_Hidden('tokenization_token','Token');
		$field->setSaveToDatabase(false);
		$group->attachView($field);
		
		return $group;
	}
	
	/**
	 * Returns the section for address fields
	 * @param bool $required Indicates if we should require the appropriate values
	 * @return TCv_FormItem_Group
	 */
	protected function addressFieldSection($required = false)
	{
		$group = new TCv_FormItem_Group('address_field_section','Address');
		$group->addClass('address_fields_section');
		$group->addClass('payment_option_section');
		
		
		/** @var TMt_Store_User|TMm_User $user */
		$user = TC_currentUser();
		
		$field = new TCv_FormItem_TextField('address_1', 'Street Address');
		if($required) { $field->setIsRequired(); }
		if($user) { $field->setDefaultValue($user->address1()); }
		$group->attachView($field);
		
		$field = new TCv_FormItem_TextField('address_2', '');
		if($user) { $field->setDefaultValue($user->address2()); }
		$group->attachView($field);
		
		$field = new TCv_FormItem_TextField('city', 'City');
		if($required) { $field->setIsRequired(); }
		if($user) { $field->setDefaultValue($user->city()); }
		$group->attachView($field);
		
		$field = new TCv_FormItem_Select('province', 'Province/State');
		if($required) { $field->setIsRequired(); }
		$field->addOption('','Select Province/State');
		$field->setDefaultValue('MB');
		
		$string = file_get_contents($_SERVER['DOCUMENT_ROOT']."/admin/store/classes/views/TMv_PaymentForm/regions.json");
		$regions = json_decode($string, true);
		foreach($regions as $country_name => $country_values)
		{
			$field->startOptionGroup($country_values['code'], $country_name);
			
			foreach($country_values['provinces'] as $province_values)
			{
				$field->addOption($province_values['code'], $province_values['name']);
			}
		}
		
		$group->attachView($field);
		
		$field = new TCv_FormItem_TextField('postal_code', 'Postal Code');
		if($required) { $field->setIsRequired(); }
		if($user) { $field->setDefaultValue($user->postalCode()); }
		$group->attachView($field);
		
		return $group;
	}
	
	//////////////////////////////////////////////////////
	//
	// FORM PROCESSING
	//
	//////////////////////////////////////////////////////
	
	/**
	 * @return TMm_ShoppingCart|TMm_Purchase
	 */
	public static function paymentShoppingCart()
	{
		return TMm_ShoppingCart::init();
	}
	
	/**
	 * Returns the payment processor to be used during form processing
	 * @param TCc_FormProcessorWithModel $form_processor
	 * @return TMt_PaymentProcessor
	 */
	public static function formPaymentProcessor($form_processor)
	{
		$processor_identifier = $form_processor->formValue('payment_processor');
		if(strpos($processor_identifier, '--') > 0)
		{
			$model = TC_initClassWithContentCode($processor_identifier);
			if(method_exists($model,'paymentProcessor'))
			{
				$payment_processor = $model->paymentProcessor();
			}
			else
			{
				// Hard error. Trying to process something we shouldn't or can't
				TC_triggerError($model.' requires a method called paymentProcessor()');
			}
		}
		else
		{
			$payment_processor = $processor_identifier::init();
		}
		
		return $payment_processor;
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		// Only bother if something is there
		if($form_processor->formValue('billing_postal') != '')
		{
			
			$country = TMm_ShoppingCart::countryCodeForProvince($form_processor->formValue('billing_province'));
			$is_valid = TMv_PaymentForm::validatePostalCode($form_processor->formValue('billing_postal'), $country);
			
			// Canadian postal code validation
			if(!$is_valid)
			{
				$form_processor->failFormItemWithID('billing_postal', 'The postal code appears poorly formatted');
			}
			
			$form_processor->addDatabaseValue('billing_country',$country);
		}
		
		// Phone validation
		if($form_processor->fieldIsSet('phone') && $form_processor->formValue('phone') != '')
		{
			$phone = $form_processor->formValue('phone');
			if(!TCu_Text::validatePhoneNumber($phone))
			{
				$form_processor->failFormItemWithID('phone', 'It appears your phone number is incorrectly formatted. Please include an area code plus 7 digits. eg: (234) 567-8901. International number must begin with +.');
			}
		}
	}
	
	
	/**
	 * Performs the primary action associated with processing this form. For this form, there are no database actions
	 * but there are actions related to testing the API and sending a request.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// Process the form with any cart related values that should be saved
		parent::customFormProcessor_performPrimaryDatabaseAction($form_processor);
		
		// Get the shopping cart
		$shopping_cart = $form_processor->model();
		
		// Handle a possible purchase ID being provided
		if($form_processor->fieldIsSet('purchase_id'))
		{
			$purchase = TMm_Purchase::init($form_processor->formValue('purchase_id'));
			if($purchase && $purchase->userIsBuyer())
			{
				$shopping_cart = $purchase;
			}
		}
		
		$payment_processor = static::formPaymentProcessor($form_processor);
		
		static::processFormWithPaymentProcessor(
			$payment_processor,
			$form_processor,
			$shopping_cart );
		
		
	}
	
	/**
	 * Takes the address fields defined and parses them into the payment processor.
	 * @param TMt_PaymentProcessor $payment_processor
	 * @param TCc_FormProcessorWithModel $form_processor The form processor
	 */
	public static function processAddressFieldsIntoPaymentProcessor($payment_processor, $form_processor)
	{
		// Set the billing information based on what is provided
		// The form might also provide override values for name and email, so we should process those if they are
		// provided as well. Otherwise, the default values are set from the logged in user.
		
		if($form_processor->fieldIsSet('first_name'))
		{
			$payment_processor->setBillingFirstName($form_processor->formValue('first_name'));
		}
		if($form_processor->fieldIsSet('last_name'))
		{
			$payment_processor->setBillingLastName($form_processor->formValue('last_name'));
		}
		if($form_processor->fieldIsSet('email'))
		{
			$payment_processor->setBillingEmailAddress($form_processor->formValue('email'));
		}
		
		if($form_processor->fieldIsSet('phone'))
		{
			$payment_processor->setBillingPhoneNumber($form_processor->formValue('phone'));
		}
		
		$payment_processor->setBillingAddress1($form_processor->formValue('billing_street'));
		$payment_processor->setBillingAddress2($form_processor->formValue('billing_street_2'));
		$payment_processor->setBillingCity($form_processor->formValue('billing_city'));
		$payment_processor->setBillingProvince($form_processor->formValue('billing_province'));
		$payment_processor->setBillingPostalCode($form_processor->formValue('billing_postal'));
		
		// Country handled via the province
	}
	
	/**
	 * The main call that processes the form using a payment processor. This method should not be extended, since the
	 * functionality is common to all processors. Special configuration can be handled in `hook_configurePayment`.
	 *
	 * This method is usually called from `customFormProcessor_performPrimaryDatabaseAction` after the payment
	 * processor is instantiated.
	 *
	 * @param TMt_PaymentProcessor $payment_processor
	 * @param TCc_FormProcessor $form_processor The form processor
	 * @param TMm_ShoppingCart|TMm_Purchase $cart The cart or purchase that is being processed.
	 */
	public static function processFormWithPaymentProcessor($payment_processor, $form_processor, $cart)
	{
		// Configure Conditional Messaging
		TC_messageConditionalTitle('Order Successfully Placed – Thank You',true);
		TC_messageConditionalTitle('There was a problem placing your order. ', false);
		
		// ---- 1 SETUP CART
		
		$payment_processor->setShoppingCart($cart);
		
		// Something went wrong, but it's not an error
		if($payment_processor->statusIsAbort())
		{
			$form_processor->setMessengerTitle($payment_processor->message()); // We don't want to show
			$form_processor->setAsInvalid(); // Set as invalid, so it goes back, but not fail
			$form_processor->finish();
		}
		
		// Major error, hard fail
		elseif($payment_processor->statusIsFailure())
		{
			$form_processor->fail($payment_processor->message());
			$form_processor->finish();
		}
		
		// ---- 2 HANDLE PAYMENT OPTION SETTINGS
		
		if($form_processor->fieldIsSet('payment_method'))
		{
			$payment_method = $form_processor->formValue('payment_method');
		}
		else // field not set. These are always new card transactions
		{
			$payment_method = 'new';
		}
		
		$save_card = $form_processor->formValue('cc_save_card');
		$card_token = $form_processor->formValue('tokenization_token');
		
		$payment_processor->setPaymentOptionForProcessing($payment_method, $card_token, $save_card);
		
		
		// ---- 3 CONFIGURE FORM VALUES
		
		// Parse the fields into the address
		static::processAddressFieldsIntoPaymentProcessor($payment_processor, $form_processor);
		
		// Call the user-defined hook method
		static::hook_configurePayment($payment_processor, $form_processor);
		
		//$form_processor->fail('Hard exit');return;
		
		// ---- 4 PROCESS PAYMENT
		
		$purchase = $payment_processor->processPayment($form_processor);
		
		// Not necessary, the status handles the failures. Could find transaction with false ID
//		if(!$purchase)
//		{
//			$form_processor->fail();
//		}
//
		
		// ---- 5 HANDLE RESPONSE
		
		// FAILURE
		if($payment_processor->statusIsFailure())
		{
			$form_processor->fail($payment_processor->message());
		}
		
		
		// ABORT
		elseif($payment_processor->statusIsAbort())
		{
			// Abort means we adjust the heading
			$form_processor->setMessengerTitle($payment_processor->message());
			$form_processor->setAsInvalid();
		}
		
		// SUCCESSFUL
		elseif($payment_processor->statusIsSuccess())
		{
			$form_processor->addMessage($payment_processor->message());
			
			if($purchase)
			{
				if($form_processor->successURL() != '')
				{
					// Redirect to the success page with the appended purchase ID
					$form_processor->setSuccessURL($form_processor->successURL() . '/' . $purchase->id());
				}
				else
				{
					$form_processor->addConsoleDebug('ha1 success');
					
				}
				
			}
		}
		
		// ---- 5 FINISH
		$form_processor->finish();
	}
	
	
	//////////////////////////////////////////////////////
	//
	// HOOK METHODS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * A hook method where payment, specific to the payment processor are set as necessary. These will vary
	 * between each processor, so most of the customization and settings happen here.
	 *
	 * This method should be overridden by the view class using this trait
	 *
	 * @param TMt_PaymentProcessor $payment_processor
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function hook_configurePayment($payment_processor, $form_processor)
	{
	
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ADDRESS VALIDATION METHODS
	//
	//////////////////////////////////////////////////////
	
	
	
	/**
	 * Validates if the postal code provided is valid
	 * @param string $postal_code The postal code
	 * @param string $country The two digit country code
	 * @return string
	 */
	public static function validatePostalCode($postal_code, $country)
	{
		// Canadian postal code validation
		if($country == 'CA')
		{
			$pattern = '/^[A-Za-z]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$/';
			if(!preg_match($pattern, $postal_code))
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	
}