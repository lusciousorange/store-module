(function ($)
{
	// INIT
	$.fn.TMv_PaymentForm = function (options)
	{
		var $form = $('.TMv_PaymentForm');

		var settings = $.extend(
			{
				has_external_button: true,
				use_tokenization : false,
				cart_id			: -1,
				add_new_card_form : false,
				force_clear : false
			}, options || {});

		var cc_fields = [
			'cc_name',
			'cc_number',
			'cc_expiry_date_year',
			'cc_expiry_date_month',
			'cc_cvd',
		];

		return this.each(function ()
		{
			// Dealing with tokenization calls to another server
			// Not all payment types use this, so they need to define paymentFormTokenize()
			settings.use_tokenization = (typeof paymentFormTokenize == 'function');


			// add listeners
			$form.find('input[name=payment_method]').change(function (e)
			{
				cardChanged();
			});

			// Deal with only 1 card, which is the "new" card
			// Check it off and enable the form since that's the only option here
			if ($('.TMv_PaymentForm .existing_card').length == 0)
			{
				$('.TMv_PaymentForm input#payment_method_new').attr('checked', 'checked');
			}

			handleSingleOptionAsDefault(); // happens before cardChanged() to detect the option
			cardChanged();

			$form.submit(function(event) { submitForm(event); });

			restoreCreditCardDataFromSessionStorage();


		});

		/**
		 * Function called when the form is submitted
		 * @param event
		 */
		function submitForm(event)
		{
			$('#card_error').hide();
			var payment_option = $form.find('input[name=payment_method]:checked').val();

			// Detect if we're adding a new card with a specific form
			if (settings.add_new_card_form)
			{
				payment_option = 'new';
			}


			// Check if we need to interject for tokenization
			cleanUpFormValues();
			disableCreditCardForms();

			if(payment_option == 'new')
			{
				saveCreditCardDataToSessionStorage();

				// Validate them
				if (validateFields())
				{
					if (settings.use_tokenization)
					{
						$form.find('.cc_field').attr('disabled', 'disabled');
						// This needs to be a syncronous call, to ensure things load in the proper order
						// Ajax call to get the token from Bambora
						// The success callback will re-trigger the submit
						var success = paymentFormTokenize(
							$form.find('#cc_name').val(),
							$form.find('#cc_number').val(),
							$form.find('#cc_expiry_date_month').val(),
							$form.find('#cc_expiry_date_year').val(),
							$form.find('#cc_cvd').val()
						);

						// Check if we generated an error
						if (!success)
						{
							cancelSubmit(event);
						}
					}
				}
				else
				{
					// Avoid future triggers on submit
					cancelSubmit(event);
				}
			}
		}

		function cancelSubmit(event)
		{
			event.preventDefault();
			$('#card_error').slideDown();
			$('#cart_tracker').removeClass('loading');
			enableCreditCardForms();
		}

		/**
		 * Disables the credit card fields which ensures they don't get submitted
		 */
		function disableCreditCardForms()
		{
			$('#cc_name, #cc_number, #cc_expiry_date_month, #cc_expiry_date_year, #cc_cvd').attr('disabled','disabled');
		}

		/**
		 * Enables the credit card fields which ensures they can be edited
		 */
		function enableCreditCardForms()
		{
			$('#cc_name, #cc_number, #cc_expiry_date_month, #cc_expiry_date_year, #cc_cvd').removeAttr('disabled');
		}

		/**
		 * Saves the CC fields to JS sessions storage to be kept for refreshes and returns
		 */
		function saveCreditCardDataToSessionStorage()
		{
			// track the cart ID to avoid passing info from one to another
			sessionStorage.setItem('session_payment_cart_id', settings.cart_id);
			$.each(cc_fields, function(index, field_name)
			{
				sessionStorage.setItem(field_name, $form.find('#'+field_name).val());
			});

		}

		function restoreCreditCardDataFromSessionStorage()
		{
			if(settings.force_clear)
			{
				clearCreditCardDataFromSessionStorage();
				return;
			}

			if(settings.cart_id == sessionStorage.getItem('session_payment_cart_id'))
			{
				$.each(cc_fields, function (index, field_name)
				{
					$form.find('#' + field_name).val(sessionStorage.getItem(field_name));
				});
			}
			else // The cart ID does not match the ID for the saved values
			{
				clearCreditCardDataFromSessionStorage();
			}
		}

		function clearCreditCardDataFromSessionStorage()
		{
			$.each(cc_fields, function(index, field_name)
			{
				sessionStorage.removeItem(field_name);
			});
		}

		function cardChanged()
		{
			var card_options_exist = $form.find('#payment_method_row').length;

			// No card options exist, we just get out
			if(!card_options_exist)
			{
				return;
			}

			var value = $form.find('input[name=payment_method]:checked').val();

			if (value != undefined)
			{
				showButton();
			}
			else if($('.cart_item_row').length == 0) // disable form submission, only if the cart is actually empty
			{
				hideButton();
			}

			// Hide all the options
			$.each($('#payment_method_row input[type="radio"]'), function(index, field)
			{
				var $field = $(field);
				var name = $field.attr('value');

				if(value != name)
				{
					$('#option_fields_' + name).slideUp();
					$('#option_fields_' + name+ ' .table_row.required').find('input, select').removeAttr('required');
				}

			});

			// enable the selected one
			$('#option_fields_' + value).slideDown();
			$('#option_fields_' + value + ' .table_row.required').find('input, select').attr('required','required');

			// Deal with saved card values, hiding
			if($.isNumeric(value))
			{
				$('#billing_address_section').slideUp();
				$('#billing_address_section .table_row.required').find('input, select').removeAttr('required');
			}
			else
			{
				$('#billing_address_section').slideDown();
				$('#billing_address_section .table_row.required').find('input, select').attr('required','required');

			}

			// Trigger scrolling in case we've messed with some other interface elements
			setTimeout(function(){$(window).trigger('scroll');},1000);

		}


		function hideButton()
		{
			$form.find('#submit').attr('disabled','disabled');


			$form.find('#submit_row').slideUp();

		}

		function showButton()
		{
			$form.find('#submit').removeAttr('disabled');

			$form.find('#submit_row').slideDown();

		}

		/**
		 * Sanity method that removes invalid characters from the fields just in case someone pasted in and those values
		 * somehow got through.
		 */
		function cleanUpFormValues()
		{
			var number = $form.find('#cc_number').val();
			number = number.replace(/\D/g, ''); // remove non-numbers
			$form.find('#cc_number').val(number);

			var cvd = $form.find('#cc_cvd').val();
			cvd = cvd.replace(/\D/g, ''); // remove non-numbers
			$form.find('#cc_cvd').val(cvd);

			var name_on_card = $form.find('#cc_name').val();
			name_on_card = $.trim(name_on_card);
			$form.find('#cc_name').val(name_on_card);
		}

		/**
		 * Returns the fields that are invalid
		 * @return {Array}
		 */
		function invalidNewCardFields()
		{
			var invalid_fields = [];

			var fields = ['cc_name', 'cc_number', 'cc_expiry_date_month', 'cc_expiry_date_year', 'cc_cvd'];

			// TODO: Confirm this functionality
			$.each(fields, function(field_id)
			{
				var $field = $form.find('#' + field_id);
				if ($field.val() == '')
				{
					invalid_fields.push(field_id);
				}
			});
			return invalid_fields;
		}s

		/**
		 * Need to manually validate the fields since the form isn't in the submit area
		 */
		function validateFields()
		{
			// remove all the flags
			$form.find('.TMv_FormItem').removeClass('invalid');

			// loop through the invalid fields
			var success = true;
			$.each(invalidNewCardFields(), function(field_id)
			{
				var $field = $form.find('#'+field_id);
				$field.parents('.TCv_FormItem').addClass('invalid');
				success = false;
			});

			return success;
		}

		function handleSingleOptionAsDefault()
		{
			// If there is only one, check that one by default
			if($('#payment_method_row input[type="radio"]').length == 1)
			{
				$('#payment_method_row input[type="radio"]').attr('checked',true);
			}

		}

	};


//END THE WRAPPER
})(jQuery);
