<?php

/**
 * Class TMv_PurchaseCart
 *
 * An extension of the shopping cart view which handles anything related to purchases.
 */
class TMv_PurchaseCart extends TMv_ShoppingCart
{
	protected array $refunds = array();
	
	protected bool $show_adjustable_blocks = true;
	
	protected ?string $remove_url_target_name = 'remove-from-purchase';
	protected ?string $update_quantity_url_target_name = 'update-purchase-quantity';
	
	/**
	 * TMv_ShoppingCart constructor.
	 * @param TMm_Purchase|bool $purchase (Optional) Default false.
	 *
	 */
	public function __construct($purchase = false)
	{
		// We are provided with a purchase
		if($purchase instanceof TMm_Purchase)
		{
			parent::__construct($purchase);
		}
		
		
		
		// Determine if the ID of a purchase is provided, at which point we try to load that one
		elseif(isset($_GET['id']))
		{
			$id_purchase = TMm_Purchase::init($_GET['id']);
			if($id_purchase && $id_purchase->isUnpaid() && $id_purchase->userIsBuyer())
			{
				parent::__construct($id_purchase);
			}
			else
			{
				TC_triggerError('Unable to load purchase');
			}
		}
		else
		{
			TC_triggerError('Unable to load purchase');
		}
		
		$this->addClassCSSFile('TMv_PurchaseCart');
		
		// Disable Editing for non-admins OR if it's already paid
		if(!TC_isTungstenView() || $purchase->isPaid())
		{
			$this->disableEditing();
		}
		else // add tracker JS to handle quantity changes
		{
			$this->attachView(TMv_ShoppingCartTracker::init());
			
			
		}
		
		
		
		
	}
	
	/**
	 * Returns the purchase
	 * @return TMm_Purchase
	 */
	public function purchase()
	{
		return $this->shopping_cart;
	}
	
	public function heading()
	{
		if(TC_isTungstenView())
		{
			return false;
		}
		
		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		$heading->addText($this->purchase()->title());
		return $heading;
	
	}
	
	/**
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $purchase_item
	 * @return TCv_HTMLTableRow
	 */
	public function rowForShoppingCartItem($purchase_item)
	{
		$row = parent::rowForShoppingCartItem($purchase_item);
		
		if($purchase_item->isRefund())
		{
			$refund = $purchase_item->refund();
			
			// First time we're encountering this refund
			if(!isset($this->refunds[$refund->id()]))
			{
				// Track it so we don't reference it again
				$this->refunds[$refund->id()] = $refund;
				
				$heading_row = $this->createRefundHeadingRow($refund);
				$heading_row->addClass('refund');
				// Add a "heading row" for the refund
				$this->cart_table->attachView($heading_row);
				
			}
			$row->addClass('refund');
		}
		
		return $row;
	}
	
	/**
	 * Replace functionality of title cell with a non-linkable title and for admins, a link to the item
	 * @param TMm_PurchaseItem $purchase_item
	 * @return TCv_HTMLTableCell
	 */
	public function titleCellForShoppingCartItem($purchase_item)
	{
		if(!TC_isTungstenView())
		{
			return parent::titleCellForShoppingCartItem($purchase_item);
		}
		
		// Admin view that shows links to edit items
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('item_column');
		
		
		$link = new TCv_View();
		$link->addClass('item_link');
		$link->setTag('span');
		$link->addText($purchase_item->cartTitle());
		$cell->attachView($link);
	
		$item_description = new TCv_View();
		$item_description->addClass('item_description');
		
		// Only show for non-refunded items
		if(!$purchase_item->isRefund())
		{
			foreach($purchase_item->createdItems() as $created_item)
			{
				$item_link = new TCv_Link();
				$item_link->setURL($created_item->adminEditURL());
				$item_link->openInNewWindow();
				$item_link->addClass('created_item_link');
				$item_link->addText($created_item->title());
				$item_description->attachView($item_link);
			}
			
		}
		
		$cell->attachView($item_description);
	
		return $cell;
	}
	
	/**
	 * Generates and returns the table cell view for the title
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function priceCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = parent::priceCellForShoppingCartItem($shopping_cart_item);
		
		if($this->allow_editing && $shopping_cart_item->item()->allowsUnpaidPurchasePriceUpdates())
		{
			$button = new TCv_Link();
			$button->addText('Edit');
			$button->addClass('price_edit_button');
			$button->setURL('#');
			$button->addDataValue('purchase-item-id', $shopping_cart_item->id());
			$button->setAttribute('onclick',"$('#cart_price_form_".$shopping_cart_item->id()."').slideToggle(); return false;");
			$cell->attachView($button);
			
			$cell->attachView($this->editPriceFormForPurchaseItem($shopping_cart_item));
			
		}
		
		return $cell;
	}
	
	/**
	 * @param TMm_PurchaseRefund $refund
	 * @return TCv_HTMLTableRow
	 */
	protected function createRefundHeadingRow($refund)
	{
		$table_row = new TCv_HTMLTableRow('refund_row_'.$refund->id());
		$table_row->addClass('cart_item_row');
		$table_row->addClass('refund_heading_row');
			
			$cell = new TCv_HTMLTableCell();
			//$cell->addClass('refund_title');
			$cell->setColumnSpan(count($this->columns) - 2);
			
			$title = new TCv_View();
			$title->addText('Refund');
			$title->addClass('refund_title');
			$cell->attachView($title);
			
			$cell->addText($refund->dateAddedFormatted());
		
			$container = new TCv_View();
			$container->addClass('refund_details');
			$container->addText('Refunded by <span class="refund_user">'.$refund->user()->fullName().'</span>');
		
			// Add in payment info if we have it
			if($refund->paymentType()!= '')
			{
				$container->addText(' on '.$refund->transactionTypeTitle());
			}
			
			if($refund->paymentCardType() != null)
			{
				$types = TMm_Purchase::$credit_card_types;
				$container->addText(' – '.$types[$refund->paymentCardType()]['title']);
				
			}
			
			if($refund->transactionID() != '')
			{
				$container->addText(' – '.$refund->transactionID());
				
			}
			
			if($refund->hasNotes())
			{
				$container->addText('<span class="refund_note">'.$refund->notes().'</span>');

			}
			
		
			$cell->attachView($container);
			$table_row->attachView($cell);
		
			// SECOND COLUMN
			$cell = new TCv_HTMLTableCell();
			$cell->addClass('subtotal_column');
			$cell->setColumnSpan(2);
			$cell->addText('<br />Subtotal : $'.$refund->subtotal());
		
			if($refund->deliveryPrice() > 0)
			{
				$cell->addText('<span class="refund_delivery"> Delivery : $'.$refund->deliveryPrice().'</span>');
			}
		
		$table_row->attachView($cell);
		
		
		
		
		return $table_row;
	}
	
	/**
	 * The quantity form for the cart
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $purchase_item
	 * @return TCv_Form
	 */
	public function editPriceFormForPurchaseItem($purchase_item)
	{
		
		$form = new TCv_Form('cart_price_form_' . $purchase_item->id(), TCv_Form::$DISABLE_FORM_TRACKING);
		$form->addClass('cart_price_form');
		$form->setAction('/admin/store/do/update-purchase-price/' . $purchase_item->id());
		$form->setMethod('get');
		
		
		$field = new TCv_FormItem_TextField('price', '');
		$field->setDefaultValue($purchase_item->pricePerItem());
		$field->addClass('cart_update_price');
		$field->disableAutoComplete();
		$field->setShowTitleColumn(false);
		$form->attachView($field);
		
		$field = new TCv_FormItem_Select('show_discount', '');
		$field->addOption('1', 'Show Discount');
		$field->addOption('0', 'No Discount Shown');
		$field->setShowTitleColumn(false);
		$form->attachView($field);
		
		$form->setButtonText('Update Price');
		//$form->hideSubmitButton();
		return $form;
	
	}
	
	/**
	 * The box that is added to a refunded column that shows the original price. Returns false if there's no difference
	 * @param float $original_value
	 * @param float $new_value
	 * @return bool|TCv_View
	 */
	protected function refundCostBox($original_value, $new_value)
	{
		if($original_value != $new_value && $original_value > 0)
		{
			$original_price = new TCv_View();
			$original_price->addClass('non_refund_price');
			$original_price->addText('$'.$this->formatCurrency($original_value));
			
			return $original_price;
		}
		
		return false;
	}
	
	/**
	 * Returns the total cell for the cart
	 * @return TCv_HTMLTableCell
	 */
	public function totalCell()
	{
		$table_cell = parent::totalCell();
		
		$original_value = $this->purchase()->originalTotal();
		$new_value = $this->purchase()->total();
		
		$table_cell->attachView($this->refundCostBox($original_value, $new_value), true);
		
		return $table_cell;
	}
	
	/**
	 * Returns the subtotal cell for the cart
	 * @return TCv_HTMLTableCell
	 */
	public function subtotalCell()
	{
		$table_cell = parent::subtotalCell();
		
		$original_value = $this->purchase()->originalSubtotal();
		$new_value = $this->purchase()->subtotal();
		
		$table_cell->attachView($this->refundCostBox($original_value, $new_value), true);
		
		
		return $table_cell;
	}
	
	/**
	 * Returns the tax cell for the cart
	 * @param TMm_Tax $tax
	 * @return TCv_HTMLTableCell
	 */
	public function taxTotalCell($tax)
	{
		$table_cell = parent::taxTotalCell($tax);
		
		$original_value = $this->purchase()->originalTotalForTax($tax);
		$new_value = $this->purchase()->totalForTax($tax);
		$table_cell->attachView($this->refundCostBox($original_value, $new_value), true);
		
		
		return $table_cell;
	}
	
	public function deliveryTotalCell()
	{
		$table_cell = parent::deliveryTotalCell();
		
		$original_value = $this->purchase()->originalTotalForDelivery();
		$new_value = $this->purchase()->deliveryPrice();
		$table_cell->attachView($this->refundCostBox($original_value, $new_value), true);
		
		
		
		return $table_cell;
	}
	/**
	 * Disables the showing of the adjustable blocks at the bottom.
	 */
	public function hideAdjustableInfoBlocks()
	{
		$this->show_adjustable_blocks = false;
	}
	
	public function hook_processAdditionalRows()
	{
		// Deal with refunds with no cart items, possibly when just refunding delivery
		foreach($this->purchase()->refunds() as $refund)
		{
			if(!isset($this->refunds[$refund->id()]))
			{
				//	$this->attachItemRowToCartTable($shopping_cart_item);
				$heading_row = $this->createRefundHeadingRow($refund);
				$heading_row->addClass('refund');
				// Add a "heading row" for the refund
				$this->cart_table->attachView($heading_row);
			}
		}
	}
	
	public function render()
	{
		parent::render();
		
		// Attach the adjustable columns
		if($this->show_adjustable_blocks)
		{
			$this->addClassCSSFile('TMv_PurchaseView');
			$this->addClassCSSFile('TMv_CheckoutConfirm');
			$this->addClass('TMv_PurchaseView TMv_CheckoutConfirm');
		
			$purchase_view = TMv_PurchaseView::init($this->purchase());
			$this->attachView($purchase_view->adjustableColumns());
		}
		
	
		
	}
}