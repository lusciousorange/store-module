<?php
class TMv_PurchaseEmail extends TCv_Email
{
	/**
	 * TMv_PurchaseEmail constructor.
	 * @param bool|TMm_Purchase $purchase
	 */
	public function __construct($purchase)
	{
		// Pass in the purchase user, may not exist
		parent::__construct($purchase->user());
		$this->addClassCSSFile('TMv_PurchaseEmail');
		// No User, Manually set the values
		if(!$purchase->user())
		{
			$this->addRecipient($purchase->email());
		}

		$intro_text = TC_getModuleConfig('store','email_intro_text');
		if($intro_text != '')
		{
			$intro = new TCv_View('intro_paragraph');
			$intro->setTag('p');
			$intro->addText($intro_text);
			$this->attachView($intro);
		}
		
		$confirm_view = TMv_PurchaseView::init($purchase);
		$confirm_view->setAsEmailView();
		$this->attachView($confirm_view);

		$this->setSubject(TC_getModuleConfig('pages', 'website_title'). ' - '.$purchase->title());
		$this->setShowInConsole();
	}

}
