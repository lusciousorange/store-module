<?php
class TMv_PurchaseEmailStaff extends TMv_PurchaseEmail
{
	/**
	 * TMv_PurchaseEmail constructor.
	 * @param bool|TMm_Purchase $purchase
	 */
	public function __construct($purchase)
	{
		parent::__construct($purchase);

		// Update the recipient
		$this->clearRecipients();
		$bcc_emails = TC_getModuleConfig('store', 'bcc_purchase_emails');
		foreach(explode(',',$bcc_emails) as $email)
		{
			$this->addRecipient( $email );
		}

	}
}
?>