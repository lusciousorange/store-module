<?php

/**
 * Class TMv_PurchaseForm
 *
 * This is a form to create or edit a purchase. Most of the time, purchases are created via the store/cart
 * mechanisms, however it's possible to manually create and edit them in the admin.
 */
class TMv_PurchaseForm extends TCv_FormWithModel
{
	use TMt_PaymentForm;
	
	/**
	 * @return bool|TCm_Model|TMm_Purchase
	 */
	public function model()
	{
		return parent::model();
	}
	
	public function configureFormElements()
	{
		$this->attachBasicSettings();
		
		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			$this->attachShippingAddressFields();
		}
		
		$this->attachBillingAddressFields();
		
		// TODO: Account for "type" which must be set for new orders
		
		// TODO: Add in ability to modify the cart. NOt actaully handled here
		// TODO: Deal with refunds when it involves shipping. That's not accounted for
		// TODO: Confirm shipping costs
	}
	
	
	protected function attachBasicSettings()
	{
		$field = new TCv_FormItem_Heading('basic_settings','Basic Settings');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('buyer_id', 'User');
		$field->setHelpText("A user is required, if the person does not exist, they must first be created in the Users module.");
		$field->setIsRequired();
		$field->addOption('','Select a User');
		$user_list = TMm_UserList::init();
		$field->useFiltering($user_list,'namesAndEmailsForSearchString', 'TMm_User');
		$this->attachView($field);
	}
	
	
	/**
	 * @param bool $disable_validation
	 */
	protected function attachShippingAddressFields($disable_validation = false)
	{
		$this->attachDeliveryFields();
		
		$this->attachView($this->shippingAddressSection($disable_validation));
		
		
		// Disable this functionality to avoid it triggering issues with ajax syncing
		// Perhaps a button that copies it instead. Avoids the off/on problem of having to switch off to trigger
		$this->removeFormItemWithID('same_billing_shipping');
		
		
		
	
	}
	
	protected function attachDeliveryFields()
	{
		$field = new TCv_FormItem_Heading('delivery_heading','Delivery');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('delivery_method','Delivery Method');
		$field->addHelpText("Choose from the delivery method. This allow you to select any method in the system");
		$field->addOption('','Select a Delivery Type');
		
		// Handle Pickup
		if(TC_getModuleConfig('store','pickup_permitted'))
		{
			$field->addOption('pickup','Pickup');
		}
		
		// Loop through all delivery providers
		// Grab the list of delivery providers and if they use the interface, add their options to the list
		$delivery_provider_classes = explode(',', TC_getModuleConfig('store','delivery_provider_classes'));
		foreach($delivery_provider_classes as $delivery_provider_class_name)
		{
			/** @var TMi_DeliveryProvider|TCm_ModelList $provider */
			$provider = ($delivery_provider_class_name)::init();
			$interfaces = class_implements($provider);
			if (isset($interfaces['TMi_DeliveryProvider']))
			{
				foreach($provider->models() as $delivery_option)
				{
					
					if(method_exists($delivery_option,'titleWithPrice'))
					{
						$field->addOption($delivery_option->contentCode(), $delivery_option->titleWithPrice());
					}
					else
					{
						$field->addOption($delivery_option->contentCode(), $delivery_option->title().' ($'
						                                                 .$delivery_option->price().')');
					}
				}
			}
		}
		$this->attachView($field);
		
		
		if($this->isEditor())
		{
			if($this->model()->isUnpaid())
			{
				$field = new TCv_FormItem_TextField('delivery_cost','Delivery Cost');
				$field->setIsFloat();
				$this->attachView($field);
			}
			else // Editing and is paid, can't alter it here. Require refund procedure
			{
				$field = new TCv_FormItem_HTML('delivery_cost','Delivery Cost');
				$field->addText('$'.$this->model()->deliveryPrice());
				$this->attachView($field);
				
			}
		}
		
	}
	
	protected function attachBillingAddressFields()
	{
		
		$billing_address_section = $this->billingAddressSection();
		$this->attachView($billing_address_section);
		
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		
		$form_processor->addDatabaseValue('user_id', $form_processor->formValue('buyer_id'));
		
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);
		
		/** @var TMm_Purchase $model */
		$model = $form_processor->model();
		
		if($form_processor->isCreator())
		{
			// Ensure the original is set
			$model->updateWithValues(array('original_purchase_id' => $model->id()));
			
			$form_processor->setSuccessURL('/admin/store/do/view/'.$model->id());
		}
	}
	
}