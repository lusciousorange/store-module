<?php
class TMv_PurchaseHistoryList extends TCv_SearchableModelList
{
	use TMt_PagesContentView;

	protected $view_purchase_menu_id;
	
	/**
	 * TMv_PurchaseHistoryList constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$user = TC_currentUser();

		$this->setFilterUsingModel($user, 'purchases');
		$this->setPagination(25);
		$this->defineColumns();
		$this->addClassCSSFile('TMv_PurchaseHistoryList');
		$this->addClassCSSFile('TMv_PurchaseList');
		$this->addClass('TMv_PurchaseList');
		
	}
	
	/**
	 * @param TMm_Purchase $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		if($model->isUnpaid())
		{
			$row->addClass('unpaid');
		}
		
		return $row;
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Your Order');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);

		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			$column = new TCv_ListColumn('delivery');
			$column->setTitle('Delivery');
			$column->setContentUsingListMethod('deliveryColumn');
			$this->addTCListColumn($column);
			
			
		}
		
		$column = new TCv_ListColumn('date_added');
		$column->setTitle('Date & Time');
		$column->setContentUsingModelMethod('dateAddedFormatted');
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('purchase_details');
		$column->setTitle('Purchase Details');
		$column->setWidthAsPercentage(40);
		$column->setContentUsingListMethod('detailsColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('taxes');
		$column->setTitle('taxes');
		$column->setContentUsingListMethod('taxesColumn');
		//$this->addTCListColumn($column);

		$column = new TCv_ListColumn('total');
		$column->setTitle('Total');
		$column->setContentUsingListMethod('totalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(120);
		$this->addTCListColumn($column);


//		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
//		$this->addTCListColumn($edit_button);
//
	}


	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$view_menu = TMm_PagesMenuItem::init($this->view_purchase_menu_id);
		$link = new TCv_Link();
		$link->setURL($view_menu->pathToFolder().$model->id());
		$link->addText($model->title());
		return $link;
	}
	
	

	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function detailsColumn($model)
	{
		$table = new TCv_HTMLTable();
		$table->addClass('purchase_details_table');
		foreach($model->purchaseItems() as $item)
		{
			$row = new TCv_HTMLTableRow();
			$row->createCellWithContent($item->quantity());
			$row->createCellWithContent($item->cartTitle());
			$table->attachView($row);
		}

		return $table;
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function deliveryColumn($model)
	{
		$string = $model->deliveryTitle();
		$string .= '<br />';
		$string .= $model->deliveryDate();

		$string .= '<br />';
		$string .= '$'.$model->deliveryPrice();
		return $string;
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function totalColumn($model)
	{
		return '$'.$model->total();
	}



	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'edit', 'fa-pencil');
		
	}


	/**
	 * Function that is called which sets up the list of filters used. By default,it includes the search field and nothing else.
	 *
	 * This method can be overridden to define other filters
	 */
	public function defineFilters()
	{
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems(): array
	{
		$form_items = array();

		
		$menu_0 = TMm_PagesMenuItem::init(0);


		$redirect = new TCv_FormItem_Select('view_purchase_menu_id','View Purchase Page');
		$redirect->setHelpText('Select a page where the customer will be sent when they click on a purchase.');
		$redirect->setIsRequired();
		$redirect->useFiltering();
		$redirect->addOption('', 'Not Set');
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}

			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}

		$form_items[] = $redirect;

		return $form_items;
	}

	public static function pageContent_ViewTitle(): string
	{ return 'Purchase History list'; }
	public static function pageContent_IconCode(): string
	{ return 'fa-cart-arrow-down'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The list of purchases for the logged in user.';
	}

		
}
?>