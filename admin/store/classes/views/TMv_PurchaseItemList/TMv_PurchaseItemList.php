<?php
class TMv_PurchaseItemList extends TCv_SearchableModelList
{
	protected $month_names = array(
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December',
	);
	
	/**
	 * TMv_PurchaseList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_PurchaseItem');
		$this->defineColumns();
		$this->setPagination(50);
		$this->setModelListClass('TMm_PurchaseList', 'processFilterPurchaseItems');
		$this->addClass('TMv_PurchaseList');
		$this->addClassCSSFile('TMv_PurchaseList');
		
		
		// Set the filter ID to be consistent for reporting
		$this->setSaveFilterID('purchase_item_list');
		
		
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		if(TC_getModuleConfig('store','use_accounting_codes'))
		{
			$column = new TCv_ListColumn('accounting_code');
			$column->setTitle('Code');
			$column->setContentUsingListMethod('accountingCodeColumn');
			$column->setWidthAsPixels(70);
			$this->addTCListColumn($column);
		}
		
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('purchase');
		$column->setTitle('Purchase');
		$column->setContentUsingListMethod('purchaseColumn');
		$column->setWidthAsPercentage(12);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('buyer');
		$column->setTitle('Buyer');
		$column->setContentUsingListMethod('buyerColumn');
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('date_added');
		$column->setTitle('Date & Time');
		$column->setContentUsingModelMethod('dateAddedFormatted');
		$column->setWidthAsPercentage(20);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('quantity');
		$column->setTitle('Qty');
		$column->setContentUsingModelMethod('quantity');
		$column->setAlignment('center');
		$column->setWidthAsPixels(40);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('subtotal');
		$column->setTitle('Subtotal');
		$column->setContentUsingListMethod('subtotalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('taxes');
		$column->setTitle('Taxes');
		$column->setContentUsingListMethod('taxesColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(100);
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('total');
		$column->setTitle('Total');
		$column->setContentUsingListMethod('totalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		
	}
	
	/**
	 * @param TMm_PurchaseItem $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		if($model->isUnpaid())
		{
			$row->addClass('unpaid');
		}
		if($model->hasRefunds())
		{
			$row->addClass('refund');
		}
		
		
		return $row;
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_PurchaseItem $model
	 * @return TCv_View|string
	 */
	public function accountingCodeColumn($model)
	{
		if($model->item())
		{
			return $model->item()->accountingCode();
		}
		
		return '';
		
	}
	
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_PurchaseItem $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$view = new TCv_View();
		$link = $this->linkForModuleURLTargetName($model->purchase(), 'view');
		
		if($model->item())
		{
			$link->addText($model->item()->cartTitle($model));
		}
		else
		{
			$class_name = $model->itemClass();
			$class_title = 'Item';
			if(class_exists($class_name))
			{
				$class_title = $class_name::$model_title;
				
			}
			$link->addText($class_title.' Not Found');
		}


//		if(method_exists($model, 'transactionID'))
//		{
//			$link->addText('<br/>' . $model->transactionID());
//		}
		
		$view->attachView($link);
		
		if($model->hasRefunds())
		{
			$title = new TCv_View();
			if($model->numRefundable() == 0)
			{
				$title->addText('Refunded');
			}
			else
			{
				$title->addText('Partial Refund');
				
			}
			
			$title->addClass('refund_title');
			$title->addClass('space_left');
			$view->attachView($title);
		}
		
		if($model->isUnpaid())
		{
			$title = new TCv_View();
			$title->addText('Unpaid');
			$title->addClass('unpaid_title');
			$title->addClass('space_left');
			$view->attachView($title);
		}
		
		
		
		$description = $model->cartDescription();
		
		if(is_string($description))
		{
			$inside = new TCv_View();
			$inside->addText($description);
			$view->attachView($inside);
			
		}
		elseif($description instanceof TCv_View)
		{
			$view->attachView($description);
			
		}
		
		
		
		return $view;
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_PurchaseItem $model
	 * @return TCv_View|string
	 */
	public function purchaseColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model->purchase(), 'view');
		$link->addText($model->purchase()->title());
		
		return $link;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_PurchaseItem $model
	 * @return TCv_View|string
	 */
	public function buyerColumn($model)
	{
		$purchase = $model->purchase();
		if($buyer = $purchase->buyer())
		{
			$string = $buyer->fullName();
			$string .= '<br />';
			$string .= $buyer->email();
			return $string;
			
		}
		$string = $purchase->buyerName();
		$string .= '<br />';
		$string .= $purchase->email();
		return $string;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function subtotalColumn($model)
	{
		return '$'.$model->subtotal();
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function taxesColumn($model)
	{
		$tax_values = array();
		$tax_list = TMm_TaxList::init();
		foreach ($tax_list->taxes() as $tax)
		{
			$tax_value = $model->totalForTax($tax);
			if($tax_value > 0)
			{
				$tax_values[] = $tax->title()." $".$tax_value;
			}
			
		}
		return implode('<br />', $tax_values);
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function totalColumn($model)
	{
		return '$'.$model->total();
	}
	
	
	
	/**
	 * Defines the filters for view
	 */
	public function defineFilters()
	{
		//parent::defineFilters();
		
		$cartable_classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		$field = new TCv_FormItem_Select('purchase_item_code','Item');
		$field->addOption('all','All types');
		$field->useFiltering();
		foreach($cartable_classes as $class_name)
		{
			$field->startOptionGroup($class_name.'_all',$class_name::modelTitlePlural());
			$field->addOption($class_name, 'All '.$class_name::modelTitlePlural());
			foreach($class_name::allModels() as $model)
			{
				$field->addOption($model->contentCode(), $model->cartTitle(null));
				
			}
		}
		$this->addFilterFormItem($field);
		
		$field = new TCv_FormItem_Select('purchase_year','Year');
		$field->addOption('', 'All Years');
		$field->setOptionsWithNumberRange(date('Y'), 2017);
		$this->addFilterFormItem($field);
		
		$field = new TCv_FormItem_Select('purchase_month','Month');
		$field->addOption('', 'All Months');
		foreach($this->month_names as $num => $name)
		{
			$field->addOption($num, $name);
		}
		
		$this->addFilterFormItem($field);
		
		$user_list = TMm_UserList::init();
		$field = new TCv_FormItem_Select('buyer_id', 'Customer');
		$field->useFiltering();
		$field->addOption('', 'Select a User');
		foreach($user_list->users() as $user)
		{
			$user_title = $user->title() . ' | ' . $user->email();
			$field->addOption($user->id(), $user_title);
		}
		$this->addFilterFormItem($field);

//		$field = new TCv_FormItem_Select('purchase_day','Day');
//		$field->setOptionsWithNumberRange(date('Y'), 2017);
//		$field->setDefaultValue(TC_getModuleConfig('system','event_year'));
//		$this->addFilterFormItem($field);
	
	}
	
	/**
	 * Method to generate the purchase list based on the values from this filter
	 */
	public function generatePurchaseItemListExcel()
	{
		$this->defineFilters();
		
		$list = TMm_PurchaseList::init();
		
		$filter_values = $this->filterValues();
		
		// Make sure to not ignore refunded items
		$purchase_items = $list->processFilterPurchaseItems($filter_values, false);
		
		$title = 'Purchase Items';
		
		if($filter_values['search'] != '')
		{
			$title .= ' : "'.$filter_values['search'].'"';
		}
		
		if($filter_values['item_class'] != '')
		{
			$class_name = $filter_values['item_class'];
			$title .= ' : '.$class_name::$model_title;
			
		}
		
		if($filter_values['purchase_month'] != '')
		{
			
			$title .= ' : '.$this->month_names[$filter_values['purchase_month']];
		}
		
		if($filter_values['purchase_year'] != '')
		{
			$title .= ' : '.$filter_values['purchase_year'];
		}
		
		$class_name = TMm_PurchaseItemListExcel::classNameForInit();
		
		$excel = new $class_name($purchase_items,$title);
		$excel->generate();
		
	}
}