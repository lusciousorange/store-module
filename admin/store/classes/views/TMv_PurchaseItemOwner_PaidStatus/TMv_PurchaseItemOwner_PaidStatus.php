<?php

/**
 * Class TMv_PurchaseItemOwner_PaidStatus
 *
 * A status for a purchase item owner that shows admin links to purchase items as well options to handle payment if
 * necessary.
 */
class TMv_PurchaseItemOwner_PaidStatus extends TCv_View
{
	protected $owner;
	protected $show_purchase_titles = false;
	
	/**
	 * TMv_PurchaseItemOwner_PaidStatus constructor.
	 * @param TCm_Model|TMt_PurchaseItemOwner $owner
	 */
	public function __construct($owner)
	{
		parent::__construct();
		
		$this->owner = $owner;
		
		$this->addClassCSSFile('TMv_PurchaseItemOwner_PaidStatus');
		
		$this->setAlignmentRight();
	}
	
	public function setAlignmentLeft()
	{
		$this->addClass('left_align');
		$this->removeClass('right_align');
	}
	
	public function setAlignmentRight()
	{
		$this->addClass('right_align');
		$this->removeClass('left_align');
	}
	
	
	
	/**
	 * Sets if the purchase IDs are shown in the link
	 * @param bool $show
	 * @return void
	 */
	public function setShowPurchaseTitles(bool $show = true)
	{
		$this->show_purchase_titles = $show;
	}
	
	public function render()
	{
		
		
		if($this->owner->isPurchased())
		{
			foreach($this->owner->purchaseItems() as $purchase_item)
			{
				if($purchase_item)
				{
					$purchase = $purchase_item->purchase();
					
					$purchase_item_link = new TCv_Link();
					$purchase_item_link->setURL('/admin/store/do/view/' . $purchase->id());
					$purchase_item_link->setTitle('View Purchase ' . $purchase->id());
					if($purchase->isOnline())
					{
						$purchase_item_link->setIconClassName('fa-credit-card');
						$purchase_item_link->addClass('paid');
					}
					elseif($purchase->isCheck())
					{
						$purchase_item_link->setIconClassName('fa-money-check');
						$purchase_item_link->addClass('paid');
					}
					elseif($purchase->isComp())
					{
						$purchase_item_link->setIconClassName('fab fa-creative-commons-zero');
					}
					
					if($purchase->isUnpaid())
					{
						$purchase_item_link->setIconClassName('fa-exclamation-triangle');
						$purchase_item_link->addClass('unpaid');
					}
					
					$purchase_item_link->addText(' $' . $purchase_item->total());
					
					if($this->show_purchase_titles)
					{
						$purchase_item_link->addText(' – '.$purchase->title());
					}
					
					$this->attachView($purchase_item_link);
				}
			}
		
			
		}
		
		$owing = round( $this->owner->totalOwing(),2);
		
		if($owing > 0)
		{
			$owing_link = new TCv_Link();
			$owing_link->setIconClassName('fa-usd-square');
			$owing_link->addText(' Owes $'.$this->formatCurrency($owing));
			$owing_link->addClass('owing');
			$owing_link->setURL('#');
			
			// If it's something we can add to our cart, provide the option to do so
			$interfaces = class_implements($this->owner);
			if(isset($interfaces['TMi_ShoppingCartable']))
			{
				$owing_link->setURL('/admin/store/do/add-to-admin-cart/?quantity=1&content_code='
									.$this->owner->contentCode());
			}
			
			$this->attachView($owing_link);
		}
		elseif($owing < 0)
		{
			$owing *= -1;
			$owing_link = new TCv_Link();
			$owing_link->setIconClassName('fa-usd-square');
			$owing_link->addText(' Ref $'.$this->formatCurrency($owing));
			$owing_link->addClass('unpaid');
			$owing_link->setURL('#');
			
			
			
			$this->attachView($owing_link);
		}
		
	}
}