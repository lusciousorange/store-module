<?php
class TMv_PurchaseList extends TCv_SearchableModelList
{
	protected $month_names = array(
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December',
	);
	
	/**
	 * TMv_PurchaseList constructor.
	 * @param TMm_User|TMt_Store_User|bool $user
	 */
	public function __construct($user = false)
	{
		parent::__construct();
		
		$this->setModelClass('TMm_Purchase');
		$this->defineColumns();
		$this->setPagination(50);
		$this->addClassCSSFile('TMv_PurchaseList');
		
		// Set the filter ID to be consistent for reporting
		$this->setSaveFilterID('purchase_list');
		
		
		
		if($user instanceof TMm_User)
		{
			$this->disableFilterFunctionality();
			$this->addModels($user->purchases());
		}
	}
	
	/**
	 * @param TMm_Purchase $model
	 * @return bool|TCu_Item|TCv_ListRow
	 */
	public function rowForModel($model)
	{
		$row = parent::rowForModel($model);
		
		// First since we always want to show these
		if($model->isTestTransaction())
		{
			$row->addClass('test_transaction');
		}
		
		// Cancelled appear but shouldn't have any other flags
		if($model->isCancelled())
		{
			$row->addClass('cancelled');
			return $row;
		}
		
		if($model->isUnpaid())
		{
			$row->addClass('unpaid');
		}
		if($model->hasRefunds())
		{
			$row->addClass('refund');
		}
		
		
		return $row;
	}
	
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('buyer');
		$column->setTitle('Buyer');
		$column->setContentUsingListMethod('buyerColumn');
		$column->setWidthAsPixels(200); // made column tiny. no sense.
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('date_added');
		$column->setTitle('Purchase Date');
		$column->setContentUsingListMethod('orderDateColumn');
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		if(TC_getModuleConfig('store','shipping_enabled'))
		{
			$column = new TCv_ListColumn('delivery');
			$column->setTitle('Delivery');
			$column->setContentUsingListMethod('deliveryColumn');
			$this->addTCListColumn($column);
		}
		
		$column = new TCv_ListColumn('type');
		$column->setTitle('Type');
		$column->setContentUsingModelMethod('typeTitle');
		$column->setWidthAsPixels(110);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		
		$column = new TCv_ListColumn('purchase_details');
		$column->setTitle('Purchase Details');
		$column->setWidthAsPercentage(20);
		$column->setContentUsingListMethod('detailsColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('subtotal');
		$column->setTitle('Subtotal');
		$column->setContentUsingListMethod('subtotalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('taxes');
		$column->setTitle('Taxes');
		$column->setContentUsingListMethod('taxesColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(100);
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('total');
		$column->setTitle('Total');
		$column->setContentUsingListMethod('totalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		
	}
	
	/**
	 * @param TMm_Purchase $purchase
	 * @return TCv_Link|TCv_View
	 */
	protected function linkForPurchase($purchase)
	{
		$link = $this->linkForModuleURLTargetName($purchase, 'view');
		$link->addText($purchase->title());
		
		return $link;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$view = new TCv_View();
		$link = $this->linkForPurchase($model);
		$view->attachView($link);
		
		if($model->isTestTransaction())
		{
			$title = new TCv_View();
			$title->addText('Test');
			$title->addClass('test_title');
			$title->addClass('space_left');
			$view->attachView($title);
		}
		
		if($model->isCancelled())
		{
			$title = new TCv_View();
			$title->addText('Cancelled');
			$title->addClass('cancelled_title');
			$title->addClass('space_left');
			$view->attachView($title);
		}
		else // only bother with paid items if it's not cancelled
		{
			if($model->hasRefunds())
			{
				$title = new TCv_View();
				if($model->total() == 0)
				{
					$title->addText('Refunded');
				}
				else
				{
					$title->addText('Partial Refund');
					
				}
				
				$title->addClass('refund_title');
				$title->addClass('space_left');
				$view->attachView($title);
			}
			
			if($model->isUnpaid())
			{
				$title = new TCv_View();
				$title->addText('Unpaid');
				$title->addClass('unpaid_title');
				$title->addClass('space_left');
				$view->attachView($title);
			}
		}
		
		
		
		
		
		if($model->isOnline() && $model->transactionID() != '')
		{
			$transaction_id = new TCv_View();
			$transaction_id->addClass('transaction_id');
			$transaction_id->addText($model->transactionID());
			$view->attachView($transaction_id);
		}
		
		
		return $view;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function buyerColumn($model)
	{
		if($buyer = $model->buyer())
		{
			$view = new TCv_View();
			
			if(TC_currentUser()->isAdmin())
			{
				
				$link = new TCv_Link();
				$link->setURL('/admin/users/do/edit/'.$buyer->id());
				$link->addText($buyer->fullName());
				$view->attachView($link);
				
				
			}
			else
			{
				$view->addText($buyer->fullName());
			}
			
			
			$view->addText('<br />'.$buyer->email());
			return $view;
			
		}
		$string = $model->buyerName();
		$string .= '<br />';
		$string .= $model->email();
		return $string;
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function orderDateColumn($model)
	{
		// Smaller text to make it easier to scan
		return $model->dateAddedFormatted('M j, Y \– g:ia');
		
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function deliveryColumn($model)
	{
		$string = $model->deliveryTitle();
		
		if($model->deliveryPrice() > 0)
		{
			$string .= '<br />';
			$string .= '$'.$model->deliveryPrice();
		}
		return $string;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function detailsColumn($model)
	{
		$table = new TCv_HTMLTable();
		$table->addClass('purchase_details_table');
		foreach($model->purchaseItems() as $item)
		{
			$row = new TCv_HTMLTableRow();
			$row->createCellWithContent($item->quantity());
			$row->createCellWithContent($item->cartTitle());
			if($item->isRefund())
			{
				$row->addClass('refund_item_row');
			}
			$table->attachView($row);
		}
		
		return $table;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function subtotalColumn($model)
	{
		return '$'.$model->subtotal();
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function taxesColumn($model)
	{
		$tax_values = array();
		$tax_list = TMm_TaxList::init();
		foreach ($tax_list->taxes() as $tax)
		{
			$tax_value = $model->totalForTax($tax);
			if($tax_value > 0)
			{
				$tax_values[] = $tax->title()." $".$tax_value;
			}
			
		}
		return implode('<br />', $tax_values);
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Purchase $model
	 * @return TCv_View|string
	 */
	public function totalColumn($model)
	{
		return '$'.$model->total();
	}
	
	
	
	
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'edit', 'fa-pencil');
		
	}
	
	
	/**
	 * Defines the filters for view
	 */
	public function defineFilters()
	{
		parent::defineFilters();
		
		$field = new TCv_FormItem_Select('type','Payment type');
		$field->addOption('', 'All payment types');
		foreach(TMm_Purchase::transactionTypes() as $code => $title)
		{
			$field->addOption($code, $title);
		}
		$this->addFilterFormItem($field);
		
		
		$field = new TCv_FormItem_Select('purchase_year','Year');
		$field->addOption('', 'All years');
		$field->setOptionsWithNumberRange(date('Y'), 2017);
		$this->addFilterFormItem($field);
		
		$field = new TCv_FormItem_Select('purchase_month','Month');
		$field->addOption('', 'All months');
		foreach($this->month_names as $num => $name)
		{
			$field->addOption($num, $name);
		}
		
		$this->addFilterFormItem($field);
		
		
		// Filter by shopping cart item
		$this->addFilterFormItem($this->cartItemFilter());
		
		$user_list = TMm_UserList::init();
		$field = new TCv_FormItem_Select('buyer_id', 'Customer');
		$field->useFiltering();
		$field->addOption('', 'Select a User');
		foreach($user_list->users() as $user)
		{
			$user_title = $user->title() . ' | ' . $user->email();
			$field->addOption($user->id(), $user_title);
		}
		$this->addFilterFormItem($field);
		
	}
	
	/**
	 * A filter for cart items. This allows someone to view only purchases with specific items
	 * This should be a separate function because it is likely to require some "custom" stuff for projects.
	 * @return TCv_FormItem_Select
	 */
	public function cartItemFilter() : TCv_FormItem_Select
	{
		$field = new TCv_FormItem_Select('purchase_item_code','Cart Item');
		$field->addOption('','All Items');
		$field->useFiltering();
		// Get and loop through classes using TMi_ShoppingCartable
		$classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		foreach ($classes as $class_name)
		{
			/** @var TCm_Model|string $class_name */
			// Two scenarios here, one where these items are in the DB, the other where they are auto-generated
			// The auto-generated ones will also have the trait TMt_PurchaseItemOwner since it's how we differentiate
			
			if($class_name::hasTrait('TMt_PurchaseItemOwner'))
			{
				$field->addOption($class_name, $class_name::modelTitlePlural());
				
			}
			else // items are existing items in the system
			{
				// Create a group of options that includes the items/models for the class name
				$field->startOptionGroup($class_name, $class_name::modelTitlePlural());
				
				// use the base class, the init will handle any extensions
				$list_name = $class_name::baseClass() . 'List';
				$items = $list_name::init()->models();
				
				foreach($items as $item)
				{
					
					// Use contentCode as the ID so we get the class name as well
					$field->addOption($item->contentCode(), $item->cartTitle(null));
				}
				
				$field->endOptionGroup();
			}
		}
		
		return $field;
	}
	
	/**
	 * Method to generate the purchase list based on the values from this filter
	 */
	public function generatePurchaseListExcel()
	{
		// this may take a while
		set_time_limit(0);
		ob_start("ob_gzhandler");
		$this->defineFilters();
		
		$list = TMm_PurchaseList::init();
		
		$filter_values = $this->filterValues();
		$purchases = $list->processFilterListAsModels($filter_values);
		
		$title = 'Purchases';
		
		if($filter_values['search'] != '')
		{
			$title .= ' : "'.$filter_values['search'].'"';
		}
		
		if($filter_values['purchase_month'] != '')
		{
			
			$title .= ' : '.$this->month_names[$filter_values['purchase_month']];
		}
		
		if($filter_values['purchase_year'] != '')
		{
			$title .= ' : '.$filter_values['purchase_year'];
		}
		
		$class_name = TMm_PurchaseListExcel::classNameForInit();
		
		$excel = new $class_name($purchases,$title);
		$excel->generate();
		ob_end_flush();
	}
	
}
?>