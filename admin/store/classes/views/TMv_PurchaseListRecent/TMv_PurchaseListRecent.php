<?php
class TMv_PurchaseListRecent extends TMv_PurchaseList
{
	use TMt_DashboardView;
	/**
	 * TMv_PurchaseList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$purchase_list = TMm_PurchaseList::init();
		$this->disableFilterFunctionality();
	
		$models = $purchase_list->processFilterListAsModels(array('limit' => 10));
		
		
		$this->addModels($models );
		
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('date_added');
		$column->setTitle('Date & Time');
		$column->setContentUsingModelMethod('dateAddedFormatted');
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('type');
		$column->setTitle('Type');
		$column->setContentUsingModelMethod('typeTitle');
		$column->setWidthAsPixels(60);
		$column->showInResponsive();
		$this->addTCListColumn($column);
	
		$column = new TCv_ListColumn('total');
		$column->setTitle('Total');
		$column->setContentUsingListMethod('totalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$column->showInResponsive();
		$this->addTCListColumn($column);
		
		
	}
	
	//////////////////////////////////////////////////////
	//
	// TMt_DashboardView
	//
	//////////////////////////////////////////////////////
	
	
	public static function pageContent_ViewTitle(): string
	{
		return 'Recent Purchases';
	}
	
	/**
	 * Returns the default title for this dashboard
	 * @return string
	 */
	public static function dashboard_DefaultTitle()
	{
		return 'Recent Purchases';
	}
	
	/**
	 * Returns the default view-all URL for this dashboard
	 * @return string
	 */
	public static function dashboard_DefaultViewAllURL()
	{
		return '/admin/store/';
	}
	
}