<?php

/**
 * Class TMv_PurchaseMissingPaymentForm
 *
 * A form that allows admins to manually process a purchase that is not yet paid
 */
class TMv_PurchaseMissingPaymentForm extends TCv_FormWithModel
{
	use TMt_ManualPaymentForm;
	
	/**
	 * TMv_PurchaseMissingPaymentForm constructor.
	 * @param TMm_Purchase $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->addClassCSSFile('TMv_PurchaseMissingPaymentForm');
		
		// track the saved cards
		if($model->buyer() && $model->paymentProcessor())
		{
			$this->saved_cards = $model->paymentProcessor()->savedCardsForUser($model->buyer());
		}
		
		
	}
	
	/**
	 * Returns the model for this form
	 * @return bool|TCm_Model|TMm_Purchase
	 */
	public function model()
	{
		return parent::model();
	}
	
	/**
	 * @param bool $show_form
	 * @return string
	 */
	public function html($show_form = true)
	{
		if($show_form && TC_getModuleConfig('store','transactions_disabled'))
		{
			$view = TMv_TransactionsDisabledWarning::init();
			$this->attachView($view);
			$show_form = false;
			
		}
	
		// Hard check to avoid showing this in scenarios that might require it
		$user = TC_currentUser();
		
		if($this->model()->isUnpaid()
			&& TC_isTungstenView()
			&& $user
			&& $user->isAdmin()
			&& $this->model()->isNotCancelled() )
		{
			return parent::html($show_form);
		}
		
		return '';
		
	}
	
	public function configureFormElements()
	{
		$this->setButtonText('Process Purchase Payment');
		
		$this->attachPaymentFormFields();
		
		$field = new TCv_FormItem_Select('send_email', 'Send Email');
		$field->setSaveToDatabase(false);
		$field->setHelpText("Indicate if the purchase email should be sent");
		$field->addOption('0', 'No - Do NOT send Purchase Email');
		$field->addOption('1', 'Yes - Send Purchase Email');
		$this->attachView($field);
		
	}
	
	
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		static::validateManualPaymentFormProcessor($form_processor);
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		/** @var TMm_Purchase $purchase */
		$purchase = $form_processor->model();
		
		static::processPaymentsForPurchase($purchase, $form_processor);
		
		if($form_processor->isValid() && $form_processor->formValue('send_email'))
		{
			$purchase->sendPaidEmail();
		}
		
		
	}
	
}