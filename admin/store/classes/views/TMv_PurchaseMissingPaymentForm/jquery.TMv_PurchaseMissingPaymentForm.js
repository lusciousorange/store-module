(function ($)
{
	// INIT
	$.fn.TMv_PurchaseMissingPaymentForm = function(options)
	{
		let $form = $('.TMv_PurchaseMissingPaymentForm');

		var settings = $.extend(
			{
			}, options || {});

		return this.each(function()
		{
			// add listeners
			$form.find('select#is_split_payment').change(function(e) { splitChanged(); });
			$form.find('select#type').change(function(e) { typeChanged(1); });
			$form.find('select#type_2').change(function(e) { typeChanged(2); });
			splitChanged();
			typeChanged(1);
			typeChanged(2);

		});



		function splitChanged()
		{
			let is_split = $form.find('select#is_split_payment').val();
			if(is_split == '1')
			{
				$form.find('.split_payment_row').slideDown();
			}
			else
			{
				$form.find('.split_payment_row').slideUp();
			}

		}

		// function type1Changed()
		// {
		//
		// 	let type = $form.find('select#type').val();
		//
		// 	if(type == 'online')
		// 	{
		// 		$form.find('#card_id_row').slideDown();
		// 		$form.find('#transaction_id_row').slideUp();
		// 	}
		// 	else
		// 	{
		// 		$form.find('#card_id_row').slideUp();
		// 		$form.find('#transaction_id_row').slideDown();
		// 	}
		//
		// }

		function typeChanged(num)
		{
			var suffix = '';
			if(num > 1)
			{
				suffix = '_'+num;
			}
			let type = $form.find('select#type'+suffix).val();

			if(type == 'online')
			{
				$form.find('#card_id'+suffix+'_row').slideDown();
				$form.find('#transaction_id'+suffix+'_row').slideUp();
			}
			else
			{
				$form.find('#card_id'+suffix+'_row').slideUp();
				$form.find('#transaction_id'+suffix+'_row').slideDown();
			}

		}

	};


//END THE WRAPPER
})(jQuery);
