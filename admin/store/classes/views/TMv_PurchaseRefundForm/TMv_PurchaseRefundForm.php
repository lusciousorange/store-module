<?php
class TMv_PurchaseRefundForm extends TCv_FormWithModel
{
	use TMt_ManualPaymentForm;
	
	/**
	 * @var TMm_Purchase $purchase
	 */
	protected $purchase;
	
	protected $num_refundable = 0;
	
	/**
	 * TMv_PurchaseRefundForm constructor.
	 * @param TMm_Purchase $purchase
	 */
	public function __construct($purchase)
	{
		parent::__construct($purchase);
		
		$this->purchase = $purchase;
		
		$this->setButtonText('Refund Purchase');
		
		$this->setSuccessURL('view');
		
		$this->addClassCSSFile('TMv_PurchaseRefundForm');
		$this->addClassJQueryFile('TMv_PurchaseRefundForm');
		$this->addClassJQueryInit('TMv_PurchaseRefundForm');
		
		$this->configureManualPaymentFormCSSandJS();
		
		
	}
	
	public function configureFormElements()
	{
		$this->attachPurchaseDetailFields();
		
		$this->attachMainSettingFields();
		
		$this->attachPurchaseItemFields();
		
		$this->attachCreatedItemFields();
		
		
		if($this->num_refundable == 0)
		{
			$this->hideSubmitButton();
			$field = new TCv_FormItem_HTML('no_refund', 'No Refunds');
			$field->addText('There is nothing left to refund on this purchase');
			$this->attachView($field);
			return;
		}
		
	}
	
	public function attachPurchaseDetailFields()
	{
		if($this->purchase->isTestTransaction())
		{
			$test_view = new TCv_View('test_transaction_warning');
			$test_view->addClass('purchase_warning_bar');
			$test_view->addText('Purchase used a test account. No financial transaction occurred.');
			$this->attachView($test_view);
		}
		
		
		$field = new TCv_FormItem_Heading('purchase_heading', 'Purchase Details');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_HTML('purchase', 'Date and Time');
		$field->addText($this->purchase->dateAddedFormatted());
		$field->preventLayoutStacking();
		$this->attachView($field);
		
		$field = new TCv_FormItem_HTML('buyer', 'Buyer');
		$field->addText($this->purchase->buyerName());
		$field->preventLayoutStacking();
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_HTML('purchase_values', 'Available Subtotal');
		$field->addText('$' . $this->purchase->subtotal());
		$field->preventLayoutStacking();
		$this->attachView($field);
		
		if($this->purchase->deliveryIsUsed())
		{
			$field = new TCv_FormItem_HTML('delivery_value', 'Available Delivery');
			$field->addText('$' . $this->purchase->deliveryPrice());
			$field->preventLayoutStacking();
			$this->attachView($field);
			
		}
		
		$tax_list = TMm_TaxList::init();
		foreach($tax_list->taxes() as $tax)
		{
			$field = new TCv_FormItem_HTML('tax_' . $tax->id(), 'Available ' . $tax->title());
			$field->addText('$' . $this->purchase->totalForTax($tax));
			$field->preventLayoutStacking();
			$this->attachView($field);
			
		}
		
		$field = new TCv_FormItem_HTML('total_values', 'Available Total');
		$field->addText('$' . $this->purchase->total());
		$field->preventLayoutStacking();
		$this->attachView($field);
		
	}
	
	public function attachPurchaseItemFields()
	{
		$field = new TCv_FormItem_Heading('purchase_item_heading', 'Refundable Purchase Items');
		$field->setHelpText("Indicate how many of each item you want to refund. If the quantity is one (1), then you can also
		choose to refund a custom amount other than the full price. ");
		$this->attachView($field);
		
		if($this->purchase->deliveryIsUsed() && $this->purchase->deliveryPrice() > 0)
		{
			$field = new TCv_FormItem_Select('delivery_refund', 'Delivery');
			$field->addOption(0, 'Do Not Refund');
			$field->addOption(1,  'Yes – Refund $' . $this->purchase->deliveryPrice() . '');
			$field->addOption('custom', 'Custom – Refund a custom amount of delivery');
			$field->addFormElementDataValue('purchase_item_id', 'delivery');
			$field->addFormElementCSSClass('quantity_field');
			$this->attachView($field);
			
			$field = new TCv_FormItem_TextField('custom_amount_delivery', 'Custom Amount');
			$field->setHelpText("Enter a pre-tax amount that should be refunded.");
			$field->setIsFloat();
			$field->addClass('custom_row');
			$field->addClass('custom_row_delivery');
			$this->attachView($field);
		}
		
		
		foreach($this->purchase->purchaseItems() as $purchase_item)
		{
			if($purchase_item->isRefundable())
			{
				// Quantity is almost always a select, possibly extended to be a hidden if disabled.
				$field = new TCv_FormItem_Select('quantity_' . $purchase_item->id(), $purchase_item->cartTitle());
				$field->setHelpText('$' . $purchase_item->pricePerItem());
				$field->addFormElementDataValue('purchase_item_id', $purchase_item->id());
				$field->addFormElementCSSClass('quantity_field');
				$field->addOption(0, 'Do Not Refund');
				for($num = 1; $num <= $purchase_item->numRefundable(); $num++)
				{
					$total = $num * $purchase_item->pricePerItem();
					$total = $purchase_item->formatCurrency($total);
					$field->addOption($num, $num . ' – $' . $total . '');
				}
				
				if($purchase_item->numRefundable() == 1)
				{
					$field->addOption('custom', 'Custom – Refund a custom amount from this item');
					
				}
				//	$field->setOptionsWithNumberRange(1, $purchase_item->numRefundable());
				$field->setIsRequired();
				$this->attachView($field);
				
				if($purchase_item->numRefundable() == 1)
				{
					$field = new TCv_FormItem_TextField('custom_amount_' . $purchase_item->id(), 'Custom Amount');
					$field->setHelpText("Enter a pre-tax amount that should be refunded.");
					$field->setIsFloat();
					$field->addClass('custom_row');
					$field->addClass('custom_row_' . $purchase_item->id());
					$this->attachView($field);
				}
				
				$this->num_refundable += $purchase_item->numRefundable();
				
			}
		}
	}
	
	public function attachCreatedItemFields()
	{
		
		$views = array();
		
		foreach($this->purchase->purchaseItems() as $purchase_item)
		{
			if($purchase_item->isRefundable())
			{
				$group = new TCv_FormItem_Group('created_purchase_group_'.$purchase_item->id(),'');
				$num_created = 0;
				$first = true;
				foreach($purchase_item->createdItems() as $created_item)
				{
					$num_created++;
					$title = '';
					if($first)
					{
						$title = $purchase_item->cartTitle();
					}
					$form_item = new TCv_FormItem_Checkbox($created_item->contentCode(), $title);
					
					// Non-Deleteable
					if($created_item->isRefundDeletable())
					{
						$form_item->setLabel($created_item->title());
					}
					else
					{
						$form_item->addClass('non_deletable');
						$form_item->setLabel($created_item->title().' – Cannot be deleted');
						
					}
					$group->attachView($form_item);
					
					
					
					$first = false;
				}
				
				if($num_created > 0)
				{
					$views[] = $group;
				}
				
			}
			
			if(count($views) > 0)
			{
				$field = new TCv_FormItem_Heading('created_items_heading','Remove Created Items');
				$field->setHelpText("Indicate if any of the created items in the system that resulted from this purchase should also be deleted. ");
				$this->attachView($field);
				
				foreach($views as $view)
				{
					$this->attachView($view);
				}
			}
			
		}
		
		
	}
	
	public function attachMainSettingFields()
	{
		$field = new TCv_FormItem_Heading('refund_heading','Refund Settings');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_TextField('notes', 'Notes/Comments');
		$field->addHelpText("An optional short note about this refund");
		$this->attachView($field);
		
		$this->attachView($this->splitTypeField(1));
		//$this->attachView($this->splitCardField(1));
		$this->attachView($this->splitCreditCardTypeField(1));
		$this->attachView($this->splitTransactionIDField(1));
		
		
		if($this->purchase->paymentRefundPossible())
		{
			
			$refund_class_name = $this->purchase->refundProcessorClass();
			$refund_processor_name = $refund_class_name::processorName();
			
			// Quantity is almost always a select, possibly extended to be a hidden if disabled.
			$field = new TCv_FormItem_Select('attempt_transaction', 'Refund Payment');
			$field->addHelpText('Indicate if the system should attempt to refund the payment via the payment processor.');
			$field->setIsRequired();
			$field->addOption('1', 'YES – Attempt to refund the money with '.$refund_processor_name);
			$field->addOption('0', 'NO – Do not perform any financial transactions');
			
			$this->attachView($field);
		}
		else
		{

			$field = new TCv_FormItem_Hidden('attempt_transaction','');
			$field->setValue(0);
			$field->setAsPrivateValue();
			$this->attachView($field);


		}

	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// IF HERE – We can create the refund
		/** @var TMm_Purchase $purchase*/
		$purchase = $form_processor->model();
		$refund_delivery_amount = 0;
		
		// STEP 1 : Check for delivery
		if($form_processor->fieldIsSet('delivery_refund'))
		{
			$quantity = $form_processor->formValue('delivery_refund');
			if($quantity == 'custom')
			{
				$amount = $form_processor->formValue('custom_amount_delivery');
				$refund_delivery_amount += $amount;
				
				if($amount > $purchase->deliveryPrice())
				{
					$form_processor->failFormItemWithID(
						'custom_amount_delivery',
						'You are unable to refund more delivery than the actual cost');
					
					return;
				}
				
			}
			elseif($quantity > 0)
			{
				$refund_delivery_amount += $quantity * $purchase->deliveryPrice();
				
			}
		}
		
		
		// STEP 2 : Check for overage on refunds
		$complete_subtotal = 0;
		foreach($purchase->purchaseItems() as $purchase_item)
		{
			$quantity = $form_processor->formValue('quantity_' . $purchase_item->id());
			
			if($quantity == 'custom')
			{
				$amount = $form_processor->formValue('custom_amount_'.$purchase_item->id());
				$complete_subtotal += $amount;
				
				if($amount > $purchase_item->pricePerItem())
				{
					$form_processor->failFormItemWithID(
						'custom_amount_'.$purchase_item->id(),
						'You are unable to refund more than the item amount of $'.$purchase_item->subtotal());
					
					return;
				}
				
			}
			elseif($quantity > 0)
			{
				$complete_subtotal += $quantity * $purchase_item->pricePerItem();
				
			}
			
			
		}
		
		$purchase_subtotal = $purchase->subtotal();
		
		// Requires number format in order to compare properly
		if(number_format($complete_subtotal,2,'.','') > $purchase_subtotal)
		{
			$form_processor->fail('You are unable to refund more than the amount remaining on the purchase');
			return;
		}
		
		
		$attempt_transaction = $form_processor->formValue('attempt_transaction');
		if($form_processor->formValue('type') != 'online')
		{
			$attempt_transaction = 0;
		}
		
		// STEP 3 : Create the Refund
		$values = array(
			'purchase_id' => $purchase->id(),
			'user_id' => TC_currentUser()->id(),
			'attempt_transaction' => $attempt_transaction,
			'notes' => $form_processor->formValue('notes'),
			'type' => $form_processor->formValue('type'),
			'payment_method' => $form_processor->formValue('credit_card_type'),
			'transaction_id' => $form_processor->formValue('transaction_id'),
			
			'delivery_cost' => $refund_delivery_amount,
		);
		
		
		// Avoid running the refund if we're invalid for any reason
		if(!$form_processor->isValid())
		{
			return;
		}
		$refund = TMm_PurchaseRefund::createWithValues($values);
		
		// STEP 4 : Generate Refund Items
		// First create the refund item
		// Then delete the created item if necessary
		foreach($purchase->purchaseItems() as $purchase_item)
		{
			$quantity = $form_processor->formValue('quantity_' . $purchase_item->id());
			if($quantity == 'custom' || $quantity > 0)
			{
				// Custom amount input
				if($quantity == 'custom')
				{
					$amount = $form_processor->formValue('custom_amount_' . $purchase_item->id());
					$purchase_item->refundPartial($refund, $amount);
					
				}
				
				// Selected a quantity
				elseif($quantity > 0)
				{
					$purchase_item->refundQuantity($refund, $quantity);
					
				}
				
				
				
			}
			
		}
		
		
		
		
		
		// STEP 5 : Perform Payment Processor refund transaction
		$response = $refund->performRefundTransaction();
		
		// SUCCESS - Delete any items that were flagged
		if($response['success'])
		{
			// DELETE THE ITEMS
			foreach($purchase->purchaseItems() as $purchase_item)
			{
				$quantity = $form_processor->formValue('quantity_' . $purchase_item->id());
				if($quantity == 'custom' || $quantity > 0)
				{
					// Deal with selected deletion items
					foreach($purchase_item->createdItems() as $created_item)
					{
						if($form_processor->formValue($created_item->contentCode()))
						{
							if($created_item->userCanDelete())
							{
								$created_item->delete();
							}
							else
							{
								TC_message('Permission to delete denied : '.$created_item->title());
							}
						}
					}
					
				}
				
			}
			
		}
		else // ERROR
		{
			// Silently delete the refund we just created. It shouldn't exist
			$refund->delete(false);
			$form_processor->fail('Error: '.$response['message']);
			TC_messageConditionalTitle('Refund could not be completed',false );
		
		}

	}
	
	/**
	 * Extend HTML function to detect the checkout status
	 * @return string|void
	 */
	public function html($show_form = true)
	{
		if($show_form && TC_getModuleConfig('store','transactions_disabled'))
		{
			$view = TMv_TransactionsDisabledWarning::init();
			$this->attachView($view);
			$show_form = false;
			
		}
		
		return parent::html($show_form);
	}
}