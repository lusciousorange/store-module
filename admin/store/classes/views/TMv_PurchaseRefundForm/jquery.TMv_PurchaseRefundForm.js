(function ($)
{
// INIT
	$.fn.TMv_PurchaseRefundForm = function(options)
	{
		var settings = $.extend(
			{
			}, options || {});


		function useCustomChanged(field)
		{
			var $field = $(field);
			var purchase_item_id = $field.attr('data-purchase_item_id');

			if($field.val() == 'custom')
			{
				$('.TMv_PurchaseRefundForm .custom_row_'+purchase_item_id).slideDown();
			}
			else
			{
				$('.TMv_PurchaseRefundForm .custom_row_'+purchase_item_id).hide();
			}

			// Handle showing the created items
			if($field.val() != '0')
			{
				$('.TMv_PurchaseRefundForm #created_purchase_group_' + purchase_item_id).slideDown();
			}
			else
			{
				$('.TMv_PurchaseRefundForm #created_purchase_group_' + purchase_item_id).slideUp();

				// uncheck the boxes, just in case
				$('.TMv_PurchaseRefundForm #created_purchase_group_' + purchase_item_id+' input').prop('checked', false);
			}

		}

		function typeChanged(field)
		{
			// Detect if it's online and show/hide the attempt purchase
			var $field =$(field);

			if($field.val() == 'online')
			{
				$('.TMv_PurchaseRefundForm #attempt_transaction_row').slideDown();
			}
			else
			{
				$('.TMv_PurchaseRefundForm #attempt_transaction_row').slideUp();
			}

		}

		return this.each(function()
		{
			$('.TMv_PurchaseRefundForm .quantity_field').change(function(e) { useCustomChanged(this)}).trigger('change');
			$('.TMv_PurchaseRefundForm #type').change(function(e) { typeChanged(this)});

		});


	};


// END THE WRAPPER
})(jQuery);
