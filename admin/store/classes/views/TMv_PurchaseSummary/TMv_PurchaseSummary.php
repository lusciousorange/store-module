<?php
class TMv_PurchaseSummary extends TCv_View
{
	use TMt_PagesContentView;

	/** @var bool|TMm_Purchase $purchase */
	protected $purchase = false;
	protected $item_table = false;
	protected $loading_in_tungsten = false;
	/**
	 * TMm_PurchaseSummary constructor.
	 * @param TMm_Purchase $purchase
	 */
	public function __construct($purchase)
	{
		$this->purchase = $purchase;

		parent::__construct();
		$this->addClassCSSFile('TMv_PurchaseSummary');

		$this->loading_in_tungsten = get_class(TC_website()) == 'TSv_Tungsten';

		if($this->loading_in_tungsten)
		{
			$this->addClass('TMv_PagesTheme');
		}
		
		$this->item_table = new TCv_HTMLTable();
	}





	public function html()
	{
		/** @var TMv_CheckoutConfirm $confirm_view */

		$container = new TCv_View();
		$confirm_view = TMv_PurchaseView::init($this->purchase);
		$confirm_view->disableCheckoutValidation();

		if($this->loading_in_tungsten)
		{
			$container->addClass('content_width_container');
			$confirm_view->hidePurchaseUpdateSection();
		}

		

		$container->attachView($confirm_view);
		
		$manual_payment_view = TMv_PurchaseMissingPaymentForm::init($this->purchase);
		$container->attachView($manual_payment_view);
		
		if(TC_isTungstenView() && $this->purchase->userCanDelete())
		{
			
			$delete_button = new TCv_Link('purchase_delete_button');
			$delete_button->setURL('/admin/store/do/delete/'.$this->purchase->id());
			$delete_button->useDialog('Are you sure you want to delete this purchase?');
			$delete_button->addText('Delete this Purchase');
			$delete_button->setIconClassName('fa-trash');
			$container->attachView($delete_button);
			
			if($this->purchase->isNotCancelled())
			{
				$delete_button = new TCv_Link('purchase_cancel_button');
				$delete_button->setURL('/admin/store/do/cancel/'.$this->purchase->id());
				$delete_button->useDialog('Are you sure you want to cancel this purchase?');
				$delete_button->addText('Cancel this Purchase');
				$delete_button->setIconClassName('fa-ban');
				$container->attachView($delete_button);
				
			}
			
		}
		
		$this->attachView($container);
		return parent::html();


	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems(): array
	{
		$form_items = array();


		return $form_items;
	}


	public static function pageContent_ViewTitle(): string
	{ return 'Purchase Summary'; }
	public static function pageContent_IconCode(): string
	{ return 'fa-cart-arrow-down'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The purchase summary.';
	}
	public static function pageContent_View_InputModelName() : null|array|string
	{
		return 'TMm_Purchase';
	}


}

?>