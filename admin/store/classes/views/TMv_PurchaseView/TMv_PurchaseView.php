<?php
class TMv_PurchaseView extends TMv_CheckoutConfirm
{
	use TMt_PagesContentView;
	
	protected bool $show_unpaid_view;
	protected string $payment_unpaid_text = 'Unpaid';
	/**
	 * TMv_PurchaseView constructor.
	 * @param TMm_Purchase $purchase
	 */
	public function __construct($purchase)
	{
		parent::__construct($purchase);
		$this->addClassCSSFile('TMv_CheckoutConfirm');
		
		$this->show_unpaid_view =
			TC_isTungstenView() && $this->purchase()->isUnpaid() && $this->purchase()->isNotCancelled();
		
	}
	
	/**
	 * @return TCv_View
	 */
	protected function purchaseTitleView()
	{
		$purchase_heading = new TCv_View('purchase_title');
		$purchase_heading->setTag('h2');
		$purchase_heading->addText($this->purchase()->title());
		
		return $purchase_heading;
	}
	
	public function render()
	{
		if($this->show_unpaid_view)
		{
			$this->attachView($this->unpaidWarningView());
		}
		
		$this->attachView($this->printHeader());
		
		if($this->is_email)
		{
			
			$this->attachView($this->purchaseTitleView());
			
			
		}
		
		parent::render();
		
		$this->addClassCSSFile('TMv_PurchaseView');
		
		$this->attachUnpaidPurchaseForm();
		
		$this->attachRefundView();
		
	}
	
	/**
	 * Sets this view as being loaded in an email, which may be used to adjust the layout
	 */
	public function setAsEmailView()
	{
		parent::setAsEmailView();
		$this->show_unpaid_view = false;
		
	}
	
	
	/**
	 * The print header which only shows when in CSS Print mode
	 * @return TCv_View
	 */
	public function printHeader()
	{
		$header = new TCv_View('purchase_print_header');
		
		$h2 = new TCv_View();
		$h2->setTag('h3');
		$h2->addText(TC_getModuleConfig('pages', 'website_title'));
		
		$header->attachView($h2);
		
		$template_value = TMm_PagesTemplateSetting::init( 'address');
		if($template_value instanceof TMm_PagesTemplateSetting)
		{
			$address = new TCv_View();
			$address->setTag('p');
			$address->addClass('header_address');
			$address->addText($template_value->value());
			
			$template_value = TMm_PagesTemplateSetting::init( 'phone_number');
			if($template_value instanceof TMm_PagesTemplateSetting)
			{
				$address->addText('<br />'.$template_value->value());
				
			}
			$header->attachView($address);
			
		}
		return $header;
		
	}
	
	
	
	/**
	 * returns the view that shows the unpaid settings for the purchase
	 * @return TCv_View
	 */
	protected function unpaidWarningView()
	{
		$unpaid_view = new TCv_View('unpaid_management');
		$view = new TCv_View('unpaid_warning_view');
		
		$message = new TCv_View();
		$message->addClass('message');
		$message->addText('Purchase is unpaid');
		$view->attachView($message);
		
		$button = new TCv_Link('edit_unpaid_purchase_button');
		$button->addText('Edit Settings');
		$button->setURL('/admin/store/do/edit/'.$this->purchase()->id());
		$view->attachView($button);
		$unpaid_view->attachView($view);
		
		$unpaid_view->attachView(TMv_CartableSearchForm::init($this->purchase()));
		
		return $unpaid_view;
	}
	
	/**
	 * Returns the title for the cartable item. If it's not meant to be shown, it will be
	 * @param TCm_Model|TMi_ShoppingCartable $cartable_item
	 * @return string|bool
	 */
	protected function titleForCartableItem($cartable_item)
	{
		$title = $cartable_item->title().' – $'.$cartable_item->price();
		if($cartable_item->barcode() != '')
		{
			$title .= ' – '.$cartable_item->barcode();
		}
		return $title;
	}
	
	/**
	 * Attaches a payment form for public views of a purchase when it is unpaid
	 */
	protected function attachUnpaidPurchaseForm()
	{
		if(!TC_isTungstenView() && $this->purchase()->isUnpaid() && $this->purchase()->userIsBuyer())
		{
			// Deal with payment of unpaid items, if it's possible
			$payment_processor = $this->purchase()->paymentProcessor();
			if($payment_processor && $payment_processor->isSelectablePaymentOption())
			{
				if(TC_getModuleConfig('store','transactions_disabled'))
				{
					$view = TMv_TransactionsDisabledWarning::init();
					$this->attachView($view);
					
					
				}
				else
				{
					$form = new TCv_Form('process_unpaid_purchase_form');
					$form->setButtonText('Proceed To Purchase Payment');
					$menu_item = TMm_PagesMenuItem::init(TC_getModuleConfig('store', 'cart_menu_id'));
					$form->setAction($menu_item->pathToFolder() . '/' . $this->purchase()->id());
					
					$this->attachView($form);
					
				}
				
			}
		}
	}
	
	/**
	 * Attaches a payment form for public views of a purchase when it is unpaid
	 */
	protected function attachRefundView()
	{
		// Only permitted in Tungsten AND when the purchase is paid
		if(TC_isTungstenView() && $this->purchase()->isPaid() && $this->purchase()->total() > 0)
		{
			$button = new TCv_Link('refund_button');
			$button->addText("Refund This Purchase");
			$button->setURL('/admin/store/do/refund/'.$this->purchase()->id());
			$this->attachView($button);
			
		}
	}
	
	/**
	 * The view that contains the shopping cart
	 * @return bool|TCv_View
	 */
	public function shoppingCartView()
	{
		$view = new TCv_View();
		$view->addClass('shopping_cart_view');
		
		$view->clear();
		
		if($this->purchase()->isTestTransaction())
		{
			$test_view = new TCv_View('test_transaction_warning');
			$test_view->addClass('purchase_warning_bar');
			$test_view->addText('Purchase used a test account. No financial transaction occurred.');
			$view->attachView($test_view);
		}
		
		if($this->purchase()->isCancelled())
		{
			$test_view = new TCv_View('cancelled_transaction_warning');
			$test_view->addClass('purchase_warning_bar');
			$test_view->addText('Purchase is cancelled');
			$view->attachView($test_view);
		}
		
		
		$view->attachView($this->purchaseCartView());
		
		return $view;
	}
	
	/**
	 * returns the purchase cart view that's used
	 * @return bool|TMv_PurchaseCart
	 */
	protected function purchaseCartView()
	{
		$cart_view = TMv_PurchaseCart::init($this->purchase());
		if($this->is_email)
		{
			$cart_view->disableEditing();
		}
		return $cart_view;
	}
	
	
	/**
	 * The table of payment values
	 * @return TCv_HTMLTable
	 */
	protected function paymentTable()
	{
		$table = new TCv_HTMLTable();
		
		// Date Created
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Created');
		$row->createCellWithContent($this->purchase()->dateAddedFormatted());
		$table->attachView($row);
		
		// Different Payment Date
		if($this->purchase()->isPaid() && $this->purchase()->paymentDateIsDifferent())
		{
			$row = new TCv_HTMLTableRow();
			$row->createCellWithContent('Paid');
			$row->createCellWithContent($this->purchase()->paymentDate('F j, Y \a\t g:ia'));
			$table->attachView($row);
		}
		
		// ID
		$row = new TCv_HTMLTableRow('payment_purchase_id_row');
		$row->createCellWithContent('Purchase ID');
		$row->createCellWithContent($this->purchase()->id());
		$table->attachView($row);
		
		if(TC_isTungstenView() && $this->purchase()->shoppingCartID() > 0)
		{
			$row = new TCv_HTMLTableRow('payment_cart_id_row');
			$row->createCellWithContent('Cart ID');
			$row->createCellWithContent( $this->purchase()->shoppingCartID());
			$table->attachView($row);
		}
		
		
		
		// Payment Type
		if($this->purchase()->isPaid())
		{
			if($this->purchase()->hasSecondPayment())
			{
				$row = new TCv_HTMLTableRow();
				$row->addClass('divider');
				$row->createCellWithContent('1st Payment');
				$row->createCellWithContent('$'.$this->purchase()->splitPayment1Total());
				$table->attachView($row);
				
			}
			
			$table->attachView($this->paymentTableRowForType());
			$table->attachView($this->paymentTableRowForTransactionID());
			
			if($this->purchase()->hasSecondPayment())
			{
				$row = new TCv_HTMLTableRow();
				$row->addClass('divider');
				$row->createCellWithContent('2nd Payment');
				$row->createCellWithContent('$'.$this->purchase()->splitPayment2Total());
				$table->attachView($row);
				
				$table->attachView($this->paymentTableRowForType(true));
				$table->attachView($this->paymentTableRowForTransactionID(true));
				
			}
		}
		
		return $table;
	}
	
	/**
	 * Shows payment view details
	 * @return bool|TCv_View
	 */
	public function paymentView()
	{
		if(!$this->isPurchaseView())
		{
			return parent::paymentView();
		}
		
	
		$view = new TCv_View();
		$view->addClass('payment_view');
		
		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		$heading->addText('Payment');
		$view->attachView($heading);
		
		$view->attachView($this->paymentTable());
		
		if($this->purchase()->isUnpaid() && $this->purchase()->isNotCancelled())
		{
			$view->attachView($this->paymentPendingView());
		}
		
		return $view;

		
	
		
	}
	
	protected function paymentTableRowForType($is_second_payment = false)
	{
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Payment Type');
		
		if($is_second_payment)
		{
			$row->createCellWithContent($this->purchase()->transactionTypeSecondTitle());
		}
		else
		{
			$row->createCellWithContent($this->purchase()->transactionTypeTitle());
		}
		return $row;
	}
	
	protected function paymentTableRowForTransactionID($is_second_payment = false)
	{
		if($is_second_payment)
		{
			$transaction_id = $this->purchase()->transactionIDSecondPayment();
			$type = $this->purchase()->typeSecondPayment();
		}
		else
		{
			$transaction_id = $this->purchase()->transactionID();
			$type = $this->purchase()->type();
		}
		
		if($transaction_id != '')
		{
			$row = new TCv_HTMLTableRow();
			
			if($type == 'online')
			{
				$row->createCellWithContent('Transaction ID');
			}
			elseif($type == 'check')
			{
				$row->createCellWithContent('Number');
			}
			else
			{
				$row->createCellWithContent('Notes');
			}
			$row->createCellWithContent($transaction_id);
			return $row;
			
		}
		
		return null;
		
	}
	
	/**
	 * @param string  $type
	 * @param string $transaction_id
	 * @param TCv_HTMLTable $table
	 * @deprecated use individual row methods instead
	 */
	protected function attachPaymentValuesToTable($type, $transaction_id, $table)
	{
		$types = $this->purchase()->transactionTypes();
		
		$row = new TCv_HTMLTableRow();
		$row->createCellWithContent('Payment Type');
		$row->createCellWithContent($types[$type]);
		$table->attachView($row);
		
		
		if($transaction_id != '')
		{
			$row = new TCv_HTMLTableRow();
			
			if($type == 'online')
			{
				$row->createCellWithContent('Transaction ID');
			}
			elseif($type == 'check')
			{
				$row->createCellWithContent('Number');
			}
			else
			{
				$row->createCellWithContent('Notes');
			}
			$row->createCellWithContent($transaction_id);
			$table->attachView($row);
			
		}
	}
	
	
	/**
	 * returns the view that shows when is payment is pending
	 * @return TCv_View
	 */
	protected function paymentPendingView()
	{
		$p = new TCv_View();
		$p->addClass("payment_pending");
		$p->addText($this->payment_unpaid_text);
		return $p;
	}
	
	/**
	 * Sets the text that is shown in the payment box if the purchase isn't paid
	 * @param string $text
	 */
	public function setPaymentUnpaidText($text)
	{
		$this->payment_unpaid_text = $text;
	}
	
	
	public function html()
	{
		// Public side, someone trying to view a purchase from someone else, hard exit
		if(!TC_isTungstenView() && !$this->purchase()->userCanViewPublic())
		{
			return '';
		}
		
		return parent::html();
		
	}
	
	
	
	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	public static function pageContent_ViewTitle() : string { return 'Purchase View'; }

	public static function pageContent_View_InputModelName() : null|array|string
	{
		return 'TMm_Purchase';
	}

	public static function pageContent_ShowPreviewInBuilder(): bool { return false; }
	public static function pageContent_ViewDescription() : string
	{
		return 'The review of the purchase.';
	}
}