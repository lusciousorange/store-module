<?php
class TMv_ShoppingCart extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $checkout_menu_id = false;
	protected bool $allow_editing = true;
	
	protected string $heading_tag = 'h3';
	/** @var bool|TMm_ShoppingCart $shopping_cart*/
	protected $shopping_cart = false;
	protected $cart_table = false;
	protected bool $show_photo_column = true;
	protected bool $force_show_taxes = false;
	protected string $cart_view_title = 'Shopping Cart';
	
	protected bool $style_as_mini_cart = false;
	protected int $num_columns = 5;
	protected bool $allow_manual_payments = true;
	
	protected ?string $remove_url_target_name = 'remove-from-cart';
	protected ?string $update_quantity_url_target_name = 'update-cart-quantity';
	
	protected int $max_to_sell = 100;
	
	
	protected int $photo_size = 150;
	
	
	// The list of columns that should be called
	protected array $columns = array(
		'photo' => array('title' => '','method' =>'photoCellForShoppingCartItem'),
		'title' => array('title' => 'Item', 'method' => 'titleCellForShoppingCartItem'),
		'price' => array('title' => 'Price', 'method' => 'priceCellForShoppingCartItem'),
		'quantity' => array('title' => 'Quantity', 'method' => 'quantityCellForShoppingCartItem'),
		'subtotal' => array('title' => 'Subtotal', 'method' => 'subtotalCellForShoppingCartItem'),
	);
	
	/**
	 * TMv_ShoppingCart constructor.
	 * @param TMm_ShoppingCart|TMm_Purchase|bool $cart_or_purchase (Optional) Default false.
	 *
	 */
	public function __construct($cart_or_purchase = false)
	{
		if (!$cart_or_purchase)
		{
			$this->shopping_cart = TMm_ShoppingCart::init();
		}
		else
		{
			$this->shopping_cart = $cart_or_purchase;
		}


		parent::__construct('shopping_cart_'.$this->shopping_cart->id());
		$this->addClassCSSFile('TMv_ShoppingCart');

		$this->cart_table = new TCv_HTMLTable('cart_table');

	}

	/**
	 * Turns off the editing capability for the shopping cart
	 */
	public function disableEditing()
	{
		$this->allow_editing = false;
	}

	public function hidePhotoColumn()
	{
		$this->show_photo_column = false;
	}

	/**
	 * Force the cart to show taxes regardless of shipping/delivery settings
	 */
	public function forceShowTaxes()
	{
		$this->force_show_taxes;
	}


	/**
	 * @param TMm_ShoppingCartItem $shopping_cart_item
	 * @return TCv_HTMLTableRow
	 */
	public function rowForShoppingCartItem($shopping_cart_item)
	{
		$table_row = new TCv_HTMLTableRow('cart_row_'.$shopping_cart_item->id());
		$table_row->addClass('cart_item_row');
		$table_row->addClass('row_'.$shopping_cart_item->itemClass());
		if($shopping_cart_item->hasErrors())
		{
			$table_row->addClass('has_errors');
		}
		if($shopping_cart_item->hasDiscount())
		{
			$table_row->addClass('discounted');
		}
		
		foreach($this->columns as  $code => $properties)
		{
			$method_name = $properties['method'];
			$table_row->attachView($this->$method_name($shopping_cart_item));
		}
		
		return $table_row;
	}
	
	/**
	 * Method to handle attached a shopping cart item to the cart table. This can be extended for when additional
	 * rows must be inserted into the layout.
	 * @param TMm_ShoppingCartItem $shopping_cart_item
	 */
	public function attachItemRowToCartTable($shopping_cart_item)
	{
		$row = $this->rowForShoppingCartItem($shopping_cart_item);
		
		$this->cart_table->attachView($row);
	}

	/**
	 * Creates a cell that fits into the table cell with a proper colspan that ensures no wrapping
	 * @param string $content
	 * @return TCv_HTMLTableCell
	 */
	protected function summaryRowCellWithContent($content)
	{
		$table_cell = new TCv_HTMLTableCell();
		$table_cell->setColumnSpan(count($this->columns) - 1);
		$table_cell->addText($content);
		$table_cell->addClass('quantity_column');
		return $table_cell;
	}
	/**
	 * @return TCv_HTMLTableRow
	 */
	protected function rowForSubtotal()
	{
		$table_row = new TCv_HTMLTableRow('subtotal_row');
		$table_row->addClass('subtotal_row');
		$table_row->attachView($this->summaryRowCellWithContent('Subtotal'));
		$table_row->attachView($this->subtotalCell());
		return $table_row;
	}

	/**
	 * @return TCv_HTMLTableRow[]
	 */
	protected function rowsForDiscounts()
	{
		$rows = array();

		$total_discount = $this->shopping_cart->totalDiscountForItems();
		if($total_discount <= 0)
		{
			$this->addClass('no_discount');
		}
		
		$table_row = new TCv_HTMLTableRow('original_total_row');
		$table_row->addClass('original_total_row');
		$table_row->attachView($this->summaryRowCellWithContent('Original Subtotal'));
		$table_row->createCellWithContent('$'.$this->shopping_cart->originalSubtotalNoDiscounts(),'subtotal_column');
		$rows[] = $table_row;


		$table_row = new TCv_HTMLTableRow('discount_total_row');
		$table_row->addClass('discount_total_row');
		$table_row->attachView($this->summaryRowCellWithContent('Product/Promotion Discounts'));
		$table_row->createCellWithContent('<span class="discount_price">-$'.$total_discount.'</span>','subtotal_column');
		$rows[] = $table_row;


		return $rows;
	}

	/**
	 * Generates and returns the table cell view for the photo
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function photoCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('photo_column');
		
		if($shopping_cart_item->item())
		{
			
			
			$photo_file = $shopping_cart_item->item()->cartPhotoFile();
			if($photo_file)
			{
				if($photo_file instanceof TCv_View)
				{
					$cell->attachView($photo_file);
				}
				elseif($photo_file instanceof TCm_File)
				{
					$photo = new TCv_Image(false, $photo_file);
					$photo->scaleInsideBox($this->photo_size, $this->photo_size);
					$cell->attachView($photo);
				}
				
			}
		}
		return $cell;
	}

	/**
	 * Generates and returns the table cell view for the title
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function titleCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('item_column');


		$link = new TCv_Link();
		$link->addClass('item_link');
		$link->addText($shopping_cart_item->cartTitle());

		if($shopping_cart_item->item() && $shopping_cart_item->item()->viewURL())
		{
			$link->setURL($shopping_cart_item->item()->viewURL());
		}
		else
		{
			$link->setTag('span');
		}

		$cell->attachView($link);
		$item_description = $this->itemDescriptionForShoppingCartItem($shopping_cart_item);
		$item_description->addClass('item_description');
		$cell->attachView($item_description);

		return $cell;
	}

	/**
	 * Returns the item description for the shopping cart item
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_View
	 */
	public function itemDescriptionForShoppingCartItem($shopping_cart_item)
	{
		$view = new TCv_View();
		$view->setTag('p');

		$view->addText($shopping_cart_item->cartDescription($shopping_cart_item));

		return $view;
	}



	/**
	 * Generates and returns the table cell view for the title
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function priceCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('price_column');

		$regular_price = new TCv_View();
		$regular_price->addClass('regular_price');
		$regular_price->addText('$'.$shopping_cart_item->pricePerItem());
		$cell->attachView($regular_price);

		if($shopping_cart_item->hasDiscount())
		{

			$regular_price = new TCv_View();
			$regular_price->addClass('discount_price');
			$regular_price->addText('<span>Save</span>');
			$regular_price->addText(' $' . $shopping_cart_item->discountPerItem());
			$cell->attachView($regular_price);
		}
		
		
		return $cell;
	}


	/**
	 * Generates and returns the table cell view for the quantity
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function quantityCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('quantity_column');

		if($this->allow_editing)
		{
			$cell->attachView($this->quantityFormForShoppingCartItem($shopping_cart_item));

			$p = new TCv_View();
			$p->addClass('remove_container');
			$remove_button = new TCv_Link();
			$remove_button->addClass('remove_link');
			$remove_button->addText('Remove');
			$remove_button->setIconClassName('fa-trash');
			$remove_button->setURL('/admin/store/do/'.$this->remove_url_target_name.'/' .$shopping_cart_item->id());
			$p->attachView($remove_button);
			$cell->attachView($p);
		}
		else
		{
			$cell->addText($shopping_cart_item->quantity());
		}
		return $cell;
	}

	/**
	 * The quantity form for the cart
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_Form|bool
	 */
	public function quantityFormForShoppingCartItem($shopping_cart_item)
	{
		$form = new TCv_Form('cart_quantity_form_'.$shopping_cart_item->id(), TCv_Form::$DISABLE_FORM_TRACKING);
		$form->setAction('/admin/store/do/'.$this->update_quantity_url_target_name.'/'.$shopping_cart_item->id());
		$form->setMethod('get');

		$in_stock = $shopping_cart_item->item()->inStock();
		if($in_stock !== false && $in_stock > 0)
		{
			$max_to_sell = $in_stock;
		}
		elseif($shopping_cart_item->item()->maxAddable() !== false)
		{
			$max_to_sell = $shopping_cart_item->item()->maxAddable();
		}
//		elseif($in_stock == 0)
//		{
//			return false;// shouldn't happen since they are removed from the cart
//		}
		else
		{
			$max_to_sell = $this->max_to_sell;
		}

		$quantity_select = new TCv_FormItem_Select('quantity', '');
		$quantity_select->setDefaultValue($shopping_cart_item->quantity());
		$quantity_select->setOptionsWithNumberRange(1, $max_to_sell);
		if($shopping_cart_item->quantity() > $max_to_sell)
		{
			$quantity_select->addOption($shopping_cart_item->quantity(),$shopping_cart_item->quantity());
		}
		
		$quantity_select->addClass('cart_select_quantity');
		$quantity_select->setShowTitleColumn(false);
		$form->attachView($quantity_select);

		$form->hideSubmitButton();
		return $form;

	}

	/**
	 * Generates and returns the table cell view for the quantity
	 * @param TMm_ShoppingCartItem|TMm_PurchaseItem $shopping_cart_item
	 * @return TCv_HTMLTableCell
	 */
	public function subtotalCellForShoppingCartItem($shopping_cart_item)
	{
		$cell = new TCv_HTMLTableCell();
		$cell->addClass('subtotal_column');

		$regular_price = new TCv_View();
		$regular_price->addClass('regular_price');
		$regular_price->addText('$'.$shopping_cart_item->subtotal());
		$cell->attachView($regular_price);
		
		$tax_info = new TCv_View();
		$tax_info->addClass('tax_info');
		
		$cartable_item = $shopping_cart_item->item();
		
		if($cartable_item)
		{
			
			
			$taxes_used = array();
			$tax_list = TMm_TaxList::init();
			foreach($tax_list->taxes() as $tax)
			{
				if($cartable_item->usesTax($tax))
				{
					$taxes_used[] = $tax->title();
				}
				
			}
			
			$tax_info->addText(implode(', ', $taxes_used));
		}
		$cell->attachView($tax_info);
		
		
		if($shopping_cart_item->hasDiscount())
		{

			$regular_price = new TCv_View();
			$regular_price->addClass('discount_price');
			$regular_price->addText('<span>Save</span>');
			$regular_price->addText(' $' . $shopping_cart_item->discountForSubtotal());
			$cell->attachView($regular_price);
		}



		return $cell;
	}


	public function attachTaxRows()
	{
		if($this->shopping_cart->taxesAreReady())
		{
			$tax_list = TMm_TaxList::init();

			foreach ($tax_list->taxes() as $tax)
			{
				$table_row = new TCv_HTMLTableRow('tax_'.$tax->id().'_row');
				$table_row->addClass('tax_row');
				$table_row->attachView($this->summaryRowCellWithContent($this->summaryTitleForTax($tax)));
				$table_row->attachView($this->taxTotalCell($tax));
				
				$this->cart_table->attachFooterRow($table_row);

			}
		}
	}
	
	/**
	 * Returns the title for the tax in the
	 * @param TMm_Tax $tax
	 * @return string
	 */
	public function summaryTitleForTax($tax)
	{
		return $tax->titleWithNumber(' - ', true).' ('.floatval($this->shopping_cart->percentageForTax($tax) ).'%)';
	}
	
	/**
	 * Returns the full string for the summary total for the tax
	 * @param TMm_Tax $tax
	 * @return string
	 */
	public function summaryTotalForTax($tax)
	{
		return '$'.$this->shopping_cart->totalForTax($tax);
	}
	
	/**
	 * Indicates if the checkout and delete buttons should be shown. They are paired together in this interface.
	 * @return bool
	 */
	public function showCheckoutAndDelete()
	{
		return $this->checkout_menu_id !== false && $this->checkout_menu_id != '';
		
	}
	
	/**
	 * The checkout Button that lets visitors proceed to checkout
	 * @return TCv_Link|bool
	 */
	public function checkoutButton()
	{
		$button = new TCv_Link();
		$button->addClass('continue_checkout_button');
		$button->addText('Checkout');
		
		$menu = TMm_PagesMenuItem::init($this->checkout_menu_id);
		if($menu)
		{
			$button->setURL($menu->pathToFolder());
			return $button;
		}
		
		return new TCv_View();
		
	}
	
	public function cartErrorBox($error_values)
	{
			$error_box = new TCv_View();
			
			if(is_array($error_values))
			{
				$error_box->addClass('cart_'.$error_values['error_type'].'_box');
				
				if(isset($error_values['button_url']))
				{
					$error_box->addText($error_values['text']);
					$link = new TCv_Link();
					$link->addClass('cart_error_button');
					$link->setURL($error_values['button_url']);
					$link->addText($error_values['button_text']);
					$error_box->attachView($link);
				}
				else
				{
					$error_box->addText($error_values['text']);
				}
			}
			else
			{
				$error_box->addClass('cart_error_box');
				$error_box->addText($error_values);
				
			}
		
		return $error_box;
	}
	
	/**
	 * Returns a collection of views that are shown after the grid
	 * @return TCv_View[]
	 */
	public function viewsAfterGrid()
	{
		$views = array();
		
		if($this->allow_editing && !$this->shopping_cart->isEmpty())
		{
			
			if($this->shopping_cart->hasErrorsOrWarnings())
			{
				$bottom_row = new TCv_View();
				$bottom_row->addClass('error_row');
				foreach($this->shopping_cart->errorsAndWarnings() as $error_message)
				{
					$error_box = $this->cartErrorBox($error_message);
					$bottom_row->attachView($error_box);
				}
				
				$views[] = $bottom_row;
			}
			
			// we're showing checkout delete
			// This appears regardless and
			if($this->showCheckoutAndDelete())
			{
				
				
				$bottom_row = new TCv_View();
				$bottom_row->addClass('action_row');
				
				$container = new TCv_View('delete_cart_button_container');
				$delete_cart_button = new TCv_Link('delete_cart_button');
				$delete_cart_button->setURL('/admin/store/do/delete-cart/');
				$delete_cart_button->addText('Delete This Cart');
				$container->attachView($delete_cart_button);
				$bottom_row->attachView($container);
				
				if(!$this->style_as_mini_cart)
				{
					$bottom_row->attachView($this->checkoutButton());
				}
				$views[] = $bottom_row;
			}
			
			
			
			if($this->allow_manual_payments)
			{
				$manual_payment_view = TMv_ShoppingCartManualPaymentForm::init($this->shopping_cart);
				$views[] = $manual_payment_view;
			}
			
		}
		
		return $views;
	}
	
	/**
	 * Disables manual payments for this cart
	 */
	public function disableManualPayments()
	{
		$this->allow_manual_payments = false;
	}
	
	
	/**
	 * Returns the total cell for the cart
	 * @return TCv_HTMLTableCell
	 */
	public function totalCell()
	{
		$table_cell = new TCv_HTMLTableCell();
		$table_cell->setColumnSpan(count($this->columns));
		
		if($this->style_as_mini_cart)
		{
			$link = new TCv_Link();
			$link->addClass('mini_view_full_cart_button');
			$link->addText("View Full Cart");
			
			//$this->cart = TMm_ShoppingCart::init();
			
			$cart_menu_id = TC_getModuleConfig('store','cart_menu_id');
			$menu = TMm_PagesMenuItem::init($cart_menu_id);
			$link->setURL($menu->pathToFolder());
			
			//$this->addText('<span class="title">Cart</span>');
			$table_cell->attachView($link);
		}
		
		
		$table_cell->addText('$'.$this->shopping_cart->total());
		$table_cell->addClass('subtotal_column');
		
		return $table_cell;
	}
	
	/**
	 * Returns the subtotal cell for the cart
	 * @return TCv_HTMLTableCell
	 */
	public function subtotalCell()
	{
		$table_cell = new TCv_HTMLTableCell();
		$table_cell->addText('$'.$this->shopping_cart->subtotal());
		$table_cell->addClass('subtotal_column');
		
		return $table_cell;
	}
	
	/**
	 * Returns the subtotal cell for the cart
	 * @param TMm_Tax $tax
	 * @return TCv_HTMLTableCell
	 */
	public function taxTotalCell($tax)
	{
		$table_cell = new TCv_HTMLTableCell();
		$table_cell->setColumnSpan(count($this->columns));
		
		
		$table_cell->addText($this->summaryTotalForTax($tax));
		$table_cell->addClass('subtotal_column');
		
		return $table_cell;
	}
	
	
	
	
	
	/**
	 * Returns the delivery total cell
	 * @return TCv_HTMLTableCell
	 */
	public function deliveryTotalCell()
	{
		$table_cell = new TCv_HTMLTableCell();
		$table_cell->addText('$'.$this->shopping_cart->deliveryPrice());
		$table_cell->addClass('subtotal_column');
		
		return $table_cell;
		
	}
	
	public function heading()
	{
		$heading = new TCv_View();
		$heading->setTag($this->heading_tag);
		if($this->cart_view_title == '')
		{
			return false;// no heading to be shown
		}
		else
		{
			$heading->addText($this->cart_view_title);
		}
	
		
		return $heading;
	}
	
	/**
	 * Sets the style to be the mini cart
	 */
	public function setAsMiniCart()
	{
		$this->style_as_mini_cart = true;
	}
	
	/**
	 * A hook method to allow the adding of additional rows
	 */
	public function hook_processAdditionalRows()
	{
		// DO nothing.
	}
	
	
	public function render()
	{
		// Undo any accidental setting of the cart id. WE're looking at it again, so never want to risk locking out the
		// transaction.
		unset($_SESSION['payment_processing_cart_id']);
		$this->attachView($this->heading());
		
		if(!$this->show_photo_column)
		{
			unset($this->columns['photo']);
		}
		
		if($this->style_as_mini_cart)
		{
			unset($this->columns['price']);
		}
		
		
		
		$table_row = new TCv_HTMLTableRow();

		if($this->style_as_mini_cart)
		{
			$this->addClass('mini_cart');
		}
		
		foreach($this->columns as  $code => $properties)
		{
			$table_row->createHeadingCellWithContent($properties['title'], $code.'_column');
		}
		$this->cart_table->attachHeaderRow($table_row);
		
		foreach($this->shopping_cart->cartItems() as $shopping_cart_item)
		{
			$this->attachItemRowToCartTable($shopping_cart_item);
		}

		$this->hook_processAdditionalRows();
		
		foreach($this->rowsForDiscounts() as $discount_row)
		{
			$this->cart_table->attachFooterRow($discount_row);
		}

		$this->cart_table->attachFooterRow($this->rowForSubtotal());



		// If we have delivery and it's being used
		$delivery_ready = $this->shopping_cart->hasDeliverySet() && $this->shopping_cart->deliveryIsUsed();
		if($delivery_ready)
		{
			$table_row = new TCv_HTMLTableRow('delivery_row');
			$table_row->addClass('delivery_row');
			$table_row->attachView($this->summaryRowCellWithContent($this->shopping_cart->deliveryTitle()));
			$table_row->attachView($this->deliveryTotalCell());
			$this->cart_table->attachFooterRow($table_row);

		}

		if($delivery_ready || !$this->shopping_cart->deliveryIsUsed())
		{
			// Attach the Tax Rows if applicable
			$this->attachTaxRows();




			$table_row = new TCv_HTMLTableRow('total_row');
			$table_row->addClass('total_row');

			

			$table_row->attachView($this->totalCell());

			$this->cart_table->attachFooterRow($table_row);


		}

		$this->attachView($this->cart_table);
		
		
		foreach($this->viewsAfterGrid() as $view)
		{
			$this->attachView($view);
			
		}
		
		// Track the view in GA, ignore for mini carts
		if(!$this->style_as_mini_cart && $this->shopping_cart instanceof TMm_ShoppingCart)
		{
			$values = [
				'currency' => 'CAD',
				'value' => $this->shopping_cart->subtotal(),
				'items' => [],
			];
			foreach($this->shopping_cart->cartItems() as $cart_item)
			{
				$values['items'][] = TSv_GoogleAnalytics::itemValuesForModel($cart_item);
			}
			
			
			$this->addText(TSv_GoogleAnalytics::trackEvent('view_cart',$values, true));
		}
		
		
		
		
	}
	
	

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems(): array
	{
		$form_items = array();

//		$checkout_url = new TCv_FormItem_TextField('checkout_url','Continue to Checkout URL');
//		$checkout_url->setDefaultValue('/checkout/account/');
//		$checkout_url->setHelpText("Indicate the URL where a user will be sent to when they click to proceed to checkout. ");
//		$form_items[] = $checkout_url;
//
		
		$redirect = new TCv_FormItem_Select('checkout_menu_id','Checkout Page');
		$redirect->setHelpText('Select the menu to start the checkout process');
		$redirect->addOption('','No Checkout Button');
		
		/** @var TMm_PagesMenuItem $menu_0 */
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$redirect->addOption($menu_item->id(), implode(' – ', $titles));
		}
		
		$form_items['redirect'] = $redirect;
		
		
		$field = new TCv_FormItem_Select('style_as_mini_cart','Style As Mini Cart');
		$field->setDefaultValue(0);
		$field->addOption(1, 'Yes');
		$field->addOption(0, 'No');
		$form_items[] = $field;
		
		$field = new TCv_FormItem_TextField('cart_view_title','Heading Text');
		$field->setHelpText('The heading for this cart. Leave blank to not show.');
		$form_items[] = $field;
		
		
		return $form_items;
	}


	public static function pageContent_ViewTitle(): string
	{ return 'Shopping Cart'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The shopping cart.';
	}
	
	
	
	/**
	 * An array of values used to instantiate the return-as-json values for this view. You might want to pass additional
	 * values back along with the js/css/html that is auto generated in TSc_ModuleController. If you want your view to
	 * return additional values that are likely used by JS or PHP, then override this method and add values to the array.
	 * @return array
	 */
	public function returnAsJSONDefaultValues() : array
	{
		return ['num_cart_items' => $this->shopping_cart->numCartItems()];
	}
	
	
}

