<?php
class TMv_ShoppingCartAddButton extends TCv_Link
{
	/**
	 * TMv_ShoppingCartAddButton constructor.
	 * @param TMi_ShoppingCartable|TCm_Model $cartable_model A model, often a TMi_ShoppingCartable, but always
	 */
	public function __construct($cartable_model)
	{
		parent::__construct(false);
		$this->setURL('/admin/store/do/add-to-cart/?quantity=1&content_code='.$cartable_model->contentCode());
		$this->addText('Add to Cart');
		$this->enableAsync();
		
	}
	
	
	/**
	 * Allows this form to be tracked for asynchronous loading
	 */
	public function enableAsync()
	{
		$this->addClass('async');
	}
	
	/**
	 * Removes the ability for this form to be tracked asynchronously
	 */
	public function disableAsync()
	{
		$this->removeClass('async');
	}
	
	
}