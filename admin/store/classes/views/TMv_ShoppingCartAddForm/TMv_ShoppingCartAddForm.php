<?php

/**
 * Class TMv_ShoppingCartAddForm
 *
 * A form that is used to create an "add to cart" button on a page.
 */
class TMv_ShoppingCartAddForm extends TCv_Form
{
	use TMt_PagesContentView;

	protected $cart_model = false;
	protected $model_class, $model_id;
	protected bool $show_quantity = true;
	protected string $button_text = 'Add to Cart';

	/**
	 * TMv_ShoppingCartAddForm constructor.
	 * @param TMi_ShoppingCartable|TCm_Model $model A model, often a TMi_ShoppingCartable, but always
	 */
	public function __construct($model)
	{
		parent::__construct(false, TCv_Form::$DISABLE_FORM_TRACKING);
		$this->setMethod('get');
		$this->setAction('/admin/store/do/add-to-cart/');
		$this->disableAllTitleColumns();
		$this->cart_model = $model;
		
		$this->setSubmitButtonID('submit_'.$model->id());
		
		// We don't need or want the JS class running. Possibly in a large list
		$this->disableFormClassJS();
		//$this->enableAsync();
	}
	
	/**
	 * @return bool|TCm_Model|TMi_ShoppingCartable
	 */
	public function cartModel()
	{
		return $this->cart_model;
	}
	
	/**
	 * Allows this form to be tracked for asynchronous loading
	 */
	public function enableAsync()
	{
		$this->addClass('async');
	}
	
	/**
	 * Removes the ability for this form to be tracked asynchronously
	 */
	public function disableAsync()
	{
		$this->removeClass('async');
	}
	
	
	
	/**
	 * Hides the quantity value, forcing a value of 1
	 */
	public function hideQuantity()
	{
		$this->show_quantity = false;
	}

	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		// Check for a cart model that was provided, otherwise we're coming in directly from page content view
		if($this->cart_model instanceof TCm_Model)
		{
			$id = $this->cart_model->contentCode();
		}
		else
		{
			$id = $this->model_class;
			if($this->model_id != '')
			{
				$id .= '--'.$this->model_id;
			}
			
			$this->cart_model = ($this->model_class)::init(0);
		}
		
		
		
		
		// Handle Not Available For Purchase
		// Check for actually out of stock, ignore false which is a "we don't care" value
		if($this->cart_model->inStock() <= 0 && $this->cart_model->inStock() !== false)
		{
			$out_of_stock = new TCv_View();
			$out_of_stock->addClass('out_of_stock_message');
			$out_of_stock->addText($this->cart_model->outOfStockMessage());
			$this->attachView($out_of_stock);
			$this->hideSubmitButton();
			return;
		}
		
		$this->setButtonText($this->button_text);

		// Detect if it's set already, avoid overriding
		if(!$this->formItemWithID('content_code'))
		{
			// By default, we assume one product, so we just need the code hidden to be passed
			$content_code = new TCv_FormItem_Hidden('content_code', 'Content Code');
			$content_code->setValue($id);
			$this->attachView($content_code);
		}
		
		if($this->show_quantity)
		{
			// Quantity is almost always a select, possibly extended to be a hidden if disabled.
			$quantity = new TCv_FormItem_Select('quantity', 'Quantity');
			$quantity->addClass('quantity_row');
			
			$max = $this->cart_model->maxAddable();
			if($max === false)
			{
				$max = 30;
			}
			$quantity->setOptionsWithNumberRange(1,$max);
			
			$this->attachView($quantity);

		}
		else
		{
			$content_code = new TCv_FormItem_Hidden('quantity', 'Quantity');
			$content_code->setValue(1);
			$this->attachView($content_code);

		}


	}


	/**
	 * This function is used to set the form items that are shown for this particular content item. The TCv_Form item
	 * that is being shown is passed as a parameter and those values are loaded as properties in your model.
	 *
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems(): array
	{
		$form_items = array();
		
		$cart_classes = TCv_Website::classesWithInterface('TMi_ShoppingCartable');
		$this->addConsoleDebug($cart_classes);
		$field = new TCv_FormItem_Select('model_class', 'Model Class Name');
		$field->setHelpText('Indicate the model class name that will be used for this cart. ');
		$field->setIsRequired();
		$field->addOption('','Select a cart type');
		foreach($cart_classes as $class_name)
		{
			/** @var TCm_Model|TMi_ShoppingCartable $class_name */
			$field->addOption($class_name, $class_name::modelTitleSingular());
		}
		$form_items[] = $field;

		$field = new TCv_FormItem_TextField('model_id', 'Model ID');
		$field->setHelpText('Indicate the model id for the class. ');
		$form_items[] = $field;

		$field = new TCv_FormItem_Select('show_quantity', 'Show Quantity');
		$field->setHelpText('Indicate if the quantity field should be shown. Otherwise 1 is always added ');
		$field->addOption(1,'Yes');
		$field->addOption(0,'No');
		$form_items[] = $field;

		$field = new TCv_FormItem_TextField('button_text', 'Button Text');
		$field->setIsRequired();
		$field->setDefaultValue('Add to Cart');
		$form_items[] = $field;


		return $form_items;
	}

	/**
	 * Returns the title of the page content view. This is what people see when considering adding that item to a page.
	 * @return string
	 */
	public static function pageContent_ViewTitle(): string
	{
		return 'Add Item Cart Form';
	}

	/**
	 * Returns the description of the content view
	 * @return string
	 */
	public static function pageContent_ViewDescription(): string
	{
		return 'A content item that is used to show a form which will add a single item to the cart with a provided quantity';
	}

	/**
	 * Returns the name if the icon code from Font Awesome for this view
	 * @return string
	 */
	public static function pageContent_IconCode(): string
	{
		return 'fa-cart-plus';
	}


}