<?php

/**
 * Class TMv_ShoppingCartAddressForm
 *
 * A form that asks for the address information for the cart.
 */
class TMv_ShoppingCartAddressForm extends TCv_FormWithModel
{
	use TMt_PaymentForm;
	
	static $shipping_address_changed = false;

	/**
	 * TMv_ShoppingCartAddressForm constructor.
	 * @param string|TMm_ShoppingCart $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		$this->setButtonText('Update Shipping Address');
		$this->setIDAttribute('shopping_cart_address_form');
		
		$this->setSubmitButtonID('address_submit');
		
	}
	
	
	


	
	/**
	 * Configures the form items for the user form.
	 * @param bool $disable_validation
	 */
	public function configureFormElements($disable_validation = false)
	{
		$this->attachView($this->shippingAddressSection($disable_validation));

		
		
	}
	
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$country = TMm_ShoppingCart::countryCodeForProvince($form_processor->formValue('shipping_province'));
		$is_valid = TMv_PaymentForm::validatePostalCode($form_processor->formValue('shipping_postal'), $country);
		
		// Canadian postal code validation
		if(!$is_valid)
		{
			$form_processor->failFormItemWithID('shipping_postal', 'The postal code appears poorly formatted');
		}
		$form_processor->addDatabaseValue('shipping_country', $country);
		
	}


	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();

		// check if a relevant change happened to the shipping address
		// Only bothers if there's an existing address
		self::$shipping_address_changed = false;
		if(($shopping_cart instanceof TMm_ShoppingCart) && $shopping_cart->hasShippingAddress() &&
			$shopping_cart->shippingValuesAreDifferent($form_processor->formValue('shipping_city'),
											  $form_processor->formValue('shipping_province'),
											  $form_processor->formValue('shipping_country')))
		{
			self::$shipping_address_changed = true;

		}
		
		// Same address box is checked, pass in extra values
		if($form_processor->formValue('same_billing_shipping'))
		{
			$form_processor->addDatabaseValue('billing_street', $form_processor->formValue('shipping_street'));
			$form_processor->addDatabaseValue('billing_street_2', $form_processor->formValue('shipping_street_2'));
			$form_processor->addDatabaseValue('billing_city', $form_processor->formValue('shipping_city'));
			$form_processor->addDatabaseValue('billing_province', $form_processor->formValue('shipping_province'));
			$form_processor->addDatabaseValue('billing_country', $form_processor->formValue('shipping_country'));
			$form_processor->addDatabaseValue('billing_postal', $form_processor->formValue('shipping_postal'));
		}

		
		
		parent::customFormProcessor_performPrimaryDatabaseAction($form_processor);
	}

	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);

		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();
		$shopping_cart->setCheckoutStageToNextAfter('address');
		if(self::$shipping_address_changed )
		{
			$shopping_cart->removeShippingSettings();
			TC_message('Shipping address settings have been changed. Delivery options may have changed as a result.');

		}

	}
	
	
	

}