<?php

/**
 * Class TMv_ShoppingCartBasicAccountForm
 *
 * A form that asks for the most basic account information. This form is only used when there is a cart but the
 * website is not setup to create accounts, so the personal information must be collected each time.
 */
class TMv_ShoppingCartBasicAccountForm extends TCv_FormWithModel
{
	/**
	 * TMv_ShoppingCartBasicAccountForm constructor.
	 * @param string|TMm_ShoppingCart $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		$this->setButtonText('Update Contact Details');
		
	}
	
	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('first_name', 'First Name');
		$field->setIsRequired();
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('last_name', 'Last Name');
		$field->setIsRequired();
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('email', 'Email');
		$field->setIsRequired();
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('phone', 'Phone');
		$field->setIsRequired();
		$this->attachView($field);
	}

	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);

		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();
		$shopping_cart->setCheckoutStageToNextAfter('account');

	}

}