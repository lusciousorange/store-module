<?php

/**
 * Class TMv_ShoppingCartConfirmForm
 *
 * A form that asks for confirmation before placing the order
 */
class TMv_ShoppingCartConfirmForm extends TCv_FormWithModel
{
	/**
	 * TMv_ShoppingCartAddressForm constructor.
	 * @param $model
	 */
	public function __construct($model = false)
	{
		if($model === false)
		{
			$model = TMm_ShoppingCart::init();
		}
		parent::__construct($model);

		$this->setButtonText('Place Order');
		
	}

	/**
	 * @return bool|TMm_ShoppingCart
	 */
	public function model()
	{
		return parent::model();
	}



	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		//$hidden = new TCv_FormItem_Hidden()


	}




	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);

		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();
		$purchase = $shopping_cart->convertToPurchase();

		if($purchase)
		{
			TC_messageConditionalTitle('Order Successfully Placed – Thank You',true);
			$purchase->addEmailsToCron();
		}
		else
		{
			TC_messageConditionalTitle('There was a problem placing your order. Please try again. ', false);

		}


	}

}