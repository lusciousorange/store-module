<?php

/**
 * Class TMv_ShoppingCartDeliveryForm
 *
 * A form that asks for the delivery information for the cart.
 */
class TMv_ShoppingCartDeliveryForm extends TCv_FormWithModel
{
	/**
	 * TMv_ShoppingCartAddressForm constructor.
	 */
	public function __construct($model = false)
	{
		
		if($model === false || !($model instanceof TMm_ShoppingCart))
		{
			
			// Detect an ID being passed via the params
			if(isset($_GET['cart_id']))
			{
				$model = TMm_ShoppingCart::init($_GET['cart_id']);
			}
			
			if(!$model)
			{
				$model =  TMm_ShoppingCart::init();
			}
			
		}
		parent::__construct($model);
		$this->setIDAttribute('shopping_cart_delivery_form');
		$this->setSubmitButtonID('delivery_submit');
		
	}

	/**
	 * @return bool|TMm_ShoppingCart
	 */
	public function model()
	{
		return parent::model();
	}
	
	/**
	 * @return bool|TMm_ShoppingCart
	 */
	public function cart()
	{
		return parent::model();
	}
	
	/**
	 * Creates a view for the delivery option
	 * @param string $content_code
	 * @param string $title
	 * @param string $description
	 * @param float $cost
	 * @return TCv_View
	 */
	public function deliveryOptionView($content_code, $title, $description, $cost)
	{
		$view = new TCv_View('delivery_option_'.$content_code);
		$view->addClass('delivery_option_view');
		$view->addText('<span class="title">'.$title.' $'.$cost.'</span>');
		$view->addText('<p class="description">'.$description.'</p>');
		return $view;
	}

	public function deliveryOptionRadioButtons()
	{
		$radio_buttons = new TCv_FormItem_RadioButtons('delivery_method','Delivery Options');
		$is_first = true;
		foreach($this->model()->deliveryOptionValues() as $content_code => $option_values)
		{
			if($option_values['price'] !== false)
			{
				$view = $this->deliveryOptionView($content_code, $option_values['title'], $option_values['description'],
												  $option_values['price']);
				$radio_buttons->addOption($content_code, $view->html());
			}
			// If the delivery method is blank, we want to reset the saved to the first option
			// Ensures an option is always picked
//			if($is_first && $this->model()->deliveryMethod() == '')
//			{
//				$is_first = false;
//				$radio_buttons->setSavedValue($content_code);
//
//			}
		}

		return $radio_buttons;
	}

	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		if($this->model()->requiresShippingAddress() && $this->model()->hasValidShippingAddress())
		{
			$instructions = new TCv_FormItem_HTML('address','Address');
			$instructions->addText('<p>'.$this->model()->shippingAddressFormatted().'</p>');
			$this->attachView($instructions);
		}
		

		$this->attachView($this->deliveryOptionRadioButtons());
	}



	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		// Find the delivery price

		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();

		$delivery_method_name = $form_processor->formValue('delivery_method');

		if($delivery_method_name == '')
		{
			$form_processor->failFormItemWithID('delivery_method', 'Please select a delivery method');
		}
		else
		{
			if($form_processor->fieldIsSet('delivery_cost')
				&& trim($form_processor->formValue('delivery_cost')) != '')
			{
				$override_cost = $form_processor->formValue('delivery_cost');
				$form_processor->addDatabaseValue('delivery_cost', $override_cost);
				
			}
			else
			{
				$options = $shopping_cart->deliveryOptionValues();
				$cost = $options[$delivery_method_name]['price'];
				$form_processor->addDatabaseValue('delivery_cost', $cost);
				
			}
		}
		// Call the DB Action
		parent::customFormProcessor_performPrimaryDatabaseAction($form_processor);


	}
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_afterUpdateDatabase($form_processor)
	{
		parent::customFormProcessor_afterUpdateDatabase($form_processor);

		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();
		$shopping_cart->setCheckoutStageToNextAfter('delivery');
		
		TC_message('Delivery settings have been updated');

	}

}