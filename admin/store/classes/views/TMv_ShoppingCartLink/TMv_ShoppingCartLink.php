<?php

/**
 * Class TMv_ShoppingCartLink
 *
 * A view that shows the current quantity in the cart. This view is also the target for AJAX updates to the cart.
 *
 *
 */
class TMv_ShoppingCartLink extends TCv_Link
{
	/** @var bool|TMm_ShoppingCart $cart */
	protected $cart = false;

	/**
	 * TMv_ShoppingCartLink constructor.
	 * @param bool $id
	 */
	public function __construct($id = false)
	{
		parent::__construct($id);

		// Instantiate the cart
		$this->cart = TMm_ShoppingCart::init();
		
		$cart_menu_id = TC_getModuleConfig('store','cart_menu_id');
		$menu = TMm_PagesMenuItem::init($cart_menu_id);
		$this->setURL($menu->pathToFolder());
		
		$this->addText('<span class="title">Cart</span>');


		$this->attachView($this->quantityBox());
		$this->setIconClassName('fa-shopping-cart');


	}

	public function quantityBox()
	{
		$box = new TCv_View();
		$box->setTag('span');
		$box->addClass('quantity_box');
		$box->addText($this->cart->numCartItems());
		return $box;
	}


		
}
?>