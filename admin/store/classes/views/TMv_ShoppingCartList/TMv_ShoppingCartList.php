<?php
class TMv_ShoppingCartList extends TCv_SearchableModelList
{
	/**
	 * TMv_ShoppingCartList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_ShoppingCart');
		$this->defineColumns();
		$this->setPagination(50);
	//	$this->addClassCSSFile('TMv_ShoppingCartList');
	}
	
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('cart');
		$column->setTitle('Cart ID');
		$column->setContentUsingListMethod('titleColumn');
		$column->setWidthAsPixels(70);
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('buyer');
		$column->setTitle('Buyer');
		$column->setContentUsingListMethod('buyerColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('date_added');
		$column->setTitle('Date & Time');
		$column->setContentUsingModelMethod('dateAddedFormatted');
		$this->addTCListColumn($column);
		
//		if(TC_getModuleConfig('store','shipping_enabled'))
//		{
//			$column = new TCv_ListColumn('delivery');
//			$column->setTitle('Delivery');
//			$column->setContentUsingListMethod('deliveryColumn');
//			$this->addTCListColumn($column);
//		}
//
//		$column = new TCv_ListColumn('purchase_details');
//		$column->setTitle('Purchase Details');
//		$column->setWidthAsPercentage(20);
//		$column->setContentUsingListMethod('detailsColumn');
//		$this->addTCListColumn($column);
//
		$column = new TCv_ListColumn('subtotal');
		$column->setTitle('Subtotal');
		$column->setContentUsingListMethod('subtotalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(70);
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('taxes');
		$column->setTitle('Taxes');
		$column->setContentUsingListMethod('taxesColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(100);
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('total');
		$column->setTitle('Total');
		$column->setContentUsingListMethod('totalColumn');
		$column->setAlignment('right');
		$column->setWidthAsPixels(80);
		$this->addTCListColumn($column);
		
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
		
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'cart-view');
		$link->addText($model->id());

		
		return $link;
	}
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function buyerColumn($model)
	{
		$string = $model->user()->fullName();
		$string .= '<br />';
		$string .= $model->user()->email();
		return $string;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function subtotalColumn($model)
	{
		return '$'.$model->subtotal();
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function taxesColumn($model)
	{
		$tax_values = array();
		$tax_list = TMm_TaxList::init();
		foreach ($tax_list->taxes() as $tax)
		{
			$tax_value = $model->totalForTax($tax);
			if($tax_value > 0)
			{
				$tax_values[] = $tax->title()." $".$tax_value;
			}
			
		}
		return implode('<br />', $tax_values);
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function totalColumn($model)
	{
		return '$'.$model->total();
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_ShoppingCart $model
	 * @return TCv_View|string
	 */
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'delete-cart', 'fa-trash');
		
	}
	
}