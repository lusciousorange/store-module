<?php

/**
 * Class TMv_ShoppingCartManualPaymentForm
 *
 * A form that allows admins to manually process a shopping cart, marking it as paid an completing the processing
 * related to it.
 */
class TMv_ShoppingCartManualPaymentForm extends TCv_FormWithModel
{
	use TMt_ManualPaymentForm;
	
	/**
	 * TMv_ShoppingCartManualPaymentForm constructor.
	 * @param TMm_ShoppingCart $model
	 */
	public function __construct($model)
	{
		$this->setSuccessURL('/admin/store/do/view/');
		parent::__construct($model);
		
		$this->addClassJQueryFile('TMv_ShoppingCartManualPaymentForm');
		$this->addClassJQueryInit('TMv_ShoppingCartManualPaymentForm');
		$this->addClassCSSFile('TMv_ShoppingCartManualPaymentForm');
		
	}
	
	/**
	 * Returns the model for this form
	 * @return bool|TCm_Model|TMm_ShoppingCart
	 */
	public function model()
	{
		return parent::model();
	}
	
	/**
	 * @param bool $show_form
	 * @return string
	 */
	public function html($show_form = true)
	{
		// Hard check to avoid showing this in scenarios that might require it
		/** @var TMm_User|TMt_Store_User $user */
		$user = TC_currentUser();
		if(!$this->model()->isComplete() && TC_isTungstenView() && $user && $user->userCanEditAllCarts() )
		{
			return parent::html($show_form);
		}
		
		return '';
		
	}
	
	public function configureFormElements()
	{
		$this->setButtonText('Manually Process Cart');
		
		$this->attachPaymentFormFields();
		
		$user_list = TMm_UserList::init();
		$field = new TCv_FormItem_Select('user_id', 'Purchase User');
		$field->useFiltering();
		$field->setIsRequired();
		$field->setHelpText("Select a user that is associated with this purchase");
		$field->addOption('', 'Select a User');
		foreach($user_list->users() as $user)
		{
			$user_title = $user->title() . ' | ' . $user->email();
			$field->addOption($user->id(), $user_title);
		}
		if($user = $this->model()->user())
		{
			$field->setDefaultValue($this->model()->user()->id());
		}
		else // try to find the first item in the cart and see if it has a user
		{
			$user = false;
			foreach($this->model()->cartItems() as $cart_item)
			{
				
				if(method_exists($cart_item->item(), 'user'))
				{
					if($user = $cart_item->item()->user())
					{
						$field->setDefaultValue($user->id());
					}
				}
			}
			
		}
		
		
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('send_email', 'Send Email');
		$field->setSaveToDatabase(false);
		$field->setHelpText("Indicate if the purchase email should be sent");
		$field->addOption('1', 'Yes - Send Purchase Email');
		$field->addOption('0', 'No - Do NOT send Purchase Email');
		$this->attachView($field);
		
	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		parent::customFormProcessor_Validation($form_processor);
		static::validateManualPaymentFormProcessor($form_processor);
	}
	
	
	/**
	 * Performs the primary action associated with processing this form. For this form, there are no database actions
	 * but there are actions related to testing the API and sending a request.
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		/** @var TMm_ShoppingCart $shopping_cart */
		$shopping_cart = $form_processor->model();
		
		/** @var TMm_User|TMt_Store_User $user */
		$user = TMm_User::init($form_processor->formValue('user_id'));
		$override_values = array();
		if($user)
		{
			$override_values = array(
				'type' => $form_processor->formValue('type'),
				'billing_street' => $user->address1(),
				'billing_street_2' => $user->address2(),
				'billing_city' => $user->city(),
				'billing_province' => $user->province(),
				'billing_postal' => $user->postalCode(),
				'billing_country' => $user->country(),
				'user_id' => TC_currentUser()->id(),
				'buyer_id' => $user->id(),
				'first_name' => $user->firstName(),
				'last_name' => $user->lastName(),
				'email' => $user->email(),
			
			
			);
		}
		$purchase = $shopping_cart->convertToPurchase(null, $override_values);
		
		// Process the purchase for payments
		static::processPaymentsForPurchase($purchase, $form_processor);
		
		
		
		if($form_processor->formValue('send_email'))
		{
			$purchase->addEmailsToCron();
		}
		
		$form_processor->addConsoleDebugObject('PURCHASE CREATED', $purchase);
		
		
		$form_processor->setSuccessURL($form_processor->formValue('success_url').$purchase->id());
		
		
		
	}
	
}