(function ($)
{
	// INIT
	$.fn.TMv_ShoppingCartManualPaymentForm = function(options)
	{
		let $form = $('.TMv_ShoppingCartManualPaymentForm');

		var settings = $.extend(
			{
			}, options || {});

		return this.each(function()
		{

			// add listeners
			$form.find('select#type').change(function(e) { typeChanged(); });
			typeChanged();


		});



		function typeChanged()
		{
			let type = $form.find('select#type').val();
			$('.TMv_ShoppingCart').removeClass('complimentary');

			if(type == 'comp')
			{
				$('.TMv_ShoppingCart').addClass('complimentary');
			}

			if(type == 'online')
			{
				$form.find('#card_id_row').slideDown();
				$form.find('#transaction_id_row').slideUp();
			}
			else
			{
				$form.find('#card_id_row').slideUp();
				$form.find('#transaction_id_row').slideDown();
			}

		}



	};


//END THE WRAPPER
})(jQuery);
