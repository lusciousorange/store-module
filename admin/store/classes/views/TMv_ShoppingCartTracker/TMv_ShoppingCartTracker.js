class TMv_ShoppingCartTracker {
	constructor()
	{
		document.querySelectorAll('.TMv_ShoppingCartAddForm.async').forEach(el => {
			el.addEventListener('submit', this.addToCartSubmit.bind(this));
		});
		document.querySelectorAll('a.TMv_ShoppingCartAddButton.async').forEach(el => {
			el.addEventListener('click', this.addToCartButtonClick.bind(this));
		});

		// Buttons that are manually added as quick buttons should have the add_to_cart class added to avoid
		// the double-click issue
		document.querySelectorAll('.TMv_QuickButton.add_to_cart').forEach(el => {
			el.addEventListener('click', () => el.parentElement.classList.add('tungsten_loading'));
		});

		let direct_form = document.querySelector('.TMv_PaypalDirectPaymentForm');
		if(direct_form)
		{
			direct_form.addEventListener('submit', (event) => {enableCartLoadingScreen(event);} )
		}

		configureCarts();
	}

	/**
	 *
	 * @param {Event} event
	 */
	addToCartSubmit(event) {
		event.preventDefault();

		let quantity = 0;
		let form = event.currentTarget;

		form.querySelectorAll('input,select').forEach(field => {
			let value = field.value;
			let id = field.getAttribute('id');

			let tag_name = field.tagName;
			if(tag_name == 'INPUT' && field.getAttribute('type')  == 'radio')
			{
				value = form.querySelector('input[name=' + field.getAttribute('name') + ']:checked').value;
				id = field.getAttribute('name');
			}

			if(id == 'quantity')
			{
				quantity = value;
			}

		});

		let content_code_field = form.querySelector('#content_code');
		if(content_code_field) {

			let content_code = null;

			// RADIO BUTTONS
			if(content_code_field.tagName == 'FIELDSET')
			{
				content_code = content_code_field.querySelector('input[name=content_code]:checked').value;
			}
			else
			{
				content_code = content_code_field.value;
			}

			if(content_code !== null) {

				let url = form.getAttribute('action');
				url += '?quantity=' + encodeURI(quantity);
				url += '&content_code=' + encodeURI(content_code);

				updateCartViewFromURLTarget(url);
			}
		}

	}

	addToCartButtonClick(event) {
		let button = event.currentTarget;
		updateCartViewFromURLTarget(button.getAttribute('href'));
	}




}


//////////////////////////////////////////////////////
//
// EXTERNAL METHODS
//
//////////////////////////////////////////////////////

/**
 * Configures a shopping cart on the page
 */
function configureCarts()
{
	let cart = document.querySelector('.TMv_ShoppingCart');
	if(cart)
	{
		// Add listeners to for cart quantity changes
		cart.querySelectorAll('select#quantity').forEach(el => {
			el.addEventListener('change', cartQuantityChanged);
		});

		cart.querySelectorAll('.remove_link').forEach(el => {
			el.addEventListener('click', cartRemove);
		});
	}

}

/**
 * Called whenever the remove button is clicked in a cart
 * @param {Event} event
 */
function cartRemove(event)
{
	event.preventDefault();
	updateCartViewFromURLTarget(event.currentTarget.getAttribute('href'));

}



function cartQuantityChanged(event)
{
	event.preventDefault();
	let field = event.currentTarget;

	let url = field.form.getAttribute('action') + '?' + 'quantity=' + encodeURI(field.value);
	updateCartViewFromURLTarget(url);



}

// External function

/**
 * Enables the cart loading
 */
function enableCartLoadingScreen() {
	let tracker = document.querySelector('.TMv_ShoppingCartTracker');
	if(tracker)
	{
		tracker.classList.add('loading');
	}

}

function disableCartLoadingScreen()
{
	let tracker = document.querySelector('.TMv_ShoppingCartTracker');
	if(tracker)
	{
		tracker.classList.remove('loading');
	}

}

/**
 * Updates the cart view by using a URL
 * @param {string} url
 */
function updateCartViewFromURLTarget(url)
{
	enableCartLoadingScreen();

	// Find the shopping cart on the page, find the type
	let type = 'cart';
	let next_url_target_name = 'cart-view-json';
	let cart_view = document.querySelector('.TMv_ShoppingCart');
	if(cart_view && cart_view.classList.contains('TMv_PurchaseCart'))
	{
		type = 'purchase';
		next_url_target_name = 'purchase-cart-view-json';
	}

	if(url.indexOf('?') == -1) // not found
	{
		url += '?';
	}
	else
	{
		url += '&';
	}

	url += 'next_url_target_name=' + encodeURIComponent(next_url_target_name)

	//let is_mini = cart_view.classList.contains('mini_cart');

	let classes = '';
	if(cart_view) {
		classes = cart_view.getAttribute('class');
	}

	fetch(url, {
		method : 'GET',
	})	.then(response => response.json())
		.then(response => {

			if (cart_view) {
				cart_view.outerHTML = response.html;

				cart_view = document.querySelector('.TMv_ShoppingCart');
				cart_view.setAttribute('class', classes);
			}

			updateCartQuantityBoxes(response.num_cart_items);


		}).catch((err) => {
			console.log(err)
		}).finally(() => {
			disableCartLoadingScreen();
			configureCarts(); // run again since cart was replaced
	});


}

/**
 * Updates the cart quantity fields that show how many things are in a cart. This value was likely passed back in the JSON
 * response from the server and that's what's being used to update it.
 * @param {Number} num_cart_items
 */
function updateCartQuantityBoxes(num_cart_items)
{
	// Find the cart and how many there are

	let box = document.querySelector('.TMv_ShoppingCartLink .quantity_box');
	if(box)
	{
		box.innerHTML = num_cart_items;
	}



}

