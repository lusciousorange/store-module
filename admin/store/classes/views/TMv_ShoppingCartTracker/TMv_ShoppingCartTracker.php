<?php

/**
 * Class TMv_ShoppingCartTracker
 *
 * This view must be attached to the main theme class for the website. It handles all the instantiation of the carts
 * as well as the javascript required to determine changes to the cart and the updates to the cart values in the system.
 *
 *
 */
class TMv_ShoppingCartTracker extends TCv_View
{
	protected $cart;
	
	/**
	 * TMv_ShoppingCartTracker constructor.
	 */
	public function __construct()
	{
		parent::__construct('cart_tracker');

		$this->addClassCSSFile('TMv_ShoppingCartTracker');
		$this->addClassJSFile('TMv_ShoppingCartTracker');
		$this->addClassJSInit('TMv_ShoppingCartTracker');
		
		// Delete Old Carts
		TMm_ShoppingCart::deleteOldCarts();
		
		// Find an existing loaded cart if it exists
		$possible_cart = TC_activeModelWithClassName('TMm_ShoppingCart');
		
		if($possible_cart instanceof TMm_ShoppingCart && $possible_cart->verifyEditable())
		{
			$this->cart = $possible_cart;
		}
		else
		{
			// Instantiate the cart
			$this->cart = TMm_ShoppingCart::init();
			
			// Clean up the cart
			if($this->cart->verifyEditable())
			{
				$this->cart->validateAndCleanCart();
			}
		}
		
		// save the cart as an active model
		// This allows us to use it in validate in Pages
		TC_saveActiveModel($this->cart);
		
		$cart_loading_screen = new TCv_View('cart_loading_icon');
		$cart_loading_screen->addClass('fa-circle-notch');
		$cart_loading_screen->addClass('fa-spin');
		$this->attachView($cart_loading_screen);

	}

	/**
	 * Return the shopping cart
	 * @return bool|TMm_ShoppingCart
	 */
	public function cart()
	{
		return $this->cart;
	}


		
}
?>