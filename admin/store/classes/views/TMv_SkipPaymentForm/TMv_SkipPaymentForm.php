<?php
class TMv_SkipPaymentForm extends TMv_PaymentForm
{
	use TMt_PagesContentView;

	
	/**
	 * TMv_PaymentForm constructor.
	 * @param bool|TMm_ShoppingCart|TMm_Purchase $cart
	 */
	public function __construct($cart = null)
	{
		parent::__construct($cart);
		
		$this->setUseSavedCards(false);
		
		$this->setPaymentProcessor(TMm_SkipPayment::init());
		
	}
	
	/**
	 * Returns the Form Item group for the card section
	 * @param string|bool $heading The heading for the section
	 * @return TCv_FormItem_Group|bool
	 */
	public function creditCardSection($heading = 'Card Information')
	{
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// FORM PROCESSING
	//
	//////////////////////////////////////////////////////
	
	

	//////////////////////////////////////////////////////
	//
	// TMt_PaymentProcessorForm
	//
	//////////////////////////////////////////////////////


	/**
	 * A hook method where payment fields, specific to the payment processor are set as necessary. These will vary
	 * between each processor, so most of the customization and settings happen here.
	 *
	 * This method sho
	 *
	 * @param TMt_PaymentProcessor|TMm_Stripe $payment_processor
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function hook_configurePayment($payment_processor, $form_processor)
	{

	}


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	//////////////////////////////////////////////////////

	/**
	 * A hook method that can be called to perform changes to the class prior to rendering but after initialization.
	 * This can be used to customize the view based on values provided in the editor.
	 *
	 */
	public function configureForPageView()
	{
	}

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems(): array
	{
		$form_items = array();
		return $form_items;

	}


	public static function pageContent_ViewTitle(): string
	{ return 'Skip Payment Form'; }

	public static function pageContent_ShowPreviewInBuilder(): bool
	{ return false; }
	public static function pageContent_ViewDescription(): string
	{
		return 'The form to process a payment via Stripe.';
	}

}