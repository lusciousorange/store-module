<?php
class TMv_StoreSettingsForm extends TSv_ModuleSettingsForm
{
	/**
	 * TMv_StoreSettingsForm constructor.
	 * @param TSm_Module $module
	 */
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}

	/**
	 * The settings for delivery
	 */
	public function deliverySettings()
	{
		$field = new TCv_FormItem_Heading('delivery_settings', 'Delivery Settings');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('shipping_enabled', 'Shipping Enabled');
		$field->setHelpText('Indicate if shipping is enabled on the website');
		$field->addOption('1', 'Yes');
		$field->addOption('0', 'No');
		$field->setDefaultValue('0');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('pickup_permitted', 'Pickup Permitted');
		$field->setHelpText('Indicate if pickup is permitted');
		$field->addOption('0', 'No');
		$field->addOption('1', 'Yes');
		$field->setDefaultValue('0');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('pickup_description', 'Pickup Description');
		$field->setDefaultValue('Pickup available in-store');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('delivery_provider_classes', 'Delivery Provider Classes');
		$field->setHelpText('The list of developer-defined delivery providers');
		$this->attachView($field);

	}

	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('transactions_disabled', 'Transaction Status');
		$field->setHelpText('This allows you to disable checkout on the website in case of updates or temporary downtime. ');
		$field->addOption('0', 'Enabled');
		$field->addOption('1', 'DISABLED - No transactions can occur, even in the admin');
		$this->attachView($field);
		
		$this->deliverySettings();


		$field = new TCv_FormItem_Heading('checkout_settings', 'Checkout Defaults');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('cart_menu_id', 'Main Cart Page');
		$field->setHelpText('Indicate the main page that contains the shopping cart');
		$field->addOption('', 'Not Set');
		$field->useFiltering();
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('success_menu_id', 'Success Page');
		$field->setHelpText('Indicate the page for when the transaction is a success');
		$field->addOption('', 'Not Set');
		$field->useFiltering();
		$menu_0 = TMm_PagesMenuItem::init(0);
		foreach($menu_0->descendants() as $menu_item)
		{
			$titles = array();
			foreach($menu_item->ancestors() as $ancestor)
			{
				$titles[] = $ancestor->title();
			}
			
			$field->addOption($menu_item->id(), implode(' – ', $titles));
		}
		$this->attachView($field);



		$field = new TCv_FormItem_TextField('default_city', 'Default City');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('default_province', 'Default Province');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('default_country', 'Default Country');
		$this->attachView($field);

		$field = new TCv_FormItem_Heading('payment_heading', 'Payment Methods');
		$field->setHelpText("These are options related to what kinds of payments are taken on the site. ");
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('new_card_title', 'New Card Title');
		$field->setHelpText('The title on the button for adding a new card');
		$field->setDefaultValue('New Card');
		$this->attachView($field);
		
		
		$field = new TCv_FormItem_Select('payment_processor', "Payment Processor");
		$field->setHelpText("Indicate how method you'll use for processing payments on the site");
		//$field->addOption('','Select a Payment Processor');
		$field->setDefaultValue('TMm_SkipPayment'); // default to the skip one, so there's always on in place
		foreach(TC_website()->classesWithTrait('TMt_PaymentProcessor') as $class_name)
		{
			$field->addOption($class_name, $class_name::processorName());
		}
		
		$this->attachView($field);
		
		foreach(TC_website()->classesWithTrait('TMt_AlternatePaymentOption') as $class_name)
		{
			/** @var string|TMt_AlternatePaymentOption $class_name */
		
			$field = new TCv_FormItem_Select('use_'.$class_name, $class_name::paymentOptionTitle());
			$field->addOption('0', 'NO – Do not use '.$class_name::paymentOptionTitle());
			$field->addOption('1', 'YES - Use '.$class_name::paymentOptionTitle());
			$this->attachView($field);
			
			// Special Case for Offline which exists within Store Module
			// All the other ones have their own module
			if($class_name == 'TMm_OfflinePaymentOption')
			{
				$field = new TCv_FormItem_TextField('payment_option_explanation', 'Offline Payment Explanation');
				$field->setDefaultValue('Cheque or In-Person Payment');
				$field->setAsAllowsHTML();
				$this->attachView($field);
				
			}
			
		}

		$field = new TCv_FormItem_Heading('misc_settings', 'Miscellaneous');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('time_format', 'Time Format');
		$field->setHelpText('Sets the time format code for all times related to the store');
		$this->attachView($field);

		$field = new TCv_FormItem_Heading('purchase_settings', 'Purchase Setting');
		$this->attachView($field);
		
		// Emails sent to staff,
		/* @see TMv_PurchaseEmailStaff */
		$field = new TCv_FormItem_TextField('bcc_purchase_emails', 'Staff Purchase Emails');
		$field->setHelpText('Comma-separate list of emails that receive updates when a purchase is made.');
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('email_intro_text', 'Purchase Email Introduction');
		$field->setHelpText('Custom text that appears at the start of each purchase email');
		$this->attachView($field);
		
		
		
		// DEPRECATED. Uses the main email to ensure consistent mailing process
//		$field = new TCv_FormItem_TextField('from_purchase_email', 'FROM Purchase Email');
//		$field->setHelpText('A single email address that is shown as the "from" when a purchase is made.');
//		$this->attachView($field);
//
		$field = new TCv_FormItem_Select('use_accounting_codes', 'Use Accounting Codes');
		$field->setHelpText('Indicate if this website uses accounting codes');
		$field->addOption('0', 'No');
		$field->addOption('1', 'Yes');
		$this->attachView($field);
		
		$field = new TCv_FormItem_Select('show_admin_cart', 'Admin Cart');
		$field->setHelpText('Indicate if an admin cart is shown');
		$field->addOption('1', 'Yes');
		$field->addOption('0', 'No');
		$this->attachView($field);
		
	}
	
	/**
	 * Returns a form item group view for a class name that shows the API fields for that item
	 * @param TMt_AlternatePaymentOption|TMt_PaymentProcessor|string $class_name
	 * @param string $subclass
	 * @return TCv_FormItem_Group
	 */
	protected function apiFieldGroupForClassName($class_name, $subclass)
	{
		$group = new TCv_FormItem_Group($class_name.'_fields');
		$group->addClass('api_field_group');
		$group->addClass('group_'.$class_name);
		$group->addClass('api_'.$subclass);
		
		
		foreach($class_name::apiFields() as $field_id => $field_values)
		{
			$field = new TMv_FormItem_TextFieldForAPIField($class_name . '_' . $field_id, $field_values);
			$group->attachView($field);
			
		}
		
		return $group;
	}
	
	
	
}
?>