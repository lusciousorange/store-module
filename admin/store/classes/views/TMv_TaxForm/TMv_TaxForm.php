<?php

/**
 * Class TMv_TaxForm
 */
class TMv_TaxForm extends TCv_FormWithModel
{
	/**
	 * TMv_CategoryForm constructor.
	 * @param string|TMm_Category $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
	}
	
	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('percent', 'Percent');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('number', 'Number');
		$field->setHelpText('An optional "number/code" which is associated with this tax');
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('country', 'Country');
		$field->setHelpText('The two-digit code for the country in which this tax applies');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('province', 'Province');
		$field->setHelpText('The two-digit code for the province in which this tax applies');
		$this->attachView($field);


		$field = new TCv_FormItem_Select('applies_to_shipping', 'Applies to Shipping');
		$field->addOption('1', 'YES - Applies to any shipping cost');
		$field->addOption('0', 'NO - Does NOT Applies to any shipping cost');

		$this->attachView($field);

	}



}