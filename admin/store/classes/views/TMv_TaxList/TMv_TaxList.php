<?php
class TMv_TaxList extends TCv_ModelList
{

	/**
	 * TMv_TaxList constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_Tax');
		$this->defineColumns();
		$this->addClassCSSFile();
	}
	
	/**
	 * Define columns
	 */
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('percent');
		$column->setTitle('%');
		$column->setContentUsingModelMethod('percent');
		$column->setAsVisibleInCondensedView();
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('country');
		$column->setTitle('Country');
		$column->setContentUsingModelMethod('country');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('province');
		$column->setTitle('Province');
		$column->setContentUsingModelMethod('province');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('shipping');
		$column->setTitle('Shipping');
		$column->setContentUsingModelMethod('appliesToShipping');
		$this->addTCListColumn($column);

		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
				
	}


	/**
	 * Returns the column value for the provided model
	 * @param TMm_Tax $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$model->validateTaxTableColumns();

		if($model->userCanEdit())
		{
			$link = $this->linkForModuleURLTargetName($model, 'tax-edit');
			$link->addText($model->titleWithNumber());
			return $link;
		}


		return $model->title();
	}




	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'tax-edit', 'fa-pencil');
		
	}
	
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'tax-delete', 'fa-trash');
	
	}
	

		
}
?>