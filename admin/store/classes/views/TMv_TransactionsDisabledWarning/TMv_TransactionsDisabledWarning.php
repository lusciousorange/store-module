<?php
class TMv_TransactionsDisabledWarning extends TCv_View
{
	protected $checkout_disabled_explanation = "Transactions are temporarily disabled. We apologize for the inconvenience.";
	
	public function __construct($id = false)
	{
		parent::__construct($id);
		
		$this->addClassCSSFile('TMv_TransactionsDisabledWarning');
	}
	
	public function render()
	{
		
		$description = new TCv_View();
		$description->setTag('p');
		$description->addText(TC_localize('checkout_disabled_explanation',$this->checkout_disabled_explanation));
		
		$this->attachView($description);
		
	}
}