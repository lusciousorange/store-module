<?php


// INSTALL DB FOR OUR TEST PRODUCTS
$db = TCm_Database::connection();
$query = "CREATE TABLE `unit_test_products` (
  `test_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `price` float(10,2) unsigned NOT NULL,
  `discount` float(10,3) unsigned NOT NULL,
  `in_stock` int NOT NULL,
  `uses_tax_1` tinyint(1) NOT NULL,
  `uses_tax_2` tinyint(1) NOT NULL,
  `barcode` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`test_product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
  ";

$statement = $db->prepare($query);
$statement->execute();


/**
 * A simple class used to test cart functionality that implements TMi_ShoppingCartable. This is different for every
 * website, so this provides a clean class to test with. It's loaded here, so that the rest of Tungsten won't find it
 * when searching for cartable items.
 */
class TMm_StoreUnitTestProduct extends TCm_Model implements TMi_ShoppingCartable
{
	protected $price, $title,  $in_stock = 0, $uses_tax_1 = false, $uses_tax_2 = false, $barcode, $discount;
	
	public static $table_id_column = 'test_product_id';
	public static $table_name = 'unit_test_products';
	public static $model_title = 'Unit Test Product';
	public static $primary_table_sort = 'date_added DESC';
	
	
	//////////////////////////////////////////////////////
	//
	// TMi_ShoppingCartable
	//
	// These are the methods for this Cartable item that needs
	// to be implemented.
	//
	//////////////////////////////////////////////////////
	
	public function viewURL()
	{
		return false;
	}
	
	public function discountPerItem()
	{
		return $this->discount;
	}
	
	public function price()
	{
		return $this->price;
	}
	
	
	
	public function numInStock()
	{
		return $this->in_stock;
	}
	
	/**
	 * Returns the number of items in stock for this item. A value of -1 indicates that stock is not tracked
	 * @return int
	 */
	public function inStock()
	{
		return false;
	}
	
	/**
	 * Returns the weight of one of these items, in KG
	 * @return mixed
	 */
	public function weight()
	{
		return 0;
	}
	
	public function usesTax($tax)
	{
		$varname = 'uses_tax_'.$tax->id();
		return $this->$varname == 1;
	}
	
	public function taxableAmountForTax($tax, $discount = 0)
	{
		return $this->price() - $discount;
	}
	
	
	public function cartDescription($cart_item)
	{
		return 'Cart Description';
	}
	
	public function cartTitle($cart_item)
	{
		return $this->title;
	}
	
	public function cartPhotoFile()
	{
		
		return false;
	}
	
	
	public function barcode()
	{
		
		return $this->barcode;
	}
	
	public function showStockAdjustmentWarnings()
	{
		return true;
	}
	
	public function outOfStockMessage()
	{
		return 'This is currently out of stock';
	}
	
	public function maxAddable()
	{
		return $this->inStock();
	}
	
	public function validateCartItem($cart_item)
	{
		$errors = array();
		return $errors;
	}
	
	public function addToCartVariableValues()
	{
		$values = array();
		return $values;
	}
	
	public function additionalCartActions($cart)
	{
		// NO Action
	}
	
	public function processAfterPurchase($purchase_item)
	{
		// NO Action
	}
	
	/**
	 * @return bool
	 */
	public function allowsUnpaidPurchasePriceUpdates()
	{
		return true;
	}
	
	public static function purchaseCreatedModelClassName()
	{
		return false;
	}
	
	public function accountingCode()
	{
		return false;
	}
}


/**
 * A simple class used to test delivery options
 */
class TMm_StoreUnitTestDeliveryOption extends TCm_Model implements TMi_DeliveryOption
{
	public static $table_id_column = 'delivery_option_id';
	
	protected $delivery_option_id, $price, $title, $description;
	
	public function __construct($id, $load_from_table = true)
	{
		if(is_array($id))
		{
			foreach($id as $index => $value)
			{
				$this->$index = $value;
			}
		}
		parent::__construct($id,false);
	}
	
	public function price()
	{
		return $this->price;
	}
	
	public function title()
	{
		return $this->title;
	}
	
	public function description()
	{
		return $this->description;
	}
	
	
	
	
}