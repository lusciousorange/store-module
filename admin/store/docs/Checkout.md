
# Checkout
The checkout process has a basic step-by-step functionality which works in a basic scenario.For every project, there are too many 
variables to define and so the system provides a series of default classes which the
developer is responsible for extending as necessary. 

## Normal Workflow
In a traditional online store, the workflow for checkout is as follows:
1. Account (`TMv_CheckoutAccount`): Login or create an account
1. Address (`TMv_CheckoutAddress`): Enter your billing/shipping address
1. Delivery (`TMv_CheckoutDelivery`): Select your delivery option
1. Payment (`TMv_CheckoutPayment`): Payment through a payment processor
1. Confirm (`TMv_CheckoutConfirm`): Review the order and confirm the payment
1. Payment and Processing

For a given website, it may not require accounts, addresses, delivery, or even payment, depending on how it operates. This 
can be adjusted by extending the `TMm_ShoppingCart` class and redefining the the `$checkout_stages` variable.

    class TMm_My_ShoppingCart extends TMm_ShoppingCart
    {
	    protected $checkout_stages = array(
            'account' => array('title' => 'Contact', 'view_url' => '/checkout/account/'),
            'payment' => array('title' => 'Payment', 'view_url' => '/checkout/payment/')
        );
    }
    
Additionally, if your particular checkout process requires a different view, you can create your own and add it to the workflow.    
    
## Checkout Status Bar
You can show the progress of your checkout process by including the `TMv_CheckoutStatusBar` which will grab those checkout
stages and then present a series of links that correspond to the proper stage if applicable. As will all views, it can be
extended and customized if necessary.

## Checkout Views
Each of the checkout views listed above, has default functionality. They are briefly explained here, but refer to the 
documentation in the classes for more details. 

### `TMv_CheckoutAccount`
This view will check to see if someone is logged into the system. This integrates with the `Users` module and the website
authentication built into Tungsten. 

If someone is logged in, they will see a summary of their basic information to verify. Otherwise they are presented with
a login or sign up option.

By default, this view does not handle "Guest" purchases.

**Note:** Using accounts, requires that the `Pages` setting for `Website Uses Authentication` must be enabled. This is how
the website knows that users can login, which is necessary for integration with Checkout Accounts.  

### `TMv_CheckoutAddress`
This view is mainly a wrapper for `TMv_ShoppingCartAddressForm` which handles the display of the shipping/billing address
fields as well as the JQuery to deal with allowing them to select the same for both.

By default, the system does not save previous addresses, so that would need to be added in the extended class if necessary. 

### `TMv_CheckoutDelivery`
This view is mainly a wrapper for `TMv_ShoppingCartDeliveryForm` which handles the integration with the delivery options
in the module and presents them as radio buttons where the user must select one. 

### `TMv_CheckoutPayment`
This view usually requires extending since there are no default payment processors. In most cases, a developer will install
a separate module for the relevant payment processor (PayPal, Square, etc) then extend this payment view and add the payment
form into the extended view. 

This approach requires more effort on behalf of the developer, but also provides the freedom to use any payment processor
without the need to undo existing work or make compromises on the processing and integration.

### `TMv_CheckoutConfirm`
The confirmation page will provide a summary of the cart and then present a `TMv_ShoppingCartConfirmForm`. That form is 
what processes the cart and converts it into a `TMm_Purchase` when clicked. It calls `TMm_ShoppingCart::convertToPurchase()` 
which handles that transition.

Extending these views is common for adding in payment processing. Some workflows may not use the confirmation at all.

    class TMv_My_ShoppingCartConfirmForm extends TMv_ShoppingCartConfirmForm
    {
        public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
        {
            // Handle transaction and payment processing
        }
    }
    
## Processing Payments
Payment processing can often vary depending on the provider and the checkout workflow. One approach is to ask for Credit Card 
information, store it in a session variable and process it in the `TMv_ShoppingCartConfirmForm` as seen above. 

A second approach is to not use the confirmation stage, and make use a payment processor form may be the final stage in 
which case the confirmation isn't used. In those
scenarios, the form processor must get the cart and call `convertToPurchase()` manually.

**IMPORTANT SECURITY NOTE:** Never store credit card information in the database, even temporarily. 
