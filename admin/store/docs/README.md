# Store/Purchases Module

The store module is used to add a lot of the basic functionality related to online stores to a website. Due to the 
customizable nature of Tungsten, this module does require additional programming and oftentimes additional modules for 3rd-party
system such as payment processors. 


## Installation
The module can be installed in the normal way that other Tungsten modules are installed. It will create the following 
MySQL database tables `shopping_carts`, `shopping_cart_items`, `purchases`, `purchase_items`, and `taxes`. 

**Note** that the module installs with the title `Purchases`. This is because the main item that is displayed in this module
are purchases. The use of the title `Store` would be confusing due to other possible modules which also interact with the store. 

## Module Settings
The module has various settings, which help configure delivery, checkout and purchases. It is highly recommended to set
these up when the values are known.

## Additional Topics
* [Shopping Carts](Shopping%20Carts.md) 
* [Taxes](Taxes.md)
* [Checkout](Checkout.md) 
