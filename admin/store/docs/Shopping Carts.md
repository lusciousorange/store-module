# Part 1 : Understanding Shopping Carts

Shopping carts are at the core of how visitors make purchases on the site. At it's most basic level, an item can be added to a cart
and that cart can be paid for and converted into a Purchase. Much of the complexity of shopping carts is handled automatically.

The `TMm_ShoppingCart` stores the various properties of a cart. These properties can divided into a few sections.

## TMi_ShoppingCartable Items
There is no default shopping cart item. These are developer defined for each website by using the `TMi_ShoppingCartable` 
interface. In order for something to be added to a cart, it must be a `TCm_Model` that implements the interface `TMi_ShoppingCartable`. This 
defines how the system interacts with purchasable items. The interface includes a range of functions that dictate how that model
is added to the cart including stock limitations, validation, as well as display properties. 

Oftentimes, this means that a separate
module is created to manage those items (eg: Products, Tickets, etc ) and then the primary model in that module implements
the `TMi_ShoppingCartable` interface.

The system will automatically look for that interface in the installed modules. The developer then uses the interface
methods to define how that model interacts with the store. There are methods for `inStock()`, `price()`, `title()` etc. 

##Shopping Cart Items
Every `TMm_ShoppingCart` instance represents a single cart in the system. The items in that cart are represented by
`TMm_ShoppingCartItem` objects. So one cart can have many items, as you'd expect. 

The `TMm_ShoppingCartItem` tracks the particular `TMi_ShoppingCartable` object in the cart by saving the PHP class name
in `item_class` as well as the ID (if applicable) in `item_id`. The cart item also tracks other properties such as the quantity, price, etc.
 
### Cart Item Variables 
A `TMm_ShoppingCartItem` can store up to 26 variables associated with that item. These are entirely developer-defined and used
to track information that might not be directly associated with the cart. 

**Example:** When selling memberships, you may want to track the `user_id` of the person that the membership is for and 
perhaps the `year` for that membership. The person may be buying the membership on behalf of someone else and it could also
be that the people purchase them every year. The cart item variables are where you store that information to be processed later. 
 
 # Completing Purchases
When a `TMm_ShoppingCart` is purchased (using whatever developer-defined payment process), that cart is converting into a 
`TMm_Purchase`. It has very similar structure to a cart and is designed to entirely mimic the structure. Each `TMm_ShoppingCartItem`
is used to create `TMm_PurchaseItem`.  

1. Payment is completed (developer-defined)
1. The `TMm_ShoppingCart::convertToPurchase()` method is called which creates a `TMm_Purchase` based on the cart. 
1. The `TMm_ShoppingCart` is marked as `is_complete` and tracks the `purchase_id`, which stops it from ever being erased from the system for historical completeness.
1. Each `TMi_ShoppingCartable` item in that purchase has a method called `processAfterPurchase($purchase_item)`. This is 
where developers can customize the actions that happen for each cart item when it's purchased. 



# Part 2 : Using Shopping Carts on a Website
In order for a website to use shopping carts it needs a few items in place:

1. The installed `store` module which provides the core functionality for carts and purchases
1. Some sort of payment processing setup, usually implemented in a payment form that can access the shopping cart
1. Some `TCm_Models` that implement the `TMi_ShoppingCartable` interface
1. Addition of some basic views into the site to provide access to the cart functionality.

This section focuses on the last item on that list and will provide options for showing the cart on the website so that 
users can interact with it effectively.

## Theme and Cart Tracker
The Theme for the website must install a shopping cart tracker, which is a placeholder for other cart related function as 
well as instantiates a cart for any page of the website. This ensures site-wide access to the site. We recommend installing 
this view in the constructor to ensure there are no scenarios that generate additional carts.

    class TMv_MyTheme extends TMv_PagesTheme
    {
        protected $cart = false;
        
        public function __construct()
        {
            // Do Constructor things
            
            $cart_tracker = new TMv_ShoppingCartTracker(); // Create Tracker
            $this->cart = $cart_tracker->cart(); // Save the cart for convenience
            $this->attachView($cart_tracker); // Attach the tracker

        }
    }  
    
## Cart Button
Most websites want to provide a mechanism to view a cart which often has the icon and the number of items. The `TMv_ShoppingCartLink`
class is a shortcut view that creates the link, shows the icon and quantity and points to `/cart/` by default (override with `setURL()` 
if necessary). This button can be added to any relevant spot in the template at the developer's discretion. 
   
## "Add to Cart" Options
There are different methods for allowing someone to add an item to their cart. All these methods require the same information:
* The item being added 
* The quantity being added

### TMv_ShoppingCartAddForm
The most robust option is a form that lets the visitor pick a quantity for the item. This view is often extended for
more complicated items as necessary. 

### URL Target
There is a publicly available URL Target which allows items to be added to the cart by redirecting the user. If this url is 
added to a button, it allows for immediately adding an item to a cart, which is particularly useful is the quantity is often 1. 

These buttons must point to `/admin/store/do/add-to-cart/<content-code>/<quantity>/`. The content code can be found by 
calling `contentCode()` on any model that implements TMi_ShoppingCartable. 

### Asynchronous Updates
On some websites, you may want the "cart icon" to immediately update asyncronously when added. This functionality is facilitated
by the `TMv_ShoppingCartTracker` added earlier. We can apply the CSS class
`shopping_cart_quantity_target` to the `TMv_ShoppingCartLink` or any other target that should receive the update. The change will 
be automatically detected as the asynchronous calls are made.

## Showing the Cart
In most cases, the page `/cart/` will add the `TMv_ShoppingCart` view to that page, which will show the cart with options 
to update the quantities or delete entirely. Depending on the configuration, it may also show a subtotal. 



#Part 3 : Advanced Settings

## Cart Page Module Setting
Part of customizing the cart involves the system knowing where the main "cart page" is on the website. This is configured
in the module settings. Once the cart is on a page, you can indicate the page and certain functionality works much smoother
including the `add-to-cart` URL Target which will redirect to that page.

## Users and Distinguishing Carts
Every `TMm_ShoppingCart` has a unique `$cart_id` but it also has a `$session_id` as well as a `$user_id`. The session is the 
same as the one for the browser and the `$user_id` connects the cart to a user. Due to the nature of how people shop online, 
the system creates a cart for every session, even if we don't have a logged in user. If a user exists, it will always use their
cart and ignore the `$session_id`. 

If a user logs into an account with an existing cart tied to their session, the system will merge the two into a single cart
to ensure they don't lose their previous cart items.

As well, since it's possible for a cart to not be tied to a user, it can store all the relevant information about the buyer
including name, email and phone number. These values are ignored for carts connected to a user.
## Addresses
Not every store will require shipping, however the system can handle different configurations. It's possible to have a 
unique shipping and billing address for each cart. 

## Delivery
The system has only one default delivery option which is "Pick Up". Other delivery options must be manually configured with
the provider or via additional modules. Websites that only sell digital products don't require any delivery integration.

### Delivery Providers
In order to define a delivery provider, you must create a model with the interface `TMi_DeliveryProvider`. that returns the list of delivery options using the method 
`deliveryOptionsForCart($cart)`. You can have multiple delivery providers and they can each return an associative array of values that 
correspond to the options for delivery, based on whatever properties of the cart it cares about. That associative array
should be indexed with unique IDs for option and the value should be an array with three values:

* title : The name of the delivery option
* description: A brief explanation of the option
* price : A float that is the price for that delivery option

You can configure the system to recognize your delivery option model by adding the class name to the `delivery_provider_classes` 
in the module's settings.  

If the "Pick-Up" option is enabled for the site, then that will be added as well. 

