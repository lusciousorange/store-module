
# Taxes
The module has a section to create and manage the taxes shown in the system. 

## Properties 
Each tax `TMm_Tax` has a percentage value and can be defined to apply to only a certain country or state/province. 
Additionally, you can configure a tax to be applicable to shipping. 

For the most part, a developer needs to only use the existing interfaces to manage the taxes.

The models and views in the system often require a tax `TMm_Tax` to determine how they interact with the system.

## Tax Columns
Taxes are tracked as individual columns on anything which involves currency. Most notably, there are columns called 
`tax_X_percent`, `tax_X_paid` and `tax_X_paid_per_item` where `X` is the ID of the tax. These columns are added to the 
`purchase_items` table but they can also be added to other tables where tax values are tracked. 

The default installation can be seen in the `validateTaxTableColumns()` method which is automatically called from the 
`TMv_TaxList` whenever it is loaded. This method adds columns for each tax in the system to the relevant tables.  

If there is a need to track tax values in additional tables, they should use this same method and avoid manually adding
the columns to tables since that doesn't handle potential new taxes in the future. We recommend extending the `TMm_Tax`
class. 

    class TMm_My_Tax extends TMm_Tax
    {
        public function validateTaxTableColumns()
        {
            parent::validateTaxTableColumns();
    
            $installer = new TSu_InstallProcessor('installer');
            $installer->addTableRows(
                array(
                    'tax_'.$this->id().'_percent float(10,2) unsigned NOT NULL',
                    'tax_'.$this->id().'_paid float(10,2) unsigned NOT NULL'
                    ), 
                'table_name'
            );
        }
    }
    
## `usesTaxes()` and `taxableAmountForTax()` methods
The `TMi_Cartable` interface has specific methods called `usersTax($tax)` and `taxableAmountForTax($tax)`. This method is implemented for each cartable
item and accepts a `TMm_Tax` as a parameter. In both cases, they should use the provided `$tax` and perform any calculations
for that item based on the provided tax. In many cases, they can return a straightforward value.

    public function usesTax($tax)
	{
	    return true;
	}

	public function taxableAmountForTax($tax)
	{
	    return $this->price();
	}
